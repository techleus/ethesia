<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<style type="text/css">
		.field-preview {
			width: 30%;
			float: left;
			display: inline-block;
			padding: 8px;
		}
		.key {
			font-weight: bold;
		}
	</style>
	<div style="width: 1000px">
		<div class="col-12">
			<div class="row">

				<div class="col-8" style="width: 600px;float: left;">
					<div class="col-12" style="width: 600px">
						<h5>JOB ID : {{$job->id}}</h5>
					</div>

					<div class="col-12" style="margin-left: 460px;top: 0px;width: 240px">
						<img class="pointer" style="width:100%"  src="{{$job->image_path}}">
						<br>
					</div>

					<div class="col-12" style="margin-left: 500px;margin-top: 20px">
						<label>{{$job->city}},  {{$job->state}}</label><br>
					</div>

					<div class="col-12" style="width: 400px;margin-top: -60px;">
						<h2>{{$job->jobTitle}}</h2>
					</div>

					


					<div class="col-12" style="width: 600px;">
				  		<div class="row">
							<div class="col-4" style="width: 150px; display: inline-block;float: left;">
								<label>{{$job->ei_facilityCity}} , {{$job->ei_facilityState}}</label>
							</div>
							<div class="col-4" style="width: 150px; display: inline-block;float: left;">
								<label>{{$job->ei_durPosition}}</label>
							</div>
							<div class="col-4" style="width: 150px; display: inline-block;float: left;">
								<label>{{$job->ei_durMin}} - {{$job->ei_durMax}}</label>
							</div>
						</div>

					</div>
					<br>
					<div class="col-12" style="width: 600px;margin-top: 20px;display: block;">
				  		<div class="row">
							<div class="col-4" style="width: 100px; display: inline-block;float: left;">
								<label>Post Date: {{$job->posted_date}}</label>
							</div>
							<div class="col-4" style="width: 100px; display: inline-block;float: left;">
								<label>Updated Date: {{$job->posted_date_updated}}</label>
							</div>
						</div>
					</div>
					<br>
					<div class="col-12" style="width: 600px;margin-top: 20px;display: block;">
						<div class="row">
							<div class="col-12" style="width: 600px; display: inline-block;float: left;">
								<h3 >Job Description:</h3>
							</div>
						</div>
					</div>
					<br>
					<div class="col-12" style="width: 600px">
						<div class="row">
							<div class="col-12" style="text-indent: 50px;">
								<label>
									{{$job->ei_jobDescription}}
								</label>
							</div>
						</div>
					</div>

					<div class="col-12" style="width: 600px">
						<div class="row">
							<h3>Job Details</h3>
						</div>
					</div>

					<div class="col-12" style="width: 1000px">
						<div class="row">
							<div class="col-12">
								<div class="row job-details">

	                                  <div class="col-12">

	                       				@if($job->facilityName != "" )
		                                    <div  class="col-4 float-left field-preview">
		                                        <div class="key">JOB START DATE</div>

		                                        <div class="value">Specified start date</div>
		                                    </div>    
	                                  	@endif


	                                  	@if($job->ei_durMin != '' )
		                                    <div ng-hide="$job->ei_durMin == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Salary Range</div>

		                                        <div class="value">{{$job->ei_durMin}} - {{$job->ei_durMax}}</div>                
		                                    </div>
		                                @endif    

		                                @if($job->ei_facilityCity != '' )
		                                    <div ng-hide="$job->ei_facilityCity == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Facility Location</div>

		                                        <div class="value">{{$job->ei_facilityCity}}, {{$job->ei_facilityState}}</div>    
		                                    </div>                
		                                @endif    
	                                    
	                                    @if($job->ei_ansFirstCall != '' )
		                                    <div ng-hide="$job->ei_ansFirstCall == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Crna on First Call?</div>

		                                        <div class="value">{{$job->ei_ansFirstCall}}</div>
		                                    </div>   
		                                @endif 

		                                @if($job->left != '' )
		                                    <div ng-hide="$job->left == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Contact Website
		                                        </div>

		                                        <div class="value">{{$job->ag_webSite}}</div>
		                                    </div>
	                         			@endif

	                         			@if($job->facilityName != '' )
		                                    <div ng-hide="$job->facilityName == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Facility Address</div>
		                                        <div class="value">Street address, zip code</div>
		                                    </div>               
		                                @endif
    
	                                   	@if($job->ei_empStatus != '' ) 	
		                                    <div ng-hide="$job->ei_empStatus == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Employment Type</div>
		                                        <div class="value">{{$job->ei_empStatus}}</div>
		                                    </div>
		                                @endif   
	                      	
	                      				@if($job->ci_email != '' )
		                                    <div ng-hide="$job->ci_email == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Contact Email</div>
		                                        <div class="value">{{$job->ci_email}}</div>
		                                    </div>
		                                @endif       

		                                @if($job->ei_ansMedDirecting != '' )
		                                    <div ng-hide="$job->ei_ansMedDirecting == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Medically Directing</div>
		                                        <div class="value">{{$job->ei_ansMedDirecting}}</div>
		                                    </div>
		                                @endif     

		                                @if($job->jr_fellowshipReq != '' )
		                                    <div ng-hide="$job->jr_fellowshipReq == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Subspecialty Fellowship</div>
		                                        <div class="value">{{$job->jr_fellowshipReq}}</div>
		                                    </div>
		                                @endif      


		                                @if($job->jr_stateLicense != '' )
		                                    <div ng-hide="$job->jr_stateLicense == ''" class="col-4 float-left field-preview">
		                                        <div class="key">State License</div>
		                                        <div class="value">{{$job->jr_stateLicense}}</div>
		                                    </div>
	                                    @endif 

	                                    @if($job->jr_stateLicense != '' )
		                                    <div ng-hide="$job->jr_abaCertStatus == ''" class="col-4 float-left field-preview">
		                                        <div class="key">ABA Certification Status</div>
		                                        <div class="value">{{$job->jr_abaCertStatus}}</div>
		                                    </div>
	                                    @endif 


	                                    @if($job->jr_stateLicense != '' )
		                                    <div ng-hide="$job->jr_abaCertStatus == ''" class="col-4 float-left field-preview">
		                                        <div class="key">ABA Certification Status</div>
		                                        <div class="value">{{$job->jr_abaCertStatus}}</div>
		                                    </div>
		                                @endif     


		                                @if($job->jr_abaCertStatus != '' )
		                                    <div ng-hide="$job->jr_abaCertStatus == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Required for this job</div>
		                                        <div class="value">
		                                            {{$job->current_acls == 'true' ? 'Current ACLS card' : ''}}
		                                            <br>
		                                            {{$job->current_pals == 'true' ? 'Current PALS card' : ''}}
		                                            <br>
		                                            {{$job->aba_pain_management == 'true' ? 'ABA Certification in Pain Management' : ''}}
		                                            <br>
		                                            {{$job->aba_critical_care == 'true' ? 'ABA Certification in Critical Care Management' : ''}}
		                                        </div>
		                                    </div>
		                                @endif  
		                                

		                                @if($job->fi_nameAns != '' )
		                                    <div ng-hide="$job->fi_nameAns == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Name of Anesthesiologist who is Group President and years with group</div>
		                                        <div class="value">{{$job->fi_nameAns}}</div>
		                                    </div>
		                                @endif  


		                                @if($job->fi_isPracLimited != '' )
		                                    <div ng-hide="$job->fi_isPracLimited == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Is practice limited to Hospital, Surgery Center and Office Based Anesthesia?</div>
		                                        <div class="value">{{$job->fi_isPracLimited}}</div>
		                                    </div>
	                                    @endif  


	                                    @if($job->jr_newGraduates != '' )
		                                    <div ng-hide="$job->jr_newGraduates == ''" class="col-4 float-left field-preview">
		                                        <div class="key">New Graduates Acceptable?</div>
		                                        <div class="value">{{$job->jr_newGraduates}}</div>
		                                    </div>
		                                @endif     

	                                    
		                                @if($job->si_SignonBonus != '' )
		                                    <div ng-hide="$job->si_SignonBonus == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Sign-on Bonus commitment</div>
		                                        <div class="value">{{$job->si_SignonBonus}}</div>
		                                    </div>
		                                @endif     

		                                @if($job->fi_nameAssistant != '' )
		                                    <div ng-hide="$job->fi_nameAssistant == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Name of Anesthesiologist Assistant(s)</div>
		                                        <div class="value">{{$job->fi_nameAssistant}}</div>
		                                    </div>
	                                    @endif 

	                                    @if($job->si_PaidLeave != '' )
		                                    <div ng-hide="$job->si_PaidLeave == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Paid Educational Leave</div>
		                                        <div class="value">{{$job->si_PaidLeave}}</div>
		                                    </div>
		                                 @endif 

		                                @if($job->si_PaidVacation != '' ) 	
		                                    <div ng-hide="$job->si_PaidVacation == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Paid Vacation</div>
		                                        <div class="value">{{$job->si_PaidVacation}}</div>
		                                    </div>
		                                @endif 

		                                @if($job->ag_name != '' ) 	
		                                    <div ng-hide="$job->ag_name == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Anesthesia Group Name</div>
		                                        <div class="value">{{$job->ag_name}}</div>
		                                    </div>
		                                 @endif
		                                

		                                @if($job->ag_webSite != '' ) 	
		                                    <div ng-hide="$job->ag_webSite == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Anesthesia Group Web Site</div>
		                                        <div class="value">{{$job->ag_webSite}}</div>
		                                    </div>
		                                @endif    

		                                @if($job->ag_haveContract != '' )
		                                    <div ng-hide="$job->ag_haveContract == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Anesthesia Group Exclusive Contract</div>
		                                        <div class="value">{{$job->ag_haveContract}}</div>
		                                    </div>
		                                @endif     

	                                    	
		                                @if($job->fi_nameCRNA != '' )
		                                    <div ng-hide="$job->fi_nameCRNA == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Name of Chief CRNA</div>
		                                        <div class="value">{{$job->fi_nameCRNA}}</div>
		                                    </div>
		                                @endif         

	                                    


	                                    
		                                @if($job->fi_isFederalJob != '' )
		                                    <div ng-hide="$job->fi_isFederalJob == undefined || $job->fi_isFederalJob == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Federal Government job?</div>
		                                        <div class="value">{{$job->fi_isFederalJob}}</div>
		                                    </div>
		                                @endif     

	                                    @if($job->ci_jobID != '' )
		                                    <div ng-hide="$job->ci_jobID == undefined || $job->ci_jobID == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Company internal Job ID</div>
		                                        <div class="value">{{$job->ci_jobID}}</div>
		                                    </div>
		                                @endif 
		                                

		                                @if($job->ci_name != '' )    
		                                    <div ng-hide="$job->ci_name == undefined || $job->ci_name == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Contact Name</div>
		                                        <div class="value">{{$job->ci_name}}</div>
		                                    </div>
		                                @endif 
		                                

		                                @if($job->ci_email != '' )        
		                                    <div ng-hide="$job->ci_email == undefined || $job->ci_email == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Contact Email</div>
		                                        <div class="value">{{$job->ci_email}}</div>
		                                    </div>
		                                @endif 
		                                   

	                                    @if($job->ci_address1 != '' )  
		                                    <div ng-hide="$job->ci_address1 == undefined || $job->ci_address1 == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Contact Street Address 1</div>
		                                        <div class="value">{{$job->ci_address1}}</div>
		                                    </div>
		                                @endif 
		                                

		                                @if($job->ci_address2 != '' )  
		                                    <div ng-hide="$job->ci_address2 == undefined || $job->ci_address2 == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Contact Street Address 2</div>
		                                        <div class="value">{{$job->ci_address2}}</div>
		                                    </div>
		                                @endif 
		                                

		                                @if($job->ci_city != '' )      
		                                    <div ng-hide="$job->ci_city == undefined || $job->ci_city == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Contact City</div>
		                                        <div class="value">{{$job->ci_city}}</div>
		                                    </div>
		                                @endif 
		                                

		                                @if($job->ci_state != '' )     
		                                    <div ng-hide="$job->ci_state == undefined || $job->ci_state == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Contact State</div>
		                                        <div class="value">{{$job->ci_state}}</div>
		                                    </div>
		                               @endif 
		                               

		                                @if($job->ci_zipcode != '' )
		                                    <div ng-hide="$job->ci_zipcode == undefined || $job->ci_zipcode == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Contact Zip Code</div>
		                                        <div class="value">{{$job->ci_zipcode}}</div>
		                                    </div>
		                                @endif 


		                                    
		                                @if($job->ci_phone != '' )    
		                                    <div ng-hide="$job->ci_phone == undefined || $job->ci_phone == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Contact Voice Phone</div>
		                                        <div class="value">{{$job->ci_phone}}</div>
		                                    </div>
		                                @endif     

		                                @if($job->ci_fax != '' )
		                                    <div ng-hide="$job->ci_fax == undefined || $job->ci_fax == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Contact Fax</div>
		                                        <div class="value">{{$job->ci_fax}}</div>
		                                    </div>
		                                @endif      

		                                @if($job->ci_website != '' )    
		                                    <div ng-hide="$job->ci_website == undefined || $job->ci_website == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Contact Web Site</div>
		                                        <div class="value">{{$job->ci_website}}</div>
		                                    </div>
		                                @endif 
		                                    
		                                @if($job->ci_prefMethod != '' )     	   
		                                    <div ng-hide="$job->ci_prefMethod == undefined || $job->ci_prefMethod == ''" class="col-4 float-left field-preview">
		                                        <div class="key">Preferred Contact Method</div>
		                                        <div class="value">{{$job->ci_prefMethod}}</div>
		                                    </div>
		                                @endif     

	                                  </div>

	                                </div> 
							</div>
						</div>
					</div>

<!-- 
					<div class="col-12"">
						<div class="row">
							<div class="col-12">
								<label>Additional Information</label>
							</div>
						</div>
					</div> -->

					<!-- <div class="col-12"">
						<div class="row">
							<div class="col-12">
								<div class="col-3">
									<label>Values</label>
									<br>
								</div>
							</div>
						</div>
					</div> -->
				</div>

				<div class="col-4" style="width: 200px;position: absolute; top:0px;left: 400px">
					
				</div>
			</div>
		</div>
	</div>
</body>
</html>