
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<!--[if (gte mso 9)|(IE)]>
	<xml>
		<o:OfficeDocumentSettings>
			<o:AllowPNG/>
			<o:PixelsPerInch>96</o:PixelsPerInch>
		</o:OfficeDocumentSettings>
	</xml>
	<![endif]-->

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>New Candidate</title>

	<!-- GOOGLE FONTS LINK -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet" />

	<style type="text/css">
		/*------ RESET STYLE ------ */
		*{-webkit-text-size-adjust:none;-webkit-text-resize:100%;text-resize:100%;}
		table{border-spacing: 0;border-collapse: collapse !important;}
		h1, h2, h3, h4, h5, h6{display:block; Margin:0; padding:0;}
		img, a img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
		body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}
		
		/*------ CLIENT-SPECIFIC STYLE ------ */
		@-ms-viewport{width:device-width;}
		table{mso-table-lspace:0pt; mso-table-rspace:0pt;}
		p, a, li, td, blockquote{mso-line-height-rule:exactly;}
		p, a, li, td, body, table, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%;}
		#outlook a{padding:0;}
		.ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
		.ExternalClass,.ExternalClass div,.ExternalClass font,.ExternalClass p,.ExternalClass span,.ExternalClass td,img{line-height:100%;}

		/*------ GOOGLE FONT STYLE ------ */
		[style*="Open Sans"] {font-family:'Open Sans', Helvetica, Arial, sans-serif !important;}
		[style*="Lora"] {font-family:'Lora', Georgia, Times, serif !important;}

		/*------ General Style ------ */
		.wrapperTable{width:100%; max-width:600px; Margin:0 auto;}
		.oneColumn {text-align:center; font-size:0;}
		
	</style>

	<style type="text/css">
		/*------ Media Width 480 to 640 ------ */
		@media screen and (min-width: 480px) and (max-width: 640px) {
			td[class="imgHero"] img{ width:100% !important;}
		}
	</style>

	<style type="text/css">
		/*------ Media Width 480 ------ */
		@media screen and (max-width:480px) {
			table[class="wrapperTable"]{width:100% !important; }
			td[class="title"] h2{font-size:26px !important;line-height:34px !important;}
			td[class="imgHero"] img{ width:100% !important;}
		}
	</style>
</head>

<body style="background-color:#F5F5F5;">
<center>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed;background-color:#F9F9F9;" id="bodyTable">
	<tbody><tr>
		<td align="center" valign="top" style="padding-right:10px;padding-left:10px;" id="bodyCell">
		<!--[if (gte mso 9)|(IE)]><table align="center" border="0" cellspacing="0" cellpadding="0" style="width:600px;" width="600"><tr><td align="center" valign="top"><![endif]-->

		<!-- Email Pre-Header Open // -->
		<table border="0" cellpadding="0" cellspacing="0" style="max-width:600px;" width="100%" class="wrapperTable">
			<tbody><tr>
				<td align="center" valign="top">
					<!-- Content Table Open // -->
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tbody><tr>
							<td align="right" valign="middle" style="padding-top:10px;padding-right:20px;" class="preHeader">
								<!-- Email View in Browser // -->
								<a class="smlText hide" href="#" target="_blank" style="color:#BBBBBB; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:11px; font-weight:400; font-style:normal; letter-spacing:normal; line-height:20px; text-transform:none; text-align:right; text-decoration:underline; padding:0; margin:0">
									Trouble seeing this email?
								</a>
							</td>
						</tr>
					</tbody></table>
					<!-- Content Table Close // -->
				</td>
			</tr>
		</tbody></table>
		<!-- Email Pre-Header Close // -->

		<!-- Email Header Open // -->
		<table border="0" cellpadding="0" cellspacing="0" style="max-width:600px;" width="100%" class="wrapperTable">
			<tbody><tr>
				<td align="center" valign="top">
					<!-- Content Table Open // -->
					<table border="0" cellpadding="0" cellspacing="0" width="100%" class="logoTable" style="">
						<tbody><tr>
							<td align="center" valign="middle" style="padding-top:40px;padding-bottom:40px">
								<!-- Logo and Link // -->
								<a href="#" target="_blank" style="text-decoration:none;" class="">
									<img src="https://marketing-image-production.s3.amazonaws.com/uploads/515cfa488822a9c56ffab95ef04b357ffe26af4984ff24acfba2b5a2af0e0646b935bd7617c72daa91ddf3d724db121c2f867122ccafc2802aae8f6354381f38.png" alt="" width="180" border="0" style="height:auto; display:block;" class="">
								</a>
							</td>
						</tr>
					</tbody></table>
					<!-- Content Table Close// -->
				</td>
			</tr>
		</tbody></table>
		<!-- Email Header Close // -->

		<!-- Email Body Open // -->
		<table border="0" cellpadding="0" cellspacing="0" style="max-width:600px;" width="100%" class="wrapperTable">
			<tbody><tr>
				<td align="center" valign="top">
					<!-- Card Table Open // -->
					<table border="0" cellpadding="0" cellspacing="0" style="background-color: rgb(255, 255, 255); box-shadow: rgb(216, 216, 216) 0px 0px 10px;" width="100%" class="oneColumn">

						<!-- Header Border // -->
						<tbody><tr>
							<td class="topBorder" height="3" style="background-color:#8D6CD1;font-size:1px;line-height:3px;">&nbsp;</td>
						</tr>

						<tr>
							<td align="center" valign="top" style="padding-bottom: 40px;" class="imgHero">
								<!-- Hero Image  // -->
								<a href="#" target="_blank" style="text-decoration:none;">
									<img src="http://grapestheme.com/notify/img/hero/account-confirmation.png" width="600" alt="" border="0" style="width:100%; max-width:600px; height:auto; display:block;">
								</a>
							</td>
						</tr>

						<tr>
							<td align="center" valign="top" style="padding-bottom: 5px; padding-left: 20px; padding-right: 20px;" class="title">
								<!-- Main Title Text // -->
								<h2 class="bigTitle" style="color:#313131; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:26px; font-weight:600; font-style:normal; letter-spacing:normal; line-height:34px; text-align:center; padding:0; margin:0;">
									Job #{{$data["job_id_src"]}} has a new applicant!
								</h2>
							</td>
						</tr>

						<tr>
							<td align="center" valign="top" style="padding-bottom: 20px; padding-left: 20px; padding-right: 20px;" class="subTitle">
								<!-- Sub Title Text // -->
								<h4 mc:edit="subTitle" class="midTitle" style="color:#919191; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:18px; font-weight:400; font-style:normal; letter-spacing:normal; line-height:26px; text-align:center; padding:0; margin:0;">
									Someone by the name of {{$data["candidate_name"]}}<br> has inquired about your job #{{$data["job_id_src"]}}.
								</h4>
							</td>
						</tr>

						<tr>
							<td align="center" valign="top" style="padding-bottom: 40px; padding-left: 20px; padding-right: 20px;" class="description">
								<!-- Description  Text// -->
								<p class="midText" style="color:#919191; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:14px; font-weight:400; line-height:22px; text-align:center; padding:0; margin:0;">
									Please sign in to your account in order to view the inquiry and contact the candidate.
								</p>
							</td>
						</tr>

						<tr>
							<td align="center" valign="top" style="padding-bottom: 40px; padding-left: 20px; padding-right: 20px;" class="btnCard">
								<!-- Button Table // -->
								<table align="center" border="0" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td align="center" class="postButton" style="background-color: rgb(84, 76, 249); padding: 10px 25px; border-radius: 2px;">
												<!-- Button Link // -->
												<a href="{{$data['inquiry_url']}}" target="_blank" style="color:#FFFFFF; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:12px; font-weight:600; letter-spacing:1px; line-height:20px; text-transform:uppercase; text-decoration:none; display:block;" class="">
													View Application
												</a>
											</td>
										</tr>
										<tr>
											<td align="center" class="postButton" style="padding: 10px 25px; border-radius: 2px;">
												<!-- Button Link // -->
												Or email to {{ $data['to'] }} directly												
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>

						<tr>
							<td height="10" style="font-size:1px;line-height:1px;">&nbsp;</td>
						</tr>

					</tbody></table>
					<!-- Card Table Close// -->

					<!-- Space -->
					<table border="0" cellpadding="0" cellspacing="0" width="100%" class="space">
						<tbody><tr>
							<td height="30" style="font-size:1px;line-height:1px;">&nbsp;</td>
						</tr>
					</tbody></table>

				</td>
			</tr>
		</tbody></table>
		<!-- Email Body Close // -->

		<!-- Email Footer Open // -->
		<table border="0" cellpadding="0" cellspacing="0" style="max-width:600px;" width="100%" class="wrapperTable">
			<tbody><tr>
				<td align="center" valign="top" class="footerCell">
                    
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="space">
                        <tbody><tr>
                            <td height="20" style="font-size:1px;line-height:1px;">&nbsp;</td>
                        </tr>
                    </tbody></table>

					<!-- Content Table Open// -->
					<table border="0" cellpadding="0" cellspacing="0" width="100%" class="footer">
						<tbody><tr>
							<td align="center" valign="top" style="padding-top:10px;padding-bottom:10px;padding-left:10px;padding-right:10px;" class="socialLinks">
								<!-- Social Links (Facebook)// -->
								<a href="#facebook-link" target="_blank" style="display:inline-block;" class="facebook">
									<img src="http://grapestheme.com/notify/img/social/facebook.png" alt="" width="40" border="0" style="height:auto;margin:2px">
								</a>
								<!-- Social Links (Twitter)// -->
								<a href="#twitter-link" target="_blank" style="display:inline-block;" class="twitter">
									<img src="http://grapestheme.com/notify/img/social/twitter.png" alt="" width="40" border="0" style="height:auto;margin:2px">
								</a>
								<!-- Social Links (Pintrest)// -->
								<a href="#pintrest-link" target="_blank" style="display:inline-block;" class="pintrest">
									<img src="http://grapestheme.com/notify/img/social/pintrest.png" alt="" width="40" border="0" style="height:auto;margin:2px">
								</a>
								<!-- Social Links (Instagram)// -->
								<a href="#instagram-link" target="_blank" style="display: inline-block;" class="instagram">
									<img src="http://grapestheme.com/notify/img/social/instagram.png" alt="" width="40" border="0" style="height:auto;margin:2px">
								</a>
								<!-- Social Links (Linkdin)// -->
								<a href="#linkdin-link" target="_blank" style="display: inline-block;" class="linkdin">
									<img src="http://grapestheme.com/notify/img/social/linkdin.png" alt="" width="40" border="0" style="height:auto;margin:2px">
								</a>
							</td>
						</tr>

						<tr>
							<td align="center" valign="top" class="footerLinks" style="">
								<!-- Use Full Links (Privacy Policy)// -->
								<p class="smlText" style="color:#313131; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:12px; font-weight:400; line-height:18px; text-align:center; margin:0; padding:0;">
									<a href="#" style="color:#8D6CD1;text-decoration:none" target="_blank">Privacy Policy</a> | <a href="#" style="color:#8D6CD1;text-decoration:none" target="_blank">Help</a> | <a href="#" style="color:#8D6CD1;text-decoration:none" target="_blank">About Us</a>
								</p>
							</td>
						</tr>

						<tr>
							<td align="center" valign="top" style="padding: 10px;" class="brandInfo">
								<!-- Information of NewsLetter (Privacy Policy)// -->
								<p class="smlText" style="color:#313131; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:11px; font-weight:400; line-height:18px; text-align:center; margin:0; padding:0;">
									This email was sent to you by Weekly, Inc. You are receiving this email because you Subscribe on Weekly.&nbsp;<br> If you wish to unsubscribe from all future emails, please <a href="#" style="color:#8D6CD1;text-decoration:none" target="_blank">click here</a><br>

									<br>Weekly, | 800 Broadway, Suite 1500 | New York, NY 000123, USA
								</p>
							</td>
						</tr>

						<tr>
							<td align="center" valign="top" style="padding-top:10px;padding-bottom:10px;padding-left:10px;padding-right:10px;" class="appLinks">
								<!-- App Links (Anroid)// -->
								<a href="#Play-Store-Link" target="_blank" style="display:inline-block;" class="play-store">
									<img src="http://grapestheme.com/notify/img/play-store.png" alt="" width="120" border="0" style="height:auto;margin:5px">
								</a>
								<!-- App Links (IOs)// -->
								<a href="#App-Store-Link" target="_blank" style="display:inline-block;" class="app-store">
									<img src="http://grapestheme.com/notify/img/app-store.png" alt="" width="120" border="0" style="height:auto;margin:5px">
								</a>
							</td>
						</tr>
					</tbody></table>
					<!-- Content Table Close// -->
				</td>
			</tr>

			<!-- Space -->
			<tr>
				<td height="30" style="font-size:1px;line-height:1px;">&nbsp;</td>
			</tr>
		</tbody></table>
		<!-- Email Footer Close // -->

		<!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
		</td>
	</tr>
</tbody></table>
	</center>
</body>
</html>