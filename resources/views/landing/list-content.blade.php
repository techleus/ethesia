@extends('landing.index-list')


@section('content')
<input type="hidden" id="jobsData" ng-model = "jobsData" value="{{json_encode(Session::get('jobsData'))}}" name="">
<style type="text/css">	
	#list-page{
		min-height: 800px;
	}
	.list-left-content{
		padding-right: 0;
		margin-top: 50px;
	}
	.list-title{
		font-size: 22px;
		font-family: "Open Sans", Sans-serif;
		color: #000000;
		padding-right: 40%;		
	}
	.card.card-job-list{
		border:0;
		box-shadow: none;		
	}
	.card-body-list{		
		border-bottom: 1px solid #ebebeb;		
	}
	.card-body-list div{
		float: left;		
	}
	.card-logo{
		padding: 5px;
		border-radius: 100%;
		box-shadow: 3px 4px 49px 0px rgba(0, 0, 0, 0.18); 
	}
	.card-logo img{
		border-radius: 100%;
		height: 100px;
		width: auto;
	}
	.card-body-list .detail-container{
		padding-left: 25px;		
		width: calc(100% - 130px);				
	}
	.detail-container .text{
		display: block;
		float: left;				
		width: 100%;
	}
	.detail-container .detail-title{
		font-family: "Open Sans", Sans-serif;
		font-weight: 700;
		color: #6684f2;
		font-size: 16px;
		padding-right: 100px;
	}
	.detail-container .detail-company{
		padding: 8px 0 8px 0;
		font-family: "Open Sans", Sans-serif;
		font-size: 14px;
		color: #262626;
	}
	.detail-container .detail-description{
		font-family: "Open Sans", Sans-serif;
		font-size: 14px;
		color: #7d7d7d;
	}
	.detail-container .detail-job-info{
		padding-top: 10px;
	}
	.location-detail{

	}
	.location-detail .location{
		font-family: "Open Sans", Sans-serif;
		font-size: 14px;
		color: #000;
	}
	.detail-job-info ul{		
		list-style: none;
		padding-left: 0;
	}
	.detail-job-info ul li{		
		height: 25px;
		padding-right: 30px;
		display: inline-block;
	}
	.location-detail .place{
		font-family: "Open Sans", Sans-serif;
		font-size: 14px;
		color: #6684f2;
	}
	.card-featured{
		background: #af71fd;		
		position: absolute;
		top: 1;
		right: 0;
		color: #fff;
		font-size: 11px;
		font-family: "Open Sans", Sans-serif;		
		text-align: right;
		padding: 3px 10px 3px 20px;		
		border-top-left-radius: 30px;
		border-bottom-left-radius: 30px;
	}
	.jobid{
		background: #af71fd;				
		color: #fff;
		font-size: 11px;
		font-family: "Open Sans", Sans-serif;		
		text-align: right;
		padding: 3px 10px 3px 20px;		
		border-top-left-radius: 30px;
		border-bottom-left-radius: 30px;
		margin-top: -50px;
	}
	.search-content{
		display: none;
		width: 0;
		-webkit-animation: hideSearchBar 0.5s;
		-animation: hideSearchBar 0.5s;
	}
	@-webkit-keyframes hideSearchBar {
		0%{
			width: 250px;			
		}	
		100%{
			width: 0px;
			display: none;			
		}

	}
	@keyframes hideSearchBar {
	  	0%{
			width: 250px;			
		}	
		100%{
			width: 0px;			
			display: none;
		}
	}
	.search-content.active{
		display: inline-block;
		float: left;
		border-left: 1px solid #ebebeb;
		width: 300px;
		padding: 0px;		
		position: absolute;
		top: 0;
		right: 3%;
		background: #fff;		
		-webkit-animation: showSearchBar 0.5s;
    	animation: showSearchBar 0.5s;    
	}
	@-webkit-keyframes showSearchBar {
		0%{
			width: 0px;
		}	
		100%{
			width: 300px;
			right: 300;
		}

	}
	@keyframes showSearchBar {
	  	0%{
			width: 0px;
		}	
		100%{
			width: 300px;
			right: 300;
		}
	}
	#left-content{
		width: 90% !important;
		margin: 0 auto;
		-webkit-animation: resizeBack 0.5s;
    	animation: resizeBack 0.5s;    
	}
	@-webkit-keyframes resizeBack{
		0%{
			padding-right: 240px;
		}
		100%{
			padding-right: 0;
		}
	}
	@keyframes resizeBack{
		0%{
			padding-right: 240px;
		}
		100%{
			padding-right: 0;
		}
	}	
	#left-content.resize{
		padding-right: 280px;
		-webkit-animation: resizeLeftContent 0.5s;
    	animation: resizeLeftContent 0.5s;   
	}
	@-webkit-keyframes resizeLeftContent{
		0%{
			padding-right: 0;
		}
		100%{
			padding-right: 280px;
		}
	}
	@keyframes resizeLeftContent{
		0%{
			padding-right: 0;
		}
		100%{
			padding-right: 280px;
		}
	}
	
	.search-content .search-title{
		font-family: "Open Sans", Sans-serif;
		color: #000;
		background: #e6eff1;		
		font-size: 14px;
		width: 100%;
		max-width: 240px;
		padding-top: 10px;
		padding-bottom: 10px;
		padding-left: 40px;
		border-top-right-radius: 15px;
		border-bottom-right-radius: 15px;
		background: url(/public/assets/images/jobs-img/sort.png) #e6eff1;
		background-position: left 18px top 15px;
		background-repeat: no-repeat;
		padding-left: 55px;
		text-overflow: ellipsis;
		white-space: nowrap;
    	text-overflow: ellipsis;
    	overflow: hidden;
		position: relative;
	}
	
	.circle-plus{
		padding: 3px 13px 3px 13px;
		background: #464646;
		text-align: center;
		color: #fff;
		border-radius: 100%;
		position: absolute;
		font-weight: bold;
		font-size: 22px;
		top: 0;
		right: 0;
	}
	.sort-content{
		margin-top: 30px;
		padding-left: 25px;
	}
	.sort-content .sort-title{
		font-family: "Open Sans", Sans-serif;
		color: #000;
		font-size: 16px;
	}
	.sort-options{
		padding-top: 25px;
	}
	.sort-options ul{
		padding:0;
	}
	.sort-options ul li{
		list-style: none;
		line-height: 40px;
		color: #7d7d7d;
		font-size: 14px;
		font-family: "Open Sans", Sans-serif;
	}
	.sort-options ul li.active{
		color: #6684f2;
		font-weight: 600;
	}
	.search-row{
		border-bottom: 1px solid #ebebeb;
	}
	.connect-title{
		font-family: "Myriad Pro";
		color: #000;
		font-size: 16px;
	}
	.connect-title.disable{
		font-size: 14px;
		color: #959595;
		margin-top: 20px;
	}
	.connect-options{
		padding-bottom: 120px;
	}
	.connect-options ul:first-child{
		padding: 50px 0 15px 15px;				
		width: 106px;
	}
	.connect-options ul li{
		list-style: none;		
		display: inline-block;
		width: 40px;		
		margin-left: -21px;
	}	
	.connect-options ul:first-child li .circle-employer{
		height: 35px;
		width: 35px;
		border-radius: 100%;
		background: #7d7d7d;
		border: 2px solid #fff;
	}
	.connect-options ul:not(:first-child){
		float: left;
		min-width: 126px;		
		width: auto;		
		padding: 50px 0 0 25px;
	}
	.connect-options ul:not(:first-child) li{
		padding: 0;
		display: block;
		font-size: 14px;
		font-family: "Myriad Pro";
	    white-space: nowrap;
	    text-overflow: ellipsis;
	    display: block;
	    overflow: hidden;
	    width: 100%;
	}
	.connect-options ul{
		float: left;
	}
	.btn-add-conn{
		background: #544cf9;
		color: #fff;
		font-family: "Myriad Pro";
		margin-top: 15px;
		margin-bottom: 15px;		
		padding: 10px 35px 10px 35px;
		border-radius: 35px;
	}
	.connect-title .toogle{
		float: right;
		cursor: pointer;
	}
	.connect-title .toogle ul{
		float: right;
		cursor: pointer;
	}
	.connect-title .toogle ul li{
		display: inline-block;
		list-style: none;
		height: 8px;
		width: 8px;
		background: #c2c2c2;
		border-radius: 100%;		
		cursor: pointer;
	}
	.people-content{		
		width: 100%;						
		padding-top: 5px;
		padding-bottom: 5px;
	}
	.people-content:last-child{
		margin-bottom: 40px;
	}	
	.people-content .circle-people{					
		width: 40px;
		height: 40px;
		background: #e8eff0;
		border-radius: 100%;		
		text-align: center;
		line-height: 40px;
		cursor: pointer;
	}
	.people-content .circle-people.active{
		background: #544cf9;
		color: #fff;
		font-family: "Open Sans";
		font-size: 13px;
	}
	.people-content .circle-people:not(:first-child){
		

	}
	#show-search{
		position: absolute;
		top: 0;
		right: 0;		
		font-family: "Open Sans", Sans-serif;
		color: #000;
		background: #e6eff1;		
		font-size: 14px;
		width: 100%;
		max-width: 240px;
		padding-top: 10px;
		padding-bottom: 10px;
		padding-left: 50px;
		padding-right: 30px;
		background: url(/public/assets/images/jobs-img/sort.png) #e6eff1;
		background-position:right 18px top 15px !important;
		border-top-left-radius: 15px;
		border-bottom-left-radius: 15px;		
		background-position: left 25px top 15px;
		background-repeat: no-repeat;		
		cursor: pointer;
	}
	.hide#show-search{
		-webkit-animation: hideBtnShow 0.5s;
    	animation: resizeLeftContent 0.5s;   
	}
	@-webkit-keyframes hideBtnShow{
		0%{
			width: 100%;
		}
		99%{
			width: 0%;
		}
		100%{
			display: none;
		}
	}
	@keyframes hideBtnShow{
		0%{
			width: 100%;
		}
		99%{
			width: 0%;
		}
		100%{
			display: none;
		}
	}
	#hide-search{
		cursor: pointer;
	}
	#show-search .circle-plus{
		padding: 3px 13px 3px 13px;
		background: #464646;
		text-align: center;
		color: #fff;
		border-radius: 100%;
		width: 40px;
		height: 40px;
		position: absolute;
		font-weight: bold;
		font-size: 22px;
		top: 0;
		left: 0;
	}
</style>
<div class="page-container"><!--page-container-->
	<div class="row">
		<div class="col-md-12 col-lg-10 offset-lg-1">
			<div class="row" id="list-page">
				<div id="left-content">
					<div class="row">
						<div class="col-md-12">
							<div class="list-title">Showing 25 of 2930 Anesthesiologist Jobs</div>							
							<div show-search target="search-content" id="show-search">
								<div class="circle-plus">+</div> <span>Show Custom Search</span>
							</div>
						</div>
						<div class="col-md-12 list-left-content">
							<div class="card card-job-list">
							  	<div class="card-body card-body-list">							  		
							  		<div class="card-logo"><img ng-src="/public/assets/images/users/1.jpg"></div>
							  		<div class="detail-container">
							  			<div class="card-featured">FEATURED</div>
							  			<div class="text detail-title">
							  				Looking for a CRNA in New York City. Well Paid. Includes sign-on bonus.
							  			</div>							  			
							  			<br>
							  			<div class="text detail-company">
							  				<div class="text-location">
							  					<i class="mdi mdi-map-marker" style="color: #6684f2;"></i>
						  						<span class="location">Location: </span><span class="place">Adak, Alaska</span>
							  				</div>
							  				<div class="text-company">
							  					Posted by: Sheridan Healthcare
							  				</div>
							  			</div>
							  			<br>
							  			<div class="text detail-description">
							  				Rapidly moving ahead, we leveraged our technology to enable other publishers to distribute their content digitally, with our mobile app as a service platform. An overnight hit, it attracted 6 million hits in less than...
							  			</div>
							  			<div class="text detail-job-info">							  				
							  				<ul>							  					
							  					<li>
							  						<div class="location-detail">
									  					<i class="fa fa-calendar" style="color: #6684f2;"></i>
									  					<span class="location">Post Date: </span><span class="place">March 28, 2018</span>
									  				</div>
							  					</li>
							  					<li>
							  						<div class="location-detail">
									  					<i class="fa money-bill-alt mdi-currency-usd" style="color: #6684f2;"></i>
									  					<span class="location">Salary: </span><span class="place">$280,000 - $320,000</span>
									  				</div>
							  					</li>
							  					<li>
							  						<div class="location-detail">
									  					<i class="fa fa-clock-o" style="color: #6684f2;"></i>
									  					<span class="location">Duration: </span><span class="place">Fellowship</span>
									  				</div>
							  					</li>							  					
							  				</ul>							  				
							  			</div>										
							  		</div>
							  		<div class="jobid float-right">#3920</div>
							  	</div>
							</div>	
							<div class="card card-job-list">
							  	<div class="card-body card-body-list">							  		
							  		<div class="card-logo"><img ng-src="/public/assets/images/users/1.jpg"></div>
							  		<div class="detail-container">
							  			<div class="card-featured">FEATURED</div>
							  			<div class="text detail-title">
							  				Looking for a CRNA in New York City. Well Paid. Includes sign-on bonus.
							  			</div>							  			
							  			<br>
							  			<div class="text detail-company">
							  				<div class="text-location">
							  					<i class="mdi mdi-map-marker" style="color: #6684f2;"></i>
						  						<span class="location">Location: </span><span class="place">Adak, Alaska</span>
							  				</div>
							  				<div class="text-company">
							  					Posted by: Sheridan Healthcare
							  				</div>
							  			</div>
							  			<br>
							  			<div class="text detail-description">
							  				Rapidly moving ahead, we leveraged our technology to enable other publishers to distribute their content digitally, with our mobile app as a service platform. An overnight hit, it attracted 6 million hits in less than...
							  			</div>
							  			<div class="text detail-job-info">							  				
							  				<ul>							  					
							  					<li>
							  						<div class="location-detail">
									  					<i class="fa fa-calendar" style="color: #6684f2;"></i>
									  					<span class="location">Post Date: </span><span class="place">March 28, 2018</span>
									  				</div>
							  					</li>
							  					<li>
							  						<div class="location-detail">
									  					<i class="fa money-bill-alt mdi-currency-usd" style="color: #6684f2;"></i>
									  					<span class="location">Salary: </span><span class="place">$280,000 - $320,000</span>
									  				</div>
							  					</li>
							  					<li>
							  						<div class="location-detail">
									  					<i class="fa fa-clock-o" style="color: #6684f2;"></i>
									  					<span class="location">Duration: </span><span class="place">Fellowship</span>
									  				</div>
							  					</li>							  					
							  				</ul>							  				
							  			</div>										
							  		</div>
							  		<div class="jobid float-right">#3920</div>
							  	</div>
							</div>							
						</div>
					</div>					
				</div>
				<div class="search-content">
					<div class="search-row">
						<div class="search-title" hide-search target="search-content" id="hide-search">
							Hide Custom search
							<div class="circle-plus">+</div>
						</div>
						<div class="sort-content">
							<div class="sort-title">Sort these jobs by:</div>
							<div class="sort-options">
								<ul>
									<li class="active">Date posted</li>
									<li>By location</li>
									<li>By Browse with top</li>
									<li>Browse Local jobs</li>
									<li>Browse Categories</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="search-row">
						<div class="sort-content">
							<div class="connect-title">Connect with Employers</div>
							<div class="connect-options">
								<ul>									
									<li><div class="circle-employer"></div></li>
									<li><div class="circle-employer"></div></li>
									<li><div class="circle-employer"></div></li>
									<li><div class="circle-employer"></div></li>
								</ul>
								<ul>
									<li>163 Connections</li>
									<li>290 Followers </li>
									<li>36 Following</li>
								</ul>
							</div>
							<div class="connect-title disable">To see more Employers</div>
							<button type="button" class="btn btn-add-conn">Add Connections</button>
						</div>						
					</div>
					<div class="search-row">
						<div class="sort-content">
							<div class="connect-title">
								People you may know
								<div class="toogle">
									<ul>
										<li></li>
										<li></li>
										<li></li>
									</ul>
								</div>
							</div>
							<div class="row people-content">
								<div class="col-md-3">
									<div class="circle-people"></div>
								</div>
								<div class="col-md-3">
									<div class="circle-people"></div>
								</div>
								<div class="col-md-3">
									<div class="circle-people"></div>
								</div>
								<div class="col-md-3">
									<div class="circle-people"></div>
								</div>
							</div>
							<div class="row people-content">
								<div class="col-md-3">
									<div class="circle-people"></div>
								</div>
								<div class="col-md-3">
									<div class="circle-people"></div>
								</div>
								<div class="col-md-3">
									<div class="circle-people"></div>
								</div>
								<div class="col-md-3">
									<div class="circle-people active">+12</div>
								</div>
							</div>
						</div>						
					</div>
				</div>				
			</div>
		</div>
	</div>	
</div>


<div class="slider-form-overlay hidden"></div>


@include('slider.job_preview')
@include('slider.apply_slider_form')
@include('slider.registration_slider')
@include('slider.filter-jobs-form')
@include('modals.shareSocial')


@endsection