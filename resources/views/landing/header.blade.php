<style type="text/css">
	.search_container ul.fields_container{
		width: 100%;
	}
	.search_container ul.fields_container li{
		width: 50%;
	}
	.menu_right .login_btn{
		border: 1px solid #708cf3;
	}	
	.menu_right .signup_btn{
		background: -webkit-linear-gradient(to right, #804cf9 1%, #554cf9 100%);
		background: -moz-linear-gradient(to right, #804cf9 1%, #554cf9 100%);
		background: linear-gradient(to right, #804cf9 1%, #554cf9 100%);
		color: #fff !important;
	}


	/*dropdown tags*/


	.txt-search-dd-btn{
		padding-top: 10px;
		padding-right: 560px;
	}
	.txt-search-dd-btn[multiple] option{
		display: none;
	}
	
	#addedoption,.txt-search-dd-btn[multiple] option, .txt-search-dd-btn[multiple] option:focus, .txt-search-dd-btn[multiple] option:active{
		display: inline-block;
		z-index: 1;
		border: 1px solid #e8eff0;
		background: #e8eff0;
		padding: 5px 10px 5px 10px;
		border-radius: 25px;
		font-size: 13px;		
		font-family: "Open Sans", Sans-serif;
	}	
	.txt-search-dd-btn option:not(:last-child){
		margin-right: 10px;
	}
	#dropDownContent{
		display: none;
	}
	.active#dropDownContent{
		display: block;
		position: absolute;
		width: calc(100% - 230px);
		border: 1px solid #ebebeb;
		background:  #fff;
		z-index: 1;
		padding : 1px;
		margin-left : 48px;
		border-top : 0;        
	}
	.active#dropDownContent ul{
		padding: 2px;
	}
	.active#dropDownContent ul li{		
		width: 100%;
		padding: 5px;
	}
	.active#dropDownContent ul li input{
		border-radius: 0;
	}
	i.closeTaginput{
		position: absolute;
		top: 7px;
		right: 17px;
		font-weight: bold;
		font-size: 24px;
		cursor: pointer;		
	}
	.bootstrap-tagsinput{

		min-height: 55px;
		border: 1px solid #ebebeb;
		border-radius: 35px;
		width: 90%;	
		padding-left: 50px;	
		background: url(/public/assets/images/jobs-img/icon-search16.png);	
		background-position: 20px;
		background-repeat: no-repeat;
		padding-right: 140px;
		font-family: 'Open Sans';
		font-size: 16px;	
		z-index:1;
		line-height: 40px;
		text-align: left;
		text-transform: capitalize;
	}
	.bootstrap-tagsinput .label{
		border: 1px solid #e8eff0;
		background: #e8eff0;
		padding: 5px 35px 5px 20px;		
		font-size: 13px;		
		font-family: "Open Sans", Sans-serif;
		color: #000;
		border-radius: 25px;
		cursor: pointer;
		position: relative;
	}
	.bootstrap-tagsinput .tag [data-role="remove"]{
		font-weight: bold;
		height: 20px;
		width: 20px;
		position: absolute;
		top: -5;
		right: 5;
		border-radius: 100%;
		background: #fff;
		text-align: center;
		line-height: 18px;
		font-size: 8px;
		border: 1px solid #979797;
	}
	.bootstrap-tagsinput input{
		border: 0;
	}
	.bootstrap-tagsinput .badge:not(:last-child){
		margin-right: 5px;
	}
	.find_jobs_btn{			

		display: block;
		position: relative;
		float: right;	
		margin-top: -51px;
		margin-right: calc(10% + 4px);
		color: #fff;
		border:0;
		height: 47px;
		padding-left: 20px;
		padding-right: 20px;	
		border-radius: 25px;
		width: 150px;
		background: rgb(86,76,249); /* Old browsers */
		background: -moz-linear-gradient(to right, rgba(86,76,249,1) 1%, rgba(128,76,249,1) 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(to right, rgba(86,76,249,1) 1%,rgba(128,76,249,1) 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to right, rgba(86,76,249,1) 1%,rgba(128,76,249,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#564cf9', endColorstr='#804cf9',GradientType=0 ); 
		width: 150px;
	}
	#job-member-container{
		height: 240px;
		position: absolute;		
		top: 70;
		right: 100;
		width: 40%;		
	}
	#job-member-container ul{
		padding: none;
	}
	#job-member-container ul li{
		display: inline-block;
		list-style: none;		
		height: 240px;		
	}
	#job-member-container ul li:first-child{
		padding-top: 20px;
	}
	#job-member-container ul li:nth-child(2){
		padding-top: 145px;
		padding-left: 30px;
	}
	#job-member-container ul li:nth-child(3){
		width: 50px;
	}
	#job-member-container ul li:nth-child(4){
		padding-left: 60px;
	}
	#job-member-container ul li:nth-child(3) img{
		display: none;
	}
	#job-member-container ul li{
		margin-top: 0;
	}
	#job-member-container ul li img{
		display: block;
		border-radius: 100%;
		border: 1px solid #fff;		
		box-shadow: 18px 30px 46px 0px rgba(0, 0, 0, 0.20);
		cursor: pointer;		
	}
	#job-member-container ul li:first-child img{
		-webkit-animation: moveCircle 10.5s ease-out infinite normal;
    	animation: moveCircle 10.5s ease-out infinite normal;
	}
	
	#job-member-container ul li:nth-child(2) img{
		-webkit-animation: moveCircle 8.5s ease-out infinite normal;
    	animation: moveCircle 8.5s ease-out infinite normal;
	}

	#job-member-container ul li:nth-child(4) img{
		-webkit-animation: moveCircle 11.5s ease-out infinite normal;
    	animation: moveCircle 11.5s ease-out infinite normal;
	}

	@-webkit-keyframes moveCircle {
	    0% {
	        transform: translate(0px,0px);
	    }
	    20% {
	        transform: translate(0px,5px);
	    }
	    40% {
	        transform: translate(0px,10px);
	    }
	    60% {
	      	transform: translate(0px,10px);
	    }
	    80% {
	      	transform: translate(0px,5px);
	    }
	    100% {
	      	transform: translate(0px,0px);
	    }
	}
	@keyframes moveCircle {
	    0% {
	        transform: translate(0px,0px);
	    }
	    20% {
	        transform: translate(0px,5px);
	    }
	    40% {
	        transform: translate(0px,10px);
	    }
	    60% {
	      	transform: translate(0px,10px);
	    }
	    80% {
	      	transform: translate(0px,5px);
	    }
	    100% {
	      	transform: translate(0px,0px);
	    }
	}
	/*body {
	  display: flex;
	  justify-content: center;
	  align-items: center;
	  min-height: 100vh;
	  background: white;
	}*/
	
	
	@media only screen and (max-width: 1275px){
		#job-member-container{
			display: none;
		}
	}
	
	
</style>
<div class="header_container">	
	
	<div class="menu_container" id="myTopnav">
		<div class="menu_left">
			<div class="logo_wrap">
			<a href="/" class="nav_logo"><img src="/public/assets/images/jobs-img/anesthesia-logo-blue.png"></a>
			<a href="javascript:void(0);" class="mobile_icon" onclick="myNavToggle()">&#9776;</a>			
			</div>
			<div class="menu_wrap">

				<ul class="topmenu">
					@verbatim
					<li class="current"><a href="/browse-jobs/{{jobsData.job_type}}" class="curr_browse">BROWSE JOBS</a></li>
					@endverbatim
					<li>
						@if(Auth::check())
							@if(Auth::User()->userType != 2)
								<a class="curr_post" href="javascript:;" modal="loginModal" anesthesia-modal modal-message="Please sign in as a Provider.">POST A JOB</a>
							@else
								<a class="curr_post" href="/application/#/jobs_list">POST A JOB</a>
							@endif
						@else
							<a class="curr_post" href="javascript:;" modal="loginModal" anesthesia-modal modal-message="Please sign in as a Provider.">POST A JOB</a>
						@endif	
					</li>
					@verbatim	
					<li><a href="/contact-us">CONTACT</a></li>				
					@endverbatim								
				</ul>				
			</div>
			<div class="clearfix"></div>
		</div>

		<div class="menu_right">
			@if(!Auth::check())
				<a modal="loginModal" modal-message="Login to your dashboard." anesthesia-modal class="login_btn pointer">Log in</a> 
				<a show-slider="" slider-width="873px" id_target = "registration-form" class="signup_btn pointer hover-animate" animate-hover>
					<span>Sign Up</span>
				</a>		
			@else
				<a href="/application" class="login_btn pointer">Dashboard</a> 
			@endif						
		</div>
		<div class="clearfix"></div>
	</div>
	
	
	<div class="search_container">		
		<div ng-cloak class="stitle">Browse <span ng-bind="jobsData.job_type" ></span> Jobs</div>		
		<ul class="fields_container">
			<li class="hover-body">				
				<input type="text" tagging class="form-control" id="key_searched">
				<button ng-click="getJobs()" find-jobs animate-hover class="btn find_jobs_btn fontPoppins hover-animate">
					<span>FIND JOBS</span>
				</button>
				<div class="adv_search"><a class="pointer" ng-cloak show-slider="" id_target = "filter-jobs-form">Show advanced filters <img src="/public/assets/images/jobs-img/sort.png"></a></div>
			</li>
		</ul>		
	</div>
	<div class="row" id="job-member-container">
		<ul>
			<li>
				<img src="/public/assets/images/jobs-img/browse-emp-1.png" alt="">
			</li>
			<li>
				<img src="/public/assets/images/jobs-img/browse-emp-2.png" alt="">
			</li>
			<li>
				<img src="/public/assets/images/jobs-img/browse-emp-1.png" alt="">
			</li>
			<li>
				<img src="/public/assets/images/jobs-img/browse-emp-3.png" alt="">
			</li>
		</ul>
	</div>
	
</div>



@include("modals.login")