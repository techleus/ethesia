<style type="text/css">
	.search_container ul.fields_container{
		width: 100%;
	}
	.search_container ul.fields_container li{
		width: 50%;
	}
	.menu_right .login_btn{
		border: 1px solid #708cf3;
	}	
	.menu_right .signup_btn{
		background: -webkit-linear-gradient(to right, #804cf9 1%, #554cf9 100%);
		background: -moz-linear-gradient(to right, #804cf9 1%, #554cf9 100%);
		background: linear-gradient(to right, #804cf9 1%, #554cf9 100%);
		color: #fff !important;
	}


	/*dropdown tags*/


	.txt-search-dd-btn{
		padding-top: 10px;
		padding-right: 560px;
	}
	.txt-search-dd-btn[multiple] option{
		display: none;
	}
	
	#addedoption,.txt-search-dd-btn[multiple] option, .txt-search-dd-btn[multiple] option:focus, .txt-search-dd-btn[multiple] option:active{
		display: inline-block;
		z-index: 1;
		border: 1px solid #e8eff0;
		background: #e8eff0;
		padding: 5px 10px 5px 10px;
		border-radius: 25px;
		font-size: 13px;		
		font-family: "Open Sans", Sans-serif;
	}	
	.txt-search-dd-btn option:not(:last-child){
		margin-right: 10px;
	}
	#dropDownContent{
		display: none;
	}
	.active#dropDownContent{
		display: block;
		position: absolute;
		width: calc(100% - 230px);
		border: 1px solid #ebebeb;
		background:  #fff;
		z-index: 1;
		padding : 1px;
		margin-left : 48px;
		border-top : 0;        
	}
	.active#dropDownContent ul{
		padding: 2px;
	}
	.active#dropDownContent ul li{		
		width: 100%;
		padding: 5px;
	}
	.active#dropDownContent ul li input{
		border-radius: 0;
	}
	i.closeTaginput{
		position: absolute;
		top: 7px;
		right: 17px;
		font-weight: bold;
		font-size: 24px;
		cursor: pointer;		
	}
</style>
<div class="header_container">	
	<div class="menu_container" id="myTopnav">
		<div class="menu_left">
			<div class="logo_wrap">
			<a href="/" class="nav_logo"><img src="/public/assets/images/jobs-img/anesthesia-logo-blue.png"></a>
			<a href="javascript:void(0);" class="mobile_icon" onclick="myNavToggle()">&#9776;</a>			
			</div>
			<div class="menu_wrap">

				<ul class="topmenu">
					@verbatim
					<li class="current"><a href="/browse-jobs/{{jobsData.job_type}}" class="curr_browse">BROWSE JOBS</a></li>
					<li><a href="/browse-employers">BROWSE EMPLOYERS</a></li>	
					<li><a href="/contact-us">CONTACT</a></li>				
					@endverbatim								
				</ul>				
			</div>
			<div class="clearfix"></div>
		</div>

		<div class="menu_right">
			@if(!Auth::check())
				<a modal="loginModal" modal-message="Login to your dashboard." anesthesia-modal class="login_btn pointer">Log in</a> 
				<a show-slider="" slider-width="873px" id_target = "registration-form" class="signup_btn pointer">Sign Up</a>		
			@else
				<a href="/application" class="login_btn pointer">Dashboard</a> 
			@endif						
		</div>
		<div class="clearfix"></div>
	</div>
	
	
	<div class="search_container">
		@verbatim
		<div ng-cloak class="stitle">Browse <span ng-bind="jobsData.job_type" ></span> Jobs</div>
		@endverbatim
		<ul class="fields_container">			
			@verbatim
			<li style="position: relative;">
				<div class="label">Duration of Position</div>
				<select class="txt-search-dd-btn pointer" ng-model="jobsData.duration_position" multiple drop-down>
					<option value="California" selected>California</option>
					<option value="New York" selected>New York</option>					
				</select>
				<div drop-down-content id="dropDownContent">
					<ul>
						<li>
							<input type="text" class="form-control" ng-model="inputs" input-tags>
							<i class="closeTaginput" close-taginput>&times;</i>
						</li>
					</ul>
				</div>
				<!-- <input ng-blur="unfocusDur()" ng-model="jobsData.duration_position" ng-focus="job_dur_focus = true" type="text" class="txt-search-dd-btn pointer" name="" readonly  placeholder="Locum Tenens, PRN, etc.">
					
				<div class="search-dropdown" ng-cloak >
					<ul class="search-results-wrapper"  ng-show="job_dur_focus == true">
						<li ng-click="populateDurSearch('Full Time')" class="li-results" >Full Time</li>
						<li ng-click="populateDurSearch('Locum Tenens')" class="li-results" >Locum Tenens</li>
						<li ng-click="populateDurSearch('Part Time')" class="li-results" >Part Time</li>
						<li ng-click="populateDurSearch('PRN')" class="li-results" >PRN</li>
						<li ng-click="populateDurSearch('Fellowship')" class="li-results" >Fellowship</li>
					</ul>
				</div> -->

				<button ng-click="getJobs()" class="btn find_jobs_btn fontPoppins" id="find_jobs_btn">FIND JOBS</button>
				<div class="adv_search"><a class="pointer" ng-cloak show-slider="" id_target = "filter-jobs-form">Show advanced filters <img src="/public/assets/images/jobs-img/sort.png"></a></div>
			</li>			
			@endverbatim
			<!--@verbatim
			<li class="pointer">
				<div class="label">Duration of Position</div>
				<div class="fields"><input type="text" ng-model = "jobsData.duration_position"  class="txt-search-dd-btn" placeholder="Locum Tenens, PRN, etc."> <button ng-click="getJobs()" class="btn find_jobs_btn fontPoppins">FIND JOBS</button></div>
				<div class="adv_search"><a ng-cloak show-slider="{{$index}}" id_target = "filter-jobs-form">Show advanced filters <img src="/public/assets/images/jobs-img/icon-plus.png"></a></div>


				</li>
				@endverbatim-->
		</ul>
	</div>
</div>



@include("modals.login")