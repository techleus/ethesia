<style type="text/css">	
	div.page-container {
	    padding-top: 30px !important;
	}
	.row.page-list-content{
		padding-top: 50px;
	}
	.new-splash{
		margin-right: -1px;
    	margin-bottom: -1px;
	}
	.bnt-job{
		font-family: "Myriad Pro";
	}
	.card-box-new .pos-bottom{
		padding-top: 0px;
	}
	.title-text .small{
		padding-top: 10px;
		overflow: hidden;		
	}



	.search_container ul.fields_container{
		width: 100%;
	}
	.search_container ul.fields_container li{
		width: 50%;
	}
	.menu_right .login_btn{
		border: 1px solid #708cf3;
	}	
	.menu_right .signup_btn{
		background: -webkit-linear-gradient(to right, #804cf9 1%, #554cf9 100%);
		background: -moz-linear-gradient(to right, #804cf9 1%, #554cf9 100%);
		background: linear-gradient(to right, #804cf9 1%, #554cf9 100%);
		color: #fff !important;
	}


	/*dropdown tags*/


	.txt-search-dd-btn{
		padding-top: 10px;
		padding-right: 560px;
	}
	.txt-search-dd-btn[multiple] option{
		display: none;
	}
	
	#addedoption,.txt-search-dd-btn[multiple] option, .txt-search-dd-btn[multiple] option:focus, .txt-search-dd-btn[multiple] option:active{
		display: inline-block;
		z-index: 1;
		border: 1px solid #e8eff0;
		background: #e8eff0;
		padding: 5px 10px 5px 10px;
		border-radius: 25px;
		font-size: 13px;		
		font-family: "Open Sans", Sans-serif;
	}	
	.txt-search-dd-btn option:not(:last-child){
		margin-right: 10px;
	}
	#dropDownContent{
		display: none;
	}
	.active#dropDownContent{
		display: block;
		position: absolute;
		width: calc(100% - 230px);
		border: 1px solid #ebebeb;
		background:  #fff;
		z-index: 1;
		padding : 1px;
		margin-left : 48px;
		border-top : 0;        
	}
	.active#dropDownContent ul{
		padding: 2px;
	}
	.active#dropDownContent ul li{		
		width: 100%;
		padding: 5px;
	}
	.active#dropDownContent ul li input{
		border-radius: 0;
	}
	i.closeTaginput{
		position: absolute;
		top: 7px;
		right: 17px;
		font-weight: bold;
		font-size: 24px;
		cursor: pointer;		
	}
	@media only screen and (max-width: 900px){
		.search_container{
			padding-top: 70px;		
		}
		.search_container ul.fields_container li{
			width: 100% !important;
			margin: 0 auto;
		}
		.header_container .sticky_navbar + .search_container{
			padding-top: 70px;
		}
		.search_container div.stitle{
			color: #6684f2;
		}
		#myTopnav .menu_left .logo_wrap img{
			content:url("../images/jobs-img/anesthesia-logo-2.png");
		}
		div.page-container{
			padding-top: 80px !important;
		}		
	}

	@media only screen and (max-width: 500px){
		.list-left-content.fadeIn{			
		    opacity: 1;
		}
		.list-left-content.fadeOut{
			opacity: 0;
		}
	}
</style>

<style type="text/css">
	#list-page{
		min-height: 800px;
	}
	.list-left-content{
		padding-right: 0;
		margin-top: 50px;
	}
	.list-title{
		font-size: 22px;
		font-family: "Open Sans", Sans-serif;
		color: #000000;
		padding-right: 40%;	
		border-bottom: 1px solid #f8f8f8;
		padding-bottom: 15px;	
	}
	.card.card-job-list{
		border:0;
		box-shadow: none;		
	}
	.card-body-list{		
		border-bottom: 1px solid #ebebeb;		
	}
	.card-body-list div{
		float: left;		
	}
	.card-logo{
		padding: 5px;
		border-radius: 100%;
		box-shadow: 3px 4px 49px 0px rgba(0, 0, 0, 0.18); 
	}
	.card-logo img{
		border-radius: 100%;
		height: 100px;
		width: auto;
	}
	.card-body-list .detail-container{
		padding-left: 25px;		
		width: calc(100% - 130px);				
	}
	.detail-container .text{
		display: block;
		float: left;				
		width: 100%;
	}
	.detail-container .detail-title{
		font-family: "Open Sans", Sans-serif;
		font-weight: 700;
		color: #6684f2;
		font-size: 16px;
		padding-right: 100px;
	}
	.detail-container .detail-company{
		padding: 8px 0 8px 0;
		font-family: "Open Sans", Sans-serif;
		font-size: 14px;
		color: #262626;
	}
	.detail-container .detail-description{
		font-family: "Open Sans", Sans-serif;
		font-size: 14px;
		color: #7d7d7d;
	}
	.detail-container .detail-job-info{
		padding-top: 10px;
	}
	.location-detail{

	}
	.location-detail .location{
		font-family: "Open Sans", Sans-serif;
		font-size: 14px;
		color: #000;
	}
	.detail-job-info ul{		
		list-style: none;
		padding-left: 0;
	}
	.detail-job-info ul li{		
		height: 25px;
		padding-right: 30px;
		display: inline-block;
	}
	.location-detail .place{
		font-family: "Open Sans", Sans-serif;
		font-size: 14px;
		color: #6684f2;
	}
	.card-featured{
		background: #af71fd;		
		position: absolute;
		top: 1;
		right: 0;
		color: #fff;
		font-size: 11px;
		font-family: "Open Sans", Sans-serif;		
		text-align: right;
		padding: 3px 10px 3px 20px;		
		border-top-left-radius: 30px;
		border-bottom-left-radius: 30px;
		
		background: -webkit-linear-gradient(to right, rgba(85,76,249), rgba(128,76,249));
		background: -o-linear-gradient(to right, rgba(85,76,249), rgba(128,76,249));
		background: linear-gradient(to right, rgba(85,76,249), rgba(128,76,249));
	}
	.jobid{
		background: #af71fd;				
		color: #fff;
		font-size: 11px;
		font-family: "Open Sans", Sans-serif;		
		text-align: right;
		padding: 3px 10px 3px 20px;		
		border-top-left-radius: 30px;
		border-bottom-left-radius: 30px;
		margin-top: -50px;
		background: -webkit-linear-gradient(to right, rgba(85,76,249), rgba(128,76,249));
		background: -o-linear-gradient(to right, rgba(85,76,249), rgba(128,76,249));
		background: linear-gradient(to right, rgba(85,76,249), rgba(128,76,249));
	}
	.search-content{
		display: none;
		width: 0;
		-webkit-animation: hideSearchBar 0.5s;
		-animation: hideSearchBar 0.5s;
	}
	@-webkit-keyframes hideSearchBar {
		0%{
			width: 250px;			
		}	
		100%{
			width: 0px;
			display: none;			
		}

	}
	@keyframes hideSearchBar {
	  	0%{
			width: 250px;			
		}	
		100%{
			width: 0px;			
			display: none;
		}
	}
	.search-content.active{
		display: inline-block;
		float: left;
		border-left: 1px solid #ebebeb;
		width: 300px;
		padding: 0px;		
		position: absolute;
		top: 0;
		right: 3%;
		background: #fff;		
		-webkit-animation: showSearchBar 0.5s;
    	animation: showSearchBar 0.5s;    
	}
	@-webkit-keyframes showSearchBar {
		0%{
			width: 0px;
		}	
		100%{
			width: 300px;
			
		}

	}
	@keyframes showSearchBar {
	  	0%{
			width: 0px;
		}	
		100%{
			width: 300px;
			
		}
	}
	#left-content{
		width: 90% !important;
		margin: 0 auto;
		-webkit-animation: resizeBack 0.5s;
    	animation: resizeBack 0.5s;    
	}
	@-webkit-keyframes resizeBack{
		0%{
			padding-right: 240px;
		}
		100%{
			padding-right: 0;
		}
	}
	@keyframes resizeBack{
		0%{
			padding-right: 240px;
		}
		100%{
			padding-right: 0;
		}
	}	
	#left-content.resize{
		padding-right: 280px;
		-webkit-animation: resizeLeftContent 0.5s;
    	animation: resizeLeftContent 0.5s;   
	}
	@-webkit-keyframes resizeLeftContent{
		0%{
			padding-right: 0;
		}
		100%{
			padding-right: 280px;
		}
	}
	@keyframes resizeLeftContent{
		0%{
			padding-right: 0;
		}
		100%{
			padding-right: 280px;
		}
	}
	
	.search-content .search-title{
		font-family: "Open Sans", Sans-serif;
		color: #000;
		background: #e6eff1;		
		font-size: 14px;
		width: 100%;
		max-width: 240px;
		padding-top: 10px;
		padding-bottom: 10px;
		padding-left: 40px;
		border-top-right-radius: 15px;
		border-bottom-right-radius: 15px;
		background: url(/public/assets/images/jobs-img/sort.png) #e6eff1;
		background-position: left 18px top 15px;
		background-repeat: no-repeat;
		padding-left: 55px;
		text-overflow: ellipsis;
		white-space: nowrap;
    	text-overflow: ellipsis;
    	overflow: hidden;
		position: relative;
	}
	
	.circle-plus{
		padding: 3px 13px 3px 13px;
		background: #464646;
		text-align: center;
		color: #fff;
		border-radius: 100%;
		position: absolute;
		font-weight: bold;
		font-size: 22px;
		top: 0;
		right: 0;
	}
	.sort-content{
		margin-top: 30px;
		padding-left: 25px;
	}
	.sort-content .sort-title{
		font-family: "Open Sans", Sans-serif;
		color: #000;
		font-size: 16px;
	}
	.sort-options{
		padding-top: 25px;
	}
	.sort-options ul{
		padding:0;
	}
	.sort-options ul li{
		list-style: none;
		line-height: 40px;
		color: #7d7d7d;
		font-size: 14px;
		font-family: "Open Sans", Sans-serif;
	}
	.sort-options ul li.active{
		color: #6684f2;
		font-weight: 600;
	}
	.search-row{
		border-bottom: 1px solid #ebebeb;
	}
	.connect-title{
		font-family: "Myriad Pro";
		color: #000;
		font-size: 16px;
	}
	.connect-title.disable{
		font-size: 14px;
		color: #959595;
		margin-top: 20px;
	}
	.connect-options{
		padding-bottom: 120px;
	}
	.connect-options ul:first-child{
		padding: 50px 0 15px 15px;				
		width: 106px;
	}
	.connect-options ul li{
		list-style: none;		
		display: inline-block;
		width: 40px;		
		margin-left: -21px;
	}	
	.connect-options ul:first-child li .circle-employer{
		height: 35px;
		width: 35px;
		border-radius: 100%;
		background: #7d7d7d;
		border: 2px solid #fff;
	}
	.connect-options ul:not(:first-child){
		float: left;
		min-width: 126px;		
		width: auto;		
		padding: 50px 0 0 25px;
	}
	.connect-options ul:not(:first-child) li{
		padding: 0;
		display: block;
		font-size: 14px;
		font-family: "Myriad Pro";
	    white-space: nowrap;
	    text-overflow: ellipsis;
	    display: block;
	    overflow: hidden;
	    width: 100%;
	}
	.connect-options ul{
		float: left;
	}
	.btn-add-conn{
		background: #544cf9;
		color: #fff;
		font-family: "Myriad Pro";
		margin-top: 15px;
		margin-bottom: 15px;		
		padding: 10px 35px 10px 35px;
		border-radius: 35px !important;
	}
	.connect-title .toogle{
		float: right;
		cursor: pointer;
	}
	.connect-title .toogle ul{
		float: right;
		cursor: pointer;
	}
	.connect-title .toogle ul li{
		display: inline-block;
		list-style: none;
		height: 8px;
		width: 8px;
		background: #c2c2c2;
		border-radius: 100%;		
		cursor: pointer;
	}
	.people-content{		
		width: 100%;						
		padding-top: 5px;
		padding-bottom: 5px;
	}
	.people-content:last-child{
		margin-bottom: 40px;
	}	
	.people-content .circle-people{					
		width: 40px;
		height: 40px;
		background: #e8eff0;
		border-radius: 100%;		
		text-align: center;
		line-height: 40px;
		cursor: pointer;
	}
	.people-content .circle-people.active{
		background: #544cf9;
		color: #fff;
		font-family: "Open Sans";
		font-size: 13px;
	}
	.people-content .circle-people:not(:first-child){
		

	}
	#show-search{
		position: absolute;
		top: 40px;
		right: 15px;		
		font-family: "Open Sans", Sans-serif;
		color: #000;
		background: #e6eff1;		
		font-size: 14px;
		width: 100%;
		max-width: 240px;
		padding-top: 10px;
		padding-bottom: 10px;
		padding-left: 50px;
		padding-right: 30px;
		background: url(/public/assets/images/jobs-img/sort.png) #e6eff1;
		background-position:right 18px top 15px !important;
		border-top-left-radius: 15px;
		border-bottom-left-radius: 15px;		
		background-position: left 25px top 15px;
		background-repeat: no-repeat;		
		cursor: pointer;
	}
	.hide#show-search{
		-webkit-animation: hideBtnShow 0.5s;
    	animation: hideBtnShow 0.5s;   
	}
	@-webkit-keyframes hideBtnShow{
		0%{
			width: 100%;
		}
		99%{
			width: 0%;
		}
		100%{
			display: none;
		}
	}
	@keyframes hideBtnShow{
		0%{
			width: 100%;
		}
		99%{
			width: 0%;
		}
		100%{
			display: none;
		}
	}
	#hide-search{
		cursor: pointer;
	}
	#show-search .circle-plus{
		padding: 3px 13px 3px 13px;
		background: #464646;
		text-align: center;
		color: #fff;
		border-radius: 100%;
		width: 40px;
		height: 40px;
		position: absolute;
		font-weight: bold;
		font-size: 22px;
		top: 0;
		left: 0;
	}
	.content-switch{
		position: relative;
	}
	.first-row{
		margin-bottom: 50px;
	}
	.switches{
		float: right;
		margin-top: -50px;
		height: 35px;
		width: 45px;		
		line-height: 35px;		
	}
	
	.animate-hover {
	  position: relative;
	  -webkit-appearance: none;
	     -moz-appearance: none;
	          appearance: none;

		padding: 1em 2em;
		border: none;
		color: white;
		font-size: 1.2em;
		cursor: pointer;
		outline: none;
		overflow: hidden;
		border-radius: 100px;
		line-height: 10px;	          
	  	 
	}
	.animate-hover span {
	  position: relative;
	  pointer-events: none;                    
	}
	
	.animate-hover::before {
	  --size: 0;
	  content: '';
	  position: absolute;
	  left: var(--x);
	  top: var(--y);
	  width: var(--size);
	  height: var(--size);
	  background: radial-gradient(circle closest-side, #58D1FA, transparent) !important;
	  -webkit-transform: translate(-50%, -50%);
	          transform: translate(-50%, -50%);
	  transition: width .2s ease, height .2s ease;
	}
	.animate-hover:hover::before {
	  --size: 400px;
	}	
</style>
@extends('landing.index')


@section('content')
<input type="hidden" id="jobsData" ng-model = "jobsData" value="{{json_encode(Session::get('jobsData'))}}" name="">
<div class="page-container"><!--page-container-->
	<div class="row">
		<div class="col-md-8 offset-md-2">
			@verbatim
			<div class="list-title">Showing {{ jobs.length - 1 }} of {{ totalJob - 1 }} {{jobsData.job_type}} Jobs</div>
			@endverbatim
			<div class="switches">
				<img ng-click="switchView('list')" class="pointer" src="/public/assets/images/jobs-img/list-view.png" alt="">
				<img ng-click="switchView('cards')" class="pointer" src="/public/assets/images/jobs-img/card-view.png" alt="">
			</div>
		</div>
	</div>
	<div class="row page-list-content"  ng-cloak ng-show="viewSwitch == 'cards'">
			


		<!-- column -->		
		@verbatim	
		<div id="card-container">			
			<div ng-cloak show-slider="{{$index}}" data-value="{{ job.id }}" data-id="{{ job.jobTitle }}" id_target = "slider-form" class="set-appform-jobid" ng-repeat = "job in jobs track by $index" ng-show="!isLoading" ng-if="job.id != undefined">
			@endverbatim
				
				<!-- Card -->
				<div class="card-job-box card-col-margins" style="float: left; width : 330px; margin-left: 10px; margin-right: 10px;">								
					<div class="card-pos-container">					  
					    <div class="cardnew-wrap">
					    	@verbatim
							<div class="card-new-logo"><img ng-src="{{job.image_path}}"></div>
							@endverbatim
							<div class="card-new-icons">							

								<button type="button" class="btn job-icon-btn active"><img src="/public/assets/images/jobs-img/btn-view.png"></button>
								@if(Auth::check())

									@if(Auth::User()->userType==1)

										<button  type="button" ng-click="bookmark(1,job.id,job.jobTitle, job);$event.stopPropagation();" ng-class="{'job-icon-red' : job.bookmark != null,'job-icon-btn' : job.bookmark.length == 0}" class="btn  no-slide active">
											<i class="fa fa-heart"></i>
										</button>

									@else
										<button type="button" ng-click="$event.stopPropagation();" modal="loginModal" anesthesia-modal modal-message="You must sign in to bookmark this job." class="btn job-icon-btn no-slide active">
											<i class="fa fa-heart"></i>
										</button>
									@endif
								@else
									<button type="button" modal="loginModal" anesthesia-modal modal-message="You must sign in to bookmark this job." class="btn job-icon-btn no-slide active">
										<i class="fa fa-heart"></i>
									</button>
								@endif

								<button type="button" class="btn job-icon-btn active no-slide" modal="shareSocial" modal-message="Available to share on" ng-click="assignSocialJobData(job)" anesthesia-modal><img src="/public/assets/images/jobs-img/btn-share.png">
								</button>

							</div>
							
							<div class="card-box-new">												
								<div class="job-company-name" ng-bind="job.company_name"></div>
								<div class="job-title">
									@verbatim
									<div class="title-text">
										<span class="text">
											{{ job.jobTitle }}
										</span>											
										<small style="line-height: 35px;vertical-align: top;">&nbsp;</small>
										<br>
										<span class="small" text-ellipsis>
											<span ng-bind="pureText(job.ei_jobDescription)"></span>
										</span>										
									</div>
									@endverbatim
								</div>											
								<div class="pos-bottom">
									<div class="sAddress">
										<div><span ng-bind="job.ei_facilityCity | capfirst"></span>, <span ng-bind="job.ei_facilityState | capfirst"></span></div>
										<small ng-bind="job.time_ago"></small>
									</div>									
															
									
									<button type="button" class="btn bnt-job text-uppercase animate-hover btn-rounded active" animate-hover>
										<span ng-bind="job.ei_durPosition"></span>
									</button>
									<button type="button" class="btn bnt-job text-uppercase animate-hover btn-rounded active" animate-hover>
										<span ng-bind="('#')+(job.id)"></span>
									</button>			
								</div>			
								
							</div>
							<div class="new-splash" ng-if="job.priorityLevel != null"><img src="/public/assets/images/jobs-img/splash5.png"></div>
						</div>																							
					</div>	
					

				</div>
				<!-- Card -->
			</div>		
		</div>	

		<div ng-cloak class="col-lg-3 col-md-6 col-lg-job" ng-repeat = "loading in fake_loading" ng-show="isLoading || loading_additional">
			<div class="card-job-box">
				<div class="card-pos-container">
					<div class="ph-item" style="height: 440px;border: none">
					    <div class="ph-col-12" style="margin-top: 40px">
					        <div class="ph-row">
					            <div class="ph-col-12"></div>
					            <div class="ph-col-8"></div>
					            <div class="ph-col-8"></div>
					        </div>

					        <div class="ph-row" style="margin-top: 87px">
					            <div class="ph-col-12"></div>
					            <div class="ph-col-8" style="margin-top: 23px"></div>
					            <div class="ph-col-8"></div>
					            <div class="ph-col-8"></div>
					            <div class="ph-col-8"></div>
					        </div>

					        <div class="ph-row" style="margin-top: 15px">
					            <div class="ph-col-12"></div>
					            <div class="ph-col-8"></div>
					            <div class="ph-col-8"></div>
					        </div>
					    </div>
					</div>
																			
				</div>																							
			</div>
		</div>


	</div>
	
	<div class="row" ng-cloak ng-show="viewSwitch == 'list'">
		<div class="col-md-12 col-lg-8 offset-lg-2">
			<div class="row" id="list-page">
				<div id="left-content">
					<div class="row">						
						<div class="col-md-12" style="padding-bottom: 80px;">							
							<div show-search target="search-content" id="show-search">
								<div class="circle-plus">+</div> <span>Show Custom Search</span>
							</div>
						</div>
						<div class="col-md-12 list-left-content">

							@verbatim

							<div class="card card-job-list" show-slider="{{$index}}" data-value="{{ job.id }}" data-id="{{ job.jobTitle }}" id_target = "slider-form" class="set-appform-jobid" ng-repeat = "job in jobs | orderBy:propertyName:reverse track by $index " ng-show="!isLoading"  ng-if="job.id != undefined">
							@endverbatim
							  	<div class="card-body card-body-list">						
							  		@verbatim	  		
							  		<div class="card-logo"><img ng-src="{{job.image_path}}"></div>
							  		@endverbatim
							  		<div class="detail-container">
							  			
									<div class="card-featured" ng-if="job.priorityLevel != null">FEATURED</div>									

							  			@verbatim	
							  			<div class="text detail-title">
							  				{{ job.jobTitle }}
							  			</div>						
							  			@endverbatim		  			
							  			<br>
							  			<div class="text detail-company">
							  				<div class="text-location">
							  					<i class="mdi mdi-map-marker" style="color: #6684f2;"></i>
						  						<span class="location">Location:
						  							<span ng-bind="job.ei_facilityCity | capfirst"></span>&nbsp;, 
							  						<span ng-bind="job.ei_facilityState | capfirst"></span>&nbsp; .
							  					</span>						  						
							  				</div>
							  				<div class="text-company">
							  					&nbsp;Posted by: <span ng-bind="job.company_name"></span>
							  				</div>
							  			</div>
							  			<br>
							  			<div class="text detail-description" text-ellipsis>
							  				<span ng-bind="pureText(job.ei_jobDescription) | limitTo : 250 "></span>
							  			</div>
							  			<div class="text detail-job-info">							  				
							  				<ul>							  					
							  					<li>
							  						<div class="location-detail">
									  					<i class="fa fa-calendar" style="color: #6684f2;"></i>
									  					<span class="location">Post Date: </span><span class="place" ng-bind="job.time_ago"></span>
									  				</div>
							  					</li>
							  					<li class="hidden">
							  						<div class="location-detail">
									  					<i class="fa money-bill-alt mdi-currency-usd" style="color: #6684f2;"></i>
									  					<span class="location">Salary: </span><span class="place">$280,000 - $320,000</span>
									  				</div>
							  					</li>
							  					<li>
							  						<div class="location-detail">
									  					<i class="fa fa-clock-o" style="color: #6684f2;"></i>
									  					<span class="location">Duration: </span><span class="place" ng-bind="job.ei_durPosition"></span>
									  				</div>
							  					</li>							  					
							  				</ul>							  				
							  			</div>										
							  		</div>
							  		<div class="jobid float-right">#3920</div>
							  	</div>
							</div>	
												
						</div>
					</div>					
				</div>
				<div class="search-content">
					<div class="search-row" style="padding-top: 40px;">
						<div class="search-title" hide-search target="search-content" id="hide-search">
							Hide Custom search
							<div class="circle-plus">+</div>
						</div>
						<div class="sort-content">
							<div class="sort-title">Sort these jobs by:</div>
							<div class="sort-options">
								<ul>
									<li class="pointer" ng-class="{ 'active' : propertyName == 'time_ago'}" ng-click="sortBy('time_ago')">Date posted</li>
									<li class="pointer" ng-class="{ 'active' : propertyName == 'ei_facilityCity'}" ng-click="sortBy('ei_facilityCity')">By location</li>
									<li class="pointer" ng-class="{ 'active' : propertyName == 'jobTitle'}" ng-click="sortBy('jobTitle')">By Job Title</li>									
									<li class="pointer" ng-class="{ 'active' : propertyName == 'company_name'}" ng-click="sortBy('company_name')">By Company</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="search-row">
						<div class="sort-content">
							<div class="connect-title">Connect with Employers</div>
							<div class="connect-options">
								<ul>									
									<li><div class="circle-employer"></div></li>
									<li><div class="circle-employer"></div></li>
									<li><div class="circle-employer"></div></li>
									<li><div class="circle-employer"></div></li>
								</ul>
								<ul>
									<li>163 Connections</li>
									<li>290 Followers </li>
									<li>36 Following</li>
								</ul>
							</div>
							<div class="connect-title disable">To see more Employers</div>
							<button type="button" class="btn btn-add-conn hover-animate" style="font-family: 'Open Sans', sans-serif; font-size: 14px;" animate-hover>Add Connections</button>
						</div>						
					</div>
					<div class="search-row">
						<div class="sort-content">
							<div class="connect-title">
								People you may know
								<div class="toogle">
									<ul>
										<li></li>
										<li></li>
										<li></li>
									</ul>
								</div>
							</div>
							<div class="row people-content">
								<div class="col-md-3">
									<div class="circle-people"></div>
								</div>
								<div class="col-md-3">
									<div class="circle-people"></div>
								</div>
								<div class="col-md-3">
									<div class="circle-people"></div>
								</div>
								<div class="col-md-3">
									<div class="circle-people"></div>
								</div>
							</div>
							<div class="row people-content">
								<div class="col-md-3">
									<div class="circle-people"></div>
								</div>
								<div class="col-md-3">
									<div class="circle-people"></div>
								</div>
								<div class="col-md-3">
									<div class="circle-people"></div>
								</div>
								<div class="col-md-3">
									<div class="circle-people active">+12</div>
								</div>
							</div>
						</div>						
					</div>
				</div>					
			</div>
		</div>
	</div>	

	
	<div class="col-12" ng-cloak ng-show="!isLoading &&  !nothing_toload">
		<div  class="form-group anesthesia-btn text-center">
			<button class="btn animate-hover" animate-hover ng-click = "loadMoreJobs();" style="width: 160px;padding:17px">
				<span>Load More</span>
			</button>
		</div>

	</div>

	<div class="col-12" ng-show="jobs.length == 0">
		@verbatim
		<div ng-cloak class="form-group  text-center" >
			No Jobs Found
		</div>
		@endverbatim
	</div>
</div>


<div class="slider-form-overlay hidden"></div>


@include('slider.job_preview')
@include('slider.apply_slider_form')
@include('slider.registration_slider')
@include('slider.filter-jobs-form')
@include('modals.shareSocial')


@endsection