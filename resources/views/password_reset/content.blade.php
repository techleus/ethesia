
@extends('password_reset.index')


@section('content')

<input type="hidden" id="verification_code" value="{{$verification_code}}" ng-model="passwordResetData.verification_code">

<div class="page-container"><!--page-container-->
	<div class="row">

		<div class="col-md-12">
			<form name="passwordResetForm" novalidate="true">
			<div class="col-md-6 offset-md-3">
				<div class="form-horizontal" class="col-md-12">
					<div class="form-group text-center">
						<label>Update Your Password</label>
					</div>

					<span class="help-success" ng-show = "successMessage.length > 0" ng-bind="successMessage"></span>

					<span class="help-error" ng-show = "errorMessage.length > 0" ng-bind="errorMessage"></span>

					<div class="form-group">
						<label>New Password</label>
						<input ng-minlength="5"  ng-required="true" name="password" type="password" ng-model="passwordResetData.password" class="form-control">
						<div class="form-control-feedback" ng-show = "passwordResetForm.password.$invalid && passwordResetForm.password.$dirty">Password is Required or atleast 5 Characters.</div>
					</div>
					<div class="form-group">
						<label>Re-type Password</label>
						<input ng-minlength="5"  ng-required="true" name="confirm_password" type="password" ng-model="passwordResetData.confirm_password" class="form-control">

						<div class="form-control-feedback" ng-show = "passwordResetData.password !=  passwordResetData.confirm_password && (passwordResetForm.confirm_password.$invalid && passwordResetForm.confirm_password.$dirty)">Password did not match</div>


					</div>
					<button class="btn btn-primary text-white form-control update-password" form-name="passwordResetForm" loading-button callback="submitNewPassword()" type="button">Update Password</button>
				</div>
			</div>
			</form>																	
		</div>
	</div>
</div>



@endsection