<style type="text/css">
	.update-password{
		border-radius: 25px;
		background: rgb(86,76,249); /* Old browsers */
		background: -moz-linear-gradient(to right, rgba(86,76,249,1) 1%, rgba(128,76,249,1) 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(to right, rgba(86,76,249,1) 1%,rgba(128,76,249,1) 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to right, rgba(86,76,249,1) 1%,rgba(128,76,249,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		height: 45px;
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#564cf9', endColorstr='#804cf9',GradientType=0 ); 
	}
	.update-password:hover,.update-password:focus,.update-password:active{
		background: rgb(128,76,249); /* Old browsers */
		background: -moz-linear-gradient(top, rgba(128,76,249,1) 0%, rgba(128,76,249,1) 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, rgba(128,76,249,1) 0%,rgba(128,76,249,1) 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, rgba(128,76,249,1) 0%,rgba(128,76,249,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#804cf9', endColorstr='#804cf9',GradientType=0 ); /* IE6-9 */
	}
	.header_container, .sticky_navbar{
		border:0;
		height: 70px !important;
		padding-bottom: 5px;
		background: -moz-linear-gradient(to right, #bc90f9 0%,#6e6af8 49%,#6e6af8 51%,#57d3fa 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(to right, #bc90f9 0%,#6e6af8 49%,#6e6af8 51%,#57d3fa 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to right, #bc90f9 0%,#6e6af8 49%,#6e6af8 51%,#57d3fa 100%);
	}
	.menu_left ul.topmenu li a.curr_browse{
		color: #fff !important;
	}
	.menu_left ul.topmenu li a{
		color: #fff !important;
	}
	a:not([href]):not([tabindex]){
		color: #fff !important;	
	}
	.menu_right .login_btn{
		border: 1px solid #fff
	}
	.page-container{
		background-color: #f2f2f2; 
	}	
	.page-container .row{
		padding-top: 50px;
		padding-bottom: 50px;		
	}
	.form-horizontal{
		background: #fff;
		padding: 80px;
		-webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.20);
		-moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.20);
		box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.20);

	}

</style>
<div class="header_container" style="">	
	<div class="menu_container" id="myTopnav">
		<div class="menu_left">
			<div class="logo_wrap">
			<a href="/" class="nav_logo"><img src="/public/assets/images/jobs-img/anesthesia-logo-2.png"></a>
			<a href="javascript:void(0);" class="mobile_icon" onclick="myNavToggle()">&#9776;</a>			
			</div>
			<div class="menu_wrap">

				<ul class="topmenu">
					@verbatim
					<li class="current"><a href="/browse-jobs/{{jobsData.job_type}}" class="curr_browse">BROWSE JOBS</a></li>
					<!--<li><a href="#">BROWSE RESUMES</a></li>-->
					<li><a href="/browse-employers">BROWSE EMPLOYERS</a></li>	
					<li><a href="/contact-us">CONTACT</a></li>				
					@endverbatim								
				</ul>				
			</div>
			<div class="clearfix"></div>
		</div>

		<div class="menu_right">
			@if(!Auth::check())
				<a modal="loginModal" modal-message="Login to your dashboard." anesthesia-modal class="login_btn pointer">Log in</a> 
				<a show-slider="" slider-width="873px" id_target = "registration-form" class="signup_btn pointer">Sign Up</a>		
			@else
				<a href="/application" class="login_btn pointer">Dashboard</a> 
			@endif						
		</div>
		<div class="clearfix"></div>
	</div>
	
</div>



@include("modals.login")