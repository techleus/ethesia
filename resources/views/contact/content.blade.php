@extends('contact.index')

@section('content')
<style type="text/css">	
	.hover-animate {
	  position: relative;
	  -webkit-appearance: none;
	     -moz-appearance: none;
	          appearance: none;
	  
	  padding: 1em 2em;
	  border: none;
	  color: white;
	  font-size: 1.2em;
	  cursor: pointer;
	  outline: none;
	  overflow: hidden;
	  border-radius: 100px;
	  line-height: 10px;
	}
	.hover-animate span {
	  position: relative;
	  pointer-events: none;                    
	}
	.hover-animate::before {
	  --size: 0;
	  content: '';
	  position: absolute;
	  left: var(--x);
	  top: var(--y);
	  width: var(--size);
	  height: var(--size);	  
	  background: radial-gradient(circle closest-side, #58D1FA, transparent);
	  -webkit-transform: translate(-50%, -50%);
	          transform: translate(-50%, -50%);
	  transition: width .2s ease, height .2s ease;
	}
	.hover-animate:hover::before {
	  --size: 400px;
	}
	
</style>
<main id="pagewrap" class="animated fadeIn">
	<div class="contact-container">
		<div class="row" id="phase-1">
			<div class="col-md-1">
				<hr>
			</div>			
			<div class="col-md-10 offset-md-1" id="first-row" bounce-right>
				<span class="contact-us-label">CONTACT US</span>
				<p>We're always here to help you</p>
				<span class="contact-us-desc">
					Browse our directory of service providers, or <a href="#">post a request</a> and have providers
					respond to you directly. If you'd like to offer your own services, <a href="#">sign up today</a>.
				</span>
			</div>
			<div class="col-md-10 offset-md-2" id="second-row">
				<div class="row">
					<div class="col-md-4 phone" id="detail-container" bounce-left>
						<div class="header-title">Phone</div>
						<span>(888) 462-2713</span>
						<!-- <span>(+032) 3456 7890</span>
						<span>(+032) 3427 7670</span> -->						
					</div>
					<div class="col-md-4 address" id="detail-container" bounce-in>
						<div class="header-title">Address</div>
						<span>4882 Niagara Avenue</span>
						<span>San Diego, California, 92107</span>
					</div>
					<div class="col-md-4 email" id="detail-container" bounce-right>
						<div class="header-title">Email</div>
						<span>support@ethesia.com</span>
						<span>Info@ethesia.com</span>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="phase-2">
			<div class="col-md-1">
				<hr>
			</div>			
			<div class="col-md-10 offset-md-1 get-intouch" id="first-row" bounce-left row="second">
				<span class="contact-us-label">SEND US A MESSAGE</span>
				<p>Get in touch with us</p>
				<span class="contact-us-desc">
					Duis lacus urna, condimentum a vehicula a, hendrerit ac nisi Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
				</span>
			</div>			
			<div class="col-md-10 offset-md-2" id="second-row" bounce-left row="third">
				<div class="row">
					<div class="col-md-4 phone">
						<div class="form-group">
							<input type="text" placeholder="Enter your name" class="form-control contact-detail" ng-model="contact.name">
						</div>
					</div>
					<div class="col-md-4 address">
						<div class="form-group">
							<input type="text" placeholder="Your Email" class="form-control contact-detail" ng-model="contact.email">
						</div>
					</div>
					<div class="col-md-4 email">
						<div class="form-group">
							<input type="text" placeholder="Subject" class="form-control contact-detail" ng-model="contact.subject">
						</div>
					</div>
					<div class="col-md-12 message">
						<div class="form-group">
							<textarea placeholder="Your message here" rows="10" class="form-control contact-detail" ng-model="contact.message"></textarea>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group hover-body">
							<button class="btn btn-send hover-animate" animate-hover ng-click="contactDev()">
								<span>Send</span>
							</button>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<div class="slider-form-overlay hidden"></div>
@include("modals.login")
@include('slider.registration_slider')
@include('slider.filter-jobs-form')
@endsection