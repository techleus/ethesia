<div class="page-footer-wrap">
	<div class="footerwrapper">			
		<div class="row">
		  <div class="col-md-6 footer-logo"><img src="/application/assets/images/footer-logo.png"></div>
		  <div class="col-md-3 num-text"><span>2,342,233</span> COMMUNITY MEMBERS</div>
		  <div class="col-md-3 num-text"><span>15,342,216</span> TOTAL JOBS POSTED</div>
		</div> 
		<div class="clearFix">&nbsp;</div>
		<div class="clearFix">&nbsp;</div>
		<div class="row">
		  <div class="col-md-4 col-footer">
			<h2>Anesthesia Community</h2>
			<div class="marginTop10">
				<ul class="footerlinks">
					<li><a href="#">About Us</a></li>
					<li><a href="#">How it Works</a></li>
					<li><a href="#">Our Services</a></li>
					<li><a href="#">Blog</a></li>
					<li><a href="#">Contact Us</a></li>										
				</ul>
			</div>				
		  </div>		      
		  <div class="col-md-3 col-footer">
			<h2>Anesthesia Community Tools</h2>
			<div class="marginTop10">
				<ul class="footerlinks">
					<li><a href="#">My Account</a></li>
					<li class="current"><a href="#">My Resumes</a></li>
					<li><a href="#">My Jobs</a></li>
					<li><a href="#">Employers’ Messages</a></li>						
				</ul>
			</div>
		  </div>
		  <div class="col-md-5 col-footer-last">
			<h2>Email Newsletters</h2>
			<h3>Keep me up to date with content, updates, and offers from Anesthesia Community</h3>
			<div class="input-group marginTop20">
				<input class="form-control" placeholder="Email address" type="text">
				<span class="input-group-btn">
					<button class="btn btn-info" type="button">SUBSCRIBE</button>
				</span>
			</div>				
		  </div>
		</div>
		<div class="clearFix">&nbsp;</div>
		<div class="clearFix">&nbsp;</div>
		
		<div class="page-footer-wrap posrelative">
			<div class="clearFix">&nbsp;</div>
			
			<div class="flinksbtm-wrap">
				<ul class="footerlinksbtm">
					<li><a show-policy href="{{ url('/policy') }}">Privacy Policy</a></li>
					<li><a show-terms href="{{ url('/terms-of-use') }}">Terms of Use</a></li>					
					<li><a href="#">Help</a></li>
					<li><a href="#">Anesthesia Community Licenses</a></li>
					<li><a href="#">Partners</a></li>
				</ul>
			
			</div>
			<div class="language-wrap">												
				<div class="btn-group">
					<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						LANGUAGE: <b>ENGLISH</b>
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item small" href="javascript:;">English</a>
						<a class="dropdown-item small" href="javascript:;">Spanish</a>
						<a class="dropdown-item small" href="javascript:;">Swidesh</a>								
					</div>
				</div>
				<div class="marginTop30 socialiconswrap">
					<button class="btn btn-facebook waves-effect btn-circle waves-light btn-sm" type="button"> <i class="fa fa-facebook"></i> </button> &nbsp; 
					<button class="btn btn-twitter waves-effect btn-circle waves-light btn-sm" type="button"> <i class="fa fa-twitter"></i> </button> &nbsp; 
					<button class="btn btn-googleplus waves-effect btn-circle waves-light btn-sm" type="button"> <i class="fa fa-google-plus"></i> </button> &nbsp; 							
					<button class="btn btn-instagram waves-effect btn-circle waves-light btn-sm" type="button"> <i class="fa fa-instagram"></i> </button>
				</div>						
			</div>
			<div class="clearFix"></div>
			
		</div>
		
	</div><!--end-footerwrapper-->												
	<div class="clearFix">&nbsp;</div>
	<div class="clearFix">&nbsp;</div>
</div> 