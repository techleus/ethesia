<style type="text/css">
	.header_container{
		height: 450px !important;
		background: url(/public/assets/images/jobs-img/emp-divider-0.png) no-repeat left bottom -20px,url(/public/assets/images/jobs-img/bg-dots.png) no-repeat left top,-moz-linear-gradient(to right, #bc90f9 0%, #6e6af8 49%, #6e6af8 51%, #57d3fa 100%); /* FF3.6-15 */
		background: url(/public/assets/images/jobs-img/emp-divider-0.png) no-repeat left bottom -20px, url(/public/assets/images/jobs-img/bg-dots.png) no-repeat left top,-webkit-linear-gradient(to right, #bc90f9 0%,#6e6af8 49%,#6e6af8 51%,#57d3fa 100%); /* Chrome10-25,Safari5.1-6 */
		background: url(/public/assets/images/jobs-img/emp-divider-0.png) no-repeat left bottom -20px, url(/public/assets/images/jobs-img/bg-dots.png) no-repeat left top, linear-gradient(to right, #bc90f9 0%,#6e6af8 49%,#6e6af8 51%,#57d3fa 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */		
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#bc90f9', endColorstr='#57d3fa',GradientType=0 );
		border-bottom: 20px solid #fff;
		background-size: contain, cover;	
	}
	.search_container{
		background: transparent !important;
	}
	.sticky_navbar{
	    background: -webkit-linear-gradient(left, #bc90f9 0%, #6e6af8 49%, #6e6af8 51%, #57d3fa 100%);
	    background: -o-linear-gradient(left, #bc90f9 0%, #6e6af8 49%, #6e6af8 51%, #57d3fa 100%);
	    background: linear-gradient(to right, #bc90f9 0%, #6e6af8 49%, #6e6af8 51%, #57d3fa 100%);
	}
	.menu_left ul.topmenu li a, .menu_left ul.topmenu li a.curr_browse{
		color: #fff !important;
	}
	.menu_right .login_btn{
		border: 1px solid #fff;
		color: #fff !important;
	}
	.menu_right .signup_btn{
		color: #000 !important;
		background: #fff;
		float: right;

	}
	.search_container .stitle{
		color: #fff !important;
	}
	
	@media only screen and (min-width: 2100px){
		.header_container{
			background-size: cover, cover;	
		}
	}
</style>
<div class="header_container">	
	

	<div class="menu_container" id="myTopnav">
		<div class="menu_left">
			<div class="logo_wrap">
			<a href="/" class="nav_logo"><img src="/public/assets/images/jobs-img/anesthesia-logo-2.png"></a>
			<a href="javascript:void(0);" class="mobile_icon" onclick="myNavToggle()">&#9776;</a>			
			</div>
			<div class="menu_wrap">
				<ul class="topmenu">
					<li><a href="/browse-jobs/{{Session::get('type')}}">BROWSE JOBS</a></li>
					<!--<li><a href="#">BROWSE RESUMES</a></li>-->					
					<li>
						@if(Auth::check())
							@if(Auth::User()->userType != 2)
								<a class="curr_post" href="javascript:;" modal="loginModal" anesthesia-modal modal-message="Please sign in as a Provider.">POST A JOB</a>
							@else
								<a class="curr_post" href="/application/#/jobs_list">POST A JOB</a>
							@endif
						@else
							<a class="curr_post" href="javascript:;" modal="loginModal" anesthesia-modal modal-message="Please sign in as a Provider.">POST A JOB</a>
						@endif	
					</li>
					<li class="current"><a href="/contact-us" class="curr_browse">CONTACT</a></li>											
				</ul>					
			</div>
			<div class="clearfix"></div>
		</div>

		<div class="menu_right">
			@if(!Auth::check())
				<a modal="loginModal" modal-message="Login to your dashboard." anesthesia-modal class="login_btn pointer">Log in</a> 
				<a href="#" class="signup_btn pointer hover-animate text-center" style="line-height: 18px;" animate-hover>
					<span>Sign Up</span>
				</a>		
			@else
				<a href="/application" class="login_btn pointer">Dashboard</a> 
			@endif						
		</div>
		<div class="clearfix"></div>
	</div>
	
	
	<div class="search_container">
		<div class="stitle">Contact Athena</div>
		<p>Where the world meets startups. Millions of small businesses use<br>Freelancer to turn their ideas into reality</p>			
	</div>

</div>

