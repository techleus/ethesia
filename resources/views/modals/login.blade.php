<div id="loginModal" class="modal" ng-controller="LoginController"> 
<div class="modal-content"> 
	<form name="loginForm" no-validate>	
	<span class="loginModalClose">&times;</span> 
	<h1>Please Sign In</h1> 

	<p class="modal-message">Sign in here to bookmark this job.</p> 

		<span class="help-success" ng-show = "loginSuccess">Authentication is a success. Redirecting...</span>

		<span class="help-error" ng-show = "!loginSuccess" ng-bind="loginError"></span>

	<!-- 	<span class="help-success" ng-show = "!confirmedEmail" >Didn't receive a verification email? <a ng-click="sendConfirmationEmail()"> Send it again.</a></span> -->

		<div class="" ng-class="{'has-danger' : (loginForm.email.$invalid && loginForm.email.$dirty) == true}">
			<input type="text" ng-model = "loginData.email" name="email" ng-pattern = "/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" ng-required="true" class="form-control form-control-lg" id="loginEmail" placeholder="Email Address">

			<div class="form-control-feedback" ng-show = "loginForm.email.$invalid && loginForm.email.$dirty">Email Address is Required or Invalid.</div>
		</div>

		<div class="" ng-class="{'has-danger' : (loginForm.password.$invalid && loginForm.password.$dirty) == true}" style="margin-top: 15px">
			<input type="password" ng-model = "loginData.password" name="password" ng-minlength="5"  ng-required="true"  class="form-control form-control-lg" id="loginPassword" placeholder="Password">

			<div class="form-control-feedback" ng-show = "loginForm.password.$invalid && loginForm.password.$dirty">Password is Required or atleast 5 Characters.</div>
		</div>

		<div class="row">				
			<div class="col">
				@verbatim
				<input type="checkbox" id="remember_user" class="" ng-model="rememberme">
			    <label for="remember_user" id="forremember_user">Remember me</label> 			
			    @endverbatim
		    </div>			    
		    <div class="col">
			    <a modal="forgotPasswordModal" anesthesia-modal modal-message="" class="float-right" id="forgot_pass">Forgot password?</a> 
		    </div>
	    </div>			    
		<br/><button style="width: 100%" class="btn btn-lg" form-name="loginForm" loading-button callback="submitLogin()">Login</button>
		<br/><p class="label-signup">Dont have an account?<a  show-slider="" slider-width="873px" id_target = "registration-form" class="pointer" close-modal> Sign up now</a></p>
	</form>
</div>			
</div>


@include('modals.forgot_password')
