<div id="forgotPasswordModal" class="modal" ng-controller="LoginController"> 
<div class="modal-content"> 
	<form name="resendPasswordForm" no-validate>	
	<span class="loginModalClose">&times;</span> 
	<h1>Reset your password</h1> 

	<p class="modal-message">Sign in here to bookmark this job.</p> 

		<span class="help-success" ng-show = "successMessage.length > 0" ng-bind="successMessage"></span>

		<span class="help-error" ng-show = "errorMessage.length > 0" ng-bind="errorMessage"></span>

	<!-- 	<span class="help-success" ng-show = "!confirmedEmail" >Didn't receive a verification email? <a ng-click="sendConfirmationEmail()"> Send it again.</a></span> -->

		<div class="" ng-class="{'has-danger' : (resendPasswordForm.email.$invalid && resendPasswordForm.email.$dirty) == true}">
			<input type="text" ng-model = "resendPasswordData.email" name="email" ng-pattern = "/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" ng-required="true" class="form-control form-control-lg" id="resendPassword" placeholder="Email Address">

			<div class="form-control-feedback" ng-show = "resendPasswordForm.email.$invalid && resendPasswordForm.email.$dirty">Email Address is Required or Invalid.</div>
		</div>


		<div class="row">				
	



		<div class="g-recaptcha" data-sitekey="6LcWZ20UAAAAAOdgY4YuYDWlrOzPjOlYAOXxqrhh" style="margin-top: 10px;margin-left: 20px;"></div>


	    </div>			    
		<br/><button style="width: 100%" class="btn btn-lg" form-name="resendPasswordForm" loading-button callback="resendPassword()">Send Reset Email</button>
		<br/>
	</form>
</div>			
</div>