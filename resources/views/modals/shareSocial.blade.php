


<div id="shareSocial" class="modal" > 
<div class="modal-content"> 
	<span class="loginModalClose">&times;</span> 
	<h2 style="font-size: 30px;text-align: center;padding-top: 11px;height: 56px;">Share this to Social Media</h2> 

	<p class="modal-message">Sign in here to bookmark this job.</p> 

	<div class="col-12 social-media">
		<a  class="btn btn-block btn-social btn-facebook" ng-click="shareToFb()">
		    <span  class="fa fa-facebook media-icon"></span> <span class="media-label">Share on Facebook</span> 
		</a>
		@verbatim
		<a ng-click="shareToTwitter()" class="btn btn-block btn-social btn-twitter" >
		    <span  class="fa fa-twitter media-icon"></span> <span class="media-label">Share on Twitter</span> 
		</a>
		@endverbatim
	</div>
	
</div>			
</div>