<div class="header_container" id="bottom-curve">	
	<div class="menu_container" id="myTopnav">
		<div class="menu_left">
			<div class="logo_wrap">
			<a href="/" class="nav_logo"><img src="/public/assets/images/jobs-img/anesthesia-logo-2.png"></a>
			<a href="javascript:void(0);" class="mobile_icon" onclick="myNavToggle()">&#9776;</a>			
			</div>
			<div class="menu_wrap">
				<ul class="topmenu">
					<li class="current"><a href="/browse-jobs/{{Session::get('type')}}" class="curr_browse">BROWSE JOBS</a></li>
					<!--<li><a href="#">BROWSE RESUMES</a></li>-->
					<li><a class="curr_post" href="javascript:;" modal="loginModal" anesthesia-modal modal-message="Please sign in as a candidate.">POST A JOB</a></li>	
					<li><a href="/contact-us">CONTACT</a></li>											
				</ul>					
			</div>
			<div class="clearfix"></div>
		</div>

		<div class="menu_right">
			@if(!Auth::check())
				<a modal="loginModal" modal-message="Login to your dashboard." anesthesia-modal class="login_btn pointer">Log in</a> 
				<a href="#" class="signup_btn pointer hover-animate text-center" style="line-height: 18px;" animate-hover>
					<span>Sign Up</span>
				</a>		
			@else
				<a href="/application" class="login_btn pointer">Dashboard</a> 
			@endif						
		</div>
		<div class="clearfix"></div>
	</div>	
</div>



@include("modals.login")