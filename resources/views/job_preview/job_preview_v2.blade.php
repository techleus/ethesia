

<div class="slider-form" id="slider-form" ng-cloak>
	@verbatim
	<div class="slider-form-close hidden pointer">
		<i class="mdi mdi-close"></i>
	</div>

	<div class="col-md-12 nopadding" >
        <div class="row container" style="margin:  0px auto;border:0px solid #000; width:100%;padding-right: 0;">
            <div class="card-block nopadding">            

                <section id="wrapper" class="job-sidebar-gray noborder" custom-scrollbar>
                      <div class="no-border-radius job-descwrap" >
                        <div class="job-block-wrap">
                          <div class="job-header">
                             <div class="sleft"><a href="/browse-jobs/{{jobs[selected_index].jobPostType | stringJobType}}"><i class="mdi mdi-chevron-left"></i> Back to job list</a></div>
                             <div class="sright"><a href="/job-posts/{{jobs[selected_index].id}}" target="_blank">Open posting in a new window <i class="mdi mdi-open-in-new"></i> </a></div>
                          </div>
                          <div class="jobbody-wrap"><!--jobbody-wrap-->
                          	<div class="row" style="padding-right: 0;border:0px solid #000">
                          		<div class="col-7 offset-2" style="border:0px solid #000">
                          			<div class="jobsidebar-wrap_v2">
                          				<div class="containerwrap">   

										<div class="clearFix">&nbsp;</div>                           	                      
			                          </div><!--end-jobbody-wrap-->


									
										<div class="nID">JOB ID: {{jobs[selected_index].id}}</div>
									
										<div class="sTitle">{{jobs[selected_index].jobTitle}}</div>
									
										<div class="address"><i class="mdi mdi-map-marker"></i>


											{{jobs[selected_index].ei_facilityCity}}, {{jobs[selected_index].ei_facilityState}} 

											{{jobs[selected_index].ei_durPosition}} <!-- Salary: <span class="text-blue-light">{{jobs[selected_index].ei_durMin | stringReplace}} - {{jobs[selected_index].ei_durMax | stringReplace}}</span> --></div>
									
										<div class="datedviews">
									
											<div class="left">
									
												<i class="fa fa-calendar text-blue"></i> Post Date:  <span class="text-blue text-bold">{{jobs[selected_index].created_date}}</span> &nbsp; &nbsp; &nbsp;
									
												<i class="fa fa-clock-o text-blue"></i> Updated:  <span class="text-blue text-bold">{{jobs[selected_index].updated_date}}</span> 
									
											</div>
											<div class="clearFix"></div>
										</div>
									</div>
									<div>&nbsp;</div>

									<div class="jobdata-wrap">
										<div class="body-title">Job Description</div>
									
										<div class="body-text" ng-bind-html="jobs[selected_index].ei_jobDescription | to_trusted">
										</div>          
									
									
										<div class="body-title">Job Details</div>
									
										<div class="row job-details">
									
										  <div class="col-12">
											<div ng-hide="jobs[selected_index].facilityName == undefined || jobs[selected_index].facilityName == ''" class="col-4 float-left field-preview">
												<div class="key">FACILITY NAME</div>
									
												<div class="value">Facility name</div>              
											</div>    
									
											<div ng-hide="jobs[selected_index].facilityName == undefined || jobs[selected_index].facilityName == ''" class="col-4 float-left field-preview">
												<div class="key">JOB START DATE</div>
									
												<div class="value">Specified start date</div>
											</div>    
										  
											<div ng-hide="jobs[selected_index].ei_durMin == undefined || jobs[selected_index].ei_durMin == ''" class="col-4 float-left field-preview">
												<div class="key">SALARY RANGE</div>
									
												<div class="value">{{jobs[selected_index].ei_durMin}} - {{jobs[selected_index].ei_durMax}}</div>                
											</div>
									
									
											<div ng-hide="jobs[selected_index].ei_facilityCity == undefined || jobs[selected_index].ei_facilityCity == ''" class="col-4 float-left field-preview">
												<div class="key">FACILITY LOCATION</div>
									
												<div class="value">{{jobs[selected_index].ei_facilityCity}}, {{jobs[selected_index].ei_facilityState}}</div>    
											</div>                
									
											
											<div ng-hide="jobs[selected_index].ei_ansFirstCall == undefined || jobs[selected_index].ei_ansFirstCall == ''" class="col-4 float-left field-preview">
												<div class="key">CRNA ON FIRST CALL?</div>
									
												<div class="value">{{jobs[selected_index].ei_ansFirstCall}}</div>
											</div>   
										 
									
											<div ng-hide="jobs[selected_index].left == undefined || jobs[selected_index].left == ''" class="col-4 float-left field-preview">
												<div class="key">CONTACT WEBSITE
												</div>
									
												<div class="value">{{jobs[selected_index].ag_webSite}}</div>
											</div>
									
									
											<div ng-hide="jobs[selected_index].facilityName == undefined || jobs[selected_index].facilityName == ''" class="col-4 float-left field-preview">
												<div class="key">FACILITY ADDRESS</div>
												<div class="value">Street address, zip code</div>
											</div>               
									
											
											<div ng-hide="jobs[selected_index].ei_empStatus == undefined || jobs[selected_index].ei_empStatus == ''" class="col-4 float-left field-preview">
												<div class="key">EMPLOYMENT TYPE</div>
												<div class="value">{{jobs[selected_index].ei_empStatus}}</div>
											</div>
									
									
											<div ng-hide="jobs[selected_index].ci_email == undefined || jobs[selected_index].ci_email == ''" class="col-4 float-left field-preview">
												<div class="key">CONTACT EMAIL</div>
												<div class="value">{{jobs[selected_index].ci_email}}</div>
											</div>
									
									
									
											<div ng-hide="jobs[selected_index].ei_ansMedDirecting == undefined || jobs[selected_index].ei_ansMedDirecting == ''" class="col-4 float-left field-preview">
												<div class="key">Medically Directing</div>
												<div class="value">{{jobs[selected_index].ei_ansMedDirecting}}</div>
											</div>
									
									
											<div ng-hide="jobs[selected_index].jr_fellowshipReq == undefined || jobs[selected_index].jr_fellowshipReq == ''" class="col-4 float-left field-preview">
												<div class="key">Subspecialty Fellowship</div>
												<div class="value">{{jobs[selected_index].jr_fellowshipReq}}</div>
											</div>
									
									
											<div ng-hide="jobs[selected_index].jr_stateLicense == undefined || jobs[selected_index].jr_stateLicense == ''" class="col-4 float-left field-preview">
												<div class="key">State License</div>
												<div class="value">{{jobs[selected_index].jr_stateLicense}}</div>
											</div>
									
									
											<div ng-hide="jobs[selected_index].jr_abaCertStatus == undefined || jobs[selected_index].jr_abaCertStatus == ''" class="col-4 float-left field-preview">
												<div class="key">ABA Certification Status</div>
												<div class="value">{{jobs[selected_index].jr_abaCertStatus}}</div>
											</div>
									
									
									
											<div ng-hide="jobs[selected_index].jr_abaCertStatus == undefined || jobs[selected_index].jr_abaCertStatus == ''" class="col-4 float-left field-preview">
												<div class="key">ABA Certification Status</div>
												<div class="value">{{jobs[selected_index].jr_abaCertStatus}}</div>
											</div>
									
									
											<div ng-hide="jobs[selected_index].jr_abaCertStatus == undefined || jobs[selected_index].jr_abaCertStatus == ''" class="col-4 float-left field-preview">
												<div class="key">Required for this job</div>
												<div class="value">
													{{jobs[selected_index].current_acls == 'true' ? 'Current ACLS card' : ''}}
													<br>
													{{jobs[selected_index].current_pals == 'true' ? 'Current PALS card' : ''}}
													<br>
													{{jobs[selected_index].aba_pain_management == 'true' ? 'ABA Certification in Pain Management' : ''}}
													<br>
													{{jobs[selected_index].aba_critical_care == 'true' ? 'ABA Certification in Critical Care Management' : ''}}
												</div>
											</div>
									
											<div ng-hide="jobs[selected_index].fi_nameAns == undefined || jobs[selected_index].fi_nameAns == ''" class="col-4 float-left field-preview">
												<div class="key">Name of Anesthesiologist who is Group President and years with group</div>
												<div class="value">{{jobs[selected_index].fi_nameAns}}</div>
											</div>
									
											<div ng-hide="jobs[selected_index].fi_isPracLimited == undefined || jobs[selected_index].fi_isPracLimited == ''" class="col-4 float-left field-preview">
												<div class="key">Is practice limited to Hospital, Surgery Center and Office Based Anesthesia?</div>
												<div class="value">{{jobs[selected_index].fi_isPracLimited}}</div>
											</div>
									
											<div ng-hide="jobs[selected_index].jr_newGraduates == undefined || jobs[selected_index].jr_newGraduates == ''" class="col-4 float-left field-preview">
												<div class="key">New Graduates Acceptable?</div>
												<div class="value">{{jobs[selected_index].jr_newGraduates}}</div>
											</div>
									
									
											
									
											<div ng-hide="jobs[selected_index].si_SignonBonus == undefined || jobs[selected_index].si_SignonBonus == ''" class="col-4 float-left field-preview">
												<div class="key">Sign-on Bonus commitment</div>
												<div class="value">{{jobs[selected_index].si_SignonBonus}}</div>
											</div>
									
											<div ng-hide="jobs[selected_index].fi_nameAssistant == undefined || jobs[selected_index].fi_nameAssistant == ''" class="col-4 float-left field-preview">
												<div class="key">Name of Anesthesiologist Assistant(s)</div>
												<div class="value">{{jobs[selected_index].fi_nameAssistant}}</div>
											</div>
									
									
											<div ng-hide="jobs[selected_index].si_PaidLeave == undefined || jobs[selected_index].si_PaidLeave == ''" class="col-4 float-left field-preview">
												<div class="key">Paid Educational Leave</div>
												<div class="value">{{jobs[selected_index].si_PaidLeave}}</div>
											</div>
									
											<div ng-hide="jobs[selected_index].si_PaidVacation == undefined || jobs[selected_index].si_PaidVacation == ''" class="col-4 float-left field-preview">
												<div class="key">Paid Vacation</div>
												<div class="value">{{jobs[selected_index].si_PaidVacation}}</div>
											</div>
									
											<div ng-hide="jobs[selected_index].ag_name == undefined || jobs[selected_index].ag_name == ''" class="col-4 float-left field-preview">
												<div class="key">Anesthesia Group Name</div>
												<div class="value">{{jobs[selected_index].ag_name}}</div>
											</div>
									
											<div ng-hide="jobs[selected_index].ag_webSite == undefined || jobs[selected_index].ag_webSite == ''" class="col-4 float-left field-preview">
												<div class="key">Anesthesia Group Web Site</div>
												<div class="value">{{jobs[selected_index].ag_webSite}}</div>
											</div>
									
									
									
											<div ng-hide="jobs[selected_index].ag_haveContract == undefined || jobs[selected_index].ag_haveContract == ''" class="col-4 float-left field-preview">
												<div class="key">Anesthesia Group Exclusive Contract</div>
												<div class="value">{{jobs[selected_index].ag_haveContract}}</div>
											</div>
									
									
											
									
											<div ng-hide="jobs[selected_index].fi_nameCRNA == undefined || jobs[selected_index].fi_nameCRNA == ''" class="col-4 float-left field-preview">
												<div class="key">Name of Chief CRNA</div>
												<div class="value">{{jobs[selected_index].fi_nameCRNA}}</div>
											</div>
									
									
											
									
									
											
									
											<div ng-hide="jobs[selected_index].fi_isFederalJob == undefined || jobs[selected_index].fi_isFederalJob == ''" class="col-4 float-left field-preview">
												<div class="key">Federal Government job?</div>
												<div class="value">{{jobs[selected_index].fi_isFederalJob}}</div>
											</div>
									
											<div ng-hide="jobs[selected_index].ci_jobID == undefined || jobs[selected_index].ci_jobID == ''" class="col-4 float-left field-preview">
												<div class="key">Company internal Job ID</div>
												<div class="value">{{jobs[selected_index].ci_jobID}}</div>
											</div>
									
									
											<div ng-hide="jobs[selected_index].ci_name == undefined || jobs[selected_index].ci_name == ''" class="col-4 float-left field-preview">
												<div class="key">Contact Name</div>
												<div class="value">{{jobs[selected_index].ci_name}}</div>
											</div>
									
											<div ng-hide="jobs[selected_index].ci_email == undefined || jobs[selected_index].ci_email == ''" class="col-4 float-left field-preview">
												<div class="key">Contact Email</div>
												<div class="value">{{jobs[selected_index].ci_email}}</div>
											</div>
									
											<div ng-hide="jobs[selected_index].ci_address1 == undefined || jobs[selected_index].ci_address1 == ''" class="col-4 float-left field-preview">
												<div class="key">Contact Street Address 1</div>
												<div class="value">{{jobs[selected_index].ci_address1}}</div>
											</div>
									
											<div ng-hide="jobs[selected_index].ci_address2 == undefined || jobs[selected_index].ci_address2 == ''" class="col-4 float-left field-preview">
												<div class="key">Contact Street Address 2</div>
												<div class="value">{{jobs[selected_index].ci_address2}}</div>
											</div>
									
									
											<div ng-hide="jobs[selected_index].ci_city == undefined || jobs[selected_index].ci_city == ''" class="col-4 float-left field-preview">
												<div class="key">Contact City</div>
												<div class="value">{{jobs[selected_index].ei_facilityCity}}</div>
											</div>
									
											<div ng-hide="jobs[selected_index].ci_state == undefined || jobs[selected_index].ci_state == ''" class="col-4 float-left field-preview">
												<div class="key">Contact State</div>
												<div class="value">{{jobs[selected_index].ei_facilityState}}</div>
											</div>
									
											<div ng-hide="jobs[selected_index].ci_zipcode == undefined || jobs[selected_index].ci_zipcode == ''" class="col-4 float-left field-preview">
												<div class="key">Contact Zip Code</div>
												<div class="value">{{jobs[selected_index].ci_zipcode}}</div>
											</div>
									
									
											<div ng-hide="jobs[selected_index].ci_phone == undefined || jobs[selected_index].ci_phone == ''" class="col-4 float-left field-preview">
												<div class="key">Contact Voice Phone</div>
												<div class="value">{{jobs[selected_index].ci_phone}}</div>
											</div>
									
											<div ng-hide="jobs[selected_index].ci_fax == undefined || jobs[selected_index].ci_fax == ''" class="col-4 float-left field-preview">
												<div class="key">Contact Fax</div>
												<div class="value">{{jobs[selected_index].ci_fax}}</div>
											</div>
									
									
									
											<div ng-hide="jobs[selected_index].ci_website == undefined || jobs[selected_index].ci_website == ''" class="col-4 float-left field-preview">
												<div class="key">Contact Web Site</div>
												<div class="value">{{jobs[selected_index].ci_website}}</div>
											</div>
									
									
											<div ng-hide="jobs[selected_index].ci_prefMethod == undefined || jobs[selected_index].ci_prefMethod == ''" class="col-4 float-left field-preview">
												<div class="key">Preferred Contact Method</div>
												<div class="value">{{jobs[selected_index].ci_prefMethod}}</div>
											</div>
									
									
										  </div>
										</div> 
									
										<div class="clearFix">&nbsp;</div>
									
										
										<div class="body-title">Additional information</div>
									
										<div class="text-gray">EXAMPLE OF QUESTION WITH MULTIPLE ANSWERS</div>
									
										<div class="add-info-btnswrap">
									
											<button type="button" class="btn btn-small-gray">Obstetric Anesthesia</button>
									
											<button type="button" class="btn btn-small-gray">Pediatric Anesthesia</button>
									
											<button type="button" class="btn btn-small-gray">Major Vascular Anesthesia</button>
									
											<button type="button" class="btn btn-small-gray">Neuroanesthesia</button>
									
											<button type="button" class="btn btn-small-gray">Administrative / Leadership Duties</button>
									
										</div>
									
										<div class="clearFix"></div>
									
										<div>&nbsp;</div>
									
								  </div>  

                          		</div>

                          		<div class="col-lg-3 col-md-12" style="border:0px solid #000;">
                          			<div class="jobsidebar-wrap_v2">

										<div class="logowrap"><img class="img-circle pointer" style="width: 100px;height: 100px;" ng-src="{{jobs[selected_index].image_path}}"></div>
										<div class="companywrap mt-20" style="margin-top: 20px">
									
											<div class="name">{{jobs[selected_index].company_name}}</div>
									
											<!-- <i class="fa fa-eye text-blue"></i> <span class="text-gray">Views: 148</span> -->
									
									
											<div class="address"><i class="mdi mdi-map-marker"></i> {{jobs[selected_index].company_city}}, {{jobs[selected_index].company_state}}</div>
									
											<div class="url"><a href="#">{{jobs[selected_index].company_website}}</a></div>               
									
										</div>          
										@endverbatim
										@if(Auth::check())								
											@if(Auth::User()->userType==1)
												@verbatim
												<button style="margin-bottom: 15px" ng-cloak show-slider="{{$index}}"  id_target = "apply-slider-form" ng-show="true" type="button" class="btn btn-btn1 close-prev-slider" ng-click="updateAppForm(jobs[selected_index].id)">Apply Now</button>
												<button type="button" ng-click="saveJob(1,jobs[selected_index].id,jobs[selected_index].jobTitle,jobs[selected_index])" class="btn btn-btn2">{{ jobs[selected_index].save == '' || jobs[selected_index].save == NULL ? 'Save Job' : 'Unsave Job' }}</button>
												@endverbatim
											@else


											@verbatim
												<button  style="margin-bottom: 12px" modal="loginModal" anesthesia-modal modal-message="Please sign in as a candidate." type="button" class="btn btn-btn1" >Apply Now</button>
												<button type="button" modal="loginModal" class="btn btn-btn2" anesthesia-modal>Save Job</button>
											@endverbatim


											@endif


										@else
											@verbatim
											<button  type="button" class="btn btn-btn1" modal="loginModal" anesthesia-modal >Apply Now</button>
											<button type="button"  class="btn btn-btn2" modal="loginModal" anesthesia-modal>Save Job</button>
											@endverbatim
										@endif

										
										<button type="button"  class="btn btn-btn2" ng-click="openPdf(jobs[selected_index].id)"; return false;">PDF version</button>

										<button type="button"  class="btn btn-btn2" modal="shareSocial" modal-message="Available to share on" ng-click="assignSocialJobData(job)" anesthesia-modal>Share</button>
									</div>
                          		</div>
                          		
                          	</div>

                          	@verbatim
                          	
							

								
							

							@endverbatim
							
							                          
                        </div>
                      </div>
                </section>                

            </div>
        </div> 
    </div>

</div>

