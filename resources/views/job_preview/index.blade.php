<!DOCTYPE html>
<html lang="en" ng-app="app" ng-controller ="JobPreviewController">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>When Great Minds Don’t Think Alike</title>

    <meta property="og:url"                content="{{url('/').'/job-posts/'.$data->id}}" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="{{$data->jobTitle}}" />
    <meta property="og:description"        content="{{$data->ei_jobDescription}}" />
    <meta property="og:image"              content="{{$data->image_path}}" />


    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/public/assets/images/favicon.png">
    	
	
	 <!-- Bootstrap Core CSS -->
    <link href="/public/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="/public/assets/material/css/style.css?VERSION="{{env("APP_VERSION")}} rel="stylesheet">
<!--     <link href="/public/assets/material/ss/colors/blue.css" id="theme" rel="stylesheet">
 --><!--     			
 -->
    <link rel="stylesheet" type="text/css" href="/public/assets/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="/public/assets/bower_components/placeholder-loading/dist/css/placeholder-loading.min.css">
	<link rel="stylesheet" type="text/css" href="/public/assets/css/style-jobsidebar.css?VERSION="{{env("APP_VERSION")}}>
    <link href="/public/assets/plugins/sweetalert/sweetalert.css" id="theme" rel="stylesheet">    <link rel="stylesheet" type="text/css" href="/public/assets/plugins/jquery-textext-master/src/css/textext.core.css" />
    <link rel="stylesheet" type="text/css" href="/public/assets/plugins/jquery-textext-master/src/css/textext.plugin.autocomplete.css" />
    <link rel="stylesheet" type="text/css" href="/public/assets/plugins/jquery-textext-master/src/css/textext.plugin.tags.css" />
    <link type="text/css" rel="stylesheet" href="/application/bower_components/ladda/dist/ladda.min.css" />
    <link href="/application/assets/plugins/toast-master/css/jquery.toast.css?VERSION=1.1-G" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/public/assets/css/style-public.css?VERSION="{{env("APP_VERSION")}}>
	<link rel="stylesheet" type="text/css" href="/public/assets/css/public-responsive.css?VERSION="{{env("APP_VERSION")}}>
    <link href="/application/assets/plugins/toast-master/css/jquery.toast.css?VERSION=1.1-G" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="/public/assets/css/jobpreview.css?VERSION="{{env("APP_VERSION")}}>
    <link rel="stylesheet" type="text/css" href="/public/assets/css/modallogin.css?VERSION="{{env("APP_VERSION")}}>
    <script type="text/javascript" src="/public/app/browse_jobs/angular.min.js"></script>

</head>

<body   >

    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div class="page-main-wrapper">
      <!-- header-menu-->
	  @include('job_preview.header')
	  <!--end-header-menu-->
	 

	   @yield('content')

		<!--END-page-container-->
				
	<div class="clearFix">&nbsp;</div>			

       
    	@include('landing.footer')
	
	</div>

	<!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,600" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Muli:400,700" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
	
    <script src="/public/assets/plugins/jquery/jquery.min.js"></script>
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.js'></script>
    
    <!-- custom modal -->

    <script src="/public/assets/plugins/jquery-textext-master/src/js/textext.core.js"></script>
    <script src="/public/assets/plugins/jquery-textext-master/src/js/textext.plugin.autocomplete.js"></script>
    <script src="/public/assets/plugins/jquery-textext-master/src/js/textext.plugin.filter.js"></script>
    <script src="/public/assets/plugins/jquery-textext-master/src/js/textext.plugin.tags.js"></script>
    <script src="/public/assets/plugins/jquery-textext-master/src/js/textext.plugin.ajax.js"></script>

    <script src="/public/assets/plugins/tagsinput/jquery.tagsinput.js"></script>	
    <script type="text/javascript" src="/public/assets/bower_components/jquery.cookie/jquery.cookie.js"></script>       
	
    <!-- Bootstrap tether Core JavaScript -->
    <script src="/public/assets/plugins/bootstrap/js/tether.min.js"></script>
    <script src="/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="/public/assets/plugins/select2/js/select2.min.js"></script>
    <script type="text/javascript" src="/public/app/browse_jobs/us_states.json"></script>
    
<!--     
 -->	
	<!-- Sticky Navbar -->
    <script  type="text/javascript" src="/public/assets/plugins/sweetalert/sweetalert.min.js"></script>    
	<script type="text/javascript" src="/public/assets/js/sticky_navbar.js"></script>   
    <script src="/public/assets/bower_components/angular-socialshare/dist/angular-socialshare.js"></script>


    <script type="text/javascript" src="/application/bower_components/angular-local-storage/dist/angular-local-storage.min.js"></script>
    <script type="text/javascript" src="/application/bower_components/ladda/dist/spin.min.js"></script>
    <script type="text/javascript" src="/application/bower_components/ladda/dist/ladda.min.js"></script>
    <script src="/application/assets/plugins/toast-master/js/jquery.toast.js"></script>
    <script src="/application/assets/plugins/toast-master/js/jquery.toast.js"></script>
    <script  type="text/javascript" src="/application/app/toaster.js"></script> 
    <script src="/public/app/app.js?VERSION="{{env("APP_VERSION")}}></script>
    <script src="https://connect.facebook.net/en_US/all.js"></script>

    <script src="/public/app/app.js?VERSION="{{env("APP_VERSION")}}></script>
    <script src="/public/app/directive.js?VERSION="{{env("APP_VERSION")}}></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <script src="/public/app/toaster.js"></script>
    <script src="/public/app/browse_jobs/directive.js?VERSION="{{env("APP_VERSION")}}></script>
    <script src="/public/app/browse_jobs/BrowseJobService.js?VERSION="{{env("APP_VERSION")}}></script>
    <script  type="text/javascript" src="/application/app/pages/register/LoginService.js?VERSION="{{env("APP_VERSION")}}></script>
    <script src="/application/app/pages/login/AuthenticationService.js?VERSION="{{env("APP_VERSION")}}></script>
    <script src="/public/app/api.js?VERSION="{{env("APP_VERSION")}}></script>
    <script src="/public/app/login/LoginController.js?VERSION="{{env("APP_VERSION")}}></script>
    <script src="/public/app/job_preview/JobPreviewController.js?VERSION="{{env("APP_VERSION")}}></script>
    <script src="/public/app/register/RegisterController.js?VERSION="{{env("APP_VERSION")}}></script>



</body>

</html>