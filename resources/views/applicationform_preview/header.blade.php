<div class="header_container" id="bottom-curve">	
	<div class="menu_container" id="myTopnav">
		<div class="menu_left">
			<div class="logo_wrap">
			<a href="/" class="nav_logo"><img src="/public/assets/images/jobs-img/anesthesia-logo-2.png"></a>
			<a href="javascript:void(0);" class="mobile_icon" onclick="myNavToggle()">&#9776;</a>			
			</div>
			<div class="menu_wrap">

				<ul class="topmenu">
					@verbatim
					<li class="current"><a href="/browse-jobs/{{jobsData.job_type}}" class="curr_browse">BROWSE JOBS</a></li>
					<li><a href="#">BROWSE RESUMES</a></li>
					<li><a href="/browse-employers">BROWSE EMPLOYERS</a></li>	
					<li><a href="#">CONTACT</a></li>				
					@endverbatim								
				</ul>				
			</div>
			<div class="clearfix"></div>
		</div>

		<div class="menu_right">
			@if(!Auth::check())
				<a modal="loginModal" modal-message="Login to your dashboard." anesthesia-modal class="login_btn pointer">Log in</a> 
				<a show-slider="" slider-width="873px" id_target = "registration-form" class="signup_btn pointer">Sign Up</a>		
			@else
				<a href="/application" class="login_btn pointer">Dashboard</a> 
			@endif						
		</div>
		<div class="clearfix"></div>
	</div>	
</div>



@include("modals.login")