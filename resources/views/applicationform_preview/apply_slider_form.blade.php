@verbatim
<div class="col-md-8 offset-md-2">
        <div class="row" >
            <div class="card-block nopadding">            
                <section id="wrapper" class="job-sidebar-gray noborder" custom-scrollbar>
                    <div class="no-border-radius job-descwrap" >
                        <div class="job-block-wrap">                          	
                          	<div class="app-form-wrap"><!--jobbody-wrap-->
								<div class="app-formdata-wrap">								
								  	<div class="container-wrap">								  		
		                                <div class="form-group">		                                	
		                                	<div class="body-header">Inquire about job # <?php echo $id; ?></div> 
											<div class="body-title"><i class="fa fa-user-md"></i> &nbsp;Inquire as: </div>
											<div class="radio-holder">		                                    
			                                    <input name="inquire-opt" value="no" type="radio" class="radio-col-purple with-gap" id="radio_3" checked="checked"/>
			                                    <label for="radio_3"><?php echo $fullname; ?></label>
			                                    <input name="inquire-opt" value="yes" type="radio" id="radio_4" class="radio-col-purple with-gap" />
			                                    <label for="radio_4">Anonymous</label>		                                    
			                                </div>			                                
	                                        <label class="body-title"><i class="fa fa-wechat"></i> &nbsp;Send Private Message</label>
											<textarea ng-model="applyForm.message" class="col-md-12 form-control app-form-textarea" rows="5" ></textarea>
	                                        <div class="row">											  
											  <div class="col-md-12 textarea-label"><span class="float-right">{{1000 - basicInfo.description.length}} characters remaining</span></div>
											</div>
	                                        <div class="body-title"><i class="fa fa-file-text-o"></i> &nbsp;Upload CV <small class="small-title">(This is optional. Maximum file size: 10MB)</small></div>
	                                        <div class="upload-btn-wrapper col-md-12">
	                                        	<form name="CVform" enctype='multipart/form-data'>
		                                        	<button class="col-md-12 btn btn-lg upload-btn-bg text-white" upload-file>Upload file</button>
		                                        	<input type="file" multiple="true" name="cv" id="inquirerCV" />
	                                        	</form>	
	                                        </div>
	                                        <div class="radio-holder">
	                                        	<br/>		                                    
			                                    <input name="privacy-opt" type="checkbox" id="privacy-label" class="filled-in chk-col-purple" ng-click="selectUnselectPrivacy()" id="privacy-opt"/>
			                                    <label for="privacy-label" class="privacy-label">I have read & Accept the <a show-policy><span style="color: #6684f2;">Privacy Policy</span></a> and <a show-terms><span style="color: #6684f2;">Terms of Use</span></a></label>
			                                </div><
			                                <br/>			                                
			                                <div class="row col-md-12">
			                                	<div class="button-holder">
			                                		<button class="btn bg-white text-black text-center" ng-click="cancelInquire()" id="app-form-cancel">Cancel</button>
			                                		<button class="btn text-white text-center" id="app-form-inquire" ng-click="inquireJob(jobs[selected_index].id,current_user_data.id,apply_form.userID);">Inquire Now</button>
			                                	</div>
			                                </div>
	                                    </div>	                                    
									</div>
								</div>
								<div class="jobsidebar-wrap">
							
									<div class="logowrap"><img class="img-circle pointer" style="width: 100px;height: 100px;" src="{{jobs[selected_index].image_path}}"></div>
								
									<div class="companywrap mt-20" style="margin-top: 20px">
								
										<div class="company_name">{{apply_form.company_name}}</div>
																																		
										<div class="company_address"><i class="mdi mdi-map-marker"></i>{{apply_form.city}}, {{apply_form.state}}</div>
								
										<div class="company_url"><a href="#">{{apply_form.website}}</a></div>               
								
									</div>          
								@endverbatim
									
									@if(Auth::check())
									@verbatim
									<button ng-cloak show-slider="{{$index}}" id_target = "apply-slider-form" ng-show="true" ng-click="inquireJob(jobs[selected_index].id,current_user_data.id);" type="button" class="btn btn-btn1">Inquire now</button>
									@endverbatim
									@else
									@verbatim
									<button ng-click="popupNeedsCandidate();$event.stopPropagation();" type="button" class="btn btn-btn1">Inquire now</button>
									@endverbatim
									@endif
								
									<div>&nbsp;</div>
								
									<button ng-show="true" type="button" class="btn btn-btn2">Save Job</button>            
								
								</div>
								@verbatim
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
@endverbatim