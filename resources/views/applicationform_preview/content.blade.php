
@extends('applicationform_preview.index')
<style type="text/css">
	
	#slider-form {
		position: relative;
	    /* overflow-y: scroll; */
	    height: auto;
	    width: 100%;
	    background: #fff;
	    top: 0px;
	    z-index: 9999;
	     right: 0px !important; 
	    overflow-x: hidden;
	    transition-property: left, right;
	    transition-duration: 0.3s;
	    transition-timing-function: ease-in-out;
	    transition-delay: 0s;
	    box-shadow: none;
		overflow-y: hidden !important;
	    
	}
	#slider-form .job-header {
		display: none !important;
	}
	.jobdata-wrap {
		width: 100% !important;
		border-right: none !important;
	}
</style>

@section('content')	

	@include('applicationform_preview.apply_slider_form')


@include('slider.apply_slider_form')
@include('slider.registration_slider')
@include('slider.filter-jobs-form')
@include('modals.shareSocial')


@endsection