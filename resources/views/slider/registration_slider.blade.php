<style type="text/css">
	/*global checkbox*/
	[type="checkbox"] + label::before,
	[type="checkbox"]:not(.filled-in) + label::after {	    	    
	    border-radius: 100%;
	    border: 2px solid #614CF9;
	}	
	[type="checkbox"]:checked + label:before{
		border: 2px solid #614CF9 !important;
		width: 18px;
    	height: 18px;    	
    	position: absolute;
    	top: 0;
    	left: 0;
    	opacity: 1;
    	-webkit-transform: rotate(0deg); 
	    -ms-transform: rotate(0deg);
	     transform: rotate(0deg); 
	}
	[type="checkbox"]:not(.filled-in) + label:after{
		transform: none;
	}
	[type="checkbox"]:checked + label:before{
		border: 2px solid #614CF9 !important;
	}
	[type="checkbox"]:checked + label:after {
	    background-color: #614CF9 !important;	    
	    position: absolute;
	    top: 2px;
	    left: 2px;
	    width: 14px;
    	height: 14px; 	    
	    border: 2px solid #fff;	    
	    -webkit-animation: ripple 0.2s linear forwards;
	    animation: ripple 0.2s linear forwards;
	}	
</style>
<div class="slider-form" id="registration-form" ng-controller="RegisterController">
	<div class="slider-form-close hidden pointer" id="close-slider-form">
		<i class="mdi mdi-close"></i>
	</div>	
        
	<div class="job_filter_sidebar">            
		<input type="hidden" id="states_input" name="">
		<section id="wrapper" class="job-sidebar-gray noborder" custom-scrollbar>                      
				@verbatim				
			
				<div class="col-12  nopadding sidebar-form-wrapper" rd-steps step-key="registration">

					<h3 style="margin-top: 70px;text-align: center;font-size: 30px;">Register with ethesia!</h3>
					
					<form class="anesthesia-btn form-horizontal anesthesia-input" style="left: 179px;position: relative;" name="regForm" novalidate>
					


					<div class=".col-12 col-md-8 section nopadding slider-sections"  id="registration_1" step="1"  style="margin-top: 60px !important">
						<div class="form-group">
							<label class="slider-menu-label">Which one are you?</label>
							<div class="col-12 pointer" >
								

								<div ng-click = "selectAccountType('candidate')" ng-class = "{'slider-box-highlight' : rdSteps['registration']['step1']['account_type'] == 'candidate'}" class="npadding col-5 slider-menu pointer" ng-click = "selectAccountType('candidate')">
									<div class="col-12 slider-menu-content">
										<img style="width: 110px;" src="/public/images/provider.jpg" >
										<br>
										<label style="margin-left: -27px" class="slider-sublabel">Anesthesia Provider</label>
									</div>
									
								</div>

								<div class="col-2 float-left " style="text-align: center;">
									<label class="or-label" >or</label>
								</div>

								<div ng-click = "selectAccountType('job_provider')" ng-class = "{'slider-box-highlight' : rdSteps['registration']['step1']['account_type'] == 'job_provider'}" class="npadding col-5 slider-menu pointer" ng-click = "selectAccountType('job_provider')">
									<div class="col-12 slider-menu-content">
										<img style="width: 110px;" src="/public/images/employer.jpg" >
										<br>
										<label style="margin-left: -27px" class="slider-sublabel">Job Provider</label>
									</div>
									
								</div>
							</div>
						</div>
					</div>



					


					<div style="margin-top: 80px !important" class="col-12 col-md-8  section nopadding slider-sections"   id="registration_2" step="2">
						<div class="form-group">
							<label class="slider-menu-label">What's your email address? </label>
							<br>
							<input class="form-control " press-enter proceed-to="3" ng-model="regData.email" type="text" ng-required="true" ng-pattern = "/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" name="email" placeholder="example@gmail.com">
                            <span class="help-block absolute" ng-show = "regForm.email.$invalid && regForm.email.$dirty">Please enter a valid email address.</span>
						</div>
						<div class="col-12 nopadding" ng-hide="regForm.email.$invalid">
							<button ng-click="proceedToStep(3)" class="btn btn-primary sidebar-btn" proceed-to="3">Next</button>
							<label>Press Enter</label>
						</div>
					</div>


					<div class="col-12 col-md-8  section nopadding slider-sections"   id="registration_3" step="3">
						<div class="form-group">
							<label style="margin-bottom: 0px !important;" class="slider-menu-label">What would you like your password to be? </label>
							<label class="subtle-hint">Use at least 8 characters.</label>
							<input press-enter proceed-to="4" style="margin-top: 17px !important" class="form-control" ng-model="regData.password" ng-minlength = "8" type="password" name="password" ng-required="true" placeholder="Password">
                        	<span class="help-block" ng-show = "regForm.password.$invalid && regForm.password.$dirty">A password with at least 8 characters is required.</span>
						</div>
						<div class="col-12 nopadding" ng-hide="regForm.password.$invalid">
							<button class="btn btn-primary sidebar-btn" ng-click="proceedToStep(4)">Next</button>
							<label>Press Enter</label>
						</div>
					</div>


					<div class="col-12 col-md-8  section nopadding slider-sections"  ng-class="{'' : rdSteps['registration']['current_step'] != 4}" id="registration_4" step="4">
						<div class="form-group">
							<label class="slider-menu-label">Select your account type</label>
							<br>
							<select press-enter proceed-to="{{ selected_account == 'job_provider' ? 5 : 6 }}" name="account_title" ng-click="changeSelectedTitle(regData.account_type);" ng-required="true" class="form-control" ng-model="regData.account_title" ng-disabled="count(selectedTitles) == 0">
                            	<option value="" selected disabled="true">{{accountTitlePlaceHolder(regData.account_type)}}</option>
                            	<option value="{{title.id}}" ng-repeat = "title in selectedTitles">{{title.user_account_title}}</option>
                            </select>
                            <span class="help-block absolute" ng-show = "regForm.account_title.$invalid && regForm.account_title.$dirty && regData.account_title != ''">Please select an account type.</span>
						</div>
						<div class="col-12 nopadding" ng-hide="regForm.account_title.$invalid">
							<button class="btn btn-primary sidebar-btn" ng-click="proceedToStep(selected_account == 'job_provider' ? 5 : 6)">Next</button>
							<label>Press Next</label>
						</div>
					</div>

					


					<div class="col-12 col-md-8  section nopadding slider-sections" ng-show="rdSteps['registration']['step1']['status'] && selected_account == 'job_provider'" ng-class="{'' : rdSteps['registration']['current_step'] != 5}" id="registration_5" step="5">
						<div class="form-group">
							<label class="slider-menu-label">What is your business name? </label>
							<br>
							<input press-enter proceed-to="8" class="form-control" ng-model="regData.company_name" type="text" name="company_name" ng-required="true" placeholder="Enter your business Name">
                        	<span class="help-block" ng-show = "regForm.company_name.$invalid && regForm.company_name.$dirty">A business name is required.</span>
						</div>
						<div class="col-12 nopadding" ng-hide="regForm.company_name.$invalid">
							<button class="btn btn-primary sidebar-btn" ng-click="proceedToStep(8)">Next</button>
							<label>Press Enter</label>
						</div>
					</div>


					<div class="col-12 col-md-8  section nopadding slider-sections" ng-if="rdSteps['registration']['step1']['status'] && selected_account == 'candidate'" ng-class="{'' : rdSteps['registration']['current_step'] != 6}" id="registration_6" step="6">
						<div class="form-group">
							<label class="slider-menu-label">What is your first name? </label>
							<br>
							<input press-enter proceed-to="7" class="form-control" ng-model="regData.first_name" type="text" name="first_name" ng-required="true" placeholder="First Name">
                        	<span class="help-block" ng-show = "regForm.first_name.$invalid && regForm.first_name.$dirty">Your first name is Required.</span>
						</div>
						<div class="col-12 nopadding" ng-hide="regForm.first_name.$invalid">
							<button class="btn btn-primary sidebar-btn" ng-click="proceedToStep(7)">Next</button>
							<label>Press Enter</label>
						</div>
					</div>

					<div class="col-12 col-md-8  section nopadding slider-sections" ng-if="rdSteps['registration']['step1']['status'] && selected_account == 'candidate'" ng-class="{'' : rdSteps['registration']['current_step'] != 7}" id="registration_7" step="7">
						<div class="form-group">
							<label class="slider-menu-label">Hi {{regData.first_name}}, what is your last name? </label>
							<br>
							<input press-enter proceed-to="8" class="form-control" ng-model="regData.last_name" type="text" name="last_name" ng-required="true" placeholder="Last Name">
                        	<span class="help-block" ng-show = "regForm.last_name.$invalid && regForm.last_name.$dirty">Your last name is required.</span>
						</div>
						<div class="col-12 nopadding" ng-hide="regForm.last_name.$invalid">
							<button class="btn btn-primary sidebar-btn" ng-click="proceedToStep(8)">Next</button>
							<label>Press Enter</label>
						</div>
					</div>


					<div class="col-12 col-md-8  section nopadding slider-sections"  ng-class="{'' : rdSteps['registration']['current_step'] != 8}" id="registration_8" step="8">
						<div class="form-group">


							<label style="margin-bottom: 0px !important;" ng-show="selected_account == 'candidate'" class="slider-menu-label">Tell us about yourself. </label>
							<label style="margin-bottom: 0px !important;" ng-show="selected_account == 'job_provider'" class="slider-menu-label">Brief company description. </label>
							<label ng-show="selected_account == 'job_provider'"  class="subtle-hint">This will be displayed publicly to Anesthesiologist and CRNAs.</label>

							<br>
							<textarea press-enter proceed-to="8" style="height: 100px;margin-top: 17px" placeholder="Enter your description here." name="description" class="form-control" ng-model="regData.description" ng-required="true"></textarea>
                        	<span class="help-block" ng-show = "regForm.description.$invalid && regForm.description.$dirty">A brief description is required</span>
						</div>

						<div class="col-8 nopadding" ng-hide="regForm.description.$invalid">
							<button class="btn btn-primary sidebar-btn" ng-click="proceedToStep(10);addClass($event)">Next</button>
							<label>Press Next</label>
						</div>
					</div>

					
					<div class="col-12 col-md-8  section nopadding slider-sections"  ng-class="{'' : rdSteps['registration']['current_step'] != 10}" id="registration_10" style="margin-bottom: 180px !important;">
						<div class="radio-holder" style="margin-bottom: 50px;">
                        	<br/>		                                    
                            <input name="reg-privacy-register" type="checkbox" id="reg-privacy-label"/>
                        	<label for="reg-privacy-label" class="privacy-label">I have read & accept the <a show-policy><span style="color: #6684f2;">Privacy Policy</span></a> and <a show-terms><span style="color: #6684f2;">Terms of Use</span></a></label>
                        </div>
						<button style="width: 200px !important;margin-left: 20px;" class="anesthesia-save btn btn-primary float-left"  form-name="regForm" regloading-button ng-click="submitRegistrationForm()">Register Now</button>

						<button style="width: 200px !important;margin-left: 20px" class="anesthesia-cancel btn btn-primary float-left"  >Cancel</button>
					</div>


					<div  class="col-12 nopadding " id="registration_100" step="100" style="height: 80px">
						
					</div>	

		
				</div>
				</form>	
				@endverbatim

				<section class="slider-footer float-left" style="display: none">
					<div class="col-12 col-md-5">
						<label>100% completed</label>
						<div class="progress">
	                        <div class="progress-bar bg-danger" style="width: 60%; height:6px;" role="progressbar"> <span class="sr-only">60% Complete</span> </div>
	                    </div>
	                </div>

	                <div class="col-7 float-right" style="text-align: right">
	                	<button type="button" class="btn btn-info btn-circle btn-sm"><i class="fa fa-arrow-down"></i> </button>
	                	<button type="button" class="btn btn-info btn-circle btn-sm "><i class="fa fa-arrow-up"></i> </button>
	                </div>
				</section>
		</section>    
		           
	</div>            
</div>		