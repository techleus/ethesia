<style type="text/css">
	.job_filter_sidebar{
		padding-top: 50px;
	}
	.close-register-form{
		color: #333;
		font-weight: bold;
	}
	.show-states{
		opacity: 0;
		height: 0;
	}
	.show-states.fadeIn{
		margin-top: 10px;
		padding:10px;
		height: 250px;
		overflow-y: scroll;
		border: 1px solid #e7e7e7;
	}
	.state-content{		
		padding: 0;	
	}
	
	.state-content li{
		list-style: none;
		display: inline-block;		
		padding-top: 5px;
		padding-bottom: 5px;
	}
	.state-content li label{
		padding-left: 5px;
		padding-right: 5px;
		text-indent: 20px;
		font-size: 14px;
	}
	/*.defaultchkbox[type="checkbox"]:not(:checked), .defaultchkbox[type="checkbox"]:checked {
	    position: static; 
	    left: 0;
	    opacity: 1;
	}*/
	/*global checkbox*/
	[type="checkbox"] + label::before,
	[type="checkbox"]:not(.filled-in) + label::after {	    	    
	    border-radius: 100%;
	    border: 2px solid #614CF9;
	}	
	[type="checkbox"]:checked + label:before{
		border: 2px solid #614CF9 !important;
		width: 18px;
    	height: 18px;    	
    	position: absolute;
    	top: 0;
    	left: 0;
    	opacity: 1;
    	-webkit-transform: rotate(0deg); 
	    -ms-transform: rotate(0deg);
	     transform: rotate(0deg); 
	}
	[type="checkbox"]:not(.filled-in) + label:after{
		transform: none;
	}
	[type="checkbox"]:checked + label:before{
		border: 2px solid #614CF9 !important;
	}
	[type="checkbox"]:checked + label:after {
	    background-color: #614CF9 !important;	    
	    position: absolute;
	    top: 2px;
	    left: 2px;
	    width: 14px;
    	height: 14px; 	    
	    border: 2px solid #fff;	    
	    -webkit-animation: ripple 0.2s linear forwards;
	    animation: ripple 0.2s linear forwards;
	}	
	/*default checkbox*/
	.defaultchkbox[type="checkbox"] + label::before,
	.defaultchkbox[type="checkbox"]:not(.filled-in) + label::after {	    	    
	    border-radius: 100%;
	    border: 2px solid #614CF9;
	}	
	.defaultchkbox[type="checkbox"]:checked + label:before{
		border: 2px solid #614CF9 !important;
		width: 18px;
    	height: 18px;    	
    	position: absolute;
    	top: 0;
    	left: 0;
    	opacity: 1;
    	-webkit-transform: rotate(0deg); 
	    -ms-transform: rotate(0deg);
	     transform: rotate(0deg); 
	}
	.defaultchkbox[type="checkbox"]:not(.filled-in) + label:after{
		transform: none;
	}
	.defaultchkbox[type="checkbox"]:checked + label:before{
		border: 2px solid #614CF9 !important;
	}
	.defaultchkbox[type="checkbox"]:checked + label:after {
	    background-color: #614CF9 !important;	    
	    position: absolute;
	    top: 2px;
	    left: 2px;
	    width: 14px;
    	height: 14px; 	    
	    border: 2px solid #fff;	    
	    -webkit-animation: ripple 0.2s linear forwards;
	    animation: ripple 0.2s linear forwards;
	}
	#stateinput{
		border-radius: 20px;		
	}	
	#btn-showstates{
		height: 100%;		
		background: #614CF9;
		width: 100%;
		border-radius: 20px;		
	}
	#table-states{
		margin-top: 10px;
	}
	div.checkbox{
		padding: 0;
	}
	#table-states tr td{
		border: 0;
		padding-left: 0;
		padding-top: 0;
		padding-bottom: 0;
	}
	button.btn-refine{
		background: #614CF9;		
	}
	#list-states{
		list-style: none;
		padding: 0;
	}
	#list-states li{
		display: inline-block;
		min-width: 150px;
		width: calc(100% / 4);
		overflow: hidden;
  		text-overflow: ellipsis;
  		padding-bottom: 5px;
	}
</style>
<div class="slider-form" id="filter-jobs-form">
	<div class="slider-form-close hidden pointer" id="close-slider-form">
		<i class="mdi mdi-close"></i>
	</div>	
        
	<div class="job_filter_sidebar" style="background-color: #fff;">
		
		<input type="hidden" id="states_input" name="">
		<section id="wrapper" class="job-sidebar-gray noborder" custom-scrollbar style="background-color: #fff;">                      
			
			<div class="fields_wrap">								
				
				<h2> Select States</h2>
				<br>				
				<ul id="list-states">
					@verbatim
					<li ng-repeat = "state in states">
						<div class="checkbox" style="position:relative;">
							<input filter-states type="checkbox" class="defaultchkbox select-state" value="{{states[$index].text}}" name="state[]" id="state_{{states[$index].text | lowercase}}" check-filter>
							<label set-filter for="state_{{states[$index].text | lowercase}}">{{ states[$index].text }}</label>						
						</div>	
					</li>					
					@endverbatim
				</ul>														
			</div>  

			<div class="fields_wrap clearFix margin_top_20">
				<h2> Employer Account Type</h2>				
				<ul class="fields_wrap">
					<li><input id="account_types_0" ng-change="processCheckboxAccount(jobsData.account_types.all, 'account_types')" ng-model="jobsData.account_types.all"  value="All" class="" type="checkbox"> <label for="account_types_0">All</label></li>
					<li><input ng-change="processCheckboxAccount(jobsData.account_types.staffing_firm)" ng-model="jobsData.account_types.staffing_firm" id="account_types_1" value="1"  type="checkbox"> <label for="account_types_1" set-account>Staffing Firm</label></li>
					<li><input ng-change="processCheckboxAccount(jobsData.account_types.hospital_surgery)" ng-model="jobsData.account_types.hospital_surgery" id="account_types_2" value="2"  type="checkbox"> <label for="account_types_2" set-account>Hospital or Surgery Center</label></li>					
					<li><input ng-change="processCheckboxAccount(jobsData.account_types.anesthesia_group)" ng-model="jobsData.account_types.anesthesia_group" id="account_types_3" value="3"  type="checkbox"> <label for="account_types_3" set-account>Anesthesia Group</label></li>
					<li><input ng-change="processCheckboxAccount(jobsData.account_types.management_firm)" ng-model="jobsData.account_types.management_firm" id="account_types_4" value="4"  type="checkbox"> <label for="account_types_4" set-account>Management Firm</label></li>
					<li><input ng-change="processCheckboxAccount(jobsData.account_types.individual)" ng-model="jobsData.account_types.individual" id="account_types_5" value="5"  type="checkbox"> <label for="account_types_5" set-account>Individual</label></li>
				</ul>														
			</div>     

			<div class="fields_wrap">                          												
				<h2>Date Posted</h2>				
				<ul class="fields_wrap">
					<li>
						<input ng-change="processCheckboxDatePosted(jobsData.date_posted.all, 'date_posted')" ng-model="jobsData.date_posted.all" id="date_posted_All" value="All"  type="checkbox"> <label for="date_posted_All">All</label>
					</li>	
					<li>
						<input ng-change="processCheckboxDatePosted(jobsData.date_posted.last_hour)" ng-model="jobsData.date_posted.last_hour" id="date_posted_1" value="1"  type="checkbox"> <label for="date_posted_1">Last Hour</label>
					</li>
					<li>
						<input ng-change="processCheckboxDatePosted(jobsData.date_posted.last_24)" ng-model="jobsData.date_posted.last_24" id="date_posted_24" value="24"  type="checkbox"> <label for="date_posted_24">Last 24 Hours</label>
					</li>					
					<li>
						<input ng-change="processCheckboxDatePosted(jobsData.date_posted.last_7)" ng-model="jobsData.date_posted.last_7" id="date_posted_7" value="7"  type="checkbox"> <label for="date_posted_7">Last 7 days</label>
					</li>
					<li>
						<input ng-change="processCheckboxDatePosted(jobsData.date_posted.last_14)" ng-model="jobsData.date_posted.last_14" id="date_posted_14" value="14"  type="checkbox"> <label for="date_posted_14">Last 14 days</label>
					</li>
					<li>
						<input ng-change="processCheckboxDatePosted(jobsData.date_posted.last_39)" ng-model="jobsData.date_posted.last_39" id="date_posted_30" value="30"  type="checkbox"> <label for="date_posted_30">Last 39 days</label>
					</li>
				</ul>				 	
			</div> 

			<div class="fields_wrap">
				<h2>Vacancy Type</h2>    				
				<ul class="fields_wrap">
					<li><input ng-change="processCheckboxVacancy(jobsData.vacancy_type.all, 'vacancy_type')" ng-model="jobsData.vacancy_type.all" id="vacancy_type_All" value="All"  type="checkbox"> <label for="vacancy_type_All">All</label></li>
					<li><input ng-change="processCheckboxVacancy(jobsData.vacancy_type.full_time)" ng-model="jobsData.vacancy_type.full_time" id="vacancy_type_Full_Time" value="Full Time"  type="checkbox"> <label for="vacancy_type_Full_Time" set-filter>Full Time</label></li>
					<li><input ng-change="processCheckboxVacancy(jobsData.vacancy_type.locum_tenens)" ng-model="jobsData.vacancy_type.locum_tenens" id="vacancy_type_Locum_Tenens" value="Locum Tenens"  type="checkbox"> <label for="vacancy_type_Locum_Tenens" set-filter>Locum Tenens</label></li>
					<li><input ng-change="processCheckboxVacancy(jobsData.vacancy_type.part_time)" ng-model="jobsData.vacancy_type.part_time" id="vacancy_type_Part_Time" value="Part Time"  type="checkbox"> <label for="vacancy_type_Part_Time" set-filter>Part Time</label></li>
					<li><input ng-change="processCheckboxVacancy(jobsData.vacancy_type.prn)" ng-model="jobsData.vacancy_type.prn"  id="vacancy_type_PRN" value="PRN"  type="checkbox"> <label for="vacancy_type_PRN" set-filter>PRN</label></li>
					<li><input ng-change="processCheckboxVacancy(jobsData.vacancy_type.fellowship)" ng-model="jobsData.vacancy_type.fellowship" id="vacancy_type_Fellowship" value="Fellowship"  type="checkbox"> <label for="vacancy_type_Fellowship" set-filter>Fellowship</label></li>
				</ul>                          								
			</div>
			<div class="fields_wrap">
				@if(isset($landing))
					<button ng-click="redirectToBroseJobs()" class="btn btn-refine fontPoppins hover-animate" animate-hover>
						<span>REFINE SEARCH</span>
					</button>
				@endif
			</div>									 
			 				                        
		</section>                
	</div>            
</div>		
