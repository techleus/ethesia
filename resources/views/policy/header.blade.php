<div class="header_container">	
	<div class="menu_container" id="myTopnav">
		<div class="menu_left">
			<div class="logo_wrap">
			<a href="/browse-jobs/{{session('type')}}" class="nav_logo"><img src="/public/assets/images/jobs-img/anesthesia-logo-2.png"></a>
			<a href="javascript:void(0);" class="mobile_icon" onclick="myNavToggle()">&#9776;</a>			
			</div>
			<div class="menu_wrap">
				<ul class="topmenu">
					<li class="current"><a href="/browse-jobs/{{session('type')}}" class="curr_browse">BROWSE JOBS</a></li>
					<li><a href="#">BROWSE RESUMES</a></li>
					<li><a href="/browse-employers/{{session('type')}}">BROWSE EMPLOYERS</a></li>	
					<li><a href="#">CONTACT</a></li>											
				</ul>				
			</div>
			<div class="clearfix"></div>
		</div>

		<div class="menu_right">
			@if(!Auth::check())
				<a href="/application" class="login_btn">Log in</a> 
				<a href="#" class="signup_btn">Sign Up</a>		
			@else
				<a href="/application" class="login_btn">Dashboard</a> 
			@endif						
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="search_container">
		<div class="stitle">Privacy Policy</div>
	</div>
	
	

</div>
