<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/public/assets/images/favicon.png">
    <title>Anesthesia - Browse Employer</title>	
	
	 <!-- Bootstrap Core CSS -->
    <link href="/public/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="/public/assets/material/css/style.css" rel="stylesheet">
    <link href="/public/assets/material/ss/colors/blue.css" id="theme" rel="stylesheet">
    <link href="/public/assets/plugins/sweetalert/sweetalert.css" id="theme" rel="stylesheet">	
	<link rel="stylesheet" type="text/css" href="/public/assets/css/style-public.css">
	<link rel="stylesheet" type="text/css" href="/public/assets/css/public-responsive.css">
	

    <link rel="stylesheet" type="text/css" href="/public/assets/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="/public/assets/bower_components/placeholder-loading/dist/css/placeholder-loading.min.css">
	<link rel="stylesheet" type="text/css" href="/public/assets/css/style-jobsidebar.css">
    
    <link rel="stylesheet" type="text/css" href="/public/assets/plugins/jquery-textext-master/src/css/textext.core.css" />
    <link rel="stylesheet" type="text/css" href="/public/assets/plugins/jquery-textext-master/src/css/textext.plugin.autocomplete.css" />
    <link rel="stylesheet" type="text/css" href="/public/assets/plugins/jquery-textext-master/src/css/textext.plugin.tags.css" />



</head>

<body ng-app="app" ng-controller ="BrowseJobController" >

    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div class="page-main-wrapper">
      <!-- header-menu-->
	  @include('policy.header')
	  <!--end-header-menu-->
	 

	   @yield('content')

		<!--END-page-container-->
				
	<div class="clearFix">&nbsp;</div>			

       
    	@include('policy.footer')
	
	</div>

	<!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,600" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Muli:400,700" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
	
    <script src="/public/assets/plugins/jquery/jquery.min.js"></script>
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.js'></script>

    <script src="/public/assets/plugins/jquery-textext-master/src/js/textext.core.js"></script>
    <script src="/public/assets/plugins/jquery-textext-master/src/js/textext.plugin.autocomplete.js"></script>
    <script src="/public/assets/plugins/jquery-textext-master/src/js/textext.plugin.filter.js"></script>
    <script src="/public/assets/plugins/jquery-textext-master/src/js/textext.plugin.tags.js"></script>
    <script src="/public/assets/plugins/jquery-textext-master/src/js/textext.plugin.ajax.js"></script>

    <script src="/public/assets/plugins/tagsinput/jquery.tagsinput.js"></script>	
	
    <!-- Bootstrap tether Core JavaScript -->
    <script src="/public/assets/plugins/bootstrap/js/tether.min.js"></script>
    <script src="/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="/public/assets/plugins/select2/js/select2.min.js"></script>
    <script type="text/javascript" src="/public/app/browse_jobs/us_states.json"></script>
    
    <script  type="text/javascript" src="/public/assets/plugins/sweetalert/sweetalert.min.js"></script>    
	
	<!-- Sticky Navbar -->
	<script  type="text/javascript" src="/public/assets/js/sticky_navbar.js"></script>   


    <script src="/public/app/browse_jobs/angular.min.js"></script>
    <script src="/public/app/app.js"></script>
    <script src="/public/app/browse_jobs/directive.js"></script>
    <script src="/public/app/browse_jobs/BrowseJobService.js"></script>
    <script src="/public/app/api.js"></script>
    <script src="/public/app/browse_jobs/BrowseJobController.js"></script>




</body>

</html>