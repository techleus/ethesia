<style type="text/css">
	.header_container{
		position: relative;
	}
	.header_container{
		
		background: #bc90f9; /* Old browsers */		    	
		background: url(../assets/images/jobs-img/bg-dots.png) no-repeat left top,-moz-linear-gradient(to right, #bc90f9 0%, #6e6af8 49%, #6e6af8 51%, #57d3fa 100%); /* FF3.6-15 */
		background: url(../assets/images/jobs-img/bg-dots.png) no-repeat left top,-webkit-linear-gradient(to right, #bc90f9 0%,#6e6af8 49%,#6e6af8 51%,#57d3fa 100%); /* Chrome10-25,Safari5.1-6 */
		background: url(../assets/images/jobs-img/bg-dots.png) no-repeat left top, linear-gradient(to right, #bc90f9 0%,#6e6af8 49%,#6e6af8 51%,#57d3fa 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */		
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#bc90f9', endColorstr='#57d3fa',GradientType=0 );
		border-bottom: 20px solid #fff;
		background-size: contain;	 
	}
	.wave1{
		width: 100%;
		position: absolute;
		bottom: 0;
		left: 0;		
		background: url(/public/assets/images/jobs-img/emp-divider.png) no-repeat center bottom -20px;
		height: 250px;		
		background-size: cover;
	}
	.wave2{
		width: 100%;
		position: absolute;
		bottom: 0;
		left: 100%;		
		background: url(/public/assets/images/jobs-img/emp-divider-opposite.png) no-repeat center bottom -20px;
		height: 250px;		
		background-size: cover;
	}
	.wave3{
		width: 150%;
		position: absolute;
		bottom: 0;
		left: 0;		
		background: url(/public/assets/images/jobs-img/emp-divider.png) no-repeat center bottom -20px;
		height: 250px;		
		background-size: cover;
	}
	.wave4{
		width: 200%;
		position: absolute;
		bottom: 0;
		left: 100%;		
		background: url(/public/assets/images/jobs-img/emp-divider-opposite.png) no-repeat center bottom -20px;
		height: 250px;		
		background-size: cover;
	}
	
	.wave2, .wave1, .wave3, .wave4{
		animation: move_wave 10s linear infinite;
	   -webkit-animation: move_wave 10s linear infinite;
	   -webkit-animation-delay: 1s;
	   animation-delay: 1s;
	}
	
	/**/
	@keyframes move_wave {
	    0% {
	        transform: translateX(0) translateZ(0) scaleY(1)
	    }
	    50% {
	        transform: translateX(-25%) translateZ(0) scaleY(0.55)
	    }
	    100% {
	        transform: translateX(-50%) translateZ(0) scaleY(1)
	    }
	}
	.waveWrapper {
	    overflow: hidden;
	    position: absolute;
	    left: 0;
	    right: 0;
	    bottom: 0;
	    height: 120px;	    
	    margin: auto;	    
	}
	.waveWrapperInner {
	    position: absolute;
	    width: 100%;
	    overflow: hidden;
	    height: 100%;
	    bottom: -1px;	    
	}
	.bgTop {
	    z-index: 15;
	    opacity: 0.5;
	}
	.bgMiddle {
	    z-index: 10;
	    opacity: 0.75;
	}
	.bgBottom {
	    z-index: 5;
	}
	.wave {
	    position: absolute;
	    left: 0;
	    width: 200%;
	    height: 100%;
	    background-repeat: repeat no-repeat;
	    background-position: 0 bottom;
	    transform-origin: center bottom;
	}
	.waveTop {
	    background-size: 50% 100px;
	}
	.waveAnimation .waveTop {
	  animation: move-wave 3s;
	   -webkit-animation: move-wave 3s;
	   -webkit-animation-delay: 1s;
	   animation-delay: 1s;
	}
	.waveMiddle {
	    background-size: 50% 120px;
	}
	.waveAnimation .waveMiddle {
	    animation: move_wave 10s linear infinite;
	}
	.waveBottom {
	    background-size: 50% 100px;
	}
	.waveAnimation .waveBottom {
	    animation: move_wave 15s linear infinite;
	}
	.searh-fields{		
		position: relative;
	}
	.search-dropdown{		
		width: 100%;
	}
	.search-dropdown ul{		
		width : 100%;
	}
	.txt-search-dd{
		padding-top: 12px;
		min-width: 300px;
	}	
	.job_type{
		line-height: 25px;
		width: 100%;
		cursor: pointer;
		border: none;
		padding-top: 3px;
		padding-left: 15px;
		font-family: 'Open Sans', sans-serif;
    	font-size: 14px;
    	background: transparent;
    	border-radius: 3px;
    	width: 0;
    	-webkit-transition: width 1s;
    	transition: width 1s;
    	color: transparent;
	}
	.job_type.leftin{
		width: 100%;
		color: #000;
	}
	.empty_jobtype{
		display: none;
	}
	.empty_jobtype.leftin{
		display: block;
		border-radius: 100%;
		position: absolute;
		top: 17px;
		right: 45px;
		height: 20px;
		width: 20px;
		font-size: 18px;
		line-height: 20px;
		border: 1px solid #fff;
		color: #4d18ff;
		text-align: center;
		background: #fff;
		cursor: pointer;
		padding-left: 1px;
	}
	.animatehide{
		width: 0;
		-webkit-transition: width 1s;	
	}
	.hidedp{
		display: none;
	}
	h1.search-title {
	    font-size: 60px;
	    width: auto;
	    color: #fff;
	    font-family: 'Montserrat', sans-serif;
	    font-weight: bold;
	    margin-bottom: 40px;
	}
	@media only screen and (max-width: 1052px){
		.search-dropdown ul{		
			width : calc(100% - 30px);
		}
	}
</style>
<div class="header_container">		
	<div class="menu_container" id="myTopnav" style="z-index: 100;">
		<div class="menu_left">
			<div class="logo_wrap">
			<a href="/" class="nav_logo"><img src="/public/assets/images/jobs-img/anesthesia-logo-2.png"></a>
			<a href="javascript:void(0);" class="mobile_icon" onclick="myNavToggle()">&#9776;</a>			
			</div>
			<div class="menu_wrap">

				<ul class="topmenu">
					<li class="current"><a href="/browse-jobs/{{Session::get('type')}}" class="curr_browse">BROWSE JOBS</a></li>
					<!--<li><a href="#">BROWSE RESUMES</a></li>-->
					<li>
						@if(Auth::check())
							@if(Auth::User()->userType != 2)
								<a class="curr_post" href="javascript:;" modal="loginModal" anesthesia-modal modal-message="Please sign in as a Provider.">POST A JOB</a>
							@else
								<a class="curr_post" href="/application/#/jobs_list">POST A JOB</a>
							@endif
						@else
							<a class="curr_post" href="javascript:;" modal="loginModal" anesthesia-modal modal-message="Please sign in as a Provider.">POST A JOB</a>
						@endif						
					</li>	
					<li><a href="/contact-us">CONTACT</a></li>											
				</ul>				
			</div>
			<div class="clearfix"></div>
		</div>

		<div class="menu_right">
			@if(!Auth::check())
				<a modal="loginModal" modal-message="Login to your dashboard." anesthesia-modal class="login_btn pointer">Log in</a> 
				<a show-slider="" slider-width="873px" id_target = "registration-form" class="signup_btn pointer hover-animate" animate-hover>
					<span>Sign Up</span>
				</a>		
			@else
				<a href="/application" class="login_btn">Dashboard</a> 
			@endif						
		</div>
		<div class="clearfix"></div>
	</div>
	
	
	<div class="search_container animated" style="z-index: 100;">
		<!-- <div class="search-title text-center">Browse Anesthesia Jobs</div> -->
		<h1 class="search-title text-center">Browse Anesthesia Jobs</h1>		
		<ul class="fields_container">				
			<li class="searh-fields">				
				<i class="fa fa-user-o icon-search"></i>
				<div class="txt-search-dd pointer" show-jobtype>
					<input type="text" class="job_type" ng-blur="unfocusType()" ng-model="jobsData.job_type" show-dropsearch readonly name="" placeholder="Select profession">
				</div>
				@verbatim
				<span class="empty_jobtype" ng-if="jobsData.job_type != ''" clear-jobtype>&times;</span>
				@endverbatim
 				<div class="search-dropdown hidedp" id="dropdown-search"> 					
					<ul class="search-results-wrapper">						
						<li ng-click="populateTypeSearch('Anesthesiologist')" set-jobtype class="li-results" id="Anesthesiologist">Anesthesiologist</li>
						<li ng-click="populateTypeSearch('CRNA')" set-jobtype class="li-results" id="CRNA">CRNA</li>
					</ul>
				</div>
			</li>						
			<li class="hover-body">	
				<i class="mdi mdi-routes icon-search"></i>
				<input type="text" class="txt-search-dd-btn pointer" ng-model ="job_location" name="" placeholder="Enter facility state" enter-state>														
				<button form-name="" ng-click="redirectToBroseJobs()" animate-hover class="btn find_jobs_btn fontPoppins pointer text-center hover-animate" >
					<span>FIND JOBS</span>
				</button>									
				<div class="adv_search"><a class="pointer" ng-cloak show-slider="" id_target = "filter-jobs-form">Show advanced filters <img src="/public/assets/images/jobs-img/icon-plus.png"></a></div>
			</li>					
		</ul>
	</div>
	<div class="row animated" id="dots">
		<div class="col-lg-12" id="member-container">
			<div id="circle-member-1"></div>
			<div id="circle-member-2"></div>
			<div id="circle-member-3"></div>
			<div id="circle-member-4"></div>
			<div id="circle-member-5"></div>
			<div id="circle-member-6"></div>
			<div id="circle-member-7"></div>
			<div id="circle-member-8"></div>
		</div>
	</div>	
	<div class="waveWrapper waveAnimation"  style="z-index: 1;">
	  <div class="waveWrapperInner bgTop">
	    <div class="wave waveTop" style="background-image: url('/public/assets/images/candidate_slider.png');z-index:-1;"></div>
	  </div>
	  <div class="waveWrapperInner bgMiddle">
	    <div class="wave waveMiddle" style="background-image: url('/public/assets/images/wave-mid.png');z-index:-1;"></div>
	  </div>  
	  <div class="waveWrapperInner bgBottom">
	    <div class="wave waveBottom" style="background-image: url('/public/assets/images/wave-bot.png');z-index:-1;"></div>
	  </div>
	</div>
	<button style="z-index: 50;" class="btn btn-sm bg-white btn-circle" page-scroll id="btn-page-down"><img src="/public/assets/images/jobs-img/arrow-down.png"></button>
</div>



