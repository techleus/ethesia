@extends('homepage.index')

@section('content')

<main id="pagewrap">

	
	<input type="hidden" value="{{$landing}}"  id="isLanding" name="">
	<div class="row" id="first-row">
		<div class="col-sm-12 col-md-4 offset-md-1 col-lg-3 offset-lg-2" id="left-content" bounce-left row="first">
			<h1>Find Employers</h1>
			<hr/><br>
			<p>
				By searching for a specific facility type or company
				name,we’ve made it easier for you to find your ideal
				employer and get in touch with them.
			</p>
			<a href = "/browse-employers"><button class="btn" id="btn-emp-list">Employer List</button></a>
		</div>
		<div class="col-sm-12 col-md-6 col-lg-5">
			<div class="col-md-12 col-lg-12" id="right-content" bounce-right row="first">
				<ul>
					<li style="position: relative;">
						<input type="text" class="input-browse-employer" ng-model="jobsData.company_name" placeholder="Enter company name">
						<button loading-button animate-small callback="redirectToBrowseEmployers()" class="btn hover-animate text-center pointer" id="btn-browse-employer">
							<span>BROWSE</span>
						</button>
					</li>
					<li>
						<p>Apply privately to thousands of vacancies with one application</p>
						<a href="#">Join us today</a>

					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="row" id="second-row">
		<div class="col-sm-12 offset-sm-0 col-md-10 offset-md-1 col-lg-4 offset-lg-2" id="left-content" bounce-left row="second">
			<div class="col-sm-12 col-md-12 col-lg-11" id="feature-logo"></div>
		</div>
		<div class="col-md-8 offset-md-2 col-lg-4 offset-lg-0">
			<div class="col-lg-12" id="job-vacancy-container" bounce-right row="second">
				<ul>
					<li class="text-right">
						<h1>Fill your job</h1>
						<h1>vacancy now!</h1>
						<hr>
					</li>
					<li>
						<br>
						<br>
						<p>
							Efficient job Uploading tool enables fast and easy job postings,
							eliminating the unnecesarry time it takes to post a large quality of openings.
						</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="row" id="bg-reg-flow">
		<div class="col-sm-8 offset-sm-1 col-md-4 offset-md-1 col-lg-3 offset-lg-2" id="left-content" bounce-left row="third">
			<h1>It’s simple</h1>
			<h1>to get started</h1>
			<hr/><br>
			<p>
				Our user friendly and highly functional management console allows you to quickly edit,
				post as new, delete and renew either individual post or multiple jobs at the same time.
			</p>
		</div>
	</div>
	<div class="row" id="forth-row">
		<div class="col-sm-10 offset-sm-1 col-md-6 offset-md-3 col-lg-4 offset-lg-2" id="bg-shadow" bounce-left row="forth">
			<h1>Smarter. Faster. Better.</h1>
			<p>with Immediate posts, there are no time delays in submitting your job opening. </p>	
		</div>
		<div class="col-lg-4">
			<div class="col-lg-12" id="workforce" bounce-right row="forth">
				<ul>
					<li class="text-right">
						<h1>Connecting the</h1>
						<h1>Anesthesia workforce</h1>
						<hr>
					</li>
					<li>
						<br>
						<br>
						<p>
							Our SMS texting technology sends alerts to the employer when an inquiry is submitted,
							creating a more proative and communicative Anesthesia community.
						</p>
					</li>
					<li>
						@if(Auth::check())
						<button class="btn" id="join-now">Join now</button>
						@else
						<button class="btn" show-slider="" slider-width="873px" id_target = "registration-form" class="signup_btn pointer" id="join-now">Join now</button>
						@endif
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="row" id="bg-shadow-lg" bounce-in row="fifth">
		<div class="col-lg-8 offset-lg-2" id="benefit-content">
			<h1>Benefits for Anesthesia Providers</h1>
			<hr/><br>
			<ul>
				<li>
					<p>
					Our page exclusively tailored for CRNAs and Anesthesiologist to locate their next ideal opportunity.
					</p>
				</li>
				<li>
					<p>
					We have given complete control to the provider for what they want to share and how they want to be alerted.
					</p>
				</li>
				<li>
					<p>
					Customizable viewing based on your location/duration/job type preferences.
					</p>
				</li>
				<li>
					<p>
					Completely free to browse or to develop your own Anesthesia profile and always stay on top of the newest career opportunities.
					</p>
				</li>
			</ul>			
			@if(Auth::check())
			<button class="btn" id="learn-more">Register now</button>
			@else
			<button class="btn" show-slider="" slider-width="873px" id_target = "registration-form" class="signup_btn pointer" id="learn-more">Register now</button>
			@endif
		</div>
	</div>
	<div class="row" id="sixth-row">
		<div class="col-lg-8 offset-lg-2" id="info-container">
			<div class="text-center info-label">Hundres of Anesthesia Job Providers<br>use Anesthesia Community to fill their positions.</div>	
			@if(Auth::check())
			<button class="btn hover-animate" animate-hover class="signup_btn pointer" id="btn-join">Join Today</button>
			@else
			<button class="btn hover-animate" animate-hover show-slider="" slider-width="873px" id_target = "registration-form" class="signup_btn pointer" id="btn-join">
				<span>Join Today</span>
			</button>
			@endif
		</div>
		<div class="col-lg-10 offset-lg-1" id="vacancy-card" ng-show="false">
			<div class="vacancy-title">Browse vacancies by top Employers</div>
			<ul>
				<li>
					<img src="/public/assets/images/jobs-img/bg-top-emp.png">
					<div class="card-name">Zebra</div>
				</li>
				<li>
					<img src="/public/assets/images/jobs-img/bg-top-emp.png">
					<div class="card-name">Moontheme Studio</div>
				</li>
				<li>
					<img src="/public/assets/images/jobs-img/bg-top-emp.png">
					<div class="card-name">La Carolina</div>
				</li>
				<li>
					<img src="/public/assets/images/jobs-img/bg-top-emp.png">
					<div class="card-name">Mberak Designs</div>
				</li>
				<li>
					<img src="/public/assets/images/jobs-img/bg-top-emp.png">
					<div class="card-name">Logo text</div>
				</li>
				<li class="active">
					<img src="/public/assets/images/jobs-img/bg-top-emp-active.png">
					<div class="card-name">Invectra</div>
				</li>				
			</ul>
		</div>
	</div>								    	
</main>

<div class="slider-form-overlay hidden"></div>

@include("modals.login")
@include('slider.registration_slider')
@include('slider.filter-jobs-form')
@endsection