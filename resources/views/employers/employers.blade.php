@extends('employers.index')

@section('content')

<div class="page-container"><!--page-container-->
	@if(Auth::check())

	@if(Auth::User()->userType!=1)
	@include("modals.login")
	@endif
	@endif
	<div class="row row-job">

		@verbatim
		<div class="col-lg-3 col-md-6 col-lg-job set-appform-jobid" ng-repeat="employer in jobsEmployer">
		@endverbatim
			
			<!-- Card -->
			<div class="card-job-box card-col-margins">								
				<div class="card-pos-container">				   
				    <div class="cardnew-wrap" id="employers-card">
				    	@verbatim
						<div class="card-new-logo"><img src="/public/assets/images/users/1.jpg"></div>
						@endverbatim
						<div class="card-new-icons">							
							@if(Auth::check())

								@if(Auth::User()->userType==1)
									<button  type="button" ng-click="bookmarkProvider(employer.id,employer.user_profile.company_name,employer);$event.stopPropagation();" class="job-icon-btn no-slide  " ng-class="{'job-icon-red' : employer.user_job_provider_bookmark != null,'job-icon-btn' : employer.user_job_provider_bookmark == null }"><i class="fa fa-heart"></i></span> </button>
								@else
									<button  type="button" ng-click="$event.stopPropagation();" modal="loginModal" anesthesia-modal modal-message="Please sign in as a candidate." class="job-icon-btn no-slide"><i class="fa fa-heart"></i> </button>
								@endif
							@else
								<button  type="button" modal="loginModal" anesthesia-modal modal-message="Please sign in as a candidate." class="job-icon-btn no-slide  "><i class="fa fa-heart"></i> </button>
							@endif

							<button type="button" class="job-icon-btn"><i class="fa fa-share-alt"></i> </button>
						</div>
						
						<div class="card-box-new">												
							<div class="company-name"><span ng-bind="employer.user_profile.company_name"></div>
							<div class="user-account-title"><span class="label label-success" ng-bind="employer.user_account_title.user_account_title">loading</span>
							</div>											
							<div class="company-desc">
								<div><span class="card-content-desc" ng-bind="employer.user_profile.description | cut:true:125:'..'">loading employer description</span></div>								
							</div>
							<br>									
							<button class="btn btn-sm float-right" id="btn-readmore" ng-show="showHideBtnReadmore(employer.user_profile.description)>125">read more</button>
						</div>						
					</div>																							
				</div>	
				

			</div>
			<!-- Card -->
		</div>
		<!--ITEM 1-->			
		<!--<div class="col-lg-3 col-md-6 card-container" ng-repeat="employer in jobsEmployer">
			<div class="card marginBtm0">
				<div class="el-card-item">
					<div class="el-card-avatar el-overlay-1"> <img id="jobsimg" src="/public/assets/images/users/1.jpg" alt="user">
						<div class="el-overlay">
							<ul class="el-info">
								<li><a class="btn default btn-outline image-popup-vertical-fit" href="/public/assets/images/users/1.jpg"><i class="icon-magnifier"></i></a></li>
								<li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
							</ul>
						</div>
						<div class="el-card-content">
							<div class="card-content-info">
								<ul>
									<li>										
										<h3 class="box-title" ng-bind="employer.user_profile.company_name"></h3>										
									</li>									
									<li>
										<span class="label label-success" ng-bind="employer.user_account_title.user_account_title">loading</span>
									</li>
									<li>
										<span class="card-content-desc" ng-bind="employer.user_profile.description | cut:true:125:'..'">loading employer description</span>										
									</li>
								</ul>														
							</div>							
						</div>
					</div>					
					<button class="btn btn-sm float-right" id="btn-readmore" ng-show="showHideBtnReadmore(employer.user_profile.description)>125">read more</button>
				</div>
			</div>
		</div>-->
		<!--END-ITEM 1-->		
		
	</div>			
</div>

@endsection