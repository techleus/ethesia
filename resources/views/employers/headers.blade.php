<style type="text/css">
	@media only screen and (min-width: 1025px){
		.search_container ul.fields_container{
			padding-right: 0;
			position: relative;
			width: 40% !important;
		}	
	}
	
	.search_container ul.fields_container li{
		width: 100% !important;
	}	
</style>
<div class="header_container">	
	

	<div class="menu_container" id="myTopnav">
		<div class="menu_left">
			<div class="logo_wrap">
			<a href="/" class="nav_logo"><img src="/public/assets/images/jobs-img/anesthesia-logo-blue.png"></a>
			<a href="javascript:void(0);" class="mobile_icon" onclick="myNavToggle()">&#9776;</a>			
			</div>
			<div class="menu_wrap">
				<ul class="topmenu">
					<li><a href="/browse-jobs/{{session('type')}}">BROWSE JOBS</a></li>
					<!--<li><a href="#">BROWSE RESUMES</a></li>-->
					<li><a class="curr_post" href="javascript:;">POST A JOB</a></li>	
					<li><a href="/contact-us">CONTACT</a></li>											
				</ul>				
			</div>
			<div class="clearfix"></div>
		</div>

		<div class="menu_right">
			@if(!Auth::check())
				<a modal="loginModal" modal-message="Login to your dashboard." anesthesia-modal class="login_btn pointer">Log in</a> 
				<a href="#" class="signup_btn pointer hover-animate" animate-hover>
					<span>Sign Up</span>
				</a>		
			@else
				<a href="/application" class="login_btn pointer">Dashboard</a> 
			@endif						
		</div>
		<div class="clearfix"></div>
	</div>
	
	
	<div class="search_container">
		<div class="stitle">Browse Employers</div>
		<ul class="fields_container">
			<!--<li>
				<div class="label">Company Name</div>
				<div class="fields"><img src="/public/assets/images/jobs-img/icon-search16.png"> <input type="text" class="loc" ng-model="company_name" placeholder="Enter Company Name"> <button class="btn find_jobs_btn fontPoppins" ng-click="getJobEmployer(company_name)">SEARCH</button></div>
			</li>-->
			<li>
				<div class="label">Company Name</div>
				<input type="text" class="txt-search-dd-btn pointer" ng-model="company_name"  placeholder="Enter Company Name">
				<button ng-click="getJobEmployer(company_name)" class="btn find_jobs_btn fontPoppins hover-animate" animate-hover id="find_jobs_btn">
					<span>SEARCH</span>
				</button>
			</li>			
		</ul>
	</div>

</div>

