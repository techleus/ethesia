
@extends('invites.index')


@section('content')
<style type="text/css">
	.menu_right .login_btn {
	    color: #fff !important;
	}
</style>
<div class="page-container"><!--page-container-->
	<div class="row">

		<div class="col-md-12">
			<form name="passwordForm" novalidate="true">
			<div class="col-md-6 offset-md-3">
				<div class="form-horizontal" class="col-md-12">
					<div class="form-group text-center">
						<label>Create your Password</label>										
					</div>					
					<span class="help-success" ng-show = "successMessage.length > 0" ng-bind="successMessage"></span>

					<span class="help-error" ng-show = "errorMessage.length > 0" ng-bind="errorMessage"></span>
					
					<div class="form-group">
						<label>First name</label>
						<input ng-required="true" name="firstName" type="text" ng-model="invite.firstName" class="form-control">						
					</div>
					
					<div class="form-group">
						<label>Last name</label>
						<input ng-required="true" name="lastName" type="text" ng-model="invite.lastName" class="form-control">						
					</div>

					<div class="form-group">
						<label>New Password</label>
						<input ng-minlength="5"  ng-required="true" name="password" type="password" ng-model="invite.password" class="form-control">
						<div class="form-control-feedback" ng-show = "passwordForm.password.$invalid && passwordForm.password.$dirty">Password is Required or atleast 5 Characters.</div>
					</div>
					<div class="form-group">
						<label>Re-type Password</label>
						<input ng-minlength="5"  ng-required="true" name="confirm_password" type="password" ng-model="invite.confirm_password" class="form-control">

						<div class="form-control-feedback" ng-show = "invite.password !=  invite.confirm_password">Password did not match</div>


					</div>
					
					
					
					<button class="btn btn-primary text-white form-control update-password" type="button" ng-click="acceptInvitation({{$data['provider_id']}},'{{$data['invited_email']}}','{{$data['invitation_key']}}')">Accept Invitation</button>
				</div>
			</div>
			</form>																	
		</div>
	</div>
</div>



@endsection