<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//test routes


    
    Route::get('/verify/{code}/{user_id}','UsersController@verifyEmail');

    Route::get('send','testmailController@send');

    Route::get('/transferFields', 'AdminController@transferFields');
    Route::get('/transferUsers', 'AdminController@transferUsers');
    Route::get('/password/form-reset/{code}', 'PasswordResetController@newPasswordForm');

    Route::get('/accept-invite/{userid}/{inviteKey}','InvitesController@acceptInvite');

    Route::post('/registerInvitedUser','InvitesController@registerInvitedUser');


Route::group(['middleware' => 'web'], function () {
    Auth::routes();    

    Broadcast::routes();
    Route::get('/logmeout', 'LandingPageController@logmeout');


    /**
     * register invitation
     */

    Route::post('/invites/sendinvite','InvitesController@sendInvitation');

    /**
     * mailbox
     */

    Route::get('/mailbox/list','MailboxController@index');
    Route::get('/mailbox/sentitems','MailboxController@sentitems');
    Route::post('/mailbox/updatemail','MailboxController@updatemail');    
    Route::get('/mailbox/getUniqueUsersFromInbox','MailboxController@getUniqueUsersFromInbox');
    //registration


    Route::get('/register', 'RegistrationController@showRegistrationForm');
    Route::post('/emails/sendPasswordForgot', 'UsersController@sendPasswordForgot');
    Route::post('/password/new', 'PasswordResetController@newPassword');
    Route::post('/emails/resendAccoutVerification','UsersController@resendAccoutVerification');
    Route::get('/user/credentials','UsersController@credentials');    

    Route::post('/jobupload','excelController@importJobs');


    Route::get('/', 'LandingPageController@showLandingPage');
    Route::get('/login', 'LandingPageController@showLandingPage');
    Route::post('/public/jobs', 'LandingPageController@searchJobs');
    Route::get('/browse-jobs/{type}', 'LandingPageController@browseJobs');
    Route::get('/browse-jobs', 'LandingPageController@browseJobsV2');
    Route::post('/redirect/browse-jobs', 'LandingPageController@redirectBrowseJobs');
    Route::post('/redirect/browse-employer', 'LandingPageController@redirectBrowseJobs');
    Route::get('/job-posts/{id}', 'LandingPageController@previewJobPostById');
    Route::get('/application_form/{id}/{fullname}/{message}','LandingPageController@application_form');
    Route::get('/browse-jobs-list', 'LandingPageController@browseJobsList');

    Route::get('/public/account_types', 'LandingPageController@getAccountTypes'); 
    Route::get('/user/profile/{id}/{name}', 'UsersController@getProfileImageForPdf');
    Route::get('/browse-employers', 'LandingPageController@browseEmployers');
    Route::get('/pdf/generate/job/{id}', 'PdfController@generateJobPdf');
    Route::get('/user/check/login', 'UsersController@checkLogin');
    Route::get('/policy', 'LandingPageController@showPolicy');
    Route::get('/terms-of-use','LandingPageController@showTermsOfUse');

    /*public contact list page*/
    
    Route::get('/contact/types','UserContactController@userContactType');
    Route::get('/contact/userlist','UserContactController@UserList');
    Route::post('/contact/searchUserlist','UserContactController@searchUserlist');
    Route::post('/contact/addtocontacts','UserContactController@addtocontacts');
    Route::get('/contact/showUserContactList','UserContactController@showUserContactList');
    Route::post('/contact/showUserContactList','UserContactController@showUserContactList');
    Route::post('/contact/removecontact','UserContactController@removeContact');
    Route::get('/contact/countcountacts', 'UserContactController@countcountacts');    
    Route::post('/contact/showcontactbylabel', 'UserContactController@showcontactbylabel');
    Route::post('/contact/up_notif_settings','UserContactController@up_notif_settings');
    
    /*Contact us page*/
    Route::get('/contact-us',function(){
        return view("contact.content");
    });

    Route::get('/sendSMS','testSMSnotification@sendSMS');

    Route::post('/contact-us/message','ContactUsController@message');
    Route::post('/admin/messages','ContactUsController@messages');
    /*mails*/
    Route::get('/testMail', 'UsersController@testMail');
    /*chat*/
    Route::post('/chat/message', 'ChatController@sendMessage');
    Route::post('/chat/message/searchContacts', 'ChatController@searchContacts');
    Route::get('/chat/getConversations', 'ChatController@getConversations');
    Route::post('/chat/openConversation', 'ChatController@openConversation');
    Route::post('/chat/message/openConvoFromUser', 'ChatController@openConvoFromUser');
    Route::get('/token', 'TokenController@token');

    /*Bookmark*/
    Route::post('/browse-jobs/bookmark','BookmarkController@bookmark');
    Route::get('/public/bookmarkedJobs','BookmarkController@bookmarkedJoblist');
    /*landing current user data*/
    Route::get('/public/current_user','LandingPageController@getUserData');
    Route::get('/public/get_job_userid','LandingPageController@get_job_userid');
    Route::post('/public/uploadInquirerCV','LandingPageController@uploadInquirerCV');
    Route::post('/public/get_employers','LandingPageController@getEmployers');
    Route::get('/public/joblist','LandingPageController@joblist');
    Route::post('/public/bookmarkJobProvider','LandingPageController@bookmarkJobProvider');

    Route::get('/user/account_types', 'UsersController@userTypes');
    Route::get('/user/account_titles', 'UsersController@userTitles');
    Route::post('/user/new', 'UsersController@newUser');
    Route::post('/user/update', 'UsersController@updateUser');
    Route::get('/user/getProfile', 'UsersController@getProfile');
    Route::post('/user/card/update', 'UsersController@updateUserCard');
    Route::get('/user/getBillingDetails/{id}', 'UsersController@getBillingDetails');
    Route::post('/user/echeck/update', 'UsersController@updateUserECheck');
    Route::get('/user/getEChecksDetails', 'UsersController@getEchecksDetails');
    Route::get('/user/credit_cards/list', 'UsersController@getCreditCards');
    Route::get('/user/e_checks/list', 'UsersController@getCheckList');
    Route::get('/user/cards/remove/{id}', 'UsersController@removeCard');
    Route::get('/user/echecks/remove/{id}', 'UsersController@removeChecks');
    Route::post('/users/upload/photo', 'UsersController@uploadPhoto');
    Route::post('/user/cardupdate', 'UsersController@cardupdate');
    Route::post('/profile/secondarymail','UsersController@secondarymail');
    Route::get('/profile/getSecondarymail','UsersController@getSecondarymail');
    Route::post('/profile/proceedupdatesecemail','UsersController@proceedupdatesecemail');
    Route::post('/profile/deleteSecEmail','UsersController@deleteSecEmail');


    Route::post('/user/newPassword', 'UsersController@newPassword');
    Route::get('/user/test/testStripe', 'UsersController@testStripe');
    Route::get('/user/createCard', 'UsersController@createCard');
    Route::post('/user/basicInfo/update', 'UsersController@updateBasicInfo');
    Route::post('/user/socialInfo/update', 'UsersController@updateSocialInfo');
    Route::post('/user/extraInfo/update', 'UsersController@updateExtraInfo');
    Route::post('/user/education/new', 'UsersController@newEducation');
    Route::get('/user/getUserEducations', 'UsersController@getUserEducations');
    Route::get('/user/education/remove/{id}', 'UsersController@removeEducationById');
    Route::post('/user/experience/new', 'UsersController@newExperience');
    Route::get('/user/getUserExperience', 'UsersController@getUserExperience');
    Route::get('/user/experience/remove/{id}', 'UsersController@removeExperience');


    Route::get('/user/get/applicants', 'UsersController@getApplicants');

    Route::get('/notifications/list', 'NotificationsController@getNotifications');
    Route::get('/notifications/preview/{notif_id}', 'NotificationsController@getNotifById');

    Route::get('/downloads/resume/{user_id}/{file_name}', 'DownloadsController@downloadResume');




    Route::get('/jobs/priorities', 'JobsController@getPriorities');
    Route::post('/jobs/post/new', 'JobsController@newJob');
    Route::post('/jobs/list', 'JobsController@jobList');
    Route::get('/jobs/getJobById/{id}', 'JobsController@getJobById');
    Route::get('/jobs/post/confirm/{id}', 'JobsController@confirmJob');
    Route::get('/jobs/post/reactivate/{id}', 'JobsController@confirmJob');
    Route::post('/jobs/search', 'JobsController@searchJobs');
    Route::get('/jobs/deactivate/{id}', 'JobsController@deactivateJob');
    Route::get('/jobs/remove/{id}', 'JobsController@deleteJob');
    Route::get('/jobs/reactivate/{id}', 'JobsController@restoreJob');
    Route::post('/jobs/multiple-deactivate', 'JobsController@multipleDeactivate');
    Route::post('/jobs/multiple-restore', 'JobsController@multipleRestore');
    Route::post('/jobs/multiple-delete', 'JobsController@multipleDelete');
    Route::post('/jobs/upgrade', 'JobsController@upgradeJobs');
    Route::post('/jobs/inqueries','JobsController@inqueries');
    Route::post('/jobs/appresponse','JobsController@appresponse');
    Route::post('/jobs/attachments','JobsController@attachments');



    /*plans*/
    //Route::get('/plans/list', 'PlansController@getPlans');
    Route::post('/plans/update', 'PlansController@updatePlans');
    Route::get('/plans/getOldCards', 'PlansController@getOldCards');
    Route::post('/plans/getNextBillingDate', 'PlansController@getNextBillingDate');
    Route::get('/plans/getUserPlans', 'PlansController@getUserPlans');
    Route::post('/plans/packagenewcard','PlansController@packagenewcard');
    Route::get('/plans/list','PlansController@postingPlans');
    Route::post('/plans/packageexistingcard','PlansController@packageexistingcard');
    Route::get('/getUserPlans','PlansController@getUserPlans');


	Route::get('/cv/priorities', 'CvController@getPriorities');
	Route::get('/manage/jobs', 'ManageJobsController@jobList');
	Route::get('/manage/getJobById/{id}', 'ManageJobsController@getJobById');
	Route::get('/manage/jobs/approved/{id}', 'ManageJobsController@approveJob');
    Route::get('/manage/candidates', 'ManageJobsController@candidateList');

    Route::get('/manage/freeposting','ManageJobsController@freeposting');
    Route::post('/manage/disablefreeposting','ManageJobsController@disablefreeposting');

    Route::get('/preferences/contact_method', 'PreferencesController@contactMethod');
    Route::get('/preferences/position_duration', 'PreferencesController@positionDuration');
    Route::post('/cv/post/new', 'CvController@saveCv');
    Route::get('/cv/list', 'CvController@getCvs');
    Route::get('/cv/getCvById/{id}', 'CvController@getCvById');

    Route::post('/prorated/details', 'PlansController@proratedDetails');

    Route::post('/saveStateCity', 'LandingPageController@saveStateCity');
    Route::get('/searchCity', 'LandingPageController@searchCity');
    Route::post('/jobs/repost','LandingPageController@repostJobs');

    /*emails*/
    Route::get('/emails/notif_settings','EmailsController@getNotifSettings');
    Route::post('/emails/change_notif_settings','EmailsController@changeMailSettings');

    Route::post('/emails/sendConfirmationEmail', 'EmailsController@sendConfirmationEmail');
    Route::get('/emails/view/{{view_name}}', 'EmailsController@showEmailTemplate');
    Route::get('/storagecheck/{userid}/{filename}', function ($userid, $filename)
    {
        $path = storage_path('app/public/uploads/cv/'. $userid .'/' . $filename);
        if (!File::exists($path)) {
            return "failed";
        }else{
            return "success";
        }
        
    });
    Route::get('/storage/{userid}/{filename}', function ($userid, $filename)
    {
        $path = storage_path('app/public/uploads/cv/'. $userid .'/' . $filename);
        if (!File::exists($path)) {
            abort(404);
        }
        return response()->download($path);
    });

    Route::get('/storage/attachment/{userid}/{filename}',function($userid, $filename){
        
        $path = storage_path('app/public/appmessages/attachments/'.$userid.'/'. Auth::User()->id .'/'.$filename);
        if (!File::exists($path)) {
            //abort(404);
        }
        return response()->download($path);        
    });

    Route::get('/storage/sentattachment/{userid}/{filename}',function($userid, $filename){
        $path = storage_path('app/public/appmessages/attachments/'. Auth::User()->id .'/'. $userid .'/'.$filename);
        if (!File::exists($path)) {
            //abort(404);
        }
        return response()->download($path);        
    });
});

