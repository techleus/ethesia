/*window.onscroll = function() {myStickyNavbar()};

var navbar = document.getElementById("mynavbar");
var sticky = navbar.offsetTop;

function myStickyNavbar() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky_navbar");	
  } else {
    navbar.classList.remove("sticky_navbar");	
  }
}*/

var num = 0; //number of pixels before modifying styles

$(window).bind('scroll', function () {
    if ($(window).scrollTop() > num) {
        $('#myTopnav').addClass('sticky_navbar');
    } else {
        $('#myTopnav').removeClass('sticky_navbar');
    }
});

function myNavToggle() {
    var x = document.getElementById("myTopnav");
    if (x.className === "menu_container") {
        x.className += " responsive";
    } else {
        x.className = "menu_container";
    }
}