$(function(){
	var body = $(window);
	var container = $('.page-container')
	var bodyHeight = body.height() / 2;
	alert(bodyHeight)
	container.append(
		'<div id="loginModal" class="modal">' +
			'<div class="modal-content">' +
				'<span class="loginModalClose">&times;</span>' +
				'<h1>Please Sign In</h1>' +
				'<p class="label-signin">Sign in here to bookmark this job</p>' +
				'<input type="text" class="form-control form-control-lg" id="loginEmail" placeholder="Email Address">'+
				'<input type="text" class="form-control form-control-lg" id="loginPassword" placeholder="Password">'+
				'<div class="row">'+				
					'<div class="col">'+
						'<input type="checkbox" class="remember_user" id="remember_user" name="remember_user" />' + 
					    '<label for="remember_user">Remember me</label>' +			
				    '</div>'+			    
				    '<div class="col">'+
					    '<a href="#" class="float-right" id="forgot_pass">Forgot password?</a>' +
				    '</div>'+
			    '</div>'+			    
				'<br/><button class="btn btn-lg">Login</button>'+
				"<br/><p class='label-signup'>Don't have an account?<a href='#'> Sign up now</a></p>" +	
			'</div>'+			
		'</div>');

	
	var modal = document.getElementById('loginModal');		
	var span = document.getElementsByClassName("loginModalClose")[0];	
	$('button#btn-loginModal').click(function(){
		modal.style.display = "block";				
		var modalContentHeight = $('.modal-content').height();
		var modalMarginTop = bodyHeight - modalContentHeight;		
	    
	})
	
	span.onclick = function() {
	    modal.style.display = "none";
	}	
	window.onclick = function(event) {
	    if (event.target == modal) {
	        modal.style.display = "none";
	    }
	}	
})