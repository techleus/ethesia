
app.service('$api', function($http, $q) {

	var pipe = {};
	var editData = {};

    pipe.post = function(data, url) {
    	var deferred = $q.defer();
    	var postData = pipe.arrayToParams(data);
		$http({
	        method : "POST",
	        url : url,
	        data: postData
	    }).then(function mySuccess(response) {
	        deferred.resolve(response);
	    }, function myError(response) {
	    	return deferred.promise;
	    });
	    return deferred.promise;
	}
	pipe.post2 = function(data, url) {
    	var deferred = $q.defer();
    	var postData = pipe.arrayToParams(data);
		$http({
	        method : "POST",
	        url : url,
	        data: postData
	    }).then(function mySuccess(response) {
	        deferred.resolve(response);
	    }, function myError(response) {
	    	console.log(response)
	    });
	    return deferred.promise;
	}

	

	pipe.post_file = function(data, url) {
    	var deferred = $q.defer();
    	//var postData = pipe.arrayToParams(data);
    	console.log(data.file)
		$http({
	        method : "POST",
	        url : url,
	        data: data,
	        headers: { 'Content-Type': undefined},
	        transformRequest: function(data) {
	            var formData = new FormData();
	            formData.append("image", data.file[0]);
	            return formData;
	        },
	    }).then(function mySuccess(response) {
	        deferred.resolve(response);
	    }, function myError(response) {
	    	console.log(response)
	    });

	    return deferred.promise;
	}


	pipe.upload_apply = function(data, url) {
    	var deferred = $q.defer();
    	//var postData = pipe.arrayToParams(data);
    	console.log(data)
		$http({
	        method : "POST",
	        url : url,
	        data: data,
	        headers: { 'Content-Type': undefined},
	        transformRequest: function(data) {
	            var formData = new FormData();
	            //formData.append("file[]", data.file);

	            var ins = data.file.length;
				for (var x = 0; x < ins; x++) {
				    formData.append("file["+x+"]", data.file[x]);
				}
				console.log(data)
				formData.append("message" , data.body.message);
				formData.append("job_id" , data.body.job_id);
				formData.append("is_anonymous" , data.body.is_anonymous);


	            return formData;
	        },
	    }).then(function mySuccess(response) {
	        deferred.resolve(response);
	    }, function myError(response) {
	    	console.log(response)
	    });
	    
	    return deferred.promise;
	}

	pipe.get = function(url) {

		var deferred = $q.defer();

		$http({
	        method : "GET",
	        url : url,
	    }).then(function mySuccess(response) {
	        deferred.resolve(response);
	    }, function myError(response) {
	    	// console.log(response)
	    	if(response.data.message == "Unauthenticated.") {
	    		AunthenticationService.setAuth(false);
				AunthenticationService.setData({});
				localStorageService.remove("credentials");
				$state.go('login');
				location.reload();
	    	}
	    	return response;
	    });

	    return deferred.promise;
	}

	pipe.arrayToParams = function($array) {
		var postParam = {};
		for (key in $array) {
	        postParam[key] = $array[key]
	    }
		return postParam;
	}
	return pipe;
}); 








