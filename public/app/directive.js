app.directive('showDropsearch', function($compile){
  return {
    link : function($scope, element, attrs){      

      $(element).click(function(){         
        
      })
    }
  }
})

app.directive('setJobtype', function($compile){
  return {
    link : function($scope, element, attrs){
      
      $('.job_type').removeClass('leftin')
      setTimeout(function(){
        $('.job_type').addClass('leftin')
      },1000) 

      $(element).click(function(){ 

        $('.job_type').removeClass('leftin')
        $('.empty_jobtype').removeClass('leftin')

        setTimeout(function(){
          $('.job_type').addClass('leftin')          
          $('#'+attrs.id).fadeOut()
          if($('.job_type').val() != ''){
            if(attrs.id == 'Anesthesiologist'){
              $('#CRNA').fadeIn()
            }else{
              $('#Anesthesiologist').fadeIn()
            }
            $('.job_type').css({
              'backgroundColor' : '#e3e3e3'
            })
          }else{
            $('.job_type').css({
              'backgroundColor' : '#fff'
            })
          }
          $('#dropdown-search').fadeOut()
          $('#dropdown-search').addClass('hidedp')                    
        },500)

        setTimeout(function(){
          $('.empty_jobtype').addClass('leftin')
        },1000)        
      
      })

    }
  }
})

app.directive('clearJobtype', function($compile){
  return {
    link : function($scope, element, attrs){
      $(element).click(function(){          
        $('.search-results-wrapper li').each(function(){
          $(this).fadeIn()
        })
        $('.job_type').css({
          'backgroundColor' : '#fff'
        })      
        $scope.jobsData.job_type = '';
        $scope.$apply()
      })
    }
  }
})

app.directive('showJobtype', function($compile){
  return {
    link : function($scope, element, attrs){
      $(element).click(function(){
        var dp = $('#dropdown-search');
        if($('#dropdown-search').hasClass('hidedp')){
          $('#dropdown-search').fadeIn()
          $('#dropdown-search').removeClass('hidedp')
        }else{
          $('#dropdown-search').fadeOut()
          $('#dropdown-search').addClass('hidedp')          
        }        
      })
    }
  }
})



app.directive('pressEnter', function($compile){
  return {
    link : function($scope, element, attrs){
      // console.log('state')
      $(element).keyup(function(e){        
        if(e.keyCode == 13)
        {            
            $scope.proceedToStep(attrs.proceedTo)
        }
      });

      $(element).on('change',function(){
        $scope.proceedToStep(attrs.proceedTo)
      })
    }
  }
})

app.directive('enterState', function($compile){
  return {
    link : function($scope, element, attrs){
      // console.log('state')
      $(element).keyup(function(e){        
        if(e.keyCode == 13)
        {            
            $scope.redirectToBroseJobs()
        }
      });
    }
  }
})

app.directive('postJob', function ($compile) {
    return {
        link: function ($scope, element, attrs) {            
            $(element).click(function(){              
              swal("Hello","Please Signup or Login first if you have registered already, Thanks!","info")
            })
        }
    };
});

app.directive('checkFilter', function ($compile) {
    return {
        link: function ($scope, element, attrs) {            
            $scope.$watch($('#key_searched').val(), function (newValue, oldValue, $scope) {
               
            });
        }
    };
});

app.directive('findJobs',function($compile){
  return {
    link : function($scope, element, attrs){
      $(element).click(function(){
        $('.select-state').each(function(){
          $(this).prop('checked',false)
        })
        angular.forEach($('#key_searched').tagsinput('items'),function(value){                 
            $('#state_'+value.toLowerCase()).prop('checked',true)                
          })
      })
    }
  }
})
app.directive('setFilter', function ($compile) {
    return {
        link: function ($scope, element, attrs) {
            $(element).click(function(){                  
              // console.log($("#key_searched").tagsinput("items"))
              // alert($(this).text())  
              if($(this).text() != 'All States'){
                if($('#'+attrs.for).is(":checked") == false){
                  $('#key_searched').tagsinput('add', $(this).text());
                }else{
                  var inputState = '';
                  inputState = $(this).text().toLowerCase()
                  // console.log(inputState)
                  $('#key_searched').tagsinput('remove', inputState);                
                  inputState = $(this).text().charAt(0).toUpperCase() + $(this).text().substr(1).toLowerCase();
                  // console.log(inputState)
                  $('#key_searched').tagsinput('remove', inputState);
                  inputState = $(this).text().toUpperCase()
                  // console.log(inputState)
                  $('#key_searched').tagsinput('remove', inputState);
                }
              }              
            })
        }
    };
});

app.directive('setAccount', function ($compile) {
    return {
        link: function ($scope, element, attrs) {
            $(element).click(function(){                  
              // console.log($("#key_searched").tagsinput("items"))
              if($('#'+attrs.for).is(":checked") == false){
                $('#key_searched').tagsinput('add', $(this).text());
              }else{                
                $('#key_searched').tagsinput('remove', $(this).text());
              }
            })
        }
    };
});

app.directive('loadingButton', function ($compile) {
    return {
        link: function ($scope, element, attrs) { 
          
        	var form_name = attrs.formName;
        	$(element).click(function() {

       			if(form_name != undefined && form_name != "") {
              if($scope[form_name].$invalid) {
                  
                  angular.forEach($scope[form_name].$error.required, function(field) {
                      field.$setPristine();
                      field.$dirty = true;
                  });

                  $scope.$apply();
                  //$.notify("Some fields are invalid or required, pls check.", "warning");
                  return true;
                }
            }
       			
       			$scope.$apply();
        		$scope.activeButton = Ladda.create(element[0]);
				    $scope.activeButton.start();
				    $scope.$eval(attrs.callback);
        	});
           	
        }
    };
});

app.directive('regloadingButton', function ($compile) {
    return {
        link: function ($scope, element, attrs) { 
          
          var form_name = attrs.formName;
          $(element).click(function() {

            if(form_name != undefined && form_name != "") {
              if($scope[form_name].$invalid) {
                  
                  angular.forEach($scope[form_name].$error.required, function(field) {
                      field.$setPristine();
                      field.$dirty = true;
                  });


                  if($("input.ng-invalid").length > 0) {
                    var invalid_input = $("input.ng-invalid");
                  }

                  if($("select.ng-invalid").length > 0) {
                    var invalid_input = $("select.ng-invalid");
                  }

                  if($("textarea.ng-invalid").length > 0) {
                    var invalid_input = $("textarea.ng-invalid");
                  }                                                  

                  $('#registration-form').animate({
                      //scrollTop: $(invalid_input[0]).parents(".section").position().top() + 50
                  }, 300);

                  $(invalid_input[0]).focus();


                  $scope.$apply();
                  // $.notify("Some fields are invalid or required, pls check.", "warning");
                  return true;
                }
            }
            
            $scope.$apply();
            $scope.activeButton = Ladda.create(element[0]);
            $scope.activeButton.start();
            $scope.$eval(attrs.callback);
          });
            
        }
    };
});



app.directive('rdSteps', function ($compile, $timeout) {
    return {
        link: function ($scope, element, attrs) {
            
            var stepkey = attrs.stepKey;  
            var w = $('#'+stepkey+'-form');

            function scrollEvent() {

                var timeout;
                //test
                $('#'+stepkey+'-form').scroll(function(e) {
                  
                  

                  $('.section').each(function(i, obj) {
                      var offset = $(obj).offset();
                      var y_position = offset.top;
                      if(y_position > 20 && y_position < 350) {
                        
                        // $(".section").addClass("slider-faded");
                        // $(obj).removeClass("slider-faded");
                      }
                      
                  });


                    
                });

           
            }
         
            $scope.$watch("rdSteps['"+stepkey+"']['current_step']", function (newValue, oldValue, $scope) {
                
                $('#'+stepkey+'-form').animate({
                    scrollTop: $("#"+stepkey+"_"+$scope.rdSteps['registration']['current_step']).position().top + 50
                }, 300);

                /*$timeout(function() {
                  $("#"+stepkey+"_"+$scope.rdSteps['registration']['current_step']).find("input").focus();
                },400);*/

                $timeout(function() {
                  scrollEvent()
                },1000);
            });

            $scope.$watch("selected_account", function (newValue, oldValue, $scope) {
                
                $('#'+stepkey+'-form').animate({
                    scrollTop: $("#"+stepkey+"_"+$scope.rdSteps['registration']['current_step']).position().top + 50
                }, 300);

                
                
            /*    $timeout(function() {
                  $("#"+stepkey+"_"+$scope.rdSteps['registration']['current_step']).find("input").focus();
                },400);*/

                $timeout(function() {
                  scrollEvent()
                },1000);
            });

            scrollEvent();
        } 
    };
});



app.directive('citiesSelect2', function ($compile) {
    return {
        link: function ($scope, element, attrs) {

            $(element).select2();
            $(element).on('select2:select', function (e) {
              // Do something
              var data = e.params.data;
             
              //$scope.getJobs();
            });


            $(element).on('select2:unselect', function (e) {
              // Do something
              var data = e.params.data;

              if(data != undefined) {

                if(data.text == "All") {
                  $(element).val(null).trigger('change');
                }
                $scope.cities = $(element).val();

                $scope.getJobs();
              }
              
            });


        }
    };
});





app.directive('tagsInput', function ($compile) {
    return {
        link: function ($scope, element, attrs) {
          $(element).textext({
              plugins : 'autocomplete filter tags ajax',
              ajax : {
                  url : '/searchCity',
                  dataType : 'json',
                  cacheResults : false,
                  'dataCallback' : function(query)
                  {
                      return { 'search' : query , "states" : $scope.getSelectedStates()};
                  } 
              }
          });

          $(document).on("click", ".text-remove" , function() {
            var tags = $('#city_tags').textext()[0].tags()._formData;
              
            $scope.jobsData.cities = tags;
            $scope.getJobs();
          
          });

          $(document).on("click", ".text-suggestion" , function() {
            var tags = $('#city_tags').textext()[0].tags()._formData;
              
            $scope.jobsData.cities = tags;
            $scope.getJobs();
          
          });


          

          $(element).css("width" , "100%");
          $(element).parents(".text-wrap").css("width" , "100%");

        }
    }
});

app.directive('statesSelect2', function ($compile) {
    return {
        link: function ($scope, element, attrs) {

            

            if($scope.jobsData.states != undefined) {
              if($scope.jobsData.states.length > 0) {

                  data = [];

                  angular.forEach($scope.jobsData.states, function(state, key){
                    data.push({ id: key, text: state });
                  });

                  
                  $(element).select2({
                    data: data
                  });

                  $scope.$apply();
              }
            }
            else{
              $(element).select2();
            }

            $(element).on('select2:select', function (e) {
              // Do something
              var data = e.params.data;
              if(data.text == "All") {
                $(element).val(null).val('All').trigger('change');
                $scope.jobsData.states = "All";
              }else{
                $scope.jobsData.states = $(element).val();
              }
              $scope.getJobs();
              $scope.convertSelectedStates(); 
              $(".select2-search__field").keydown(function(e) {
                var value = $(this).val();
              })

            });


            $(element).on('select2:unselect', function (e) {
              // Do something
              var data = e.params.data;

              if(data != undefined) {

                if(data.text == "All") {
                  $(element).val(null).trigger('change');
                }
                $scope.jobsData.states = $(element).val();
                $scope.getJobs();
              }
              
            });


        }
    };
});



app.directive('searchCities', function ($compile) {
    return {
        link: function ($scope, element, attrs) {
            $(element).keydown(function(e) {
              var value = $(this).val();
              $scope.searchCities(value);
            })
        }
    };
});

app.directive('pageScroll',function ($compile){
    return {
        link: function ($scope, element, attrs) {            
            $(element).click(function(){              
              $('html').animate({ 
                scrollTop: 750
              }, 300);
            }); 
        }        
    };    
});

app.directive('anesthesiaModal', function ($compile) {
    return {
        link: function ($scope, element, attrs) {      


          var target_modal = attrs.modal;
          var message = attrs.modalMessage;
          var modal = $("#"+target_modal)[0];
          var body = $('body');
          var bodyHeight = body.height() / 2;

          $(element).click(function() {
            
            $(".loginModalClose").trigger("click")
            $(".modal-message").html(message);
            if(modal != undefined) {
              modal.style.display = "block";  
            }
            var modalContentHeight = $('.modal-content').height();
            var modalMarginTop = "40px";   
              $('.modal-content').css(
                'marginTop', modalMarginTop,
              )

          });


          $(".loginModalClose").click(function() {
            if(modal != undefined) {
                modal.style.display = "none";
            }
            
          });

          $(window).click(function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
          });
        }
    };
});



app.directive('vacancySelect2', function ($compile) {
    return {
        link: function ($scope, element, attrs) {

            $(element).select2();
            $(element).on('select2:select', function (e) {
              // Do something
              var data = e.params.data;
              if(data.text == "All") {
                $(element).val(null).val('All').trigger('change');
                $scope.jobsData.vacancy_type = "All";
              }else{
                $scope.jobsData.vacancy_type = $(element).val();
              }
              $scope.getJobs();
            });


            $(element).on('select2:unselect', function (e) {
              // Do something
              var data = e.params.data;

              if(data != undefined) {

                if(data.text == "All") {
                  $(element).val(null).trigger('change');
                }
                $scope.jobsData.vacancy_type = $(element).val();

                $scope.getJobs();
              }
              
            });

        }
    };
});


app.directive('datepostedSelect2', function ($compile) {
    return {
        link: function ($scope, element, attrs) {

            $(element).select2();
            $(element).on('select2:select', function (e) {
              // Do something
              var data = e.params.data;
              if(data.text == "All") {
                $(element).val(null).val('All').trigger('change');
                $scope.jobsData.date_posted = "All";
              }else{
                $scope.jobsData.date_posted = $(element).val();
              }
              $scope.getJobs();
            });


            $(element).on('select2:unselect', function (e) {
              // Do something
              var data = e.params.data;

              if(data != undefined) {

                if(data.text == "All") {
                  $(element).val(null).trigger('change');
                }
                $scope.jobsData.date_posted = $(element).val();

                $scope.getJobs();
              }
              
            });

        }
    };
});

app.directive('accountsSelect2', function ($compile) {
    return {
        link: function ($scope, element, attrs) {

            $(element).select2();
            $(element).on('select2:select', function (e) {
              // Do something
              var data = e.params.data;
              if(data.text == "All") {
                $(element).val(null).val('All').trigger('change');
                $scope.jobsData.account_types = "All";
              }else{
                $scope.jobsData.account_types = $(element).val();
              }
              $scope.getJobs();
            });


            $(element).on('select2:unselect', function (e) {
              // Do something
              var data = e.params.data;

              if(data != undefined) {

                if(data.text == "All") {
                  $(element).val(null).trigger('change');
                }
                $scope.jobsData.account_types = $(element).val();

                $scope.getJobs();
              }
              
            });

        }
    };
});

app.directive('closeRegister', function ($timeout, $compile) {
  return {

    link : function ($scope, element, attrs) {
      $(element).click(function(e) {
       if($(element).hasClass("close-register-form")){                
          $('#close-slider-form').trigger('click')
        }
      });      
    }
  };
});

app.directive('showPolicy',function($compile){
  return {
    link: function($scope, element, attrs){
      $(element).click(function(){
        window.open('/policy', '_blank');
      })
    }
  }
})

app.directive('showTerms',function($compile){
  return {
    link: function($scope, element, attrs){
      $(element).click(function(){
        window.open('/terms-of-use', '_blank');
      })
    }
  }
})
app.directive('closeModal',function($compile){
  return {
    link: function($scope, element, attrs){      
      $(element).click(function(){
        $('.loginModalClose').trigger('click')
      })
    }
  }
})


app.directive('showSlider', function ($timeout, $compile) {
    return {
        link: function ($scope, element, attrs) {
          if(attrs.sliderWidth != undefined && attrs.sliderWidth != "") {
              $("#"+attrs.idTarget).css("width", attrs.sliderWidth);
              
          }
          


          $(element).click(function(e) { 

              var class_target = attrs.class_target;
              var id_target = attrs.idTarget;
              
              if($(e.target).hasClass("no-slide") || $(e.target).parents(".no-slide").length > 0) {
                return;
              }
              
              $timeout(function() {
                  if($(element).hasClass("close-prev-slider")){
                    $('#close-slider-form').trigger('click')
                  }

                  if($(element).hasClass("set-appform-jobid")){                
                    $('#btn-apply').data('value',$(element).data('value'))                
                    $('#btn-apply').data('id',$(element).data('id'))                     

                  }

                  if($(element).hasClass("close-prev-slider")){                
                    $scope.selected_job_id = $(element).data("value")
                    $scope.save_job_id = $(element).data("value")
                    
                    //$scope.inquireMessage = 'Hi {company_name},\n\tI noticed your job #'+$(element).data("value")+' and would like to know more about the position. We can discuss any details over chat.'               
                  }                              

                  var right = $("#"+id_target).width()

                  $("#"+id_target).css("right","0%");
                  $(".slider-form-overlay").removeClass("hidden");
                  $(".slider-form-close").removeClass("hidden")
                  document.body.style.overflow = "hidden";
                  var id = attrs.showSlider;
                  $scope.updateSelectedIndex(id);


                  $(window).resize(function() {
                    $("#"+id_target).css("right","0%");
                  })  

              }, 400)
                  
              
          });
          $('.hideslider').click(function(){
              var class_target = attrs.class_target;
              var id_target = attrs.idTarget;


              $("#"+id_target).css("right","-500%");
              $(".slider-form-overlay").addClass("hidden");
              $(".slider-form-close").addClass("hidden");
              document.body.style.overflow = "";            
          })
          $(document).on("click", ".slider-form-close" , function() {

              var class_target = attrs.class_target;
              var id_target = attrs.idTarget;


              $("#"+id_target).css("right","-500%");
              $(".slider-form-overlay").addClass("hidden");
              $(".slider-form-close").addClass("hidden");
              document.body.style.overflow = "";
          });
          $(".slider-form-overlay").click(function() {

              var class_target = attrs.class_target;
              var id_target = attrs.idTarget;

              $("#"+id_target).css("right","-500%");
              $(".slider-form-overlay").addClass("hidden");
              $(".slider-form-close").addClass("hidden");
              document.body.style.overflow = "";
          });

          $(document).keyup(function(e) {
            var class_target = attrs.class_target;
            var id_target = attrs.idTarget;
            if (e.keyCode == 27) { // escape key maps to keycode `27`
               $("#"+id_target).css("right","-500%");
               $(".slider-form-overlay").addClass("hidden")
               document.body.style.overflow = "";
            }
          });
        }
    };
});

app.directive('hideSelection', function ($compile) {
    return {
        link: function ($scope, element, attrs) {
            $(document).click(function(e) {
              if ($(e.target).closest(".not-close").length === 0 && $(e.target).closest(".select2-selection__choice").length === 0) {
                  $scope.active_section = "";
                  $scope.$apply();
              }
            });

        }
    };
});

app.directive('mapFilter', function ($compile) {
    return {
        link: function ($scope, element, attrs) {
           $(element).vectorMap({
              map : 'us_aea_en',
              backgroundColor : 'transparent',
              zoomOnScroll: false,
              regionStyle : {
                  initial : {
                      fill : '#c9d6de'
                  }
              },
              markers: [{
                      latLng : [40.71, -74.00],
                      name : 'Newyork: 250'
                      , style: {fill: '#1e88e5'}
                  },{
                      latLng : [39.01, -98.48],
                      name : 'Kansas: 250'
                      , style: {fill: '#fc4b6c'}
                  },
                {
                  latLng : [37.38, -122.05],
                  name : 'Vally : 250'
                  , style: {fill: '#26c6da'}
                }]
          });


        }
    };
});

app.directive('bounceLeft',function(){
  return {
    link : function($scope,element,attrs){            
      $(window).scroll(function(){
          //
          if(attrs.row == "first"){
            if($(this).scrollTop() >= 300 && $(this).scrollTop() <= 1050){
              $(element).addClass('animated fadeInLeft')
            }else{
              // $(element).removeClass('animated fadeInLeft')
            }          
          }else if(attrs.row == "second"){
            if($(this).scrollTop() >= 850 && $(this).scrollTop() <= 1600){
              $(element).addClass('animated fadeInLeft')
            }else{
              // $(element).removeClass('animated fadeInLeft')
            }   
          }else if(attrs.row == "third"){
            if($(this).scrollTop() >= 1600 && $(this).scrollTop() <= 2400){
              $(element).addClass('animated fadeInLeft')
            }else{
              // $(element).removeClass('animated fadeInLeft')
            }   
          }else if(attrs.row == "forth"){
            if($(this).scrollTop() >= 2600 && $(this).scrollTop() <= 3500){
              $(element).addClass('animated fadeInLeft')
            }else{
              // $(element).removeClass('animated fadeInLeft')
            }   
          }           
      });      
    }
  }
})

app.directive('bounceRight',function(){
  return {
    link : function($scope,element,attrs){      
      $(window).scroll(function(){
          //
          if(attrs.row == "first"){
            if($(this).scrollTop() >= 300 && $(this).scrollTop() <= 1050){
              $(element).addClass('animated fadeInRight')
            }else{
              // $(element).removeClass('animated fadeInRight')
            }
          }
          else if(attrs.row == "second"){
            if($(this).scrollTop() >= 850 && $(this).scrollTop() <= 1600){
              $(element).addClass('animated fadeInRight')
            }else{
              // $(element).removeClass('animated fadeInRight')
            }  
          }else if(attrs.row == "third"){
            if($(this).scrollTop() >= 1600 && $(this).scrollTop() <= 2400){
              $(element).addClass('animated fadeInRight')
            }else{
              // $(element).removeClass('animated fadeInRight')
            }   
          }else if(attrs.row == "forth"){
            if($(this).scrollTop() >= 2600 && $(this).scrollTop() <= 3500){
              $(element).addClass('animated fadeInRight')
            }else{
              // $(element).removeClass('animated fadeInRight')
            }  
          }              
      });      
    }
  }
})

app.directive('bounceIn', function ($compile) {
    return {
        link: function ($scope, element, attrs) {
          $(window).scroll(function(){            
            if(attrs.row == "fifth"){
              if($(this).scrollTop() >= 3300 ){
                $(element).addClass('animated fadeInRight')
              }else{
                // $(element).removeClass('animated fadeInRight')
              }
            }
          });
        }
    };
});


app.directive('filterStates',function($compile){
  return {
    link: function($scope, element, attrs){
      
      $(element).click(function(){        
        if($(this).val() != "All States"){
          if($scope.jobsData.states.indexOf($(this).val()) == -1){
            $scope.jobsData.states.push($(this).val())
            $scope.getJobs();
            
          }else{
            $scope.jobsData.states.splice($scope.jobsData.states.indexOf($(this).val()),1)
            
            $scope.getJobs();
          }        
        }else{          
          if($(this).is(":checked") == true){
            $('.select-state').each(function(){            
              $(this).prop('checked',true);
              if($scope.jobsData.states.indexOf($(this).val()) == -1){
                $scope.jobsData.states.push($(this).val())              
              }
            })
            $scope.jobsData.states = 'All'              
          }else{
            $('.select-state').each(function(){            
              $(this).prop('checked',false);
              // $scope.jobsData.states.splice($scope.jobsData.states.indexOf($(this).val()),1)
            })
            $scope.jobsData.states = [];
          }          
          $scope.getJobs();
          
        }
      })
    }
  }
})

app.directive('showStates',function($compile){
  return {
    link: function($scope, element, attrs){
      
      $(element).click(function(){        
        if($('.show-states').hasClass('fadeIn')){
          $('.show-states').removeClass('animated fadeIn')
        }else{
          $('.show-states').addClass('animated fadeIn')
        }
      })
    }
  }
})

app.directive('inputState',function($compile){
  return {
    link: function($scope, element, attrs){
      
      $(element).on('input',function(){        
        if($(this).val() == ""){
          $('.show-states').removeClass('animated fadeIn')
        }else{
          if($('.show-states').hasClass('fadeIn')){
            
          }else{
            $('.show-states').addClass('animated fadeIn')
          }
            
        }        
      })
    }
  }
})


app.directive('animateHover',function($compile){
  return {
    link: function($scope, element, attrs){    
      $(element).mousemove(function( e ) {
        var x = e.offsetX;
        var y = e.offsetY;        
        e.target.style.setProperty('--x', `${ x }px`)
        e.target.style.setProperty('--y', `${ y }px`)
      });
    }
  }
})

app.directive('animateSmall',function($compile){
  return {
    link: function($scope, element, attrs){    
      $(element).mousemove(function( e ) {
        var x = e.offsetX;
        var y = e.offsetY;        
        
        e.target.style.setProperty('--x', `${ x }px`)
        e.target.style.setProperty('--y', `${ y }px`)
      });
    }
  }
})