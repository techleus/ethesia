app.controller('InvitesControllerController', function($scope, $api, BrowseJobService) {
	
	"use strict";
	
	$scope.invite = {};
	$scope.invite.password = '';
	$scope.invite.confirm_password = '';

	$scope.acceptInvitation = function(provider_id,invited_email,invitation_key){		
		
		if($scope.passwordForm.password.$invalid){
			return swal('Sorry!','Invalid Password','info')
		}
		if($scope.invite.password != $scope.invite.confirm_password){
			return swal('Sorry!','Password did not match','info')
		}

		var data = {
			password : $scope.invite.password,
			confirm_password : $scope.invite.confirm_password,
			provider_id : provider_id,
			invited_email : invited_email,
			invitation_key : invitation_key,
		};
		
		var url = '/registerInvitedUser';

		$api.post(data,url).then(function(response){
			console.log(response)
		})

	}

});