
app.controller('PasswordResetController', function($scope,$timeout, $api, BrowseJobService,$window, $http) {
	$scope.development = false;
	$scope.us_states = us_states;
	$scope.jobsData = {};

	if($("#jobsData").length > 0 && $("#jobsData").val() != "null") {
		$scope.jobsData = JSON.parse($("#jobsData").val())
	}

	$scope.social_job = {};
	$scope.config = {};

	$scope.jobsData.cities = [];
	console.log($scope.jobsData)
	$scope.active_section = "";
	$scope.states = {};
	$scope.isLoading = true;
	$scope.fake_loading = [1,2,3,4,5,6,7,8]
	$scope.jobsData.cities = [];	
	$scope.applyForm = [];
	$scope.loginSuccess = false;
	$scope.current_page_count = 1;
	$scope.jobsData.current_index = 0;
	$scope.job_type_search = "";
	$scope.job_type_focus = false;
	$scope.nothing_toload = false;
	$scope.job_dur_search = "";
	$scope.job_dur_focus = false;
	$scope.base_url = "https://dev.anesthesia.community";

	$scope.job_type = ["Anesthesiologist", "CRNA"];
	$scope.dur_pos = ["Full Time", "Locum Tenens" , "Part Time" , "PRN", "Fellowship"];

	$scope.unfocusType = function() {

		$timeout(function(){
			$scope.job_type_focus = false;
		}, 200);
	}
	$scope.pureText = function(text) {
		var oginaltext = text;
		return oginaltext.replace(/<br[^>]*>/g,"").replace(/(&nbsp;)*/g,"").replace(/(&amp;)*/g,"");
	}
	$scope.redirectToBrowseEmployers = function() {
		var url = "/redirect/browse-employer";

		$timeout(function() {
			$scope.activeButton.stop();
	    	var data = $scope.jobsData;
	    	$api.post(data, url).then(function(response) {
	    		if(response.data.status) {
	    			$window.location.href = '/browse-employers';
	    		}
			});
		}, 200);
	}

	$scope.limitTitle = function(title,description){
		
		var titleCharSet = title.length;
		var maxTitleCharSet = 150;		
		
		if(Number(titleCharSet) <= Number(maxTitleCharSet)){			
			//return description;
		}
	}

	$scope.redirectToBroseJobs = function() {
		var job_type = $scope.jobsData.job_type;
		var pos_dur = $scope.job_dur_search;
		var job_location = $scope.job_location;
		var url = "/redirect/browse-jobs";

		/*
		$timeout(function() {
			$scope.activeButton.stop();
			$scope.jobsData.job_type = job_type;
			$scope.jobsData.pos_dur = pos_dur;
			$scope.jobsData.job_location = job_location;
	    	var data = $scope.jobsData;

	    	$api.post(data, url).then(function(response) {
	    		if(response.data.status) {
	    			$window.location.href = '/browse-jobs';
	    		}
			});
		}, 200);
		
		*/

		$scope.jobsData.job_type = job_type;
		$scope.jobsData.pos_dur = pos_dur;
		$scope.jobsData.job_location = job_location;
    	var data = $scope.jobsData;

    	$api.post(data, url).then(function(response) {
    		if(response.data.status) {
    			$window.location.href = '/browse-jobs';
    		}
		});
		
	}

	$scope.closeslider = function(){
		alert()
	}

	$scope.unfocusDur = function() {
		$timeout(function(){
			$scope.job_dur_focus = false;
		}, 200);
	}

	$scope.populateTypeSearch = function(type) {
		$scope.jobsData.job_type = type;
		$scope.job_type_focus = false;
	}
	$scope.populateDurSearch = function(type) {
		$scope.jobsData.duration_position = type;
		$scope.job_dur_focus = false;
	}


	$scope.loadMoreJobs = function() {
		$scope.current_page_count++;
		var url = "/public/jobs";
    	

    	$scope.jobsData.job_type = $("#job_type").val();
    	$scope.jobsData.current_index++;	
    	var data = $scope.jobsData;
    	$scope.loading_additional = true;


    	$api.post(data, url).then(function(response) {
    		var data_respo = response.data;
    		$scope.loading_additional = false;
    		if(data_respo != "" && response.data != "") {
    		/*	angular.forEach(data_respo, function(respo, key){
    				$scope.jobs[100+key] = data_respo[key];
    			});*/

    			$scope.jobs.push.apply($scope.jobs, data_respo);
    		}


    		if(data_respo.length == 0) {
    			$scope.nothing_toload = true;
    		}                                                                                      
    		console.log($scope.jobs)
		});
	}

	$scope.getJobs = function() {

		var url = "/public/jobs";
    	
		$scope.jobsData.current_index = 0;
    	
    	$scope.isLoading = true;

    	var data = $scope.jobsData;

    	$api.post(data, url).then(function(response) {
    		$scope.jobs = response.data;                                                                                          
    		$scope.isLoading = false;
    		console.log($scope.jobs)

		});
	}

	$scope.popupNeedsCandidate = function() {
		
	}
	$scope.saveJob = function(JobId){
		
	}
	$scope.bookmark = function(userType,jobId,jobTitle, jobObj){
	    if(userType === 1){    	    	
	    		var url = "/browse-jobs/bookmark";
	    		var data = {userType : userType, jobId : jobId}
	    		$api.post(data,url).then(function(response){    			
	    			if(response.data == "Added"){
	    				jobObj.bookmark = ["something"];
	    				swal("Job: "+jobTitle, "Job successfully added to bookmark", "success");
	    			
	    			}else{
	    				jobObj.bookmark = null;
	    				swal("Job: "+jobTitle, "Job removed from bookmark", "success");
	    			}    		
	    			$scope.$apply();	
	    		});
	    }else{
	    	swal("Please sign-in as a Candidate account");
	    }
  	}	
  	
  	$scope.inquireJob = function(job_id,UserId,JobUserID){  		  	
  
  		var privacyCheck = $('input#privacy-label');
  		var is_anonymous = $('input[name=inquire-opt]:checked').val()  		
  		var Message = $('textarea#app-form-textarea').val()
  		var elem = $("#inquirerCV")    	    	    	        	
  		var data = [];
    	
    	var url = "/public/uploadInquirerCV";

    	data.file = elem[0].files;    	
    	data.body = [];    	
		data.body.message      = $scope.applyForm.message;    	
		data.body.job_id        = job_id;
		data.body.is_anonymous = is_anonymous;

    	console.log(data)
    	if(data.file.length == 0) {
    		swal("Sorry!", "You need to select a CV to proceed.", "warning");
    		return;
    	}

    	var ValidCVFiles = ["doc","dot","wbk","docx","docm","dotx","dotm","docb","pdf"];
    	    	
    	var name = data.file[0].name;
    	var fileType = name.substr(name.indexOf(".") + 1)

    	if ($.inArray(fileType, ValidCVFiles) < 0) {
			swal("Sorry","Please select a valid Word document file", "warning");
			console.log(data.file)
			 return;			
    	}

    	if(!privacyCheck.is(':checked')){
  			swal("Sorry!", "Please Agree on Privacy Policy and Terms of Use first");
  			return;
  		}

    	$api.upload_apply(data, url).then(function(response) {    		
    		if(response.data.success == true){    			
    			swal({
				  	title: 'Congratulations!',
				  	text: "Your inquiry has been successfully sent!",
				  	type: 'info',
				  	showCancelButton: false,				  				  
				},function(){
					elem.val('')
	    			privacyCheck.prop('checked',false)
	    			$('#close-app-form').trigger('click')
	    			console.log(response.data)
				})
    		}
    	})



  	}

  	$scope.saveJob = function(userType,jobId,jobTitle,savedJob){
  		if(userType === 1){    	    	
	    		var url = "bookmark";
	    		var data = {userType : userType, jobId : jobId}
	    		$api.post(data,url).then(function(response){    			
	    			if(response.data == "Added"){
	    				savedJob.save = ["something"];
	    				swal("Job: "+jobTitle, "Job successfully added to bookmark", "success");
	    			
	    			}else{
	    				savedJob.save = null;
	    				swal("Job: "+jobTitle, "Job removed from bookmark", "success");
	    			}    			
	    		});
	    }else{
	    	swal("Please sign-in as a Candidate account");
	    }
  	}

  	$scope.cancelInquire = function(){
  		$('#close-app-form').trigger('click')
  	}

  	$scope.updateAppForm = function(jobId){
  		var url = "/public/current_user";
  		var joburl = "/public/get_job_userid?jobId="+jobId;  		  		
    	$api.get(joburl).then(function(jobdata) {
			$scope.apply_form = jobdata.data			
		}).then(function(){
			$api.get(url).then(function(response) {
	    		$scope.current_user_data = response.data; 
	    		$scope.applyForm.message = "Hi "+ $scope.apply_form.company_name +",\n\n\t I noticed your job #"+ $scope.jobs[$scope.selected_index].id +" and would like to know more about the position. We can discuss any details over chat";
			});
		});    					
		
  	}

  	
  	$scope.selectUnselectPrivacy = function(){
  			 	
  	}

	$scope.openPdf = function(id) {
		window.open("/pdf/generate/job/"+id, '_blank', 'fullscreen=yes');
	}
	$scope.updateSelectedIndex = function(id) {
        $scope.selected_index = id;
        $scope.$apply();
    }
    $scope.processCheckboxAccount = function(model , type) {

    	model = !model;

    	if(type != undefined) {
    		console.log(model)
    		if(!model) {
    			$scope.jobsData[type].staffing_firm = true;
				$scope.jobsData[type].hospital_surgery = true;
				$scope.jobsData[type].anesthesia_group = true;
				$scope.jobsData[type].management_firm = true;
				$scope.jobsData[type].individual = true;
    		}else{
    			$scope.jobsData[type].staffing_firm = false;
				$scope.jobsData[type].hospital_surgery = false;
				$scope.jobsData[type].anesthesia_group = false;
				$scope.jobsData[type].management_firm = false;
				$scope.jobsData[type].individual = false;
    		}
    		
    	}else{
    		$scope.jobsData.account_types.all = false;
    	}


    	$scope.getJobs();
    }

    $scope.processCheckboxVacancy = function(model , type) {

    	model = !model;

    	if(type != undefined) {
    		console.log(model)
    		if(!model) {
    			$scope.jobsData[type].full_time = true;
				$scope.jobsData[type].locum_tenens = true;
				$scope.jobsData[type].part_time = true;
				$scope.jobsData[type].prn = true;
				$scope.jobsData[type].fellowship = true;
    		}else{
    			$scope.jobsData[type].full_time = false;
				$scope.jobsData[type].locum_tenens = false;
				$scope.jobsData[type].part_time = false;
				$scope.jobsData[type].prn = false;
				$scope.jobsData[type].fellowship = false;
    		}
    		
    	}else{
    		$scope.jobsData.vacancy_type.all = false;
    	}


    	$scope.getJobs();
    }

    $scope.processCheckboxDatePosted = function(model , type) {

    	model = !model;

    	if(type != undefined) {
    		console.log(model)
    		if(!model) {
    			$scope.jobsData[type].last_hour = true;
				$scope.jobsData[type].last_24 = true;
				$scope.jobsData[type].last_7 = true;
				$scope.jobsData[type].last_14 = true;
				$scope.jobsData[type].last_39 = true;
    		}else{
    			$scope.jobsData[type].last_hour = false;
				$scope.jobsData[type].last_24 = false;
				$scope.jobsData[type].last_7 = false;
				$scope.jobsData[type].last_14 = false;
				$scope.jobsData[type].last_39 = false;
    		}
    		
    	}else{
    		$scope.jobsData.date_posted.all = false;
    	}


    	$scope.getJobs();
    }
	$scope.populateCities = function() {
		if($scope.jobsData.states != "") {
			//$scope.jobsData.cities = [];
			angular.forEach($scope.jobsData.states, function(state, key){
	            
				angular.forEach($scope.us_states, function(city, key){

		            if(state == key) {
						
						if($scope.jobsData.cities.length == 0) {
							angular.forEach($scope.us_states[key], function(city_to_put, key_to_put){
								var data = [];
								data["state"] = state;
								data["city"] = city_to_put;
								$scope.jobsData.cities.push(data);
							});
						}else{
							angular.forEach($scope.us_states[key], function(city_to_put, key_to_put){
								var data = [];
								data["state"] = state;
								data["city"] = city_to_put;
								$scope.jobsData.cities.push(data);
							});
						}


				        
		            }
	            });

	            
	        });

	        console.log($scope.jobsData.cities)
			$("#select2-city").select2("data",$scope.jobsData.cities, true);
    		$scope.$apply();



	        


		}
	}

	$scope.searchCities = function(key) {
		
	}


	$scope.convertSelectedStates = function() {
		if($scope.jobsData.states != undefined) {
			$("#states_input").val($scope.jobsData.states.toString());
			console.log($("#states_input").val())
		}
	}

	$scope.getSelectedStates = function() {
		return $("#states_input").val();
	}	

	$scope.showSectionActive = function(key) {
		if($scope.active_section != "") {
			$scope.active_section = "";
			return
		}

		$scope.active_section = key;
	}

	$scope.getStates = function() {
		$scope.states = BrowseJobService.getStates();
		return $scope.states;
	}

	$scope.getAccountTypes = function() {
		var url = "/public/account_types";
		$api.get(url).then(function(response) {
    		$scope.account_types = response.data;
		});
	}


	$scope.saveStateCity = function() {
		
		var url = "/saveStateCity";
    	var data = [];
    	data["states"] = $scope.us_states;

    	$api.post(data, url).then(function(response) {
    		$scope.jobs = response.data;                                                                                          
    		$scope.isLoading = false;
		});
	}


	

	$scope.assignSocialJobData = function(job) {
		$scope.social_job = job;
	}

	$scope.shareToTwitter = function() {
		console.log($scope.social_job)
		var url_to = "https://twitter.com/share?text=";
		var message = $scope.social_job.jobTitle;
		var url = $scope.base_url+"/job-posts/"+$scope.social_job.id;
		var final_url = url_to+message+"&url="+url;
		$window.open(final_url, 'twitter share','width=640,height=360')
	}

	$scope.shareToFb = function() {

		var job = $scope.social_job;

		var link = $scope.base_url+"/job-posts/"+$scope.social_job.id;
		var name = "Anesthesia Job Post";
		var image = $scope.base_url+"/"+$scope.social_job.image_path;
		var caption = $scope.social_job.jobTitle;
		var description = $scope.social_job.ei_jobDescription;
		$window.FB.ui(
	    {
	        method: 'feed',
	        name: name,
	        link: link,
	        picture: image,
	        caption: caption,
	        description: description,
	        message: ''
	    });
	}
	$scope.fbAsyncInit = function() {
	    $window.FB.init({ 
	      appId: '403569090053578',
	      status: true, 
	      cookie: true, 
	      xfbml: true,
	      version: 'v2.4'
	    });
	};


	$scope.resetAwhile = function(model ,time) {
		$timeout(function() {
			model = "";
			$scope.$apply();
		}, time);

	}

	$scope.submitNewPassword = function() {		
		var url = "/password/new";
		$scope.passwordResetData.verification_code = $("#verification_code").val()
    	var data = $scope.passwordResetData;

    	$api.post(data, url).then(function(response) {
    		if(response.data.status) {
    			$scope.successMessage = response.data.message;
    			$scope.activeButton.stop();

    			$timeout(function() {
					$scope.successMessage = "";
				}, 2000);

    			$timeout(function() {
    				$window.location.href = '/';
    			},2500);
    			
    			
    		}else if(response.data.status == false) {
    			$scope.errorMessage = response.data.message;
    			$timeout(function() {
					$scope.errorMessage = "";
				}, 2000);
    			$scope.activeButton.stop();
    		}
		});
	}
	

});