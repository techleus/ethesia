
app.controller('BrowseEmployerController', function($scope, $api, BrowseJobService) {
	
	$scope.isLoading = true;
	$scope.jobsEmployer = [];

	$scope.getJobEmployer = function(company_name = null) {

		var url = "/public/get_employers";
    	var data = {}    	
    	data.company_name = ''
    	$scope.isLoading = true;
    	if(company_name != "" || company_name != undefined){
    		console.log(company_name)
    		data.company_name = company_name
    	}
    	$api.post(data,url).then(function(response) {
    		$scope.jobsEmployer = response.data;                                                                                          
    		$scope.isLoading = false;
    		console.log(response.data)
		});
	}

	$scope.unfocusDur = function() {
        $timeout(function(){
            $scope.job_dur_focus = false;
        }, 200);
    }

    $scope.populateTypeSearch = function(type) {
        $scope.jobsData.job_type = type;
        $scope.job_type_focus = false;
    }
    
    $scope.populateDurSearch = function(type) {
        $scope.jobsData.duration_position = type;
        $scope.job_dur_focus = false;
    }
	
    $scope.bookmarkProvider = function(providerId,company_name,employer){					

		var url = "/public/bookmarkJobProvider"
		var data = {}
			data.providerId = providerId

		$api.post(data,url).then(function(response) {    		
    		if(response.data == "Added"){
    			swal("Company: "+ company_name, "Job provider successfully added to bookmark", "success");
    			employer.user_job_provider_bookmark = ["something"];
    		}else{
    			swal("Company: "+ company_name, "Job provider removed from bookmark", "success");
    			employer.user_job_provider_bookmark = null;
    		}
		});
	}

	$scope.showHideBtnReadmore = function(description){
		return description.length;
	}
	
	$scope.findJob = function(test){
		alert(test)
	}

	$scope.getJobEmployer();

});

angular.module('ng').filter('cut', function () {
    return function (value, wordwise, max, tail) {
        if (!value) return '';

        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace !== -1) {
              //Also remove . and , so its gives a cleaner result.
              if (value.charAt(lastspace-1) === '.' || value.charAt(lastspace-1) === ',') {
                lastspace = lastspace - 1;
              }
              value = value.substr(0, lastspace);
            }
        }

        return value + (tail || ' …');
    };
});