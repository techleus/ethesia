
app.service('BrowseJobService', function($http, $q) {

	var pipe = {};
	var states = [
		    {
		        "text": "All States",
		        "abbreviation": "All States"
		    },
		    {
		        "text": "Alabama",
		        "abbreviation": "AL"
		    },
		    {
		        "text": "Alaska",
		        "abbreviation": "AK"
		    },		   
		    {
		        "text": "Arizona",
		        "abbreviation": "AZ"
		    },
		    {
		        "text": "Arkansas",
		        "abbreviation": "AR"
		    },
		    {
		        "text": "California",
		        "abbreviation": "CA"
		    },
		    {
		        "text": "Colorado",
		        "abbreviation": "CO"
		    },
		    {
		        "text": "Connecticut",
		        "abbreviation": "CT"
		    },
		    {
		        "text": "Delaware",
		        "abbreviation": "DE"
		    },
		    {
		        "text": "District Of Columbia",
		        "abbreviation": "DC"
		    },
		    {
		        "text": "Florida",
		        "abbreviation": "FL"
		    },
		    {
		        "text": "Georgia",
		        "abbreviation": "GA"
		    },
		    {
		        "text": "Hawaii",
		        "abbreviation": "HI"
		    },
		    {
		        "text": "Idaho",
		        "abbreviation": "ID"
		    },
		    {
		        "text": "Illinois",
		        "abbreviation": "IL"
		    },
		    {
		        "text": "Indiana",
		        "abbreviation": "IN"
		    },
		    {
		        "text": "Iowa",
		        "abbreviation": "IA"
		    },
		    {
		        "text": "Kansas",
		        "abbreviation": "KS"
		    },
		    {
		        "text": "Kentucky",
		        "abbreviation": "KY"
		    },
		    {
		        "text": "Louisiana",
		        "abbreviation": "LA"
		    },
		    {
		        "text": "Maine",
		        "abbreviation": "ME"
		    },		   
		    {
		        "text": "Maryland",
		        "abbreviation": "MD"
		    },
		    {
		        "text": "Massachusetts",
		        "abbreviation": "MA"
		    },
		    {
		        "text": "Michigan",
		        "abbreviation": "MI"
		    },
		    {
		        "text": "Minnesota",
		        "abbreviation": "MN"
		    },
		    {
		        "text": "Mississippi",
		        "abbreviation": "MS"
		    },
		    {
		        "text": "Missouri",
		        "abbreviation": "MO"
		    },
		    {
		        "text": "Montana",
		        "abbreviation": "MT"
		    },
		    {
		        "text": "Nebraska",
		        "abbreviation": "NE"
		    },
		    {
		        "text": "Nevada",
		        "abbreviation": "NV"
		    },
		    {
		        "text": "New Hampshire",
		        "abbreviation": "NH"
		    },
		    {
		        "text": "New Jersey",
		        "abbreviation": "NJ"
		    },
		    {
		        "text": "New Mexico",
		        "abbreviation": "NM"
		    },
		    {
		        "text": "New York",
		        "abbreviation": "NY"
		    },
		    {
		        "text": "North Carolina",
		        "abbreviation": "NC"
		    },
		    {
		        "text": "North Dakota",
		        "abbreviation": "ND"
		    },
		    {
		        "text": "Ohio",
		        "abbreviation": "OH"
		    },
		    {
		        "text": "Oklahoma",
		        "abbreviation": "OK"
		    },
		    {
		        "text": "Oregon",
		        "abbreviation": "OR"
		    },
		    {
		        "text": "Pennsylvania",
		        "abbreviation": "PA"
		    },
		    // {
		    //     "text": "Puerto Rico",
		    //     "abbreviation": "PR"
		    // },
		    {
		        "text": "Rhode Island",
		        "abbreviation": "RI"
		    },
		    {
		        "text": "South Carolina",
		        "abbreviation": "SC"
		    },
		    {
		        "text": "South Dakota",
		        "abbreviation": "SD"
		    },
		    {
		        "text": "Tennessee",
		        "abbreviation": "TN"
		    },
		    {
		        "text": "Texas",
		        "abbreviation": "TX"
		    },
		    {
		        "text": "Utah",
		        "abbreviation": "UT"
		    },
		    {
		        "text": "Vermont",
		        "abbreviation": "VT"
		    },
		    {
		        "text": "Virgin Islands",
		        "abbreviation": "VI"
		    },
		    {
		        "text": "Virginia",
		        "abbreviation": "VA"
		    },
		    {
		        "text": "Washington",
		        "abbreviation": "WA"
		    },
		    {
		        "text": "West Virginia",
		        "abbreviation": "WV"
		    },
		    {
		        "text": "Wisconsin",
		        "abbreviation": "WI"
		    },
		    {
		        "text": "Wyoming",
		        "abbreviation": "WY"
		    },
		    {
		        "text": "Other",
		        "abbreviation": "Other"
		    }
		];

	pipe.getStates = function() {
		return states;
	}

	return pipe;
}); 

