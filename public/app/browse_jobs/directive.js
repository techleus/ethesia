app.directive('citiesSelect2', function ($compile) {
    return {
        link: function ($scope, element, attrs) {

            $(element).select2();
            $(element).on('select2:select', function (e) {
              // Do something
              var data = e.params.data;
             
              //$scope.getJobs();
            });


            $(element).on('select2:unselect', function (e) {
              // Do something
              var data = e.params.data;

              if(data != undefined) {

                if(data.text == "All") {
                  $(element).val(null).trigger('change');
                }
                $scope.cities = $(element).val();

                $scope.getJobs();
              }
              
            });


        }
    };
});

app.directive('tagging', function ($compile) {
    return {
        link: function ($scope, element, attrs) {
            $(element).val($scope.jobsData.job_location)
            $(element).tagsinput();            
            $(element).on('itemAdded', function(event) {
                // console.log('item added : '+event.item);
                //$('#state_'+event.item.toLowerCase()).prop('checked',true) 
            });
            $(element).on('itemRemoved', function(event) {
                //$('#state_'+event.item.toLowerCase()).prop('checked',false) 
            });
        }
    }
});



app.directive('tagsInput', function ($compile) {
    return {
        link: function ($scope, element, attrs) {
          $(element).textext({
              plugins : 'autocomplete filter tags ajax',
              ajax : {
                  url : '/searchCity',
                  dataType : 'json',
                  cacheResults : false,
                  'dataCallback' : function(query)
                  {
                      return { 'search' : query , "states" : $scope.getSelectedStates()};
                  } 
              }
          });

          $(document).on("click", ".text-remove" , function() {
            var tags = $('#city_tags').textext()[0].tags()._formData;
              // console.log(tags)
            $scope.jobsData.cities = tags;
            $scope.getJobs();
          
          });

          $(document).on("click", ".text-suggestion" , function() {
            var tags = $('#city_tags').textext()[0].tags()._formData;
              // console.log(tags)
            $scope.jobsData.cities = tags;
            $scope.getJobs();
          
          });


          

          $(element).css("width" , "100%");
          $(element).parents(".text-wrap").css("width" , "100%");

        }
    }
});

app.directive('statesSelect2', function ($compile) {
    return {
        link: function ($scope, element, attrs) {

            

            if($scope.jobsData.states != undefined) {
              if($scope.jobsData.states.length > 0) {

                  data = [];

                  angular.forEach($scope.jobsData.states, function(state, key){
                    data.push({ id: key, text: state });
                  });

                  // console.log(data)
                  $(element).select2({
                    data: data
                  });

                  $scope.$apply();
              }
            }
            else{
              $(element).select2();
            }

            $(element).on('select2:select', function (e) {
              // Do something
              var data = e.params.data;
              if(data.text == "All") {
                $(element).val(null).val('All').trigger('change');
                $scope.jobsData.states = "All";
              }else{
                $scope.jobsData.states = $(element).val();
              }
              $scope.getJobs();
              $scope.convertSelectedStates(); 
              $(".select2-search__field").keydown(function(e) {
                var value = $(this).val();
              })

            });


            $(element).on('select2:unselect', function (e) {
              // Do something
              var data = e.params.data;

              if(data != undefined) {

                if(data.text == "All") {
                  $(element).val(null).trigger('change');
                }
                $scope.jobsData.states = $(element).val();
                $scope.getJobs();
              }
              
            });


        }
    };
});



app.directive('searchCities', function ($compile) {
    return {
        link: function ($scope, element, attrs) {
            $(element).keydown(function(e) {
              var value = $(this).val();
              $scope.searchCities(value);
            })
        }
    };
});

app.directive('pageScroll',function ($compile){
    return {
        link: function ($scope, element, attrs) {            
            $(element).click(function(){    
              $('html').animate({ 
                scrollTop: 650
              }, 300);                       
            });
            function detectmob() { 
              if( navigator.userAgent.match(/Android/i)
              || navigator.userAgent.match(/webOS/i)
              || navigator.userAgent.match(/iPhone/i)
              || navigator.userAgent.match(/iPad/i)
              || navigator.userAgent.match(/iPod/i)
              || navigator.userAgent.match(/BlackBerry/i)
              || navigator.userAgent.match(/Windows Phone/i)
              ){
                return true;
              }else {
                return false;
              }
            } 
        }        
    };    
});

app.directive('anesthesiaModal', function ($compile) {
    return {
        link: function ($scope, element, attrs) {
          // console.log(attrs)
          var target_modal = attrs.modal;
          var message = attrs.modalMessage;
          var modal = $("#"+target_modal)[0];
          var body = $('body');
          var bodyHeight = body.height() / 2;

          $(element).click(function() {
            // console.log(modal)

            $(".modal-message").html(message);

            modal.style.display = "block";        
            var modalContentHeight = $('.modal-content').height();
            var modalMarginTop = "40px";   
              $('.modal-content').css(
                'marginTop', modalMarginTop,
              )

          });


          $(".loginModalClose").click(function() {
            modal.style.display = "none";
          });

          $(window).click(function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
          });
        }
    };
});



app.directive('vacancySelect2', function ($compile) {
    return {
        link: function ($scope, element, attrs) {

            $(element).select2();
            $(element).on('select2:select', function (e) {
              // Do something
              var data = e.params.data;
              if(data.text == "All") {
                $(element).val(null).val('All').trigger('change');
                $scope.jobsData.vacancy_type = "All";
              }else{
                $scope.jobsData.vacancy_type = $(element).val();
              }
              $scope.getJobs();
            });


            $(element).on('select2:unselect', function (e) {
              // Do something
              var data = e.params.data;

              if(data != undefined) {

                if(data.text == "All") {
                  $(element).val(null).trigger('change');
                }
                $scope.jobsData.vacancy_type = $(element).val();

                $scope.getJobs();
              }
              
            });

        }
    };
});


app.directive('datepostedSelect2', function ($compile) {
    return {
        link: function ($scope, element, attrs) {

            $(element).select2();
            $(element).on('select2:select', function (e) {
              // Do something
              var data = e.params.data;
              if(data.text == "All") {
                $(element).val(null).val('All').trigger('change');
                $scope.jobsData.date_posted = "All";
              }else{
                $scope.jobsData.date_posted = $(element).val();
              }
              $scope.getJobs();
            });


            $(element).on('select2:unselect', function (e) {
              // Do something
              var data = e.params.data;

              if(data != undefined) {

                if(data.text == "All") {
                  $(element).val(null).trigger('change');
                }
                $scope.jobsData.date_posted = $(element).val();

                $scope.getJobs();
              }
              
            });

        }
    };
});

app.directive('accountsSelect2', function ($compile) {
    return {
        link: function ($scope, element, attrs) {

            $(element).select2();
            $(element).on('select2:select', function (e) {
              // Do something
              var data = e.params.data;
              if(data.text == "All") {
                $(element).val(null).val('All').trigger('change');
                $scope.jobsData.account_types = "All";
              }else{
                $scope.jobsData.account_types = $(element).val();
              }
              $scope.getJobs();
            });


            $(element).on('select2:unselect', function (e) {
              // Do something
              var data = e.params.data;

              if(data != undefined) {

                if(data.text == "All") {
                  $(element).val(null).trigger('change');
                }
                $scope.jobsData.account_types = $(element).val();

                $scope.getJobs();
              }
              
            });

        }
    };
});

app.directive('closeRegister', function ($timeout, $compile) {
  return {

    link : function ($scope, element, attrs) {
      $(element).click(function(e) {
        if($(element).hasClass("close-register-form")){                
          $('#close-slider-form').trigger('click')
        }
      });      
    }
  };
});

app.directive('showSlider', function ($timeout, $compile) {
    return {
        link: function ($scope, element, attrs) {
          if(attrs.sliderWidth != undefined && attrs.sliderWidth != "") {
              $("#"+attrs.idTarget).css("width", attrs.sliderWidth);
              // console.log($("#"+attrs.idTarget).find(".slider-form"))
          }
          


          $(element).click(function(e) { 

              var class_target = attrs.class_target;
              var id_target = attrs.idTarget;
              // console.log($(e.target).parents(".no-slide").length)
              if($(e.target).hasClass("no-slide") || $(e.target).parents(".no-slide").length > 0) {
                return;
              }
              
              $timeout(function() {
                  if($(element).hasClass("close-prev-slider")){
                    $('#close-slider-form').trigger('click')
                  }

                  if($(element).hasClass("set-appform-jobid")){                
                    $('#btn-apply').data('value',$(element).data('value'))                
                    $('#btn-apply').data('id',$(element).data('id'))                     

                  }

                  if($(element).hasClass("close-prev-slider")){                
                    $scope.selected_job_id = $(element).data("value")
                    $scope.save_job_id = $(element).data("value")
                    // console.log($(element).data("value"))
                    //$scope.inquireMessage = 'Hi {company_name},\n\tI noticed your job #'+$(element).data("value")+' and would like to know more about the position. We can discuss any details over chat.'               
                  }                              

                  var right = $("#"+id_target).width()

                  $("#"+id_target).css("right","0%");
                  $(".slider-form-overlay").removeClass("hidden");
                  $(".slider-form-close").removeClass("hidden")
                  document.body.style.overflow = "hidden";
                  var id = attrs.showSlider;
                  $scope.updateSelectedIndex(id);


                  $(window).resize(function() {
                    $("#"+id_target).css("right","0%");
                  })  

              }, 400)
                  
              
          });
          $('.hideslider').click(function(){
              var class_target = attrs.class_target;
              var id_target = attrs.idTarget;


              $("#"+id_target).css("right","-500%");
              $(".slider-form-overlay").addClass("hidden");
              $(".slider-form-close").addClass("hidden");
              document.body.style.overflow = "";            
          })
          $(document).on("click", ".slider-form-close" , function() {

              var class_target = attrs.class_target;
              var id_target = attrs.idTarget;


              $("#"+id_target).css("right","-500%");
              $(".slider-form-overlay").addClass("hidden");
              $(".slider-form-close").addClass("hidden");
              document.body.style.overflow = "";
          });
          $(".slider-form-overlay").click(function() {

              var class_target = attrs.class_target;
              var id_target = attrs.idTarget;

              $("#"+id_target).css("right","-500%");
              $(".slider-form-overlay").addClass("hidden");
              $(".slider-form-close").addClass("hidden");
              document.body.style.overflow = "";
          });

          $(document).keyup(function(e) {
            var class_target = attrs.class_target;
            var id_target = attrs.idTarget;
            if (e.keyCode == 27) { // escape key maps to keycode `27`
               $("#"+id_target).css("right","-500%");
               $(".slider-form-overlay").addClass("hidden")
               document.body.style.overflow = "";
            }
          });
        }
    };
});

app.directive('hideSelection', function ($compile) {
    return {
        link: function ($scope, element, attrs) {
            $(document).click(function(e) {
              if ($(e.target).closest(".not-close").length === 0 && $(e.target).closest(".select2-selection__choice").length === 0) {
                  $scope.active_section = "";
                  $scope.$apply();
              }
            });

        }
    };
});

app.directive('mapFilter', function ($compile) {
    return {
        link: function ($scope, element, attrs) {
           $(element).vectorMap({
              map : 'us_aea_en',
              backgroundColor : 'transparent',
              zoomOnScroll: false,
              regionStyle : {
                  initial : {
                      fill : '#c9d6de'
                  }
              },
              markers: [{
                      latLng : [40.71, -74.00],
                      name : 'Newyork: 250'
                      , style: {fill: '#1e88e5'}
                  },{
                      latLng : [39.01, -98.48],
                      name : 'Kansas: 250'
                      , style: {fill: '#fc4b6c'}
                  },
                {
                  latLng : [37.38, -122.05],
                  name : 'Vally : 250'
                  , style: {fill: '#26c6da'}
                }]
          });


        }
    };
});


app.directive('limitTitle',function ($compile){
     return {
        link: function ($scope, element, attrs) {
           
          // console.log(attrs)          
          $(element).html(attrs.jobTitle)
        }
    };
})

app.directive('textEllipsis',function ($compile){
     return {
        link: function ($scope, element, attrs) {          
          // console.log(element[0].innerText)   
                    
        }
    };
})

app.directive('uploadFile',function ($compile){
     return {
        link: function ($scope, element, attrs) {          
          $(element).click(function(){
            $('#inquirerCV').trigger('click')
          })
        }
    };
})

app.directive('showPolicy',function($compile){
  return {
    link: function($scope, element, attrs){
      $(element).click(function(){
        window.open('/policy', '_blank');
      })
    }
  }
})

app.directive('showTerms',function($compile){
  return {
    link: function($scope, element, attrs){
      $(element).click(function(){
        window.open('/terms-of-use', '_blank');
      })
    }
  }
})

app.directive('showSearch',function($compile){
  return {
    link: function($scope, element, attrs){
      var searchBar = attrs.target        

      $("#show-search span").text('Hide Custom Search')
      $('#show-search').addClass('hide')
      $("#show-search .circle-plus").text("-")
      $(this).addClass('shown')
      $('.'+searchBar).addClass('active')
      $('#left-content').addClass('resize')
      $('.list-left-content').removeClass('fadeIn')
      $('.list-left-content').addClass('fadeOut')
      
      $(element).click(function(){
        
        if($(element).hasClass('shown')){
          $("#show-search span").text('Show Custom Search')          
          $("#show-search .circle-plus").text("+")
          $(this).removeClass('shown')
          $('.'+searchBar).removeClass('active')
          $('#left-content').removeClass('resize')
          $('.list-left-content').removeClass('fadeOut')
          $('.list-left-content').addClass('fadeIn')
        }else{          
          $("#show-search span").text('Hide Custom Search')
          $('#show-search').addClass('hide')
          $("#show-search .circle-plus").text("-")
          $(this).addClass('shown')
          $('.'+searchBar).addClass('active')
          $('#left-content').addClass('resize')
          $('.list-left-content').removeClass('fadeIn')
          $('.list-left-content').addClass('fadeOut')
        }
      })      
    }
  }
})

app.directive('hideSearch',function($compile){
  return {
    link: function($scope, element, attrs){
      $(element).click(function(){   
        var searchBar = attrs.target
        $('#show-search').removeClass('hide')            
        $("#show-search span").text('Show Custom Search')          
        $("#show-search .circle-plus").text("+")
        $('#show-search').removeClass('shown')
        $('.'+searchBar).removeClass('active')
        $('#left-content').removeClass('resize')
        $('.list-left-content').removeClass('fadeOut')
        $('.list-left-content').addClass('fadeIn')
      })      
    }
  }
})

app.directive('dropDown',function($compile){
  return {
    link : function($scope,element,attrs){
      var options = element.find("option")

      $(options).click(function(){                
        $(this).remove()        
      }) 

      $(element).click(function(){   
        $('#dropDownContent').addClass('active')        
      })      
    }
  }
})

app.directive('dropDownContent',function(){
  return {
    link : function($scope,element,attrs){
      var options = $('.txt-search-dd-btn').find("option")
      // console.log(options)                 
    }
  }
})

app.directive('closeTaginput',function(){
  return {
    link : function($scope,element,attrs){
      $(element).click(function(){
        $('#dropDownContent').removeClass('active')
      })
    }
  }
})

app.directive('inputTags',function(){
  return {
    restrict: 'A',
    link : function($scope, element, attrs){
      $scope.$watch(attrs.ngModel, function (v) {
          // console.log($scope.inputs);
      });

      element.bind("keydown keypress", function (event) {
        if(event.which === 13) {            
            if($scope.inputs != undefined && $scope.inputs != ""){
              // console.log($scope.inputs)
              event.preventDefault();
              var CheckExist = $('.txt-search-dd-btn').find('option[value="'+$scope.inputs+'"]')
              if(CheckExist.length == 0){
                $('.txt-search-dd-btn').append('<option value='+ $scope.inputs +'>'+ $scope.inputs +'</option>')

                var options = $('.txt-search-dd-btn').find("option")
                $(options).click(function(){        
                  $(this).remove()
                })

                $('.txt-search-dd-btn option[value="'+ $scope.inputs +'"]').css({

                })

              }else{
                swal("This tag is already added!")
              }              
            }else{
              swal("not valid")
            }            
        }
      });
    }
  }
})


app.directive('bounceRight',function(){
  return {
    link : function($scope,element,attrs){  
      setTimeout(function () {
        $(element).addClass('animated fadeInRight')
        $(window).scroll(function(){
            // console.log($(this).scrollTop())                    
            if(attrs.id == "first-row"){
              if($(this).scrollTop() <= 550){
                $(element).addClass('animated fadeInRight')
              }else{
                // $(element).removeClass('animated fadeInRight') 
              }
            }              
        });
      },1000)    
            
    }
  }
})

app.directive('bounceLeft',function(){
  return {
    link : function($scope,element,attrs){      
      setTimeout(function () {
        $(element).addClass('animated fadeInLeft')

        $(window).scroll(function(){
            console.log($(this).scrollTop())                              

            if(attrs.id == "detail-container"){
              if($(this).scrollTop() <= 610){
                $(element).addClass('animated fadeInLeft')
              }else{
                // $(element).removeClass('animated fadeInLeft') 
              }
            }

            if(attrs.row == "second"){
              if($(this).scrollTop() <= 900){
                $(element).addClass('animated fadeInLeft')
              }else{
                // $(element).removeClass('animated fadeInLeft')
              }
            }

            if(attrs.row == "third"){
              if($(this).scrollTop() <= 1200){
                $(element).addClass('animated fadeInLeft')
              }else{
                // $(element).removeClass('animated fadeInLeft')
              }
            }

        });   
      },1000)
           
    }
  }
})

app.directive('bounceIn',function(){
  return {
    link : function($scope,element,attrs){  
      setTimeout(function () {
        $(element).addClass('animated rollIn')

        $(window).scroll(function(){
            console.log($(this).scrollTop())                    
            if(attrs.id == "detail-container"){
              if($(this).scrollTop() <= 610){
                $(element).addClass('animated rollIn')
              }else{
                // $(element).removeClass('animated rollIn') 
              }
            }              
        });  
      },1000)

          
    }
  }
})

app.directive('bounceRight',function(){
  return {
    link : function($scope,element,attrs){      
      setTimeout(function () {
        $(window).scroll(function(){
            // console.log($(this).scrollTop())                    
            if(attrs.id == "detail-container"){
              if($(this).scrollTop() <= 610){
                $(element).addClass('animated fadeInRight')
              }else{
                // $(element).removeClass('animated fadeInRight') 
              }
            }              
        });      
      },1000)      
    }
  }
})

app.directive('animateHover',function($compile){
  return {
    link: function($scope, element, attrs){          
      $(element).mousemove(function( e ) {
        const x = e.offsetX;
        const y = e.offsetY;
        e.target.style.setProperty('--x', `${ x }px`)
        e.target.style.setProperty('--y', `${ y }px`)        
      });
    }
  }
})

app.directive('testHover',function($compile){
  return {
    link: function($scope, element, attrs){          
      $(element).mousemove(function( e ) {
        const x = e.pageX - e.target.offsetLeft
        const y = e.pageY - e.target.offsetTop
        e.target.style.setProperty('--x', `${ x }px`)
        e.target.style.setProperty('--y', `${ y }px`)
        // console.log(y)
      });
    }
  }
})


