var app = angular.module('app', ['LocalStorageModule']);

var base_url = "/";
var APP_VERSION_URL = "?VERSION=1.0-E";

app.config(function (localStorageServiceProvider) {
  localStorageServiceProvider
    .setPrefix('anesthesia');
});




app.filter('to_trusted', ['$sce', function($sce){
    return function(text) {
         var doc = new DOMParser().parseFromString(text, "text/html");
         var   rval= doc.documentElement.textContent;
        // console.log(rval)
        return $sce.trustAsHtml(rval)
    };
}]);


app.filter('noFractionCurrency',
[ '$filter', '$locale', function(filter, locale) {
  var currencyFilter = filter('currency');
  var formats = locale.NUMBER_FORMATS;
  return function(amount, currencySymbol) {
	var value = currencyFilter(amount, currencySymbol);
	var sep = value.indexOf(formats.DECIMAL_SEP);
	console.log(amount, value);
	if(amount >= 0) { 
	  return value.substring(0, sep);
	}
	return value.substring(0, sep) + ')';
  };
} ]);


app.filter('capfirst', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});


app.filter('stringReplace',
[ '$filter', '$locale', function(filter, locale) {

	
  	var currencyFilter = filter('currency');
  	var formats = locale.NUMBER_FORMATS;
    return function(amount, currencySymbol) {
	  if(amount == undefined) {
	  	return;
	  }
	  var newstr = amount.toString().replace(".00","");   
	  return newstr;
    }  
 
} ]);

app.filter('stringJobType',
[ '$filter', '$locale', function(filter, locale) {

  	var currencyFilter = filter('currency');
  	var formats = locale.NUMBER_FORMATS;

    return function(param, currencySymbol) {	

      
	 	if(param==1){
			return 'Anesthesiologist';
		}else{
			return 'CRNA';	
		}
    }  
 
} ]);

