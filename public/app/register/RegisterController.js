
app.controller('RegisterController', function($scope,AunthenticationService , $timeout,$window, $api, LoginService,localStorageService) {

	$scope.selectedTitles = [];
	$scope.regData = [];
	$scope.rdSteps = [];
	$scope.rdSteps['registration'] = [];
	$scope.rdSteps['registration']['current_step'] = 1;


	$('#btn-page-down').click(function(){			
			
	})	

	$scope.selected_account	= "";

	$scope.setSelectedAccount = function() {
		LoginService.setSelectedAccount($scope.selected_account);
	}

	$scope.selectAccountType = function(account_type) {
		$scope.rdSteps['registration']['step1'] = [];
		$scope.rdSteps['registration']['step1']['account_type'] = account_type;
		$scope.rdSteps['registration']['step1']['status'] = true;
		$scope.rdSteps['registration']['current_step'] = 2;

		$scope.selected_account = account_type;
		$scope.setSelectedAccount();
		$scope.getSelectedAccount();
	}
	

	$scope.getSelectedAccount = function() {
		$scope.selected_account = LoginService.getSelectedAccount();

		if($scope.selected_account == undefined || $scope.selected_account == "") {
			return;
		}
		if($scope.selected_account == "candidate") {
			$scope.changeSelectedTitle(1);//candidate id
			$scope.regData.account_type = 1;
		}else{
			$scope.changeSelectedTitle(2);//job provider id
			$scope.regData.account_type = 2;
		}
		
	}

	$scope.addClass = function($event) {
		console.log($event)

		$($event).parents(".section").addClass("slider-faded");
	}

	$scope.proceedToStep = function(step) {
		$scope.rdSteps['registration']['current_step'] = step;
	}

	$scope.getUserTypes = function() {
		var url = "/user/account_types";
		$api.get(url).then(function(response) {
			$scope.userTypes = response.data;
		});
	}

	$scope.getAccountTitles = function() {
		var url = "/user/account_titles";
		$api.get(url).then(function(response) {
			$scope.userAccountTitles = response.data;
			$scope.getSelectedAccount();
		});
	}

	$scope.changeSelectedTitle = function(id) {
		$scope.selectedTitles = [];
		$scope.regData.account_type = id;

		angular.forEach($scope.userAccountTitles, function(obj) {
		   if(id == obj.user_type_id) {
		   		$scope.selectedTitles.push(obj);
		   }
		});
	}

	$scope.submitRegistrationForm = function() {

		var privacyChecked = $('input[name="reg-privacy-register"]').prop('checked')		
		if(privacyChecked == false){
			swal("Sorry!", "Please Accept the privacy and terms of use.", "info");
			$scope.activeButton.stop();
			return;
		}
		
		var url = "/user/new";
		var data = $scope.regData;
		data["selected"] = $scope.selected_account;
		console.log(data)
		$api.post(data, url).then(function(response) {
			if(response.data.status == true) {
				swal("Registration: ", "You have been registered. Redirecting..", "success");
				//$scope.activeButton.stop();
				$scope.regData = {};
				$scope.regForm.$setPristine();


				AunthenticationService.setAuth(true);
				AunthenticationService.setData(response.data.credentials);
				localStorageService.set("credentials", response.data.credentials);
				localStorageService.set("profile", response.data.profile);


				$timeout(function() {
					$window.location.href = '/application';
				}, 1500);
				
			}else if(response.data.status == false){
				console.log(response.data.message)
				$('#error-modal').modal('show')
				swal("Registration: ", response.data.message, "warning");
				$scope.activeButton.stop();
				//$scope.regData.email = "";
			}
		});
	}

	$scope.accountTitlePlaceHolder = function(id) {		
		if(id == 1) {
			return "Please Select Your Profession.";
		}else{
			return "Please Select Your Account Type.";
		}
	}

	$scope.getUserTypes();
	$scope.getAccountTitles();
	
});