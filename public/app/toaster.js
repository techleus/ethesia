
function notif_danger(heading,text) {

	$.toast({
	    heading: heading,
	    text: text,
	    position: 'top-right',
	    loaderBg:'#ff6849',
	    icon: 'error',
	    hideAfter: 3500
	    
	});
}

function notif_success(heading,text) {
  	$.toast({
        heading: heading,
        text: text,
        position: 'top-right',
        loaderBg:'#ff6849',
        icon: 'success',
        hideAfter: 3500, 
        stack: 6
    });
}


function notif_warning(heading,text) {
  	$.toast({
	    heading: heading,
	    text: text,
	    position: 'top-right',
	    loaderBg:'#ff6849',
	    icon: 'warning',
	    hideAfter: 4000, 
	    stack: 6
	});
}

