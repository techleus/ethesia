app.controller('LoginController', function($scope, $timeout, $api,localStorageService,AunthenticationService, $window) {

	$scope.token;
	$scope.loginData = {};

	
	if($.cookie('remember') == 'true'){
		$scope.rememberme = true;
		$scope.loginData.email = $.cookie('email');
		$scope.loginData.password = $.cookie('password');
	}else{
		$scope.loginData.email = "";
		$scope.loginData.password = "";
	}

/*	TokenService.get().then(function(response) {
		$scope.token = response;
	});
*/
	$scope.isAuthenticated = function() {
    	return false;
    }
    $scope.sendConfirmationEmail = function() {
    	var url = "/emails/sendConfirmationEmail";
    	var data = $scope.loginData;
    	
    	$api.post(data, url).then(function(response) {
    		if(response.data.status == true) {
    			//
    			$.notify("Email confirmation was sent. Check ur email.", "success");
    		}
		});
    }
    $scope.resendPassword = function() {    
    	var url = "/emails/sendPasswordForgot";
    	var data = $scope.resendPasswordData;
    	    	
    	$api.post(data, url).then(function(response) {
    		$scope.activeButton.stop();
    		if(response.data.status == true) {
    			$scope.successMessage = response.data.message;
    			$timeout(function() {
    				$scope.successMessage = "";
    			}, 3000);
    		}else{

    			$scope.errorMessage = response.data.message;

    			$timeout(function() {
    				$scope.errorMessage = "";
    			}, 3000);
    			
    		}
		});
    	    	
    	if(grecaptcha.getResponse() == null || grecaptcha.getResponse() == "") {
    		$scope.errorMessage = "Captcha is invalid. ";
    		$scope.$apply();
			$timeout(function() {
				$scope.errorMessage = "";
			}, 3000);
    		$scope.activeButton.stop();
    	}else{
    		var url = "/emails/sendPasswordForgot";
	    	var data = $scope.resendPasswordData;
	    	
	    	$api.post(data, url).then(function(response) {
	    		$scope.activeButton.stop();
	    		if(response.data.status == true) {
	    			$scope.successMessage = response.data.message;
	    			$timeout(function() {
	    				$scope.successMessage = "";
	    			}, 3000);
	    		}else{

	    			$scope.errorMessage = response.data.message;

	    			$timeout(function() {
	    				$scope.errorMessage = "";
	    			}, 3000);
	    			
	    		}
			});
    	}
    }
	$scope.submitLogin = function() {
		
		angular.forEach($scope.loginForm.$error.required, function(field) {
		    field.$dirty = true;
		    field.$setPristine();
		});

		if($scope.loginForm.$valid) {

			AunthenticationService.login($scope.loginData).then(function(response) {
				
				if(response.data.status == true) {

					if($scope.rememberme ==true){
						$.cookie('email', $scope.loginData.email, { expires: 14 });
			            $.cookie('password', $scope.loginData.password, { expires: 14 });
			            $.cookie('remember', true, { expires: 14 });                   			
					}else{
						$.cookie('email', null, {});
			            $.cookie('password', null, {});
			            $.cookie('remember', null, {});
					}

					$scope.activeButton.stop();
					$scope.loginData.password = "";
					$scope.loginForm.$setPristine();
					AunthenticationService.setAuth(true);
					$scope.$parent.authenticated = true;
					AunthenticationService.setData(response.data.credentials);
					localStorageService.set("credentials", response.data.credentials);
					localStorageService.set("profile", response.data.profile);
					console.log(response.data)
					$scope.loginSuccess  = true;
					$scope.confirmedEmail = true;
					$timeout(function() {
						$window.location.href = '/application';
					},1500);
				}
				else if(response.data.status == "unconfirmed") {
					$scope.activeButton.stop();
					$scope.loginData.password = "";
					$scope.loginSuccess  = false;
					$scope.confirmedEmail = false;
					$('.modal-content').css({
						height : '570px',
						position: 'absolute',
					  	top: 'calc( 50% - 310px )',
					  	left: 'calc( 50% - 325px )', 
					})
				}else{
					//$.notify("Wrong Credentials, please try again.", "warning");
					$scope.activeButton.stop();
					$scope.loginData.password = "";
					$scope.loginSuccess  = false;
					$scope.loginError = response.data.message
					$('.modal-content').css({
						height : '570px',
						position: 'absolute',
					  	top: 'calc( 50% - 310px )',
					  	left: 'calc( 50% - 325px )', 
					})
				}
			});
		}
	}

	//AunthenticationService.logout()

});