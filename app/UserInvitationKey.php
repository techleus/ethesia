<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInvitationKey extends Model
{
    protected $table = 'user_invitation_key';

    protected $fillable = ['provider_id','invited_email','invitation_key'];
}
