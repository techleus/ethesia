<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class applicationMessagesAttachment extends Model
{
    protected $table = 'application_messages_attachments';
    protected $fillable = ['attachment_id', 'attachment_file'];
}
