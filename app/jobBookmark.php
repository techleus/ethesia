<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobBookmark extends Model
{
    protected $table = 'job_bookmark';

    public function Jobs()
    {
    	return $this->belongsTo('App\Jobs','jobId');
    }

    public function User()
    {
    	return $this->belongsTo('App\User','userId');
    }

    public function profile()
    {
    	return $this->belongsTo('App\UsersProfile','userId','userID');
    }

}
