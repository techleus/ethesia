<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTitles extends Model
{
    protected $table = "user_account_titles";
}
