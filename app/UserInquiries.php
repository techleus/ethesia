<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInquiries extends Model
{
    protected $table = 'user_inquiries';


    public function Job()
    {
        return $this->belongsTo('App\Jobs', 'job_id');
    } 

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function profile()
    {
        return $this->belongsTo('App\UsersProfile', 'user_id','userID');
    }

    public function attachments()
    {
    	return $this->hasMany('App\applicationMessagesAttachment','attachment_id');
    }   
}
