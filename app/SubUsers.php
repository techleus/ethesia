<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubUsers extends Model
{
    protected $table = 'sub_users';

    protected $fillable = ['user_id','sub_user_id'];
    
}
