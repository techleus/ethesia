<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserContactList extends Model
{
    protected $table = "user_contact_list";

    public function user()
    {
    	return $this->hasOne('App\User', 'id','user_contact_id');
    }

    public function userlist()
    {
    	return $this->hasMany('App\User','id','user_contact_id');
    }

    

}
