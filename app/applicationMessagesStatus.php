<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class applicationMessagesStatus extends Model
{
    protected $table = 'application_messages_status';
}
