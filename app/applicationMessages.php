<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class applicationMessages extends Model
{
    protected $table = 'application_messages';
    protected $fillable = ['sender_id','recipient_id','subject','message','status'];

    public function users()
    {
    	return $this->belongsTo('App\User','sender_id');
    }

    public function users2()
    {
        return $this->belongsTo('App\User','recipient_id');
    }

    public function attachments()
    {
    	return $this->hasMany('App\applicationMessagesAttachment','attachment_id');
    }

    public function profile()
    {
        return $this->belongsTo('App\UsersProfile','sender_id', 'userID');
    }
}
