<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\JobBookmark;

class Jobs extends Model
{
    //
    protected $table = "jobs_list";
    protected $dates = ['created_at', 'updated_at'];


    public function user()
    {
        return $this->belongsTo('App\User', 'userID');
    }  

    public function profile()
    {
        return $this->belongsTo('App\UsersProfile', 'userID','userID');
    }   

    public function cases()
    {
        return $this->hasOne('App\JobsCases', 'job_id');
    } 

    public function bookmark()
    {
        return $this->hasMany('App\JobBookmark', 'jobId');
    }
    public function upgrade()
    {
        return $this->hasOne('App\JobUpgrade', 'job_id');
    }   

    public function inqueries()
    {
        return $this->hasMany('App\UserInquiries', 'job_id');
    }
  
    
}
