<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationMessageNotifications extends Model
{
    
    protected $table = 'application_message_notification';
	
	protected $fillable = ['user_id','sms_application','sms_messages','sms_job_expiration','email_applicaton','email_messages','email_job_expiration'];

}
