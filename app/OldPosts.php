<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OldPosts extends Model
{
    protected $table = "wp_posts";
}
