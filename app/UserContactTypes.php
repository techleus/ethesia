<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserContactTypes extends Model
{
    protected $table = "user_contact_label";
}
