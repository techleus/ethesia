<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatesAndCity extends Model
{
    protected $table = "state_city";
}
