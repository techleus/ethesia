<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Auth;
use App\User;
use App\UserContactTypes;
use App\UserContactList;
use App\SubUsers;

class UserContactController extends Controller
{
    public function userContactType()
    {
    	return UserContactTypes::where('user_id',Auth()->user()->id)->get();
    }
    public function UserList()
    {
    	$users = new User;        
    	$user_id = Auth()->user()->id;
        $users = $users
        			->where('id','!=',$user_id)
                    ->where('userType',2)
                    ->with('userAccountTitle')
        			->with(['contacts'=>function($query){
                        $query->where('user_id',Auth()->user()->id);
                    }])        			
        			->get();
        return $users;
    }
    public function searchUserlist(Request $r)
    {
    	$users = new User;        
        $users = $users->where('username',$r->username)
        ->where('userType',2)
        ->orWhere('username', 'like', '%' . $r->username . '%')->with('userAccountTitle')->with(['contacts'=>function($query){
                        $query->where('user_id',Auth()->user()->id);
                    }])->get();
        return $users;
    }
    public function addtocontacts(Request $r, SubUsers $subusers)
    {	    	
    	$UserContactList = new UserContactList();
    	$UserContactList->user_id 			= Auth()->user()->id;
    	$UserContactList->user_contact_id	= $r->contactId;
    	$UserContactList->contact_label_id	= $r->contactgroupid;
    	$UserContactList->notif_on_new_post	= "Yes";
    	$UserContactList->save();


        $subusers = $subusers->create([
            'user_id' => Auth()->User()->id,
            'sub_user_id' => $r->contactId, 
        ]);

    	return "Successfully Added on contacts";
    }
    public function showUserContactList(Request $r)
    {
    	if($r->username){
    		
    		$username = $r->username;
    		
    		return UserContactList::where('user_id','=',Auth()->user()->id)
    		->whereHas('userlist', function($query) use ($username){
    			$query->where('username','like','%' . $username . '%');
    		})    		
    		->with(['user'=>function($query){
                $query->where('userType',2);
            }],'user.userAccountTitle','user.userProfile')    		
    		->get();
    		
    		
    	}    		
    		
		return UserContactList::where('user_id','=',Auth()->user()->id)    		    	
    		->with(['user'=>function($query){
                $query->where('userType',2);
            }],'user.userAccountTitle','user.userProfile')    		
    		->get();

    }
    
    public function showcontactbylabel(Request $r)
    {
    	if($r->label_id != 0){    		
    		return UserContactList::where('contact_label_id','=',$r->label_id)
            ->where('user_id',Auth()->user()->id)    		    	
    		->with(['user'=>function($query){
                $query->where('userType',2);
            }],'user.userAccountTitle','user.userProfile')    		
    		->get();
			
    	}else{
    		return UserContactList::where('user_id','=',Auth()->user()->id)    		    	
            ->where('user_id',Auth()->user()->id)
    		->with(['user'=>function($query){
                $query->where('userType',2);
            }],'user.userAccountTitle','user.userProfile')    		
    		->get();    	
    	}
    	
    }
    public function removeContact(Request $r,SubUsers $subusers)
    {
    	UserContactList::destroy($r->id);

        $subusers->destroy($r->id);
    	
        return "Successfully removed from contacts";
    }
    public function countcountacts(Request $r)
    {   
    	return UserContactList::where('user_id',Auth()->user()->id)->where('contact_label_id',$r->label_id)->count();
    }

    public function up_notif_settings(Request $r){
    	$UserContactList = UserContactList::find($r->contactId);    	
    	$UserContactList->notif_on_new_post = $r->allowNotif;
    	$UserContactList->save();
    	return $r;
    }
}
