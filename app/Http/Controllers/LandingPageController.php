<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs;
use App\UserTitles;
use Auth;
use App\User;
use App\UsersProfile;
use App\StatesAndCity;
use App\JobProvider;
use App\JobBookmark;
use Carbon\Carbon;
use App\UserTypes;
use App\UsersEducation;
use App\UsersExperience;
use App\UsersCreditCardDetails;
use App\UsersECheckDetails;
use App\UserInquiries;
use App\SecondaryEmail;
use App\applicationMessages;
use Cartalyst\Stripe\Stripe;
use App\Jobs\NewCandidateAppliedJob;
use App\Mail\NewApplicationNotification;
use App\Events\Notifications;
use App\ApplicationMessageNotifications;
use App\applicationMessagesAttachment;
use Twilio\Rest\Client;
use Storage;
use Hash;
use Image;
use File;
use DB;
use Session;
use Mail;




class LandingPageController extends Controller
{   

    public function logmeout()
    {
        Auth::logout();
    }
    
    public function uploadInquirerCV(Request $rq,ApplicationMessageNotifications $notifications, SecondaryEmail $secondaryemail)
    {        

        $id = Auth::User()->id;
        $name = null;
        if($rq->hasFile('file'))
        {
            $file = $rq->file("file")[0];
            $name = $file->getClientOriginalName();      
            $count = 1;                    

            while(Storage::disk('public')->has("uploads/cv/$id/$name")) {
                $name = $count."-".$name;
                $count++;
            }

            Storage::disk("public")->put("uploads/cv/$id/$name",file_get_contents($rq->file("file")[0]));    
        }
        

        /*$storagePath_raw = "/cv/user/$id/$name";
        $storagePath = url('/').$storagePath_raw;*/

        $inquiries = new UserInquiries();
        $inquiries->user_id = $id;
        $inquiries->job_id = $rq->job_id;
        $inquiries->message = $rq->message;
        $inquiries->cv_file_name = $name;
        $inquiries->is_anonymous = $rq->is_anonymous;
        $inquiries->save();        

        $profile = UsersProfile::where("userID", $id)->first();

        $job_owner = Jobs::find($rq->job_id);
        

        $params["url"]         = url('/');
        $params["subject"]     = "New Candidate Application.";
        $params["email_from"]  = "noreply@ethesia.com";
        $params["job_id_src"]  = $rq->job_id;
        $params["id"]          = $inquiries->id;
        $params["user_id"]     = $job_owner->userID;
        $params["applicantemail"] = Auth::User()->email;
        $params["name"]        = $profile->firstName." ".$profile->lastName;
        $params["inquiry_url"] = url("/")."/application/#/notifications/".$inquiries->id;
        $params["inquiry"]     = $inquiries;
        $params["time_ago"]    = $this->getTimeAgo($inquiries->created_at);
        $params["to"]          = User::find($job_owner->userID)->email;        

        if($inquiries->is_anonymous == "no") 
        {
            $params["candidate_name"] = $profile->firstName." ".$profile->lastName;
        }
        else
        {
            $params["candidate_name"] = "Anonymous";
        }        

        if(count($notifications->where('user_id',$job_owner->userID)->get()) != 0)
        {
            if($notifications->where('user_id',$job_owner->userID)->first()->email_applicaton)
            {   
                Mail::send(new NewApplicationNotification($params));            

                $secondaryemail = $secondaryemail->where('user_id',$job_owner->userID)->get();

                foreach ($secondaryemail as $value) {
                    
                    $params["to"] = $value->secondary_email;

                    Mail::send(new NewApplicationNotification($params));
                
                }
            }
            
            if($notifications->where('user_id',$job_owner->userID)->first()->sms_messages)
            {
                
                // $sid = 'ACc2de25ee3f6bf6e3606b70814766c02a';
                
                // $token = '9758a85f9172677b914810fd852e839b';
                
                // $client = new Client($sid, $token);
                
                // $smsfrom = '+17178332649';

                // $smsbody = 'Job #'. $rq->job_id .' has a new applicant! Someone by the name of '. $params["candidate_name"] .' has inquired about your job. Please sign in to your account in order to view the inquiry and contact the candidate.';

                // $client->messages->create(          
                //     '+16193946929',
                //     array(                
                //         'from' => $smsfrom,               
                //         'body' => $smsbody,
                //     )
                // );
            }
        }                       


        $applicationMessages = new applicationMessages();
        $applicationMessages = $applicationMessages->create(array(
            'sender_id' => Auth::User()->id,
            'recipient_id' => User::where('id',$job_owner->userID)->first()->id,
            'subject' => 'Job Application',
            'message' => $rq->message,
        ));


        if($rq->hasFile('file'))
        {            

            $attachment = $rq->file('file');
            
            foreach ($attachment as $file) {
                $filename = $file->getClientOriginalName();
                $applicationMessagesAttachments = new applicationMessagesAttachment();
                $applicationMessagesAttachments = $applicationMessagesAttachments->create(array(
                    'attachment_id' => $applicationMessages->id,
                    'attachment_file' => $filename
                ));
                $count = 1;                    
                while(Storage::disk('public')->has("appmessages/attachments/".Auth::User()->id."/".$job_owner->userID."/$filename")) {
                    $filename = $count."-".$filename;
                    $count++;
                }
                Storage::disk("public")->put("appmessages/attachments/".Auth::User()->id."/".$job_owner->userID."/$filename",file_get_contents($file));
            }
                                                
                        

            return $applicationMessagesAttachments;
        }

        broadcast(new Notifications($params))->toOthers();

        
        return array('success'=>true,'user_id'=>$id,'job_id'=>$rq->job_id,'Message'=>$rq->message,'cv_file_name'=>$rq->cv_file_name,'is_anonymous'=>$rq->is_anonymous);
    }
        
	public function showLandingPage()
	{
        $landing = true;        
		return view("homepage.content")->with("landing", $landing);
	}
    
    public function browseJobsV2(Request $request)
    {   
        return view("landing.content");
    }   

    public function browseJobsList(){
        return view("landing.list-content");
    }

	public function browseJobs(Request $request, $type)
	{	
        $request->session()->put('type', $type);
        Session::save();
		return view("landing.content");
	}	

    public function previewJobPostById($id) 
    {

        $jobs = Jobs::find($id);

        if($jobs == null) 
        {
             return redirect('/');
        }

        $data = $jobs;


        $data->job_type = $data->jobPostType == '1' ? "Anesthesiologist" : "CRNA";
        $data->time_ago = $this->getTimeAgo($data->created_at);
        $user = UsersProfile::where("userID" , $data->userID)->first();

        if($user != null) 
        {   
            if($user->image_path == "") 
            {
                $user->image_path = "/public/images/default.png";
            }
            $data->image_path = $user->image_path;
            $data->company_city = $user->city;
            $data->company_state = $user->state;
            $data->company_website = $user->website;
            $data->company_name = $user->company_name;
        } 
        else
        {
            $data->image_path = "/public/images/default.png";
            $data->company_city = "";
            $data->company_state = "";
            $data->company_website = "";
            $data->company_name = "";
        }


        //check high prio

        $data->highPrio = $data->priorityLevel == 1 ? true : false;


        $data["created_date"] = date("F d, Y", strtotime($data["created_at"]->toDateTimeString()));
        $data["updated_date"] = date("F d, Y", strtotime($data["updated_at"]->toDateTimeString()));


        
        return view("job_preview.content")->with("data", $data);



    }

    public function application_form($id,$fullname,$message){
        $data = array(
                    'id' => $id,
                    'fullname' => $fullname,
                    'message' => $message,
                );

        return view("applicationform_preview.content",$data);
    }
    public function redirectBrowseJobs(Request $request) 
    {   
        $data = $request->all();
        $request->session()->put('jobsData', $data);
        Session::save();
        $response = array();
        $response["status"] = true;
        return $response;
    }


	public function showPolicy()
    {
        return view("policy.policy");
    }

    public function showTermsOfUse()
    {
        return view("policy.terms-of-use");
    }

    public function getUserData()
    {
        $UsersProfile = UsersProfile::where('userID',Auth::User()->id)->first();
        return $UsersProfile;
    }

    public function get_job_userid(Request $rq)
    {
        $job_user_id = Jobs::where('id',$rq->jobId)->first();
        $UsersProfile = UsersProfile::where('userID',$job_user_id->userID)->first();
        return $UsersProfile;        
    }

    public function bookmarkJobProvider(Request $rq)
    {        
        if(isset($rq->providerId)){
            $bookmarkExist = JobProvider::where('userId',Auth::User()->id)
            ->where('jobProviderId',$rq->providerId)
            ->first();      
            if ($bookmarkExist === null) 
            {   
                $bookmark = new JobProvider;        
                $bookmark->userId = Auth::User()->id;
                $bookmark->jobProviderId = $rq->providerId;
                $bookmark->save();
                return "Added";
            }else
            {
                $removeBookmark = JobProvider::where('userId',Auth::User()->id)
                    ->where('jobProviderId',$rq->providerId)
                    ->first();              
                $removeBookmark->delete();
                return "Removed";
            }  
        }
    }

    public function getEmployers(Request $rq)
    {
        $users = new User;        
        $users = $users->where('userType',2)->with('userProfile')->with('userAccountTitle')->with('userJobProviderBookmark')->get();
        
        if(isset($rq->company_name))
        {
            $userProfile = array();
            foreach ($users as $user)
            {
                if(strpos(strtolower($user->userProfile['company_name']), strtolower($rq->company_name)) !== false)
                {                    
                     $userProfile[] = $user;
                }
            }
            return $userProfile;
        }else
        {
            return $users;
        }
        
    }

	public function browseEmployers()
	{	
		return view("employers.employers");
	}

	public function getAccountTypes() 
	{
		$titles = new UserTitles();
		$titles = $titles->where("user_type_id", 2)->get();//job provider

		return $titles;
	}

	public function getTimeAgo($carbonObject) 
	{  
        if($carbonObject == null) 
        {
            return "";
        }
	    return $carbonObject->diffForHumans();
	}
    public function searchJobs(Request $rq, Jobs $jobs)
    {
                
        $states = $rq->states;

        $key_searched = $rq->key_searched;
        
        $account_types = array();        


        if($rq->job_type != "")
        {
            $job_type = $rq->job_type == "Anesthesiologist" ? 1 : 2;            
        }else{
            $job_type = '';
        }


        $jobs = $jobs->Where(function ($query) use($key_searched) {
             for ($i = 0; $i < count($key_searched); $i++){
                $query->orwhere('ei_facilityCity', 'like',  '%' . $key_searched[$i] .'%');
             }

             for ($i = 0; $i < count($key_searched); $i++){
                $query->orwhere('ei_durPosition', 'like',  '%' . $key_searched[$i] .'%');
             } 

             for ($i = 0; $i < count($key_searched); $i++){
                $query->orwhere('ei_facilityState', 'like',  '%' . $key_searched[$i] .'%');
             } 

        })        
        ->where('jobPostType', $job_type)->with(["bookmark"=>function($query){
                    if(Auth::check()){
                        $query->where('userId',Auth::User()->id);
                    }                                    
                }],"cases","upgrade","user")->orderByDesc('priorityLevel');        
        
        if($rq->date_posted != "All" && $rq->date_posted != "") 
        {
           
            if(isset($rq->date_posted["last_hour"]) && $rq->date_posted["last_hour"] == true) 
            {      
                $jobs = $jobs->where('created_at','>=',Carbon::parse('-1 hour'));
            }
            if(isset($rq->date_posted["last_24"]) && $rq->date_posted["last_24"] == true) 
            {   
                $jobs = $jobs->where('created_at','>=',Carbon::parse('-24 hours'));
            }

            if(isset($rq->date_posted["last_7"]) && $rq->date_posted["last_7"] == true) 
            {
                $jobs = $jobs->where('created_at','>=',Carbon::parse('-7 days'));
            }

            if(isset($rq->date_posted["last_14"]) && $rq->date_posted["last_14"] == true) 
            {
                $jobs = $jobs->where('created_at','>=',Carbon::parse('-14 days'));
            }

            if(isset($rq->date_posted["last_39"]) && $rq->date_posted["last_39"] == true) 
            {
                $jobs = $jobs->where('created_at','>=',Carbon::parse('-39 days'));                                                
            }            
        }

        if($rq->states != "All" && $rq->states != "") 
        {
            $jobs->Where(function ($query) use($states) {
                 for ($i = 0; $i < count($states); $i++){
                    $query->orwhere('ei_facilityState', 'like',  '%' . $states[$i] .'%');
                 }      
            });
        }        

        if($rq->account_types != "All" && $rq->account_types != "") 
        {                                                     

            $jobs = $jobs->orWhereHas('user', function ($query) use ( $rq ) {

                        $account_types = array();

                        if(isset($rq->account_types["staffing_firm"]) && $rq->account_types["staffing_firm"] == true) 
                        {                            
                            array_push($account_types, 1);
                        }
                        if(isset($rq->account_types["hospital_surgery"]) && $rq->account_types["hospital_surgery"] == true) 
                        {                            
                            array_push($account_types, 2);
                        }
                        if(isset($rq->account_types["anesthesia_group"]) && $rq->account_types["anesthesia_group"] == true) 
                        {                            
                            array_push($account_types, 3);
                        }
                        if(isset($rq->account_types["management_firm"]) && $rq->account_types["management_firm"] == true) 
                        {                            
                            array_push($account_types, 4);
                        }
                        if(isset($rq->account_types["individual"]) && $rq->account_types["individual"] == true) 
                        {                            
                            array_push($account_types, 5);
                        }

                        $query->whereIn('userTitle',$account_types);

                    });   

        }

        if($rq->vacancy_type != "All"  && $rq->vacancy_type != "") 
        {

            $vacancy_type = array();

            if(isset($rq->vacancy_type["full_time"]) && $rq->vacancy_type["full_time"] == true) 
            {                   
                array_push($vacancy_type,'Full Time');                
            }

            if(isset($rq->vacancy_type["locum_tenens"]) && $rq->vacancy_type["locum_tenens"] == true) 
            {
                array_push($vacancy_type,'Locum Tenens');
            }


            if(isset($rq->vacancy_type["part_time"]) && $rq->vacancy_type["part_time"] == true) 
            {
                array_push($vacancy_type,'Part Time');
            }

            if(isset($rq->vacancy_type["prn"]) && $rq->vacancy_type["prn"] == true) 
            {
                array_push($vacancy_type,'PRN');
            }

            if(isset($rq->vacancy_type["fellowship"]) && $rq->vacancy_type["fellowship"] == true) 
            {
                array_push($vacancy_type,'Fewllowship');
            }

            $jobs = $jobs->whereIn('ei_durPosition',$vacancy_type);                
        }        
        
        
        $multiplier = 20;

        $totalJob = $jobs->count();        

        if($rq->current_index >= 1) 
        {
           $offset = $rq->current_index * $multiplier; 
        }
        else 
        {
            $offset = 0;
        }                    

        $jobs = $jobs->offset($offset)->limit($multiplier);
        
        $data = $jobs->where('post_status','=', 'active')->get();        
        
        foreach ($data as $key => $value) {
            $value->job_type = $value->jobPostType == '1' ? "Anesthesiologist" : "CRNA";
            $value->time_ago = $this->getTimeAgo($data[$key]->created_at);
            $user = UsersProfile::where("userID" , $value->userID)->first();

            if($user != null) 
            {   
                if($user->image_path == "") 
                {
                    $user->image_path = "/public/images/default.png";
                }
                $value->image_path = $user->image_path;
                $value->company_city = $user->city;
                $value->company_state = $user->state;
                $value->company_website = $user->website;
                $value->company_name = $user->company_name;
            } 
            else
            {
                $value->image_path = "/public/images/default.png";
                $value->company_city = "";
                $value->company_state = "";
                $value->company_website = "";
                $value->company_name = "";
            }


            //check high prio

            $value->highPrio = $value->priorityLevel == 1 ? true : false;

            if($value["created_at"]!= null && $value["updated_at"]!= null){
                $value["created_date"] = date("F d, Y", strtotime($value["created_at"]->toDateTimeString()));
                $value["updated_date"] = date("F d, Y", strtotime($value["updated_at"]->toDateTimeString()));
            }               

        }

        $data[] = array('total'=>$totalJob);

        return $data;
    }
    public function searchJobs1(Request $rq)
    {

    	$jobs = new Jobs();        
        $jobs = $jobs->with("bookmark", "cases","upgrade")->orderByDesc('priorityLevel');
        
        // $jobs = $jobs::where('post_status','active')->with("bookmark", "cases")->get();

        $multiplier = 20;
        $totalJob = $jobs->count();
        if(count($rq->key_searched) > 0)
        {
            foreach ($rq->key_searched as $search_key => $search) {                
                if(
                    $search != 'Staffing Firm' &&
                    $search != 'Hospital or Surgery Center' &&
                    $search != 'Anesthesia Group' &&
                    $search != 'Management Firm' &&
                    $search != 'Individual'                    
                )
                {                    
                    $jobs = $jobs->orHavingRaw("ei_facilityCity like '%".$search."%'");

                    $jobs = $jobs->orHavingRaw("ei_durPosition like '%".$search."%'");

                    $jobs = $jobs->orHavingRaw("ei_facilityState like '%".$search."%'");
                }                
            }
        }


        if($rq->job_type != "")
        {
            $job_type = $rq->job_type == "Anesthesiologist" ? 1 : 2;
            $jobs = $jobs->where('jobPostType', $job_type);
        }
    	



        if(isset($rq["state_city"]) != "") 
        {               
           
            $jobs = $jobs->whereRaw("CONCAT(ei_facilityCity, ' ', ei_facilityState) like '%".$rq["state_city"]."%'");
        }

        if(isset($rq["duration_position"]) && $rq["duration_position"] != "") 
        {
            $jobs = $jobs->orHavingRaw("ei_durPosition like '%".$rq["duration_position"]."%'");
        }


    	if($rq->states != "All" && $rq->states != "") 
    	{
    		if(count($rq->states) > 0) 
    		{                             
                foreach ($rq->states as $key => $state) 
    			{                               
	    			$jobs = $jobs->orHavingRaw("ei_facilityState like '%".$state."%'");                    
	    		}
    		}
    	}

        if($rq->cities != "All" && $rq->cities != "") 
        {
            if(count($rq->cities) > 0) 
            {
                foreach ($rq->cities as $key => $city) 
                {   
                    $jobs = $jobs->orHavingRaw("ei_facilityCity like '%".$city."%'");
                }
            }
        }
        

    	if($rq->account_types != "All" && $rq->account_types != "") 
    	{              
            
            $flag = 0;
                     
            if(isset($rq->account_types["anesthesia_group"]) && $rq->account_types["anesthesia_group"] == true) 
            {
                $flag++;
            }
            if(isset($rq->account_types["hospital_surgery"]) && $rq->account_types["hospital_surgery"] == true)
            {
                $flag++;
            } 
            if(isset($rq->account_types["individual"]) && $rq->account_types["individual"] == true) 
            {
                $flag++;
            }
            if(isset($rq->account_types["management_firm"]) && $rq->account_types["management_firm"] == true) 
            {
                $flag++;
            }
            if(isset($rq->account_types["staffing_firm"]) && $rq->account_types["staffing_firm"] == true) 
            {
                $flag++;
            }

            if(isset($rq->account_types["anesthesia_group"]) && $rq->account_types["anesthesia_group"] == true) 
            {
                if($flag > 1)
                {
                    $jobs = $jobs->orWhereHas('user', function ($query) {
                            $query->where('userTitle',3);
                        });
                }else{
                    $jobs = $jobs->whereHas('user', function ($query) {
                            $query->where('userTitle',3);
                        });
                }                
            }

            if(isset($rq->account_types["hospital_surgery"]) && $rq->account_types["hospital_surgery"] == true) 
            {
                if($flag > 1)
                {
                    $jobs = $jobs->orWhereHas('user', function ($query) {
                            $query->where('userTitle',2);
                        });
                }else{
                    $jobs = $jobs->whereHas('user', function ($query) {
                            $query->where('userTitle',2);
                        });
                }   
            }         

            if(isset($rq->account_types["individual"]) && $rq->account_types["individual"] == true) 
            {
                if($flag > 1)
                {
                    $jobs = $jobs->orWhereHas('user', function ($query) {
                            $query->where('userTitle',5);
                        });
                }else{
                    $jobs = $jobs->whereHas('user', function ($query) {
                            $query->where('userTitle',5);
                        });
                }   
            }

            if(isset($rq->account_types["management_firm"]) && $rq->account_types["management_firm"] == true) 
            {
                if($flag > 1)
                {
                    $jobs = $jobs->orWhereHas('user', function ($query) {
                            $query->where('userTitle',4);
                        });
                }else{
                    $jobs = $jobs->whereHas('user', function ($query) {
                            $query->where('userTitle',4);
                        });
                }   
            }

            if(isset($rq->account_types["staffing_firm"]) && $rq->account_types["staffing_firm"] == true) 
            {
                if($flag > 1)
                {
                    $jobs = $jobs->orWhereHas('user', function ($query) {
                            $query->where('userTitle',1);
                        });
                }else{
                    $jobs = $jobs->whereHas('user', function ($query) {
                            $query->where('userTitle',1);
                        });
                }   
            }          


    	}
                

    	if($rq->date_posted != "All" && $rq->date_posted != "") 
    	{

            //return $rq->date_posted;
            $dateFlag = 0;
            if(isset($rq->date_posted["last_hour"]) && $rq->date_posted["last_hour"] == true)
            {
                $dateFlag++;
            } 
            if(isset($rq->date_posted["last_24"]) && $rq->date_posted["last_24"] == true) 
            {
                $dateFlag++;
            }
            if(isset($rq->date_posted["last_7"]) && $rq->date_posted["last_7"] == true) 
            {
                $dateFlag++;
            }
            if(isset($rq->date_posted["last_14"]) && $rq->date_posted["last_14"] == true) 
            {
                $dateFlag++;
            }
            if(isset($rq->date_posted["last_39"]) && $rq->date_posted["last_39"] == true) 
            {
                $dateFlag++;
            }

            if(isset($rq->date_posted["last_hour"]) && $rq->date_posted["last_hour"] == true) 
            {      
                if($dateFlag > 1)
                {
                    $jobs = $jobs->orHavingraw('jobs_list.created_at >=  '."'".Carbon::parse('-1 hour')."'");
                }else
                {
                    $jobs = $jobs->havingraw('jobs_list.created_at >=  '."'".Carbon::parse('-1 hour')."'");                                    
                }                          
            }
            if(isset($rq->date_posted["last_24"]) && $rq->date_posted["last_24"] == true) 
            {   
                if($dateFlag > 1)
                {
                    $jobs = $jobs->orHavingraw('jobs_list.created_at >=  '."'".Carbon::parse('-24 hours')."'");                                
                }else
                {
                    $jobs = $jobs->havingraw('jobs_list.created_at >=  '."'".Carbon::parse('-24 hours')."'");
                }
                                                
            }

            if(isset($rq->date_posted["last_7"]) && $rq->date_posted["last_7"] == true) 
            {
                if($dateFlag > 1)
                {
                    $jobs = $jobs->orHavingraw('jobs_list.created_at >=  '."'".Carbon::parse('-7 days')."'");                          
                }else
                {
                    $jobs = $jobs->havingraw('jobs_list.created_at >=  '."'".Carbon::parse('-7 days')."'");                                
                }                
            }

            if(isset($rq->date_posted["last_14"]) && $rq->date_posted["last_14"] == true) 
            {
                if($dateFlag > 1)
                {
                    $jobs = $jobs->orHavingraw('jobs_list.created_at >=  '."'".Carbon::parse('-14 days')."'");
                }else
                {
                    $jobs = $jobs->havingraw('jobs_list.created_at >=  '."'".Carbon::parse('-14 days')."'");    
                }
                
            }


            if(isset($rq->date_posted["last_39"]) && $rq->date_posted["last_39"] == true) 
            {
                if($dateFlag > 1)
                {
                    $jobs = $jobs->orHavingraw('jobs_list.created_at >=  '."'".Carbon::parse('-39 days')."'");
                }else
                {
                    $jobs = $jobs->havingraw('jobs_list.created_at >=  '."'".Carbon::parse('-39 days')."'");    
                }                                                
            }            
    	}


    	if($rq->vacancy_type != "All"  && $rq->vacancy_type != "") 
    	{
            $vacancyFlag = 0;

            if(isset($rq->vacancy_type["full_time"]) && $rq->vacancy_type["full_time"] == true) 
            {
                $vacancyFlag++;
            }
            if(isset($rq->vacancy_type["locum_tenens"]) && $rq->vacancy_type["locum_tenens"] == true) 
            {
                $vacancyFlag++;
            }
            if(isset($rq->vacancy_type["part_time"]) && $rq->vacancy_type["part_time"] == true) 
            {
                $vacancyFlag++;
            }
            if(isset($rq->vacancy_type["prn"]) && $rq->vacancy_type["prn"] == true) 
            {
                $vacancyFlag++;
            }
            if(isset($rq->vacancy_type["fellowship"]) && $rq->vacancy_type["fellowship"] == true) 
            {
                $vacancyFlag++;
            }

            if(isset($rq->vacancy_type["full_time"]) && $rq->vacancy_type["full_time"] == true) 
            {
                if($vacancyFlag > 1)
                {
                    $jobs = $jobs->orHavingRaw("ei_durPosition = 'Full Time'");
                }else
                {
                    $jobs = $jobs->HavingRaw("ei_durPosition = 'Full Time'");    
                }
                
            }

            if(isset($rq->vacancy_type["locum_tenens"]) && $rq->vacancy_type["locum_tenens"] == true) 
            {
                if($vacancyFlag > 1)
                {
                    $jobs = $jobs->orHavingRaw("ei_durPosition = 'Locum Tenens'");
                }else
                {
                    $jobs = $jobs->HavingRaw("ei_durPosition = 'Locum Tenens'");
                }                
            }


            if(isset($rq->vacancy_type["part_time"]) && $rq->vacancy_type["part_time"] == true) 
            {
                if($vacancyFlag > 1)
                {
                    $jobs = $jobs->orHavingRaw("ei_durPosition = 'Part Time'");
                }else
                {
                    $jobs = $jobs->HavingRaw("ei_durPosition = 'Part Time'");
                }                  
            }

            if(isset($rq->vacancy_type["prn"]) && $rq->vacancy_type["prn"] == true) 
            {
                if($vacancyFlag > 1)
                {
                    $jobs = $jobs->orHavingRaw("ei_durPosition = 'PRN'");
                }else
                {
                    $jobs = $jobs->HavingRaw("ei_durPosition = 'PRN'");
                }                    
            }

            if(isset($rq->vacancy_type["fellowship"]) && $rq->vacancy_type["fellowship"] == true) 
            {
                if($vacancyFlag > 1)
                {
                    $jobs = $jobs->orHavingRaw("ei_durPosition = 'Fewllowship'");
                }else
                {
                    $jobs = $jobs->HavingRaw("ei_durPosition = 'Fewllowship'");
                }                  
            }
    	}        

    	if($rq->current_index >= 1) 
        {
           $offset = $rq->current_index * $multiplier; 
        }
        else 
        {
            $offset = 0;
        }
                                

        $jobs = $jobs->offset($offset)->limit($multiplier);
        
    	$data = $jobs->where('post_status','=', 'active')->get();        
    	
        foreach ($data as $key => $value) {
    		$value->job_type = $value->jobPostType == '1' ? "Anesthesiologist" : "CRNA";
    		$value->time_ago = $this->getTimeAgo($data[$key]->created_at);
    		$user = UsersProfile::where("userID" , $value->userID)->first();

            if($user != null) 
            {   
                if($user->image_path == "") 
                {
                    $user->image_path = "/public/images/default.png";
                }
                $value->image_path = $user->image_path;
                $value->company_city = $user->city;
                $value->company_state = $user->state;
                $value->company_website = $user->website;
                $value->company_name = $user->company_name;
            } 
    		else
            {
                $value->image_path = "/public/images/default.png";
                $value->company_city = "";
                $value->company_state = "";
                $value->company_website = "";
                $value->company_name = "";
            }


            //check high prio

            $value->highPrio = $value->priorityLevel == 1 ? true : false;

            if($value["created_at"]!= null && $value["updated_at"]!= null){
                $value["created_date"] = date("F d, Y", strtotime($value["created_at"]->toDateTimeString()));
                $value["updated_date"] = date("F d, Y", strtotime($value["updated_at"]->toDateTimeString()));
            }               

    	}

        $data[] = array('total'=>$totalJob);

    	return $data;
    }

    public function saveStateCity(Request $rq)
    {
        foreach ($rq->states as $key => $value) {
            # code...
            foreach ($value as $key2 => $value2) {
                $state = new StatesAndCity();
                $state->state = $key;
                $state->city = $value2;
                $state->save();
            }
        }

        return "done";
    }

    public function searchCity(Request $rq)
    {
        $src_states = explode("," , $rq->states);
        $states = new StatesAndCity();

        foreach ($src_states as $key => $state) { 
            $states = $states->orHavingRaw("state = ". "'".$state."'");
        }

        $states = $states->where('city', 'like', '%'.$rq->search.'%');

        $data = $states->limit(10)->get();
        $response = array();

        foreach ($data as $key => $value) {
            array_push($response , $value->city);
        }

        return $response;
    }

    public function repostJobs(Request $rq){
        $jobids = array();
        

        foreach ($rq->selected as $jobid) {            
            $jobs = new Jobs();
            $jobs->where('id',$jobid)
                ->update(['created_at'=>carbon::now()]);            
        }
        return array('repost'=>true);
    }


}
