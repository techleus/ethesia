<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plans;
use Carbon\Carbon;
use Cartalyst\Stripe\Stripe;
use App\User;
use App\UsersCreditCardDetails;
use Auth;
use App\PlanUser;
use App\postingPlans;

class PlansController extends Controller
{
    //

    public function __construct()
    {
     
    }

    public function getNextBillingDate(Request $rq)
    {
    	
    	$month_interval = $rq->plan_selected["interval"];
    	$plan = Plans::find($rq->plan_selected["id"]);
    	$total_price = $plan["plan_price"] * $rq->plan_selected["interval"];

    	$today = Carbon::today();

    	$next_billing = $today->addMonths($month_interval);

    	$response = array();
    	$response["total_price"] = $total_price;
    	$response["next_billing"] = $next_billing->format("l jS \\of F Y");

    	return $response;
    }

    public function postingPlans()
    {
    	$plans = new postingPlans();
    	return $plans->with(['activeplan' => function($query) {
				  $query->where('status', 'active');
				},'labels'])->get();
    }

    public function getPlans() 
    {
    	$plans = new Plans();
    	$results = $plans->get();

    	return $results;
    }

    public function getUserPlans() 
    {
    	$plans = new PlanUser();
    	$plans = $plans->orderby("status", "asc")->get();

    	foreach ($plans as $key => $plan)
    	{
    		$plan["plan_src"] = Plans::find($plan["plan_id"]);
    	}
    	
    	return $plans;
    }
    public function resetPlans()
    {	
    	$plan_active = new PlanUser();
    	$plan_active = $plan_active->where("user_id" , Auth::User()->id)->get();

    	foreach ($plan_active as $key => $active_plan) {
    		$active_plan->status  = "inactive";
    		$active_plan->save();
    	}
    }
    public function getOldCards()
    {
    	$old = new UsersCreditCardDetails();
        $cards = $old->where("userID" , Auth::User()->id)->get();

        return $cards;
    }

    public function packagenewcard(Request $rq)
    {    	

    	$stripe = new Stripe();
        $customer = $stripe->customers()->create([
            'email' => Auth::User()->email,
        ]);

        if(Auth::User()->stripe_id == "") 
        {
          $user = Auth::User();
          $user->stripe_id = $customer["id"];
          $user->save();
        }


        $card = array(
        	'name' => $rq->firstName ." ". $rq->lastName,
        	'number' => $rq->cardNumber,
        	'exp_month' => $rq->expMonth,
        	'exp_year' => $rq->expYear,
        	'cvc' => $rq->cardIDNumber,
        );
    	

        try 
		{
			$data = new UsersCreditCardDetails();

			$stripe = new Stripe();
			$token = $stripe->tokens()->create([
		      'card' => $card,
			]);

			$card = $stripe->cards()->create(Auth::User()->stripe_id, $token['id']);
		  	
		  	$data->card_id = $card["id"];
		  	$data->card_last_four = $card["last4"];
		  	$data->name = $card["name"];		  	
		  
		} 
		catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {		  	
		  	$code = $e->getCode();
		  	$message = $e->getMessage();
		  	$type = $e->getErrorType();

		  	$response = array();
		  	$response["status"] = false;
		  	$response["message"] = $message;
		  	return $response;
		  
		} catch (\Cartalyst\Stripe\Exception\InvalidRequestException $e) {
		  	$code = $e->getCode();
		  	$message = $e->getMessage();		  	
		  	$type = $e->getErrorType();

		  	$response = array();
		  	$response["status"] = false;
		  	$response["message"] = $message;
		  	return $response;
		} catch (\Cartalyst\Stripe\Exception\BadRequestException $e) {		  	
		  	$code = $e->getCode();
		  	$message = $e->getMessage();
		  	$type = $e->getErrorType();

		  	$response = array();
		  	$response["status"] = false;
		  	$response["message"] = $message;
		  	return $response;
		}

		$data->userID = Auth::User()->id;	    
	    $data->save();
    	
    	
	    $selected_plan = postingPlans::find($rq->planid)->get();	    
	    
	    $price = $selected_plan[0]->price * $rq->numberOfmonths;

	    $charge = $stripe->charges()->create([
		    'customer' => Auth::User()->stripe_id,
		    'currency' => 'USD',
		    'amount'   => $price,
		    'source'   => $data->card_id
		]);

		$card_id = $data->card_id;
		
    	$this->resetPlans();		
		
		$today = Carbon::today();
		$planData = new PlanUser();
		$planData->plan_id = $rq->planid;
		$planData->plan_interval =  $rq->numberOfmonths;
		$planData->recurring = $rq->recurring;
		$planData->plan_current_price = $selected_plan[0]->price;
		$planData->total_price = $price;
		$planData->status = "active";
		$planData->date_billed = $today->format("Y/m/d");
		$planData->plan_expiration = $today->addMonths($rq->numberOfmonths)->format("Y/m/d");
		$planData->user_id = Auth::User()->id;
		$planData->save();

    	$response = array();
	    $response["status"] = true;
	    $response["message"] = "card has been charged";
	    return $response;
    }

    public function packageexistingcard(Request $rq)
    {
		
		$selected_plan = postingPlans::find($rq->planid);	    
		
		$price = $selected_plan['price'] * $rq->numberOfmonths;

	    $stripe = new Stripe();
		$card = UsersCreditCardDetails::find($rq->existing_card_id);

		
		$charge = $stripe->charges()->create([
		    'customer' => Auth::User()->stripe_id,
		    'currency' => 'USD',
		    'amount'   => $price,
		    'source'   => $card->card_id
		]);

		$card_id = $card->card_id;  		
		$this->resetPlans();		

		$today = Carbon::today();
		$planData = new PlanUser();
		$planData->plan_id = $rq->planid;
		$planData->plan_interval =  $rq->numberOfmonths;
		$planData->recurring = $rq->recurring;
		$planData->plan_current_price = $selected_plan['price'];
		$planData->total_price = $price;
		$planData->status = "active";
		$planData->date_billed = $today->format("Y/m/d");
		$planData->plan_expiration = $today->addMonths($rq->numberOfmonths)->format("Y/m/d");
		$planData->user_id = Auth::User()->id;
		$planData->save();


    	$response = array();
	    $response["status"] = true;
	    $response["message"] = "card has been charged";
	    return $response;  	
    }

    public function updatePlans(Request $rq)
    {

    	$user_stripe = Auth::User()->stripe_id;

    	if($user_stripe == "")
    	{
    		$stripe = new Stripe();
            $customer = $stripe->customers()->create([
                'email' => Auth::User()->email,
            ]);

            if(Auth::User()->stripe_id == "") 
            {
              $user = Auth::User();
              $user->stripe_id = $customer["id"];
              $user->save();
            }
    	}

    	$card_id = "";


    	if($rq->selected_payment_method == "existing_card") 
        {
          $data = UsersCreditCardDetails::find($rq->existing_card_id);
        }else
        {
          $data = new UsersCreditCardDetails();
        }


    	if($rq->selected_payment_method == "new_card") //if new card
    	{

    		$card = array();
	    	$card["name"] = $rq->billData["name"];
	        $card["number"] = $rq->billData["cardNumber"];
	        $dates = explode("/", $rq->billData["expirationDate"]); 

	   		$month = $dates[0];
	        $year = $dates[1];
	        $card["exp_month"] = $month;
	        $card["exp_year"] = $year;
	        $card["cvc"] = $rq->billData["cardIDNumber"];


			try 
			{
				$stripe = new Stripe();
				$token = $stripe->tokens()->create([
			      'card' => $card,
				]);

				$card = $stripe->cards()->create(Auth::User()->stripe_id, $token['id']);
			  
			  	$data->card_id = $card["id"];
			  	$data->card_last_four = $card["last4"];
			  	$data->name = $card["name"];

			  	$default = "no";

				if($rq->isDefault == true) 
				{
				    	$default = "yes";
				}
			  	if($rq->isDefault == false) 
			  	{
			    	$rq->isDefault = "no";
			  	}

			  	if($default == "yes") 
			  	{
				    $stripe = new Stripe();
				    $stripe->customers()->update(Auth::User()->stripe_id, [
			        'default_source' => $data->card_id,
			    	]);
			  	}
			  
			} 
			catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
			  	// Get the status code
			  	$code = $e->getCode();

			  	// Get the error message returned by Stripe
			  	$message = $e->getMessage();

			  	// Get the error type returned by Stripe
			  	$type = $e->getErrorType();

			  	$response = array();
			  	$response["status"] = false;
			  	$response["message"] = $message;
			  	return $response;
			  
			} catch (\Cartalyst\Stripe\Exception\InvalidRequestException $e) {
			  	// Get the status code
			  	$code = $e->getCode();

			  	// Get the error message returned by Stripe
			  	$message = $e->getMessage();

			  	// Get the error type returned by Stripe
			  	$type = $e->getErrorType();

			  	$response = array();
			  	$response["status"] = false;
			  	$response["message"] = $message;
			  	return $response;
			} catch (\Cartalyst\Stripe\Exception\BadRequestException $e) {
			  	// Get the status code
			  	$code = $e->getCode();

			  	// Get the error message returned by Stripe
			  	$message = $e->getMessage();

			  	// Get the error type returned by Stripe
			  	$type = $e->getErrorType();

			  	$response = array();
			  	$response["status"] = false;
			  	$response["message"] = $message;
			  	return $response;
			}

			if(isset($rq["isDefault"]))
	   		{
	   			if($rq->isDefault == true) {
	   				$rq->isDefault = "yes";

		          	$old = new UsersCreditCardDetails();
		          	$old_res = $old->where("userID" , Auth::User()->id)->where("isDefault", "yes")->get();

		          	foreach ($old_res as $key => $value) 
		          	{
		            	$value->isDefault = "no";
		           	 	$value->save();
		          	}

	   			}
	   			if($rq->isDefault == false) 
	   			{
	   				$rq->isDefault = "no";
	   			}
	   			
	   		}


			$data->userID = Auth::User()->id;
		    $data->isDefault = $rq->isDefault;
		    $data->save();


		    //charging
			//get data from mysql using id
		    $selected_plan = Plans::find($rq->plan_selected["id"]);

		    //computations
		    $price = $selected_plan["plan_price"] * $rq->plan_selected["interval"];

		    $charge = $stripe->charges()->create([
			    'customer' => Auth::User()->stripe_id,
			    'currency' => 'USD',
			    'amount'   => $price,
			    'source'   => $data->card_id
			]);

			$card_id = $data->card_id;

		}
	   	
	   	$selected_plan = Plans::find($rq->plan_selected["id"]);

   		
		if($rq->selected_payment_method == "existing_card") //if new card
		{

			

		    //computations
		    $price = $selected_plan["plan_price"] * $rq->plan_selected["interval"];

		    $stripe = new Stripe();
			$card = UsersCreditCardDetails::find($rq->existing_card_id);
			$charge = $stripe->charges()->create([
			    'customer' => Auth::User()->stripe_id,
			    'currency' => 'USD',
			    'amount'   => $price,
			    'source'   => $card->card_id
			]);

			$card_id = $card->card_id;
		}

		
		//save plan
    	$this->resetPlans();

		$selected_plan = Plans::find($rq->plan_selected["id"]);
		$total_price = $selected_plan["plan_price"] * $rq->plan_selected["interval"];
		$today = Carbon::today();
		$planData = new PlanUser();
		$planData->plan_id = $rq->plan_selected["id"];
		$planData->plan_interval =  $rq->plan_selected["interval"];
		$planData->recurring = $rq->plan_selected["recurring"];
		$planData->plan_current_price = $selected_plan["plan_price"];
		$planData->total_price = $total_price;
		$planData->status = "active";
		$planData->date_billed = $today->format("Y/m/d");
		$planData->plan_expiration = $today->addMonths($rq->plan_selected["interval"])->format("Y/m/d");
		$planData->user_id = Auth::User()->id;
		$planData->save();

	    $response = array();
	    $response["status"] = true;
	    $response["message"] = "card has been charged";

	    return $response;
   	}
    

    public function proratedDetails(Request $rq) 
    {
    	$now = Carbon::now();
    	
		$from                  = $now;
		$to                    = "";
		$prorated_fee          = 0;
		$date_diff             = 0;
		$current_month_price   = 0;
		$selected_billing_date = "";
		$default_days_in_month = 31;
		$day_today             = $from->day;
		$perday_price          = 0;

    	if($rq->plan_selected->recurring == 'yes') 
    	{
    		$to = $now->addMonths($rq->plan_selected->interval); 
    	}
    	else
    	{
    		$to = $now->addMonths($rq->plan_selected->month_to_pay);     
    	}

    	$selected_billing_date = (int) $rq->plan_selected->recurring_date;
    	$date_diff = $from->diffInDays($to);


    	$perday_price =  $rq->plan_selected->plan_price  / $default_days_in_month;

    	if($day_today < $selected_billing_date) 
    	{
    		$current_month_price = $perday_price * ($selected_billing_date - $day_today);
    	}
    	else
    	{
    		
    	}
    	
    	
    }
}
