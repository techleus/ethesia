<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\VerificationUser;
use Hash;

class PasswordResetController extends Controller
{
    //

    public function newPasswordForm($code)
    {

    	return view("password_reset.content")->with("verification_code" , $code);
    }

    public function newPassword(Request $rq) 
    {

    	$date = new \DateTime();
		$date->modify('-4 hours');
		$formatted_date = $date->format('Y-m-d H:i:s');

    	$verify = new VerificationUser();
    	$verify = $verify->where('created_at', '>',$formatted_date)
						  ->where("verification_code", $rq->verification_code)->get();

    	if($rq->password != $rq->confirm_password) 
		{
			$response = array();
	    	$response["status"] = false;
	    	$response["message"] = "Password and confirm password didnt match.";

	    	return $response;
		}

    	if(count($verify) > 0) 
    	{

    		$verify = $verify[0];
    		$user = User::find($verify->user_id);
    		$user->password = Hash::make($rq->password);
    		$user->save();

    		$verify->delete();
    	}else{
    		$response = array();
	    	$response["status"] = false;
	    	$response["message"] = "Sorry verification code is invalid or expired.";

	    	return $response;
    	}

    	$response = array();
    	$response["status"] = true;
    	$response["message"] = "Password has been changed, redirecting..";

    	return $response;

    }
}
