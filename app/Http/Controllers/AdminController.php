<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs;
use App\OldPosts;
use App\UsersProfile;
use App\OldUsers;
use App\User;
use Hash;

class AdminController extends Controller
{
    //

    public function transferFields()
    {

    	return;

    	$old = OldUsers::find(230);

    	$data_state = $this->selectRandomState();

    	$new_user = new User();
		$new_user->email = $old->user_email;
		$new_user->password = Hash::make("looping123");
		$new_user->userType = 2;
		$new_user->userTitle = 1;
		$new_user->save();

		$new_user_id = $new_user->id;

		$new_profile = new UsersProfile();
		$new_profile->userID = $new_user_id;
		$new_profile->company_name = $old->display_name;
		$new_profile->image_path = "";
		$new_profile->city = $data_state["city"];
		$new_profile->state = $data_state["state"];
		$new_profile->description = $old->display_name." is one of the leading providers of healthcare solutions for anesthesiology and other specialties to physicians, hospitals and outpatient centers. Physician led and managed,.  provides comprehensive hospital-based clinical and management solutions for anesthesia outsourcing and other specialty areas including emergency medicine, women's and children's.";
		$new_profile->save();
		


		$old_posts = new OldPosts();
    	$old_posts = $old_posts->where("post_author",230)->limit(30)->get();


    	foreach ($old_posts as $key => $old_post) {
    		
    		$data_state = $this->selectRandomState();

			$new_post                     = new Jobs();
			$new_post->userID             = $new_user_id;
			$new_post->jobTitle           = $old_post->post_title;
			$new_post->ei_jobDescription  = $old_post->post_content;
			$new_post->created_at         = $old_post->post_date_gmt;
			$new_post->post_status        = "active";
			$new_post->ei_durPosition     = "Full Time"; 
			$new_post->jobPostType        = 1; 
			$new_post->ei_facilityAddress = ""; 
			$new_post->ei_facilityCity    = $data_state["city"]; 
			$new_post->ei_facilityState   = $data_state["state"]; 
			$new_post->ei_startImmediate  = true; 
			$new_post->save();
		}

    	return "transfered";
    }

    public function selectRandomState() 
    {
    	$states = array("Florida" => "Alford" ,
    					"Florida" => "Appolo Beach",
    					"Florida" => "Baldwin",
    					"California" => "San Diego",
    					"California" => "Lake San Marcos",
    					"California" => "Alturas",
    					"California" => "San Diego",
    					"California" => "Lake San Marcos",
    					"California" => "Carmichael",
    					"Illinois" => "Addison",
    					"Illinois" => "Albany",
    					"Illinois" => "Adeline"
    					);


    	$state = array_rand($states);

    	$data = array();
    	$data["state"] = $state;
    	$data["city"] = $states[$state];
    	return $data;
    		 
    }

    public function transferUsers()
    {
    	$old_users = new OldUsers();
    	$old_users = $old_users->limit(100)->get();

    	foreach ($old_users as $key => $old) {
			$new_user                     = new User();
			$new_user->email = $old->user_email;
			$new_user->password = Hash::make("looping123");
			$new_user->userType = 2;
			$new_user->userTitle = 1;
			$new_user->save();

			$new_profile = new UsersProfile();
			$new_profile->userID = $new_user->id;
			$new_profile->company_name = $old->display_name;
			$new_profile->image_path = "";
			$new_profile->city = "San Diego";
			$new_profile->state = "Ca";
			$new_profile->description = $old->display_name." is one of the leading providers of healthcare solutions for anesthesiology and other specialties to physicians, hospitals and outpatient centers. Physician led and managed,.  provides comprehensive hospital-based clinical and management solutions for anesthesia outsourcing and other specialty areas including emergency medicine, women's and children's.";
			$new_profile->save();
		}

    	return "transfered";
    }
}
