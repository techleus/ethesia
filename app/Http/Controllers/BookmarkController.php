<?php

namespace App\Http\Controllers;
use App\JobBookmark;
use Auth;
use Illuminate\Http\Request;

class BookmarkController extends Controller
{	
    public function bookmark(Request $rq)
    {
    	return $this->storeOrRemove($rq->jobId);    	    	
    }

    public function bookmarkedJoblist()
    {    	
    	
        $jobsList = JobBookmark::where('userId',Auth::User()->id)->with("Jobs", "User","Jobs.profile")->get();
            
        foreach ($jobsList as $key => $value) {
            $jobsList[$key]['timeago'] = $this->getTimeAgo($jobsList[$key]->created_at);
        }

    	return $jobsList;
    }
    
    public function getTimeAgo($carbonObject) 
    {  
        if($carbonObject == null) 
        {
            return "";
        }
        return $carbonObject->diffForHumans();
    }

    public function storeOrRemove($jobId)
    {
    	$bookmarkExist = JobBookmark::where('userId',Auth::User()->id)
    		->where('jobId',$jobId)
    		->first();    	
			if ($bookmarkExist === null) 
			{	
				$bookmark = new JobBookmark;	   	
		      	$bookmark->userId = Auth::User()->id;
		      	$bookmark->jobId = $jobId;
		      	$bookmark->save();
		      	return "Added";
			}else
			{
				$removeBookmark = JobBookmark::where('userId',Auth::User()->id)
					->where('jobId',$jobId)
    				->first();    			
				$removeBookmark->delete();
				return "Removed";
			}    	
    }
}
