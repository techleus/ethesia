<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Jobs;
use Excel;
use Auth;

class excelController extends Controller
{
    public function importJobs(Request $request)
    {    	    	
        if($request->hasFile('file'))
        {                    	        	
            $path = $request->file('file')->getRealPath();
            $data = \Excel::load($path)->get();            
            if($data->count())
            {
            	$count = 1;
            	$rowErrors = array();
                $jobs = array();
                foreach ($data as $key => $value) 
                {                	                    
                    $count++;
                    $flag = false;
                    $rowError = array();
                    if($value['1_job_titlerequired'] == "")
                    {
                        $flag = true;
                        $rowError['title'] = "title should not be empty";
                    }
                    if($value['2_job_descriptionrequired'] == "")
                    {
                        $flag = true;
                        $rowError['decription'] = "decription should not be empty";
                    }
                    if($value['3_job_typerequired1anesthesiologist_or_2crna'] != 1 && $value['3_job_typerequired1anesthesiologist_or_2crna'] != 2)
                    {
                        $flag = true;
                        $rowError['jobtype'] = "Type should be 1 or 2 and not empty";
                    }
                    if($value['7_facility_city_where_the_candidate_will_be_workingrequired']  == "")
                    {
                        $flag = true;
                        $rowError['facility_city'] = "facility city should not be empty";
                    }
                    if($value['8_facility_staterequired']  == "")
                    {
                        $flag = true;
                        $rowError['facility_state'] = "facility state should not be empty";
                    }
                    if($value['9_status_of_employmentrequired']  == "")
                    {
                        $flag = true;
                        $rowError['empstatus'] = "status of employment should not be empty";
                    }
                    if($value['11_will_crna_be_on_first_callrequired']  == "")
                    {
                        $flag = true;
                        $rowError['first_call'] = "first call should not be empty";
                    }
                    if($value['14_state_license_requiredrequired']  == "")
                    {
                        $flag = true;
                        $rowError['state_license'] = "state license should not be empty";
                    }
                    if($value['15_company_namerequired']  == "")
                    {
                        $flag = true;
                        $rowError['company_name'] = "company name should not be empty";
                    }
                    if($value['17_contact_emailrequired']  == "")
                    {
                        $flag = true;
                        $rowError['contact_email'] = "contact email should not be empty";
                    }
                    if($value['22_contact_phonerequired']  == "")
                    {
                        $flag = true;
                        $rowError['contact_num'] = "contact number should not be empty";
                    }

                    if($flag == false)
                    {
                        $jobs[] = array(                            
                            'userID' => Auth::User()->id,
                            'jobTitle' => $value['1_job_titlerequired'],
                            'ei_jobDescription' => $value['2_job_descriptionrequired'],
                            'jobPostType' => $value['3_job_typerequired1anesthesiologist_or_2crna'],
                            'privateNotes' => $value['4_private_notes'],
                            'ei_facilityAddress' => $value['6_facility_address'],
                            'ei_facilityCity' => $value['7_facility_city_where_the_candidate_will_be_workingrequired'],
                            'ei_facilityState' => $value['8_facility_staterequired'],
                            'ei_empStatus' => $value['9_status_of_employmentrequired'],
                            'ei_startDate' => $value['10_job_start_date'],
                            'ei_ansFirstCall' => $value['11_will_crna_be_on_first_callrequired'],
                            'jr_stateLicense' => $value['14_state_license_requiredrequired'],
                            'ci_company' => $value['15_company_namerequired'],
                            'ci_email' => $value['17_contact_emailrequired'],
                            'ci_address1' => $value['18_contact_address'],
                            'ci_city' => $value['19_contact_city'],
                            'ci_state' => $value['20_contact_state'],
                            'ci_zipcode' => $value['21_contact_zip_code'],
                            'ci_phone' => $value['22_contact_phonerequired'],
                            'ci_prefMethod' => $value['23_preferred_contact_method'],
                            'ci_website' => $value['24_link_to_apply_job_directly_with_the_company'],
                            'jr_newGraduates' => $value['27_new_grads_accepted_yesno'],
                            'ei_durMin' => $value['28_minimum_salary'],
                            'ei_durMax' => $value['29_maximum_salary'],
                        );   
                    }else{                        
                        $rowError['row'] = $count;
                        $rowErrors[] = $rowError;
                    }                    
                }
                
                if(!empty($jobs) && empty($rowErrors))
                {
                    Jobs::insert($jobs);
                    return ['import'=>'success'];
                }else{
                	return ['import'=>'failed', 'errors'=>$rowErrors];
                }
            }
        }else
        {
        	return ['import'=>'import error!'];
        }
        
    } 
}
