<?php

namespace App\Http\Controllers;
use App\ContactMethod;
use App\PositionDuration;
use Illuminate\Http\Request;

class PreferencesController extends Controller
{
    //

    public function contactMethod() 
    {
    	$data = new ContactMethod();
    	return $data->get();
    }

    public function positionDuration()
    {
    	$data = new PositionDuration();
    	return $data->get();
    }
}
