<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;

class DownloadsController extends Controller
{
    //
    public function downloadResume($user_id, $file_name)
    {

 
    	return response()->download(storage_path("app/public/uploads/cv/$user_id/$file_name"));
    }
}
