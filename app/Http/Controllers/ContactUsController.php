<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactUsMessages;
use DB;

class ContactUsController extends Controller
{
    public function message(Request $r)
    {
    	$message = new ContactUsMessages();
    	$message->name = $r->name;
    	$message->email = $r->email; 
    	$message->subject = $r->subject;
    	$message->message = $r->message;    	
    	$message->save();

    	return "true";
    }
    public function messages(Request $r)
    {
        return ContactUsMessages::all();
    }
}
