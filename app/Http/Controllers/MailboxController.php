<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\applicationMessages;
use Auth;

class MailboxController extends Controller
{
    public function index(Request $request, applicationMessages $appmsgs)
    {
    	$appmsgs = $appmsgs->where('recipient_id',Auth::User()->id)->with("users","attachments",'users.userProfile')->orderByDesc('id')->get();

        foreach ($appmsgs as $key => $value) {
            $value->time_ago = $this->getTimeAgo($value->created_at);
        }

        return $appmsgs;
    }

    public function getTimeAgo($carbonObject) 
    {
        return $carbonObject->diffForHumans();
    }

    public function getUniqueUsersFromInbox(Request $request, applicationMessages $appmsgs)
    {
        return $appmsgs->select('sender_id')->where('recipient_id',Auth::User()->id)->with(array('users'=>function($q){
            $q->select('id','username');            
        }))->with('profile')->groupBy('sender_id')->orderByDesc('id')->get();
    }
    public function updatemail(Request $rq, applicationMessages $applicationMessages)
    {
        
        foreach ($rq->id as $id) {
            $applicationMessages = $applicationMessages->where('id',$id)->first();
            $applicationMessages->status_id = $rq->status_id;   
            $applicationMessages->save();
        }

        return response(array('message'=>'deleted'),200);
    }

    public function sentitems(Request $request, applicationMessages $appmsgs)
    {
    	return $appmsgs->where('sender_id',Auth::User()->id)->with("users2","attachments","users2.userProfile")->orderByDesc('id')->get();
    }
    
}
