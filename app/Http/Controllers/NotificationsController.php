<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserInquiries;
use Carbon\Carbon;
use Auth;

class NotificationsController extends Controller
{
    //

    public function getTimeAgo($carbonObject) 
	{  
        if($carbonObject == null) 
        {
            return "";
        }
	    return $carbonObject->diffForHumans();
	}

	public function getNotifById($notif_id) 
	{
		$inquiries = new UserInquiries();

		$inquiries = $inquiries->where("id", $notif_id)
						->with("Job")
						->with("user.userProfile")
						->first();


		$inquiries->notif_type     = "Application";
		$inquiries->date_sent      =  $this->getTimeAgo($inquiries->created_at);
		$inquiries->description    =  $inquiries->message;
		$inquiries->candidate_name =  $inquiries->user->userProfile->firstName. " ".$inquiries->user->userProfile->lastName;
		$inquiries->downloadable_link    =  url("/")."/downloads/resume/$inquiries->user_id/$inquiries->cv_file_name";

		$response = array();
		$response = $inquiries;

		return $response;
	}

    public function getNotifications(Request $rq)
    {
    	$inquiries = new UserInquiries();
    	$inquiries = $inquiries->orderBy("created_at", "desc");
		$inquiries = $inquiries->whereHas('job', function ($query) {
		    $query->where('userID', Auth::User()->id);
		})->with('User','profile');


		$multiplier = 10;

		if($rq->current_index >= 1) 
        {
           $offset = $rq->current_index * $multiplier; 
        }
        else 
        {
            $offset = 0;
        }

		$inquiries = $inquiries->offset($offset)->limit($multiplier)->get();

		foreach ($inquiries as $key => $inquire) {
			$temp = $inquire;
			$inquire->notif_type = "Job Notification";
			$inquire->date_sent =  $this->getTimeAgo($inquire->created_at);
			$inquire->description =  $inquire->message;

			$inquire->user_id  = $inquire->userID;
			$inquire->inquiry  = $inquire->first();
			$inquire->time_ago = $this->getTimeAgo($inquire->created_at);
			$inquire->downloadable_link = url("/")."/downloads/resume/". $inquire->profile['userID'] ."/$inquire->cv_file_name";
		}

		$response = array();
		$response = $inquiries;

		return $response;
    }
}
