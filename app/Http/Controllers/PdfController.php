<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Jobs;
use Carbon\Carbon;
use App\UsersProfile;

class PdfController extends Controller
{
    //
    public function generateJobPdf($job_id)
    {
    	
		$job                      = Jobs::find($job_id);
		$user                     = UsersProfile::where("userID", $job->userID)->first();
		$job->job_type            = $job->jobPostType == '1' ? "Anesthesiologist" : "CRNA";
		$job->time_ago            = $this->getTimeAgo($job->created_at);
		$job->city                = isset($user->city) ? $user->city : "";
		$job->state               = isset($user->state) ? $user->state : "";
		$user                     = UsersProfile::where("userID" , $job->userID)->first();
		$job->image_path          = isset($user->image_path) ? $user->image_path : "";
		$job->posted_date         = date('M,d Y', strtotime(str_replace('-', '/', $job->created_at)));
		$job->posted_date_updated = date('M,d Y', strtotime(str_replace('-', '/', $job->updated_at)));
		
		$data                     = array("job" => $job);

		$pdf = PDF::loadView('pdf.job_preview', $data);
		//return view("pdf.job_preview")->with("job", $data["job"]);
		return $pdf->stream($job->jobTitle.'.pdf');
    }

    public function getTimeAgo($carbonObject) 
	{
	    return $carbonObject->diffForHumans();
	}
}
