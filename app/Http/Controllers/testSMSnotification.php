<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Nexmo\Laravel\Facade\Nexmo;
use Twilio;

use Twilio\Rest\Client;

class testSMSnotification extends Controller
{
    public function sendSMS()
    {	
    	$sid = 'ACc2de25ee3f6bf6e3606b70814766c02a';
		$token = '9758a85f9172677b914810fd852e839b';
		$client = new Client($sid, $token);
		
		$client->messages->create(		    
		    '+16193946929',
		    array(
		        // A Twilio phone number you purchased at twilio.com/console
		        'from' => '+17178332649',		        
		        'body' => 'Hey this is jerome.'
		    )
		);
				
    }

    public function sms()
    {
    	$nexmo = app('Nexmo\Client');

		$nexmo->message()->send([
		    'to'   => 639309733350,
		    'from' => 'ETHESIA',
		    'text' => 'Using the instance to send a message.'
		]);
    }
}
