<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\WelcomeEmployers;
use App\Jobs\SendConfirmEmail;
use Mail;
use Auth;
use App\UsersProfile;
use App\ApplicationMessageNotifications;

class EmailsController extends Controller
{
    //

	public function changeMailSettings(Request $request, ApplicationMessageNotifications $settings)
	{
				
		$settings->where('user_id',Auth::User()->id)->update(array(
			$request->column => $request->setting
		));

		return $settings->where('user_id',Auth::User()->id)->first();

	}

	public function getNotifSettings(ApplicationMessageNotifications $settings)
	{							

		if(count($settings->where('user_id',Auth::User()->id)->get()) == 0)
		{			
			$settings->create(array(
				'user_id' => Auth::User()->id,
				'sms_application' => true,
				'sms_messages' => true,
				'sms_job_expiration' => true,
				'email_applicaton' => true,
				'email_messages' => true,
				'email_job_expiration' => true,
			));
		}		
		return $settings->where('user_id',Auth::User()->id)->first();
	}

	public function showEmailTemplate() 
	{
		
	}
    public function sendConfirmationEmail(Request $request) 
    {
    	$data = array();
	    $data["subject"] = "Please verify your email address.";
	    $data["email_from"] = "hello@anesthesia.community";
	    $data["to"] = $request->email;
	    $data["verification_url"] = url('/')."/verify/user/".Auth::User()->id;


	    $profile = UsersProfile::where("userID", Auth::User()->id)->first();

	    if($profile != null)
	    {
	    	if($profile->company_name == "")
		    {
		    	if($profile->first_name == "") 
		    	{
		    		$data["name"] = Auth::User()->email;
		    	}else{
		    		$data["name"] = $profile->first_name." ".$profile->last_name;
		    	}
		    }else{
		    	$data["name"] = $profile->company_name;
		    }
	    }
	    else
	    {
	    	$data["name"] = Auth::User()->email;
	    }
	    


	    SendConfirmEmail::dispatch($data);

	    $response = array();
	    $response["status"] = true;
	    $response["message"] = "Verification email has been sent. Check your email.";

	    return $response;
    }
}
