<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\UsersProfile;
use App\User;
use Chat;
use App\ChatMessages;
use App\Events\MessageSent;
use App\Events\NotifyNewMessageUser;
use Carbon\Carbon;

class ChatController extends Controller
{
    //

    public function sendMessage(Request $rq)
    {
    	$user = Auth::User();
        $profile = UsersProfile::where("userID", Auth::User()->id)->first();
    	$message = array();
    	
        $conversation = Chat::conversation($rq->convo_id);

		$chat_new = Chat::message($rq->message)
            ->from($user)
            ->to($conversation)
            ->send(); 

        $message["profile"] = UsersProfile::where("userID", Auth::User()->id)->first();
        $message["message"] = $rq->message;
        $message["user_id"] = Auth::User()->id;    
        $message["date_sent"] = $this->getTimeAgo($chat_new["created_at"]);


        if($message["profile"] != "" && $message["profile"] != null) 
        {
            if($message["profile"]->company_name == "" || $message["profile"]->company_name == null) 
            {
                $message["name"] = $message["profile"]->firstName." ".$message["profile"]->lastName;
            }
            else
            {
                $message["name"] = Auth::User()->email;
            }

            $message["image_path"] = $message["profile"]["image_path"];
        }
        else
        {
            $message["name"] = Auth::User()->email;
            $message["image_path"] = url('/')."/public/images/default.png";
        }
        


        $data = array();
        $data["messages"] = $message;
        $data["conversation"] = $conversation;

        $users = $conversation->users;
        $not_current_users = array();


        if(count($users) > 0) 
        {

            foreach ($users as $user_key => $user_data) {
                if($user_data->id != Auth::User()->id) 
                {
                    array_push($not_current_users , $user_data);
                }
            }
        }


        //broadcast new messages to users except sender, seriously
        foreach ($not_current_users as $not_current_key => $not_current_user) {
            $resentMessages = array();
            
            $profile = UsersProfile::where("userID", $not_current_user->id)->first();
            

            if($profile != "") 
            {
                if($profile->company_name == "" || $profile->company_name == null) 
                {
                    $profile->name = $profile->firstName." ".$profile->lastName;
                }
                else
                {
                    $profile->name = $profile->company_name;
                }

            }
            else
            {
                $profile = array();
                $profile["name"] = $not_current_user->email;
            }
            
            $date_sent = $this->getTimeAgo($chat_new["created_at"]);

            $resentMessages["profile"] = $profile;
            $resentMessages["message"] = $rq->message;
            $resentMessages["convo_id"] = $rq->convo_id;
            $resentMessages["date_sent"] = $date_sent;

            broadcast(new NotifyNewMessageUser($not_current_user->id , $resentMessages, $data))->toOthers();
        }

		broadcast(new MessageSent($data, $rq->convo_id))->toOthers();

		return ['status' => 'Message Sent!'];
    }
    public function getTimeAgo($carbonObject) 
    {
        return $carbonObject->diffForHumans();
    }

    public function openConvoFromUser(Request $rq)
    {
        $perPage = 10;
        $page = $rq->page;

        $user2 = new \stdClass();
        $user2->id = $rq->user_id;

        $conversation = Chat::getConversationBetween(Auth::User(), $user2);

        if($conversation == null || $conversation == "") 
        {

            $participants = array($user2->id, Auth::User()->id);
            $conversation = Chat::createConversation($participants); 
        }
        

        $messages = ChatMessages::where("conversation_id" , $conversation->id)->orderby('created_at')->get();
        $profile = UsersProfile::where("userID", $rq->user_id)->first();

        

        $maxrow = $perPage * $page;
        $minrow = $maxrow - $perPage;
        
        $tempMessages = array();
        
        $counter = 0;
        
        foreach ($messages as $row)
        {            
            
            if($counter >= $minrow && $counter < $maxrow)
            {                
                $tempMessages[] = $row;                
            }            
            
            $counter++;
        }
        
        $messages = $tempMessages;  
        
        foreach ($messages as $key => $value) {
            $messages[$key]["profile"] = UsersProfile::where("userID", $value->user_id)->first();

            if($value->profile->company_name == "" || $value->profile->company_name == null) 
            {
                $value->name = $value->profile->firstName." ".$value->profile->lastName;
            }
            else
            {
                $value->name = $value->profile->company_name;
            }

            $value->date_sent = $this->getTimeAgo($value["created_at"]);
            $value->message = $value->body;
        }

        $data = array();
        $data["messages"] = $messages;
        $data["profile"] = $profile;
        $data["conversation"] = $conversation;

        return $data;
    }
    public function openConversation(Request $rq) 
    {
        $perPage = 10;
        $page = $rq->page;

        $maxrow = $perPage * $page;
        $minrow = $maxrow - $perPage;

        $conversation = Chat::conversation($rq->convo_id);

        $users = $conversation->users;

        $messages = ChatMessages::where("conversation_id" , $rq->convo_id)->orderby('created_at')->get();        

        $tempMessages = array();
        
        $counter = 0;
        
        foreach ($messages as $row)
        {            
            
            if($counter >= $minrow && $counter < $maxrow)
            {                
                $tempMessages[] = $row;                
            }            
            
            $counter++;
        }
        
        $messages = $tempMessages;        
        
        foreach ($messages as $key => $value) {
            $messages[$key]["profile"] = UsersProfile::where("userID", $value->user_id)->first();

            if($messages[$key]["profile"] != "" && $messages[$key]["profile"] != null)
            {
                if($value->profile->company_name == "" || $value->profile->company_name == null) 
                {
                    $value->name = $value->profile->firstName." ".$value->profile->lastName;
                }
                else
                {
                    $value->name = $value->profile->company_name;
                } 

                $value->image_path = $value->profile->image_path;
            }
            else
            {
                $value->name = User::find($value->user_id)->email;
                $value->image_path = url('/')."/public/images/default.png";
            }

            $value->date_sent = $this->getTimeAgo($value["created_at"]);
            $value->message = $value->body;
        }

        $data = array();
        $data["messages"] = $messages;
        $data["conversation"] = $conversation;

        return $data;
    }

    public function searchContacts(Request $rq) 
    {
    	$users_profile = new UsersProfile();
    	$users_profile = $users_profile
    		->whereHas('user',  function ($query) use ($rq) {
			    $query->where('email', 'like', '%'.$rq["key"].'%');
			})
			->with('user')
            ->where("userID","<>",Auth::User()->id)
    		->groupby('userID')->limit(10)->get();

    	return $users_profile;
    }

    public function getConversations()
    {   
        $user = Auth::User()->id;
        $conversations = Chat::commonConversations([$user]);
        $data = array();
        $data_to_return = array();


        foreach ($conversations as $key => $convo) {
            $conversation_obj = Chat::conversation($convo->id);

            foreach ($conversation_obj->users as $user_key => $user) 
            {
                # code...
                if(Auth::User()->id != $user->id) 
                {
                    $data["users"] = $user;
                }
                
            }


            $user_id = $data["users"]["id"];

            $data["profile"]= UsersProfile::where("userID", $user_id)->first();

            if($data["profile"] != null) 
            {  
                if($data["profile"]["company_name"] == "" || $data["profile"] == null) 
                {
                    $data["profile"]["name"] = $data["profile"]["firstName"]." ".$data["profile"]["lastName"];
                }
                else
                {
                    $data["profile"]["name"] = $data["profile"]["company_name"];
                }
            }
            else
            {
                $data["profile"] = array();
                $data["profile"]["name"] = $data["users"]["email"];
                $data["profile"]["image_path"] =  url('/')."/public/images/default.png";
            }


            
            $message = ChatMessages::where("conversation_id" , $convo->id)->orderby('created_at', 'desc')->first();
            $data["message"] = $message->body;
            $data["date_sent"] = $this->getTimeAgo($message["created_at"]);
            $data["convo_id"] = $convo->id;
            $data["conversation"] = $convo;
            $data["id"] = $convo->id;

            array_push($data_to_return, $data);
        }
        
        return $data_to_return;
    }
}
