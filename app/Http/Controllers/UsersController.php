<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\UserTypes;
use App\UsersProfile;
use App\UserTitles;
use App\UsersEducation;
use App\UsersExperience;
use App\UsersCreditCardDetails;
use App\UsersECheckDetails;
use App\SecondaryEmail;
use App\UserInquiries;
use Cartalyst\Stripe\Stripe;
use App\Mail\WelcomeEmployers;
use App\Mail\WelcomeNewUser;
use App\Jobs\SendConfirmEmail;
use App\Jobs\WelcomeNewUserJob;
use App\VerificationUser;
use App\Jobs\SendPasswordResetJob;
use App\UserContactList;
use App\UserContactTypes;
use App\Mail\SendNewUserMail;
use App\Mail\sendEmployerMail;
use App\Mail\SendPasswordResetMail;

use Storage;
use Hash;
use Auth;
use Image;
use File;
use Mail;

class UsersController extends Controller
{
    //


	  public function __construct()
    {
      
    }    

    public function deleteSecEmail(Request $rq, SecondaryEmail $mails)
    {

      $mails->find($rq->mailid)->delete();      

    }

    public function proceedupdatesecemail(Request $rq,SecondaryEmail $mails)
    {

      $rq->validate([
        'email' => 'required|email',
      ]);

      $mails->find($rq->mailid)->update(array(
        'secondary_email' => $rq->email,
      ));
    }

    public function getSecondarymail(SecondaryEmail $mails)
    {
      return $mails->where('user_id',Auth::User()->id)->get();
    }

    public function secondarymail(Request $rq, SecondaryEmail $mails)
    {
      $rq->validate([
        'email' => 'required|email',
      ]);

      $mails->create(array(
        'user_id' => Auth::User()->id,
        'secondary_email' => $rq->email,
      ));

    }

    public function resendAccoutVerification()
    {

      $user = Auth::User();

      $verification_code = $this->generateRandomString(10);

      $sendto = array(
        'name'=> $user->account_type == 2 ? $user->company_name : $user->first_name. " ".$user->last_name,
        'to' => $user->email,
        'verification_code' => $verification_code,
        'link' => url("/")."/verify/".$verification_code."/".$user->id,
      );

      VerificationUser::where('user_id', Auth::User()->id)->delete();      

      $user_verify = new VerificationUser();    
      $user_verify->verification_code = $verification_code;
      $user_verify->user_id = Auth::User()->id;
      $user_verify->save();

      Mail::send(new SendNewUserMail($sendto));
    }

    public function credentials()
    {
      return Auth::User()->is_confirmed;
    }

    public function verifyEmail(Request $rq, VerificationUser $verificationUser, User $user)
    {

      $verify = $verificationUser->where('user_id',$rq->user_id)->where('verification_code',$rq->code)->get();            

      if(count($verify) >= 1)
      {

        $user->where('id',$rq->user_id)->update(array(
          
          'is_confirmed' => 'yes',
        
        ));        

        return redirect(url('/application/#/dashboard'));

      }

    }

    public function testMail() 
    {

      $data = array();
      $data["subject"] = "Some Subject right hersssse";
      $data["email_from"] = "hello@anesthesia.community";
      $data["name"] = "Ralph Degoma";
      $data["to"] = "personal.ralphdegoma@gmail.com";

      SendConfirmEmail::dispatch($data);
    }

    public function checkLogin() 
    {
      $data = array();
      if(Auth::check())
        {
          $data["status"] = true;
        }
        else
        {
          $data["status"] = false;
        }

        return $data;
    }

    public function userTypes() 
    {
    	$data = new UserTypes();
    	$results = $data->get();
    	return $results;
    }


    public function testStripe() 
    {
      $stripe = new Stripe();
      $customer = $stripe->customers()->create([
          'email' => Auth::User()->email,
      ]);

      echo $customer['id'];
    }

    public function createCard() 
    {
      $stripe = new Stripe();
      $token = $stripe->tokens()->create([
          'card' => [
              'number'    => '4242424242424242',
              'exp_month' => 10,
              'cvc'       => 314,
              'exp_year'  => 2020,
          ],
      ]);

      $card = $stripe->cards()->create(Auth::User()->stripe_id, $token['id']);

      return $card;
    }
    public function userTitles() 
    {
    	$data = new UserTitles();
    	$results = $data->get();
    	return $results;
    }

    public function sendPasswordForgot(Request $rq)
    {
      $check = new User();
      $checkRes = $check->where("email" , $rq->email)->get();

      if(count($checkRes) == 0) {
        $data = array();
        $data["status"] = false;
        $data["message"] = "Sorry email not found or not registered.";
        return $data;
      }


      $profile = UsersProfile::where("userID", $checkRes[0]->id)->first();


      $params = array();
      
      if($profile != null) 
      {
          if($profile->company_name == "")
          {
            if($profile->first_name == "") 
            {
              $params["to_name"] = $profile->email;
            }else{
              $params["to_name"] = $profile->first_name." ".$profile->last_name;
            }
          }else{
            $params["to_name"] = $profile->company_name;
          }
      }
      


      $params["verification_code"] = $this->generateRandomString(10);
      $params["user_id"] = $profile->userID;
      $params["subject"] = "Forgotten password reset.";
      $params["name"] = "Ethesia";
      $params["email_from"] = "donotreply@anesthesia.community";
      $params["to"] = $rq->email;
      $params["link"] = url("/")."/password/form-reset/".$params["verification_code"];


      $dev_emails = array("lyndon@techleus.com", "jetbuquematias@gmail.com");

      if(env("APP_ENV") == "local" || env("APP_ENV") == "") 
      {
          $params["to"]             = $dev_emails;
      }
      

      $user_verify = new VerificationUser();
      $user_verify->verification_code = $params["verification_code"];
      $user_verify->user_id = $params["user_id"];
      $user_verify->save();

      Mail::send(new SendPasswordResetMail($params));

      // Mail::send(['text'=>'emails.welcome_new_user'],['name'=>'jerome matias'],function($message){        
      //  $message->to('jetbuquematias@gmail.com','to jerome')->subject('test mail');
      //  $message->from('jetbuquematias@gmail.com','Jerome');            
      // });   

      $response = array();
      $response["status"] = true;
      $response["message"] = "Password reset has been sent.";
      return $response;
      //send email
    }

    public function newUser(Request $rq)
    {

      ini_set('max_execution_time', 1000);

      $check = new User();
      $checkRes = $check->where("email" , $rq->email)->get();

      if(count($checkRes) > 0) {
        $data = array();
        $data["status"] = false;
        $data["message"] = "Sorry email is taken. Please choose another one.";
        return $data;
      }

      if($rq->account_title == "") 
      {
        $data = array();
        $data["status"] = false;
        $data["message"] = "User Type is required.";
        return $data;
      }

    	$data = new User();
    	$data->email = $rq->email;
    	$data->password = Hash::make($rq->password);
    	$data->username = $rq->email;
    	$data->userType = $rq->account_type;
    	$data->userTitle = $rq->account_title;
      $data->is_confirmed = "no";
    	$data->save();

      $users_profile = new UsersProfile();
      $users_profile->description = $rq->description;

      if($rq->account_type == 2) //company
      {
        $users_profile->company_name = $rq->company_name;
      }
      else
      {
        $users_profile->firstName = $rq->first_name;
        $users_profile->lastName = $rq->last_name;
      }


      Auth::attempt(['email' => $rq->email, 'password' => $rq->password]);


      $users_profile->userID = Auth::User()->id;
      $users_profile->save();

      $profile = UsersProfile::where("userID", $data->id)->first();
      $params = array(); 


      $params["name"] = $rq->account_type == 2 ? $rq->company_name : $rq->first_name. " ".$rq->last_name;
      
      $params["url"] = url('/');
      $params["subject"] = "Welcome to Anesthesia Community";
      $params["email_from"] = "jeromematias@anesthesia.com";
      $params["to"] = $rq->email;

      // WelcomeNewUserJob::dispatch($params);
      $verification_code = $this->generateRandomString(10);
      
      $sendto = array(
        'name'=> $rq->account_type == 2 ? $rq->company_name : $rq->first_name. " ".$rq->last_name,
        'to' => $rq->email,
        'verification_code' => $verification_code,
        'link' => url("/")."/verify/".$verification_code."/".$users_profile->userID,
      );
      
      $user_verify = new VerificationUser();
      $user_verify->verification_code = $verification_code;
      $user_verify->user_id = Auth::User()->id;
      $user_verify->save();

      Mail::send(new SendNewUserMail($sendto));
      
    	$data = array();
    	$data["status"] = true;
      $data["credentials"] = Auth::user();
    	$data["message"] = "Information has been saved.";


      /*add new contact labels here*/

      $label = array(
        array('user_id'=>Auth::User()->id,'contact_label'=>'Work','label_id'=>1),
        array('user_id'=>Auth::User()->id,'contact_label'=>'Family','label_id'=>2),
        array('user_id'=>Auth::User()->id,'contact_label'=>'Friends','label_id'=>3),
        array('user_id'=>Auth::User()->id,'contact_label'=>'Private','label_id'=>4),
      );

      UserContactTypes::insert($label);

    	return $data;

    }

    public function newPassword(Request $rq) 
    {
      $user = Auth::User();
      
      if($rq->password == "" || $rq->new_password == "") {
        $response = array();
        $response["status"] = false;
        $response["message"] = "Password and new password is required.";
        return $response;
      }

      if (Hash::check($rq->password, $user->password)) {
        $user = User::find($user->id);
        $user->password = Hash::make($rq->new_password);
        $user->save();
      }else{
        $response = array();
        $response["status"] = false;
        $response["message"] = "Old password didnt matched.";
        return $response;
      }

      $response = array();
      $response["status"] = true;
      $response["message"] = "Password has been updated.";
      return $response;
    }

    public function updateUser(Request $rq)
    {

    	if($rq->id == "")
    	{
    		$data = new UsersProfile();
    	}else
    	{
    		$data = UsersProfile::find($rq->id);
    	}
   		

   		if(isset($rq["company_name"]))
   		{	
   			$data->company_name = $rq->company_name;
   		}

   		if(isset($rq["firstName"]))
   		{
   			$data->firstName = $rq->firstName;
   		}

   		if(isset($rq["lastName"]))
   		{
   			$data->lastName = $rq->lastName;
   		}

   		if(isset($rq["address"]))
   		{
   			$data->address = $rq->address;
   		}

   		if(isset($rq["city"]))
   		{
   			$data->city = $rq->city;
   		}

   		if(isset($rq["state"]))
   		{
   			$data->state = $rq->state;
   		}

   		if(isset($rq["country"]))
   		{
   			$data->country = $rq->country;
   		}

   		if(isset($rq["zipcode"]))
   		{
   			$data->zipcode = $rq->zipcode;
   		}

   		if(isset($rq["phone"]))
   		{
   			$data->phone = $rq->phone;
   		}

   		if(isset($rq["fax"]))
   		{
   			$data->fax = $rq->fax;
   		}

   		if(isset($rq["website"]))
   		{
   			$data->website = $rq->website;
   		}

   		if(isset($rq["emailPreference"]))
   		{
   			$data->emailPreference = $rq->emailPreference;
   		}


   		$data->userID = Auth::User()->id;
    	$data->save();

    	$response = array();
    	$response["status"] = true;
    	$response["message"] = "Saved.";
    	$response["id"] = $data->id;
    	return $response;
    }

    public function updateBasicInfo(Request $rq) 
    {


      if($rq->id == "")
      {
        $data = new UsersProfile();
      }else
      {
        $data = UsersProfile::find($rq->id);
      }

      if(isset($rq["minimumSalary"])) 
      {
        $data->minimumSalary = $rq->minimumSalary;
      }

      if(isset($rq["allowSearch"])) 
      {
        $data->allowSearch = $rq->allowSearch;
      }
      
      if(isset($rq["description"])) 
      {
        $data->description = $rq->description;
      }

      if(isset($rq["firstName"])) 
      {
        $data->firstName = $rq->firstName;
      }  

      if(isset($rq["lastName"])) 
      {
        $data->lastName = $rq->lastName;
      } 

      if(isset($rq["company_name"])) 
      {
        $data->company_name = $rq->company_name;
      }
      
      if(isset($rq->tag_line))
      {
        $data->tag_line = $rq->tag_line;
      }
      
      $data->address = $rq->address;
      $data->city = $rq->city;
      $data->state = $rq->state;
      $data->country = $rq->country;
      $data->zipcode = $rq->zipcode;
      $data->phone = $rq->phone;
      $data->fax = $rq->fax;
      $data->website = $rq->website;
      $data->emailPreference = $rq->emailPreference;

      $data->userID = Auth::User()->id;
      $data->save();

      $response = array();
      $response["status"] = true;
      $response["message"] = "Saved.";
      $response["id"] = $data->id;
      return $response;
    }

    public function updateSocialInfo(Request $rq) 
    {
      if($rq->id == "")
      {

        $check = UsersProfile::where('userID',Auth::User()->id)->first();

        if($check == "") 
        {
          $data = new UsersProfile();
        }else
        {
          $data = UsersProfile::where('userID',Auth::User()->id)->first();
        }


      }else
      {
        $data = UsersProfile::find($rq->id);
      }

      $data->facebookUrl = $rq->facebookUrl;
      $data->twitterUrl = $rq->twitterUrl;
      $data->youtubeUrl = $rq->youtubeUrl;
      $data->linkedinUrl = $rq->linkedinUrl;
      $data->googlePlusUrl = $rq->googlePlusUrl;
      $data->save();
      

      return $rq->linkedinUrl;
      $response = array();
      $response["status"] = true;
      $response["message"] = "Saved.";
      $response["id"] = $data->id;
      return $response;
    }
    public function removeEducationById(Request $rq) 
    {
      $data = UsersEducation::find($rq->id);
      $data->delete();

      $response = array();
      $response["status"] = true;
      $response["message"] = "Successfully deleted.";
      return $response;
    }
    public function removeExperience(Request $rq)
    {
      $data = UsersExperience::find($rq->id);
      $data->delete();

      $response = array();
      $response["status"] = true;
      $response["message"] = "Successfully deleted.";
      return $response;
    }
    public function getUserEducations()
    {
      $data = UsersEducation::where('userID',Auth::User()->id)->get();
      return $data;
    }
    public function getUserExperience() 
    {
      $data = UsersExperience::where('userID',Auth::User()->id)->get();
      return $data;
    }
    public function newEducation(Request $rq) 
    {
      if($rq->id != "") 
      {
        $data = UsersEducation::find($rq->id);
      }
      else
      {
        $data = new UsersEducation();
      }
    
      $data->userID = Auth::User()->id;
      $data->title_education = $rq->title_education;
      $data->from_education = $rq->from_education;
      $data->to_education = $rq->to_education;
      $data->school_name = $rq->school_name;
      $data->save();

      $response = array();
      $response["status"] = true;
      $response["message"] = "Saved.";
      $response["id"] = $data->id;
      return $response;
    }

    public function newExperience(Request $rq) 
    {
      if($rq->id != "") 
      {
        $data = UsersExperience::find($rq->id);
      }
      else
      {
        $data = new UsersExperience();
      }
    
      $data->userID          = Auth::User()->id;
      $data->titleExperience = $rq->titleExperience;
      $data->fromExperience  = $rq->fromExperience;
      $data->toExperience    = $rq->toExperience;
      $data->companyName     = $rq->companyName;
      $data->save();

      $response = array();
      $response["status"] = true;
      $response["message"] = "Saved.";
      $response["id"] = $data->id;
      return $response;
    }
    public function updateExtraInfo(Request $rq) 
    {
      if($rq->id == "")
      {

        $check = UsersProfile::where('userID',Auth::User()->id)->first();

        if($check == "") 
        {
          $data = new UsersProfile();
        }else
        {
          $data = UsersProfile::where('userID',Auth::User()->id)->first();
        }


      }else
      {
        $data = UsersProfile::find($rq->id);
      }

      if($rq->file("file")[0] != null){

        $file = $rq->file("file")[0];      
      
        $name = $file->getClientOriginalName();      
        
        $data_obj = json_decode($rq->data);      
        
        $count = 1;        
        
        $id = Auth::User()->id;

        while(Storage::disk('public')->has("uploads/cover-letter/$id/$name")) {
            $name = $count."-".$name;
            $count++;
        }

        Storage::disk("public")->put("uploads/cover-letter/$id/$name",file_get_contents($rq->file("file")[0]));

        $data->resume_path = "uploads/cover-letter/$id/$name";
      }      
            
      $data->currentSalary = $data_obj->currentSalary;
      $data->expectedSalary = $data_obj->expectedSalary;
      $data->positionDuration = $data_obj->positionDuration;
      $data->coverLetter = $data_obj->coverLetter;
      $data->save();
      
      $response = array();
      $response["status"] = true;
      $response["message"] = "Saved.";
      $response["id"] = $data->id;
      return $response;
    }
    public function cardupdate(Request $rq)
    {
      
      
      if($rq->default)
      {
        $old = new UsersCreditCardDetails();
        $old_res = $old->where("userID" , Auth::User()->id)->where("isDefault", "yes")->get();

        foreach ($old_res as $key => $value) {
          $value->isDefault = "no";
          $value->save();
        }
      }

      $data = UsersCreditCardDetails::find($rq->id);
      $card = array();
            
      $user = Auth::User();
      
      $card["name"] = $rq->name;

      try {
          $card_id = $data['card_id'];
          $stripe = new Stripe();          

          $stripe->cards()->update($user->stripe_id, $card_id, $card);
      } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
          // Get the status code
          $code = $e->getCode();

          // Get the error message returned by Stripe
          $message = $e->getMessage();

          // Get the error type returned by Stripe
          $type = $e->getErrorType();
          $response = array();
          $response["status"] = false;
          $response["message"] = $message;
          return $response;
      } catch (\Cartalyst\Stripe\Exception\InvalidRequestException $e) {
          // Get the status code
          $code = $e->getCode();

          // Get the error message returned by Stripe
          $message = $e->getMessage();

          // Get the error type returned by Stripe
          $type = $e->getErrorType();
          $response = array();
          $response["status"] = false;
          $response["message"] = $message;
          return $response;
      } catch (\Cartalyst\Stripe\Exception\BadRequestException $e) {
          // Get the status code
          $code = $e->getCode();

          // Get the error message returned by Stripe
          $message = $e->getMessage();

          // Get the error type returned by Stripe
          $type = $e->getErrorType();

          $response = array();
          $response["status"] = false;
          $response["message"] = $message;
          return $response;
      }


      

      if($rq->default)
      {
        $data->isDefault = "yes";  
      }

      $data->name = $rq->name;
      $data->save();

    }
    public function updateUserCard(Request $rq)
    {

        putenv('STRIPE_API_KEY=sk_test_bQRvTILgvtm3puD0JXIb23jb');


        if(Auth::User()->stripe_id == "") {
            $stripe = new Stripe();
            $customer = $stripe->customers()->create([
                'email' => Auth::User()->email,
            ]);

            if(Auth::User()->stripe_id == "") 
            {
              $user = Auth::User();
              $user->stripe_id = $customer["id"];
              $user->save();
            }

        }
    
        if($rq->id != "") 
        {
          $data = UsersCreditCardDetails::find($rq->id);
        }else
        {
          $data = new UsersCreditCardDetails();
        }
        

        $user = Auth::User();

        $card["name"] = $rq->name;
        $card["number"] = $rq->card_number;

   			$month = $rq->exp_month;
        $year = $rq->exp_year;
        $card["exp_month"] = $month;
        $card["exp_year"] = $year;
        $card["cvc"] = $rq->cvc;

        if(isset($rq["address"])) {
          if($rq["address"] != "") {
            $card["address_line1"] = $rq->address;
            $data->address = $rq->address;
          }
        }

    /*    if(isset($rq["state"])) {
          if($rq["state"] != "") {
            $card["address_state"] = $rq->state;
            $data->state = $rq->state;
          }
        }*/

        if(isset($rq["zipcode"])) {
          if($rq["zipcode"] != "") {
            $card["address_zip"] = $rq->zipcode;
            $data->zipcode = $rq->zipcode;
          }
        }
    
        if($rq->id != "") 
        {
          try {
              $card_id = $rq->card_id;
              $stripe = new Stripe();

              unset($card["number"]);
              unset($card["cvc"]);

              $stripe->cards()->update($user->stripe_id, $card_id, $card);
          } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
              // Get the status code
              $code = $e->getCode();

              // Get the error message returned by Stripe
              $message = $e->getMessage();

              // Get the error type returned by Stripe
              $type = $e->getErrorType();
              $response = array();
              $response["status"] = false;
              $response["message"] = $message;
              return $response;
          } catch (\Cartalyst\Stripe\Exception\InvalidRequestException $e) {
              // Get the status code
              $code = $e->getCode();

              // Get the error message returned by Stripe
              $message = $e->getMessage();

              // Get the error type returned by Stripe
              $type = $e->getErrorType();
              $response = array();
              $response["status"] = false;
              $response["message"] = $message;
              return $response;
          } catch (\Cartalyst\Stripe\Exception\BadRequestException $e) {
              // Get the status code
              $code = $e->getCode();

              // Get the error message returned by Stripe
              $message = $e->getMessage();

              // Get the error type returned by Stripe
              $type = $e->getErrorType();

              $response = array();
              $response["status"] = false;
              $response["message"] = $message;
              return $response;
          }

          

        }else
        {
          
          try {


             

              $stripe = new Stripe();
              $token = $stripe->tokens()->create([
                  'card' => $card,
              ]);

              $card = $stripe->cards()->create(Auth::User()->stripe_id, $token['id']);
              
              $data->card_id = $card["id"];
              $data->card_last_four = $card["last4"];
              $data->name = $card["name"];

              $default = "no";

              if($rq->isDefault == true) 
              {
                $default = "yes";
              }
              if($rq->isDefault == false) 
              {
                $rq->isDefault = "no";
              }

              if($default == "yes") 
              {
                $stripe = new Stripe();
                $stripe->customers()->update(Auth::User()->stripe_id, [
                    'default_source' => $data->card_id,
                ]);
              }
              
          } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
              // Get the status code
              $code = $e->getCode();

              // Get the error message returned by Stripe
              $message = $e->getMessage();

              // Get the error type returned by Stripe
              $type = $e->getErrorType();

              $response = array();
              $response["status"] = false;
              $response["message"] = $message;
              return $response;
              
          } catch (\Cartalyst\Stripe\Exception\InvalidRequestException $e) {
              // Get the status code
              $code = $e->getCode();

              // Get the error message returned by Stripe
              $message = $e->getMessage();

              // Get the error type returned by Stripe
              $type = $e->getErrorType();

              $response = array();
              $response["status"] = false;
              $response["message"] = $message;
              return $response;
          } catch (\Cartalyst\Stripe\Exception\BadRequestException $e) {
              // Get the status code
              $code = $e->getCode();

              // Get the error message returned by Stripe
              $message = $e->getMessage();

              // Get the error type returned by Stripe
              $type = $e->getErrorType();

              $response = array();
              $response["status"] = false;
              $response["message"] = $message;
              return $response;
          }
          
        }
   		
   		if(isset($rq["isDefault"]))
   		{
   			if($rq->isDefault == true) {
   				$rq->isDefault = "yes";

          $old = new UsersCreditCardDetails();
          $old_res = $old->where("userID" , Auth::User()->id)->where("isDefault", "yes")->get();

          foreach ($old_res as $key => $value) {
            $value->isDefault = "no";
            $value->save();
          }

   			}
   			if($rq->isDefault == false) {
   				$rq->isDefault = "no";
   			}
   			
   		}

   		$data->userID = Auth::User()->id;
      $data->isDefault = $rq->isDefault;
      $data->save();

    	$response = array();
    	$response["status"] = true;
    	$response["message"] = "Saved.";
    	$response["id"] = $data->id;
    	return $response;
    }

    public function updateUserECheck(Request $rq)
    {

      if($rq->id == "")
      {
        $data = new UsersECheckDetails();
      }else
      {
        $data = UsersECheckDetails::find($rq->id);
      }
      
      if(isset($rq["name"]))
      { 
        $data->name = encrypt($rq->name);
      }


      if(isset($rq["bankAccountNumber"]))
      { 
        $data->bankAccountNumber = encrypt($rq->bankAccountNumber);
      }

      if(isset($rq["routingNumber"]))
      { 
        $data->routingNumber = encrypt($rq->routingNumber);
      }


      if(isset($rq["isDefault"]))
      {
        if($rq->isDefault == true) {
          $rq->isDefault = "yes";

          $old = new UsersECheckDetails();
          $old_res = $old->where("userID" , Auth::User()->id)->where("isDefault", "yes")->get();

          foreach ($old_res as $key => $value) {
            $value->isDefault = "no";
            $value->save();
          }
        }
        if($rq->isDefault == false) {
          $rq->isDefault = "no";
        }
        $data->isDefault = $rq->isDefault;
      }



      $data->userID = Auth::User()->id;
      $data->save();

      $response = array();
      $response["status"] = true;
      $response["message"] = "Saved.";
      $response["id"] = $data->id;
      return $response;

    }

    public function getProfile() 
    {
    	$data = UsersProfile::where("userID", Auth::User()->id)->first();

      if($data == null || $data == "") 
      {
        $data = array();
        $data["image_path"] = "/public/images/default.png";
      }else 
      {

        $pic_array = explode("/", $data["image_path"]);
        $pic_name = $pic_array[count($pic_array) - 1];
        if(!$this->checkProfilePicExist(Auth::user()->id , $pic_name))
        {
          $data["image_path"] = "/public/images/default.png";
        }
        else
        {
          $data["image_path"] = url('/').$data->image_path;
        }
      }
      
    	return $data;
    }
    public function getBillingDetails($id) 
    { 
      putenv('STRIPE_API_KEY=sk_test_bQRvTILgvtm3puD0JXIb23jb');

      $stripe               = new Stripe();
      $user                 = Auth::User();
      $data                 = UsersCreditCardDetails::find($id);
      $card                 = $stripe->cards()->find($user->stripe_id, $data->card_id);
      $data->name           = $card["name"];
      $data->last_card_four = "********".$card["last4"];
      $data->cardIDNumber   = "****";
      $data->address        = $card["address_line1"];
      return $data;
    }

    public function getCreditCards() {
      $data = UsersCreditCardDetails::where("userID", Auth::User()->id)->get();

      foreach ($data as $key => $value) {
        $value->name       = $value->name;
        $value->card_last_four = "*******".$value->card_last_four;
      }
      return $data;
    }

    public function getCheckList() 
    {
      putenv('STRIPE_API_KEY=sk_test_bQRvTILgvtm3puD0JXIb23jb');
      $data = UsersECheckDetails::where("userID", Auth::User()->id)->get();
      foreach ($data as $key => $value) {
        $value->name              = decrypt($value->name);
        $value->routingNumber     = decrypt($value->routingNumber);
        $value->bankAccountNumber = decrypt($value->bankAccountNumber);
      }
      return $data;
    }

    public function removeCard($id)
    {
      putenv('STRIPE_API_KEY=sk_test_bQRvTILgvtm3puD0JXIb23jb');
      $stripe = new Stripe();
      $user = Auth::User();
      $data = UsersCreditCardDetails::find($id);
      $stripe->cards()->delete($user->stripe_id, $data->card_id);
      $data->forceDelete();
      $response = array();
      $response["status"] = true;
      $response["message"] = "deleted.";
      return $response;
    }

    public function getProfileImageForPdf($id, $name) {
      if(!file_exists(storage_path()."/app/public/uploads/profiles/$id/$name")) {
            $storagePath = $_SERVER['DOCUMENT_ROOT']."/"."public/images/default.png";
            $file = File::get($storagePath);
            $type = pathinfo($storagePath);
            $type = $type["extension"];

            if($type == "jpg") {
                $mime = "image/jpg";
            }else if($type == "jpeg")  {
                $mime = "image/jpeg";    
            }else if($type == "png")  {
                $mime = "image/png";    
            }

            $file = Image::make($storagePath)->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

            return $file->response($type);
        }

        $storagePath = storage_path()."/app/public/uploads/profiles/$id/$name";
        $file = File::get($storagePath);
        $type = pathinfo($storagePath);
        $type = $type["extension"];

        $file = Image::make($storagePath)->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

        return $file->response($type);
    }


    public function checkProfilePicExist($id, $name) {
      if(!file_exists(storage_path()."/app/public/uploads/profiles/$id/$name")) 
      {
            $storagePath = $_SERVER['DOCUMENT_ROOT']."/"."public/images/default.png";
            $file = File::get($storagePath);
            $type = pathinfo($storagePath);
            $type = $type["extension"];

            if($type == "jpg") {
                $mime = "image/jpg";
            }else if($type == "jpeg")  {
                $mime = "image/jpeg";    
            }else if($type == "png")  {
                $mime = "image/png";    
            }

            return false;
        }
        else
        {
            return true;
        }

      
    }

    public function getProfileImage($id, $name) {
      if(!file_exists(storage_path()."/app/public/uploads/profiles/$id/$name")) {
            $storagePath = storage_path()."/app/public/uploads/profiles/default.png";
            $file = File::get($storagePath);
            $type = pathinfo($storagePath);
            $type = $type["extension"];

            if($type == "jpg") {
                $mime = "image/jpg";
            }else if($type == "jpeg")  {
                $mime = "image/jpeg";    
            }else if($type == "png")  {
                $mime = "image/png";    
            }


            $file = Image::make($storagePath)->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

            return $file->response($type);
        }

        $storagePath = storage_path()."/app/public/uploads/profiles/$id/$name";
        $file = File::get($storagePath);
        $type = pathinfo($storagePath);
        $type = $type["extension"];

        $file = Image::make($storagePath)->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

        return $file->response($type);
    }

    public function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function uploadPhoto(Request $rq) {

      $file = $rq->file("image");
      $file_ex = ".png";
      $name = $this->generateRandomString(6).".".$file_ex;

      $count = 1;

      $id = Auth::User()->id;

      while(Storage::disk('public')->has("uploads/profiles/$id/$name")) {
          $name = $count."-".$name;
          $count++;
      }

      Storage::disk("public")->put("uploads/profiles/$id/$name",file_get_contents($rq->file("image")));

      $storagePath_raw = "/user/profile/$id/$name";
      $storagePath = url('/').$storagePath_raw;

      $user = UsersProfile::where("userID", $id)->first();

      if($user == null || $user == "") 
      {
        $user = new UsersProfile();
        $user->userID = $id ;
      }

      $user->image_path =  $storagePath_raw;
      $user->save();

      $data = array("success" => true, "path" => $storagePath);

      return $data;

    }

    public function removeChecks($id) 
    {
      UsersECheckDetails::find($id)->forceDelete();
      $response = array();
      $response["status"] = true;
      $response["message"] = "deleted.";
      return $response;
    }

    public function getEchecksDetails() 
    {
      $data = UsersECheckDetails::where("userID", Auth::User()->id)->first();
      $data->name              = decrypt($data->name);
      $data->routingNumber     = decrypt($data->routingNumber);
      $data->bankAccountNumber = decrypt($data->bankAccountNumber);
      return $data;
    }
    public function getApplicants(){
      $user = User::where("id", Auth::User()->id)->first();
      $applicant_count = 0;
      $jobs_id = array();      
      foreach($user->jobs as $job){
        $jobs_id[] = $job->id;        
        $applicant_count += count($job->inqueries);        
      }            
      $application_latest = UserInquiries::whereIn('job_id', $jobs_id)->orderBy('created_at', 'desc')->limit(3)->get();      
      $application_count = UserInquiries::whereIn('job_id', $jobs_id)->count();
      $jobs_count = count($jobs_id);
      
      $applicants = array();
      foreach($application_latest as $application){
        $applicants[] = array(
          'user_id' => $application->user->id,
          'name' => $application->user->userProfile->firstName.' '.$application->user->userProfile->lastName,
          'message' => $application->message,
          'date' => $application->created_at->format('M. d Y H:i:s'),
          'profile_image' => $application->user->userProfile->image_path
        );
      }

      $data['jobs_count'] = $jobs_count;
      $data['applicants'] = $applicants;
      $data['application_count'] = $application_count;

      $subscribe_plan = $user->plans()->where('status', 'active')->first()->plan;
      $limit = ($subscribe_plan->is_unlimited == 'yes') ? 'Unlimited' : $subscribe_plan->post_allowed;
      $data['limit'] = $limit;
      
      return $data;
    }

}
