<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs;
use App\JobsCases;
use App\Priorities;
use App\JobPostSettings;
use Auth;
class ManageJobsController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function freeposting(JobPostSettings $postingsettings)
    {  

        return $postingsettings->all()->first()->free_posting;

    }

    public function disablefreeposting(Request $rq, JobPostSettings $postingsettings)
    {

        $postingsettings->find(1)->update(array(
            'free_posting' => $rq->enable
        ));
        
        return response()->json(['freeposting'=> $postingsettings->find(1)->first()->free_posting == 1 ? 'free posting enabled' : 'free posting disabled'], 200);
    }

    public function getPriorities()
    {
    	$prios = new Priorities();
    	$res = $prios->get()->groupBy("levelType");

    	return $res;
    }

    public function approveJob($id) {
    	$jobs = Jobs::find($id);
    	$jobs->post_status = "active";
    	$jobs->save();

    	$response = array();
		$response["status"] = true;
		$response["message"] = "You have approved the job post. This will be automatically display on the website.";
		return $response;
    }

    public function getJobById($id) {
    	$jobs = Jobs::find($id);

    	if($jobs == "") {
    		$response = array();
			$response["status"] = "forbidden";
			$response["message"] = "Sorry job is not available.";
			return $response;
    	}
    	/*if(Auth::User()->id != $jobs->userID) {
    		$response = array();
			$response["status"] = "forbidden";
			$response["message"] = "Sorry job is not available.";
			return $response;
    	}*/

    	$jobs_details = JobsCases::where('job_id', $id)->first();
    	$jobs->priority = $jobs->priorityLevel;
		$jobs->postAnonymously				 = $jobs->postAnonymously == 1 ? "yes" : "no";         

		if($jobs->ei_durPosition == "Full Time") {
    		$jobs->full_time_min = $jobs->ei_durMin;
    		$jobs->full_time_max = $jobs->ei_durMax;
    	}else if($jobs->ei_durPosition == "Locum Tenens") {
    		$jobs->locum_min = $jobs->ei_durMin;
    		$jobs->locum_max = $jobs->ei_durMax;
    	}else if($jobs->ei_durPosition == "Part Time") {
    		$jobs->part_time_min = $jobs->ei_durMin;	
    		$jobs->part_time_max = $jobs->ei_durMax;
    	}else if($jobs->ei_durPosition == "PRN") {
    		$jobs->prn_min = $jobs->ei_durMin;
    		$jobs->prn_max = $jobs->ei_durMax;
    	}else if($jobs->ei_durPosition == "Fellowship") {
    		$jobs->fellowship_min = $jobs->ei_durMin;
    		$jobs->fellowship_max = $jobs->ei_durMax;
    	}

    	if($jobs_details != '' && $jobs_details != null) 
    	{
    		$jobs->accute_pain_service           = $jobs_details->accute_pain_service;
			$jobs->anesthesia_out_surgery        = $jobs_details->anesthesia_out_surgery;
			$jobs->cardiac_anesthesia            = $jobs_details->cardiac_anesthesia; 
			$jobs->chronic_pain_man              = $jobs_details->chronic_pain_man; 
			$jobs->critical_care_med             = $jobs_details->critical_care_med; 
			$jobs->major_vascular_anesthesia     = $jobs_details->major_vascular_anesthesia; 
			$jobs->orthopedic_anesthesia         = $jobs_details->orthopedic_anesthesia; 
			$jobs->pediatric_anesthesia          = $jobs_details->pediatric_anesthesia; 
			$jobs->regional_anesthesia           = $jobs_details->regional_anesthesia; 
			$jobs->thoracic_anesthesia           = $jobs_details->thoracic_anesthesia; 
			$jobs->trauma_anesthesia             = $jobs_details->trauma_anesthesia; 
			$jobs->neuroanesthesia               = $jobs_details->neuroanesthesia; 
			$jobs->obstetric_anesthesia          = $jobs_details->obstetric_anesthesia; 
    	}
		
		$response = array();
		$response["status"] = true;
		$response["jobs"] = $jobs;
		return $response;
    }

    public function jobList() 
    {
    	$jobs = new Jobs();
    	$res = $jobs->where("post_status" , "awaiting_activation")->with("bookmark", "cases","inqueries","profile")->orwhere("post_status" , "active")->get();
    	return $res;
    }

    public function candidateList()
    {
        $candidates = new Users();
        $res = $jobs->where("post_status" , "awaiting_activation")->orwhere("post_status" , "active")->get();
        return $res;
    }

   
}
