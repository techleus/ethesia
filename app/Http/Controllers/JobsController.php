<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs;
use App\JobsCases;
use App\Priorities;
use App\UsersProfile;
use App\JobUpgrade;
USE App\UsersCreditCardDetails;
use App\UserInquiries;
use App\applicationMessages;
use App\applicationMessagesAttachment;
use App\ApplicationMessageNotifications;
use App\PlanUser;
use App\JobPostSettings;
use App\User;
use Carbon\Carbon;
use Cartalyst\Stripe\Stripe;
use Auth;
use App\Jobs\SendNotifications;
use App\Jobs\SendNewJobPostedJob;
use App\Mail\SendNewUserMail;
use Mail;
use file;
use Storage;


class JobsController extends Controller
{
    //    
    public function __construct()
    {
        $this->middleware('auth');        
        // putenv('STRIPE_API_KEY=sk_test_pJaH8XfGGddPfUfTcskX75wb')
    }    

    public function inqueries(Request $rq){        
        $jobs = UserInquiries::where('job_id',$rq->jobid)->with('user','profile')->orderBy('created_at', 'desc')->get();
        foreach ($jobs as $key => $value) {
            $value->time_ago = $this->getTimeAgo($value->created_at);
        }

        return $jobs;
    }

    public function attachments(Request $rq, applicationMessagesAttachment $attachments)
    {
        return $attachments->where('attachment_id',$rq->id)->get();
    }

    public function appresponse(Request $rq, User $user, ApplicationMessageNotifications $notification)
    {               

        if(count($user->where('id',$rq->userid)->get()) == 0)
        {
            return 'not found';
        }

        $applicationMessages = new applicationMessages();
        $applicationMessages = $applicationMessages->create(array(
            'sender_id' => Auth::User()->id,
            'recipient_id' => $user->where('id',$rq->userid)->first()->id,
            'subject' => $rq->subject,
            'message' => $rq->message,
        ));


        if($rq->hasFile('file'))
        {            

            $attachment = $rq->file('file');
            
            foreach ($attachment as $file) {
                $filename = $file->getClientOriginalName();
                $applicationMessagesAttachments = new applicationMessagesAttachment();
                $applicationMessagesAttachments = $applicationMessagesAttachments->create(array(
                    'attachment_id' => $applicationMessages->id,
                    'attachment_file' => $filename
                ));
                $count = 1;                    
                while(Storage::disk('public')->has("appmessages/attachments/".Auth::User()->id."/".$rq->userid."/$filename")) {
                    $filename = $count."-".$filename;
                    $count++;
                }
                Storage::disk("public")->put("appmessages/attachments/".Auth::User()->id."/".$rq->userid."/$filename",file_get_contents($file));
            }
                                                
                        

            return $applicationMessagesAttachments;
        }
        
        $user_id = $user->where('id',$rq->userid)->first()->id;
        
        if($notification->find($user_id))
        {

            if($notification->find($user_id)->email_messages == 1)
            {

                $sendto = array(
                    'name'=> $rq->account_type == 2 ? $rq->company_name : $rq->first_name. " ".$rq->last_name,
                    'to' => $rq->email,
                    'verification_code' => $verification_code,
                    'link' => url("/")."/verify/".$verification_code."/".$users_profile->userID,
                );
                  
                $user_verify = new VerificationUser();
                $user_verify->verification_code = $verification_code;
                $user_verify->user_id = Auth::User()->id;
                $user_verify->save();

                Mail::send(new SendNewUserMail($sendto));

            }

        }
        
        return $applicationMessages;
        
    }

    public function getPriorities()
    {
    	$prios = new Priorities();
    	$res = $prios->get()->groupBy("levelType");

    	return $res;
    }

    public function testData(Request $rq) 
    {
    	return $rq;
    }


    public function upgradeJobs(Request $rq) 
    {

        $user_stripe = Auth::User()->stripe_id;

        if($user_stripe == "") // if user is not registered in stripe
        {
            $stripe = new Stripe();
            $customer = $stripe->customers()->create([
                'email' => Auth::User()->email,
            ]);

            if(Auth::User()->stripe_id == "") 
            {
              $user = Auth::User();
              $user->stripe_id = $customer["id"];
              $user->save();
            }
        }

        $card_id = "";


        if($rq["plansData"]["selected_payment_method"] == "existing_card") 
        {
          $data = UsersCreditCardDetails::find($rq["plansData"]["existing_card_id"]);
        }else
        {
          $data = new UsersCreditCardDetails();
        }


        if($rq["plansData"]["selected_payment_method"] == "1") //if new card
        {

            $card = array();
            $card["name"] = $rq->billData["firstName"]." ".$rq->billData["lastName"];
            $card["number"] = $rq->billData["cardNumber"];
            $month = $rq->billData["expMonth"];
            $year = $rq->billData["expYear"];
            $card["exp_month"] = $month;
            $card["exp_year"] = $year;
            $card["cvc"] = $rq->billData["cardIDNumber"];


            try 
            {
                
                $stripe = new Stripe();
                $token = $stripe->tokens()->create([
                  'card' => $card,
                ]);


                $card = $stripe->cards()->create(Auth::User()->stripe_id, $token['id']);
              
                $data->card_id = $card["id"];
                $data->card_last_four = $card["last4"];
                $card["name"] = $rq->billData["firstName"]." ".$rq->billData["lastName"];

                $default = "no";

                if($rq->isDefault == true) 
                {
                        $default = "yes";
                }
                if($rq->isDefault == false) 
                {
                    $rq->isDefault = "no";
                }

                if($default == "yes") 
                {
                    $stripe = new Stripe();
                    $stripe->customers()->update(Auth::User()->stripe_id, [
                    'default_source' => $data->card_id,
                    ]);
                }
              
            } 
            catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
                // Get the status code
                $code = $e->getCode();

                // Get the error message returned by Stripe
                $message = $e->getMessage();

                // Get the error type returned by Stripe
                $type = $e->getErrorType();

                $response = array();
                $response["status"] = false;
                $response["message"] = $message."1";
                return $response;
              
            } catch (\Cartalyst\Stripe\Exception\InvalidRequestException $e) {
                // Get the status code
                $code = $e->getCode();

                // Get the error message returned by Stripe
                $message = $e->getMessage();

                // Get the error type returned by Stripe
                $type = $e->getErrorType();

                $response = array();
                $response["status"] = false;
                $response["message"] = $message."2";
                return $response;
            } catch (\Cartalyst\Stripe\Exception\BadRequestException $e) {
                // Get the status code
                $code = $e->getCode();

                // Get the error message returned by Stripe
                $message = $e->getMessage();

                // Get the error type returned by Stripe
                $type = $e->getErrorType();

                $response = array();
                $response["status"] = false;
                $response["message"] = $message."3";
                return $response;
            }

            if(isset($rq["isDefault"]))
            {
                if($rq->isDefault == true) {
                    $rq->isDefault = "yes";

                    $old = new UsersCreditCardDetails();
                    $old_res = $old->where("userID" , Auth::User()->id)->where("isDefault", "yes")->get();

                    foreach ($old_res as $key => $value) 
                    {
                        $value->isDefault = "no";
                        $value->save();
                    }

                }
                if($rq->isDefault == false) 
                {
                    $rq->isDefault = "no";
                }
                
            }


            $data->userID = Auth::User()->id;
            $data->isDefault = $rq->isDefault;
            $data->save();


            //charging
            //get data from mysql using id
            $price = $rq->billData["price"];

            $charge = $stripe->charges()->create([
                'customer' => Auth::User()->stripe_id,
                'currency' => 'USD',
                'amount'   => 1,
                // 'amount'   => $price,
                'source'   => $data->card_id
            ]);

            $card_id = $data->card_id;

        }
        

        
        if($rq["plansData"]["selected_payment_method"] == "2") //if new card
        {
            
            //computations
            $price = $rq->billData["price"];

            $stripe = new Stripe();
            $card = UsersCreditCardDetails::find($rq["plansData"]["existing_card_id"]);
            $charge = $stripe->charges()->create([
                'customer' => Auth::User()->stripe_id,
                'currency' => 'USD',
                'amount'   => 1,
                // 'amount'   => $price,
                'source'   => $card->card_id
            ]);

            $card_id = $card->card_id;
        }


        foreach ($rq->jobs as $key => $job) {
            $upgrade = new JobUpgrade();
            $upgrade->job_id = $job;
            $upgrade->price = $price;
            $upgrade->save();

            $jobs = Jobs::find($job);
            $jobs->prioritylevel = $rq->priority;
            $jobs->save();
        }
        

        $response = array();
        $response["status"] = true;
        $response["message"] = "Job has been upgraded";


        return $response;
    }


    public function confirmJob($id) {
    	$jobs = Jobs::find($id);
    	$jobs->post_status = "active";
    	$jobs->save();

    	$response = array();
		$response["status"] = true;
		$response["message"] = "You have confirmed the job post. This will be automatically submitted for admin apporival.";
		return $response;
    }

    public function deactivateJob(Request $rq, $id)
    {
    	$jobs = Jobs::find($id);
    	$jobs->post_status = "inactive";
    	$jobs->save();

    	$response = array();
    	$response["status"] = true;
		$response["message"] = "Job has been deactivated.";
		return $response;
    }

    public function multipleDeactivate(Request $rq) 
    {

        foreach ($rq->all() as $key => $job_id) {
            $jobs = Jobs::find($job_id);
            $jobs->post_status = "inactive";
            $jobs->save();
        }
        
        $response = array();
        $response["status"] = true;
        $response["message"] = "Jobs has been deactivated.";
        return $response;
    }

    public function restoreJob(Request $rq, $id)
    {
    	$jobs = Jobs::find($id);
    	$jobs->post_status = "active";
    	$jobs->save();

    	$response = array();
    	$response["status"] = true;
		$response["message"] = "Job has been restored.";
		return $response;
    }

    public function multipleDelete(Request $rq) 
    {

        foreach ($rq->all() as $key => $job_id) {
            $jobs = Jobs::find($job_id);
            $jobs->post_status = "deleted";
            $jobs->save();
        }
        
        $response = array();
        $response["status"] = true;
        $response["message"] = "Jobs has been deleted.";
        return $response;
    }
    public function multipleRestore(Request $rq) 
    {

        foreach ($rq->all() as $key => $job_id) {
            $jobs = Jobs::find($job_id);
            $jobs->post_status = "active";
            $jobs->save();
        }
        
        $response = array();
        $response["status"] = true;
        $response["message"] = "Jobs has been reactivated.";
        return $response;
    }

    public function deleteJob(Request $rq, $id)
    {
    	$jobs = Jobs::find($id);
    	$jobs->post_status = "deleted";
    	$jobs->save();

    	$response = array();
    	$response["status"] = true;
		$response["message"] = "Job has been deleted.";
		return $response;
    }

    public function getJobById($id) 
    {
    	$jobs = Jobs::find($id);

    	if($jobs == "") 
    	{
    		$response = array();
			$response["status"] = "forbidden";
			$response["message"] = "Sorry job is not available.";
			return $response;
    	}
    	if(Auth::User()->id != $jobs->userID) 
    	{
    		$response = array();
			$response["status"] = "forbidden";
			$response["message"] = "Sorry job is not available.";
			return $response;
    	}

    	$jobs_details = JobsCases::where('job_id', $id)->first();
    	$jobs->priority = $jobs->priorityLevel;
		$jobs->postAnonymously = $jobs->postAnonymously == 1 ? "yes" : "no";         


        if($jobs->ei_startImmediate == false) {
            $jobs->ei_startDate = date('d/m/Y', strtotime($jobs->ei_startDate));
        }else{
            $jobs->ei_startDate = "";
        }

        

		if($jobs->ei_durPosition == "Full Time") {
    		$jobs->full_time_min = $jobs->ei_durMin;
    		$jobs->full_time_max = $jobs->ei_durMax;
    	}else if($jobs->ei_durPosition == "Locum Tenens") {
    		$jobs->locum_min = $jobs->ei_durMin;
    		$jobs->locum_max = $jobs->ei_durMax;
    	}else if($jobs->ei_durPosition == "Part Time") {
    		$jobs->part_time_min = $jobs->ei_durMin;	
    		$jobs->part_time_max = $jobs->ei_durMax;
    	}else if($jobs->ei_durPosition == "PRN") {
    		$jobs->prn_min = $jobs->ei_durMin;
    		$jobs->prn_max = $jobs->ei_durMax;
    	}else if($jobs->ei_durPosition == "Fellowship") {
    		$jobs->fellowship_min = $jobs->ei_durMin;
    		$jobs->fellowship_max = $jobs->ei_durMax;
    	}

    	if($jobs_details != '' && $jobs_details != null) 
    	{
    		$jobs->accute_pain_service           = $jobs_details->accute_pain_service;
			$jobs->anesthesia_out_surgery        = $jobs_details->anesthesia_out_surgery;
			$jobs->cardiac_anesthesia            = $jobs_details->cardiac_anesthesia; 
			$jobs->chronic_pain_man              = $jobs_details->chronic_pain_man; 
			$jobs->critical_care_med             = $jobs_details->critical_care_med; 
			$jobs->major_vascular_anesthesia     = $jobs_details->major_vascular_anesthesia; 
			$jobs->orthopedic_anesthesia         = $jobs_details->orthopedic_anesthesia; 
			$jobs->pediatric_anesthesia          = $jobs_details->pediatric_anesthesia; 
			$jobs->regional_anesthesia           = $jobs_details->regional_anesthesia; 
			$jobs->thoracic_anesthesia           = $jobs_details->thoracic_anesthesia; 
			$jobs->trauma_anesthesia             = $jobs_details->trauma_anesthesia; 
			$jobs->neuroanesthesia               = $jobs_details->neuroanesthesia; 
			$jobs->obstetric_anesthesia          = $jobs_details->obstetric_anesthesia; 
    	}
		


		$response = array();
		$response["status"] = true;
		$response["jobs"] = $jobs;
		return $response;
    }

    public function sendJobNotif($title)
    {
        $profile = UsersProfile::where("userID", Auth::User()->id)->first();
        $params = array(); 


        if($profile != null) 
        {
            if($profile->company_name == "")
            {
              if($profile->first_name == "") 
              {
                $params["name"] = Auth::User()->email;
              }else{
                $params["name"] = $profile->first_name." ".$profile->last_name;
              }
            }else{
              $params["name"] = $profile->company_name;
            }
        }
        else
        {
            $params["name"] = Auth::User()->email;
        }


        $params["url"] = url('/');
        $params["subject"] = "New Job has been created.";
        $params["email_from"] = "hello@anesthesia.community";
        $params["job_title"] = $title;
        $params["to"] = Auth::User()->email;

        SendNewJobPostedJob::dispatch($params);
    }

    public function getTimeAgo($carbonObject) 
    {
        return $carbonObject->diffForHumans();
    }

    public function jobList(Request $rq) 
    {

    	
    	$userID = Auth::User()->id;
    	$jobs =  new Jobs();
        $jobs =  $jobs->with("bookmark", "cases","inqueries");
    	$usersProfile = UsersProfile::where("userID" , $userID)->first();
    	$company_name = $usersProfile->company_name;
    	

        if(isset($rq["job_post_type"]) && $rq["job_post_type"] != "" && $rq["job_post_type"] != "All")
        {
            $job_type = $rq->job_post_type == "Anesthesiologist" ? 1 : 2;
            $jobs = $jobs->where('jobPostType', $job_type);
        }

        if(isset($rq["job_title"]) && $rq["job_title"] != "")
        {
            $jobs = $jobs->whereRaw("CONCAT(jobTitle, ' ', jobTitle) like '%".$rq["job_title"]."%'");
        }

        if(isset($rq["job_id"]) && $rq["job_id"] != "")
        {
            $jobs = $jobs->where('id', $rq["job_id"]);
        }

        if(isset($rq["job_location"]) && $rq["job_location"] != "")
        {
            $jobs = $jobs->whereRaw("CONCAT(ei_facilityState, ' ', ei_facilityState) like '%".$rq["job_location"]."%'");

            $jobs = $jobs->orWhereRaw("CONCAT(ei_facilityCity, ' ', ei_facilityCity) like '%".$rq["job_location"]."%'");
        }

        if(isset($rq["status"])) 
    	{
    		$jobs = $jobs->where("userID" , $userID)->where("post_status" , $rq["status"])->get();
    	}
    	else
    	{
    		$jobs = $jobs->where("userID" , $userID)->get();
    	}
    	

    	$userProfile = UsersProfile::where("userID" , $userID)->first();

    	foreach ($jobs as $key => $job) {
    		$priority_data =  Priorities::find($job->priorityLevel);

    		if($priority_data != null) 
    		{
    			$priority_level = $priority_data->levelType;
    			$job["level"] = $priority_level;
    			
				if($priority_level == "1") 
				{
					$priority_level = "High Priority Posting";

				}
				else if($priority_level == "2")
				{
					$priority_level = "Priority Posting";
				}
				else
				{
					$priority_level = "General Posting";
				}

	    		$job["priority_str"] = $priority_level;
    		}
			else
			{
				$job["priority_str"] = "";
			}

            $job->job_type = $job->jobPostType == '1' ? "Anesthesiologist" : "CRNA";
            $job->time_ago = $this->getTimeAgo($job->created_at);

            if($userProfile != null) 
            {   
                if($userProfile->image_path == "") 
                {
                    $userProfile->image_path = "/public/images/default.png";
                }
                $job->image_path = $userProfile->image_path;
                $job->company_city = $userProfile->city;
                $job->company_state = $userProfile->state;
                $job->company_website = $userProfile->website;
                $job->company_name = $userProfile->company_name;
            } 
            else
            {
                $job->image_path = "/public/images/default.png";
                $job->company_city = "";
                $job->company_state = "";
                $job->company_website = "";
                $job->company_name = "";
            }


            //check high prio

            $job->highPrio = $job->priorityLevel == 1 ? true : false;

            $job["created_date"] = date("F d, Y", strtotime($job["created_at"]->toDateTimeString()));
            $job["updated_date"] = date("F d, Y", strtotime($job["updated_at"]->toDateTimeString()));



			$job["posted_date"] = date('M,d Y', strtotime(str_replace('-', '/', $job->created_at)));
			$job["posted_date_updated"] = date('M,d Y', strtotime(str_replace('-', '/', $job->updated_at)));

    		$jobs_details = JobsCases::where('job_id', $job->id)->first();

            if($jobs_details != null) 
            {
                $job["accute_pain_service"]           = $jobs_details->accute_pain_service;
                $job["anesthesia_out_surgery"]        = $jobs_details->anesthesia_out_surgery;
                $job["cardiac_anesthesia"]            = $jobs_details->cardiac_anesthesia; 
                $job["chronic_pain_man"]              = $jobs_details->chronic_pain_man; 
                $job["critical_care_med"]             = $jobs_details->critical_care_med; 
                $job["major_vascular_anesthesia"]     = $jobs_details->major_vascular_anesthesia; 
                $job["orthopedic_anesthesia"]         = $jobs_details->orthopedic_anesthesia; 
                $job["pediatric_anesthesia"]          = $jobs_details->pediatric_anesthesia; 
                $job["regional_anesthesia"]           = $jobs_details->regional_anesthesia; 
                $job["thoracic_anesthesia"]           = $jobs_details->thoracic_anesthesia; 
                $job["trauma_anesthesia"]             = $jobs_details->trauma_anesthesia; 
                $job["neuroanesthesia"]               = $jobs_details->neuroanesthesia; 
                $job["obstetric_anesthesia"]          = $jobs_details->obstetric_anesthesia;
            
            }

			
        }
		

    	return $jobs;
    }

    public function searchJobs(Request $rq) 
    {	

    	$userID = Auth::User()->id;
        $jobs =  new Jobs();
    	$jobs =  $jobs->with("bookmark", "cases");

    	$jobs = $jobs->where("userID" , $userID)->where("post_status" , $rq["selected_status"]);

    	if($rq["job_id"] != "") 
    	{
    		$jobs = $jobs->where('id', 'like', '%'.$rq["job_id"].'%');
    	}

    	if($rq["specialty"] != "") 
    	{
    		$jobs = $jobs->where('ei_durPosition', 'like', '%'.$rq["specialty"].'%');
    	}

    	if($rq["job_title"] != "") 
    	{
    		$jobs = $jobs->where('jobTitle', 'like', '%'.$rq["job_title"].'%');
    	}

    	$jobs = $job->get();

    	$userProfile = UsersProfile::where("userID" , $userID)->first();

    	foreach ($jobs as $key => $job) {
    		$priority_data =  Priorities::find($job->priorityLevel);

    		if($priority_data != null) 
    		{
    			$priority_level = $priority_data->levelType;

				if($priority_level == "1") 
				{
					$priority_level = "High Priority Posting";
				}
				else if($priority_level == "2")
				{
					$priority_level = "Priority Posting";
				}
				else
				{
					$priority_level = "General Posting";
				}

	    		$job["priority_str"] = $priority_level;
    		}
			else
			{
				$job["priority_str"] = "";
			}

			$job["image_path"] = $userProfile->image_path;

    		$jobs_details = JobsCases::where('job_id', $job->id)->first();
			$job["accute_pain_service"]           = $jobs_details->accute_pain_service;
			$job["anesthesia_out_surgery"]        = $jobs_details->anesthesia_out_surgery;
			$job["cardiac_anesthesia"]            = $jobs_details->cardiac_anesthesia; 
			$job["chronic_pain_man"]              = $jobs_details->chronic_pain_man; 
			$job["critical_care_med"]             = $jobs_details->critical_care_med; 
			$job["major_vascular_anesthesia"]     = $jobs_details->major_vascular_anesthesia; 
			$job["orthopedic_anesthesia"]         = $jobs_details->orthopedic_anesthesia; 
			$job["pediatric_anesthesia"]          = $jobs_details->pediatric_anesthesia; 
			$job["regional_anesthesia"]           = $jobs_details->regional_anesthesia; 
			$job["thoracic_anesthesia"]           = $jobs_details->thoracic_anesthesia; 
			$job["trauma_anesthesia"]             = $jobs_details->trauma_anesthesia; 
			$job["neuroanesthesia"]               = $jobs_details->neuroanesthesia; 
			$job["obstetric_anesthesia"]          = $jobs_details->obstetric_anesthesia;
    	}
		

    	return $jobs;
    }

    public function newJob(Request $rq, PlanUser $plan, Jobs $jobscount, JobPostSettings $postingsetting) 
    {

        // return $rq->sub_user_id.'test';

        $validator = \Validator::make($rq->all(), [
           'jobPostType' => 'required',
           'ei_durPosition' => 'required',
           'ei_jobDescription' => 'required',
           'ei_facilityCity' => 'required',
           'ei_facilityState' => 'required',
        ]);
        
        if ($validator->fails()) {
            
            return response()->json(['process'=>'error','error'=>$validator->errors()->all()]);
        
        }      

        if($postingsetting->all()->first()->free_posting != true){

            $plan = $plan->where('user_id', Auth::User()->id)->where('status','active')->with('plan')->first();

            if( $plan->plan_expiration >= Carbon::now()->format('Y-m-d') ){            
            
                if($plan->plan['is_unlimited'] != "yes")
                {
                    
                    $jobposted = $jobscount->where('created_at','>=',$plan->created_at)->where('userID',Auth::User()->id)->get();

                    $jobcount = $jobposted->count();

                    if(!($plan->plan['post_allowed'] > $jobcount))
                    {
                                
                        return response()->json(['process'=>'error','message'=>'Allowed number of posting consumed already!']);

                    }
                }
            
            }else{

                return response()->json(['process'=>'error','message'=>'Job posting package already expired, please choose a new posting plan.']);
            
            }       

        }                    

        //validated

    	if($rq->id != "") 
    	{
    		$jobs = Jobs::find($rq->id);
    	}else
    	{
    		$jobs = new Jobs();
    		$jobs->post_status = "awaiting_activation";
    	}


    
    	if($rq->ei_durPosition == "Full Time") 
    	{
    		$durMin =  $rq->full_time_min;
    		$durMax =  $rq->full_time_max;
    	}else if($rq->ei_durPosition == "Locum Tenens") 
    	{
    		$durMin =  $rq->locum_min;
    		$durMax =  $rq->locum_max;
    	}else if($rq->ei_durPosition == "Part Time") 
    	{
    		$durMin =  $rq->part_time_min;	
    		$durMax =  $rq->part_time_max;
    	}else if($rq->ei_durPosition == "PRN")
    	{
    		$durMin = $rq->prn_min;
    		$durMax =  $rq->prn_max;
    	}else if($rq->ei_durPosition == "Fellowship") 
    	{
    		$durMin =  $rq->fellowship_min;
    		$durMax =  $rq->fellowship_max;
    	}
        
        $jobs->sub_user_id           = $rq->sub_user_id;

    	$jobs->userID                = Auth::User()->id;
    	$jobs->jobTitle 			 = $rq->jobTitle; 

    	$jobs->ei_durMin             = $durMin;
    	$jobs->ei_durMax             = $durMax;
    	$jobs->jobPostType           = $rq->jobPostType; 
		$jobs->priorityLevel         = $rq->priority; 
		$jobs->ei_jobDescription     = $rq->ei_jobDescription;  
		$jobs->postAnonymously       = $rq->postAnonymously == "yes" ? 1 : 0; 
		$jobs->privateNotes          = $rq->privateNotes; 
		$jobs->ag_haveContract       = $rq->ag_haveContract; 
		$jobs->ag_name               = $rq->ag_name; 
		$jobs->ag_webSite            = $rq->ag_webSite; 
		$jobs->ei_callRequired       = $rq->ei_callRequired; 
		$jobs->ci_address1           = $rq->ci_address1; 
		$jobs->ci_address2           = $rq->ci_address2; 
		$jobs->ci_city               = $rq->ci_city; 
		$jobs->ci_company            = $rq->ci_company; 
		$jobs->ci_country            = $rq->ci_country; 
		$jobs->ci_email              = $rq->ci_email;
		$jobs->ci_fax                = $rq->ci_fax;
		$jobs->ci_jobID              = $rq->ci_jobID;
		$jobs->ci_name               = $rq->ci_name;
		$jobs->ci_phone              = $rq->ci_phone;
		$jobs->ci_prefMethod         = $rq->ci_prefMethod;
		$jobs->ci_state              = $rq->ci_state;
		$jobs->ci_website            = $rq->ci_website;
		$jobs->ci_zipcode            = $rq->ci_zipcode;
		
		$jobs->ei_ansFirstCall       = $rq->ei_ansFirstCall;
		$jobs->ei_ansMedDirecting    = $rq->ei_ansMedDirecting;
		$jobs->ei_durPosition        = $rq->ei_durPosition; 
		$jobs->ei_empStatus          = $rq->ei_empStatus; 
		$jobs->ei_facilityAddress 	 = $rq->ei_facilityAddress; 
		$jobs->ei_facilityCity       = $rq->ei_facilityCity; 
		$jobs->ei_facilityState      = $rq->ei_facilityState; 
		$jobs->ei_jobDescription     = $rq->ei_jobDescription; 
		
		$jobs->ei_startImmediate     = $rq->ei_startImmediate; 
        
        if($rq->ei_startImmediate != true) 
        {
            $startDate = date('Y-m-d', strtotime(str_replace('-', '/', $rq->ei_startDate)));
            $jobs->ei_startDate = $startDate; 
        }
        else
        {
            $jobs->ei_startDate          = ""; 
        }

		$jobs->fi_isFederalJob       = $rq->fi_isFederalJob; 
		$jobs->fi_isPracLimited      = $rq->fi_isPracLimited; 
		$jobs->fi_nameAns            = $rq->fi_nameAns; 
		$jobs->fi_nameAssistant      = $rq->fi_nameAssistant; 
		$jobs->fi_nameCRNA           = $rq->fi_nameCRNA; 
		$jobs->jr_abaCertStatus      = $rq->jr_abaCertStatus; 
		$jobs->jr_fellowshipReq      = $rq->jr_fellowshipReq; 
		$jobs->jr_newGraduates       = $rq->jr_newGraduates; 
		$jobs->jr_stateLicense       = $rq->jr_stateLicence; 
		$jobs->privateNotes          = $rq->privateNotes; 
		$jobs->si_PaidLeave          = $rq->si_PaidLeave; 
		$jobs->si_PaidVacation       = $rq->si_PaidVacation; 
		$jobs->si_SignonBonus        = $rq->si_SignonBonus; 
		$jobs->type_cases_other      = $rq->type_cases_other; 



		$jobs->current_acls          = isset($rq["current_acls"]) ? var_export($rq->current_acls, true) : "false";
		$jobs->current_pals          = isset($rq["current_pals"]) ? var_export($rq->current_pals, true) : "false";
		$jobs->aba_pain_management   = isset($rq["aba_pain_management"]) ? var_export($rq->aba_pain_management, true) : "false"; 
		$jobs->aba_critical_care     = isset($rq["aba_critical_care"]) ? var_export($rq->aba_critical_care, true) : "false";

		$jobs->current_acls = preg_replace('/[^A-Za-z0-9\-]/', '', $jobs->current_acls);
		$jobs->current_pals = preg_replace('/[^A-Za-z0-9\-]/', '', $jobs->current_pals);
		$jobs->aba_pain_management = preg_replace('/[^A-Za-z0-9\-]/', '', $jobs->aba_pain_management);
		$jobs->aba_critical_care = preg_replace('/[^A-Za-z0-9\-]/', '', $jobs->aba_critical_care);

		$jobs->save();

		if($rq->id != "") 
    	{
    		$jobsCases = JobsCases::where("job_id" , $jobs->id)->first();
    		if($jobsCases == "" || $jobsCases == null) {
    			$jobsCases = new JobsCases();
    		}
    	}else
    	{
    		$jobsCases = new JobsCases();

            if(env("APP_ENV") == "production") 
            {
                $this->sendJobNotif($rq->jobTitle);
            }
            
    	}

    	
    	$jobsCases->job_id						  = $jobs->id;
		$jobsCases->accute_pain_service           = $rq->accute_pain_service;
		$jobsCases->anesthesia_out_surgery        = $rq->anesthesia_out_surgery;
		$jobsCases->cardiac_anesthesia            = $rq->cardiac_anesthesia; 
		$jobsCases->chronic_pain_man              = $rq->chronic_pain_man; 
		$jobsCases->critical_care_med             = $rq->critical_care_med; 
		$jobsCases->major_vascular_anesthesia     = $rq->major_vascular_anesthesia; 
		$jobsCases->orthopedic_anesthesia         = $rq->orthopedic_anesthesia; 
		$jobsCases->pediatric_anesthesia          = $rq->pediatric_anesthesia; 
		$jobsCases->regional_anesthesia           = $rq->regional_anesthesia; 
		$jobsCases->thoracic_anesthesia           = $rq->thoracic_anesthesia; 
		$jobsCases->trauma_anesthesia             = $rq->trauma_anesthesia; 
		$jobsCases->neuroanesthesia               = $rq->neuroanesthesia; 
		$jobsCases->obstetric_anesthesia          = $rq->obstetric_anesthesia; 
		$jobsCases->save();


        

		$response = array();
		$response["status"] = true;
		$response["id"] = $jobs->id;
		$response["message"] = "Job has been saved";
		return $response;
    }    
}
