<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\SendRegistrationInvite;
use App\UsersProfile;
use App\UserInvitationKey;
use App\User;
use Mail;
use Auth;

class InvitesController extends Controller
{

	protected $params = array();

    public function sendInvitation(Request $request, UsersProfile $profile,UserInvitationKey $invitationkey)
    {
    	$check = new User();
	    $checkRes = $check->where("email" , $request->email)->get();
	    
	    if(count($checkRes) > 0)
	    {
	    	return response()->json(['error'=>'this email is already taken']);
	    }

    	$profile = $profile->where('userID',Auth()->user()->id)->first();        

    	$this->params = [
    		'to' => $request->email,
    		'from' => $profile->firstName . ' ' . $profile->lastName,
    		'message' => $request->message,
    		'link' => url('/').$this->createInvitationLink(Auth()->user()->id,$request->email,$invitationkey),
    	];

    	Mail::send(New SendRegistrationInvite($this->params));
    }

    public function createInvitationLink($providerUserId,$emailToInvite,$invitationkey)
    {
    	$key = $this->generateRandomString(100);

    	$data = $invitationkey->create([
    		'provider_id' => $providerUserId,
			'invited_email' => $emailToInvite,
			'invitation_key' => $key,
    	]);

    	return '/accept-invite/'.$providerUserId.'/'. $key;
    }

    public function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function acceptInvite($providerId,$inviteKey)
    {
    	$invitationkey = new UserInvitationKey();
    	$invitationkey = $invitationkey->where('provider_id',$providerId)
						->where('invitation_key',$inviteKey)
						->first();
		
		if(!isset($invitationkey->invited_email))
		{
			return response()->json(['error'=>"invitation key is not valid"]);
		}

		$data = array(
			'provider_id' => $providerId,
			'invited_email' => $invitationkey->invited_email,
			'invitation_key' => $inviteKey,
		);
		return view('invites.content',['data'=>$data]);    	
    }

    public function registerInvitedUser(Request $request)
    {    	
    	$check = new User();
	    $checkRes = $check->where("email" , $request->invited_email)->get();

	    if(count($checkRes) > 0) {
	        $data = array();
	        $data["status"] = false;
	        $data["message"] = "Sorry email is taken. Please choose another one.";
	        return $data;
	    }
	    $data = new User();
    	$data->email = $rq->email;
    	$data->password = Hash::make($rq->password);
    	$data->username = $rq->email;
    	$data->userType = $rq->account_type;
    	$data->userTitle = $rq->account_title;
      	$data->is_confirmed = "no";
    	$data->save();

      	$users_profile = new UsersProfile();
    	$users_profile->description = $rq->description;      	
    }

}
