<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CvPriority;
use App\Cv;
use App\CvCases;
use App\CvCountry;
use App\CvState;
use App\CvFellowship;
use App\CvWantedStates;
use App\CvDesiredDuration;
use Auth;

class CvController extends Controller
{
    //

    public function getPriorities()
    {
    	$prios = new CvPriority();
    	$res = $prios->get()->groupBy("levelType");

    	return $res;
    }

    public function saveCv(Request $rq)
    {

    	if($rq->id == "") 
    	{
    		$data = new Cv();
    	}else
    	{
    		$data = Cv::find($rq->id);
    	}

    	if($rq->ei_durPosition == "Full Time") {
    		$data->min_salary = $rq->full_time_min;
    		$data->max_salary = $rq->full_time_max;
    	}else if($rq->ei_durPosition == "Locum Tenens") {
    		$data->min_salary = $rq->locum_min;
    		$data->max_salary = $rq->locum_max;
    	}else if($rq->ei_durPosition == "Part Time") {
    		$data->min_salary = $rq->part_time_min;	
    		$data->max_salary = $rq->part_time_max;
    	}else if($rq->ei_durPosition == "PRN") {
    		$data->min_salary = $rq->prn_min;
    		$data->max_salary = $rq->prn_max;
    	}else if($rq->ei_durPosition == "Fellowship") {
    		$data->min_salary = $rq->fellowship_min;
    		$data->max_salary = $rq->fellowship_max;
    	}


    	$data->locumTenensEndDate 	   = $rq->locumTenensEndDate;
    	$data->userID 		  		   = Auth::User()->id;
    	$data->anesTrainingPhysician   = $rq->anesTrainingPhysician;
    	$data->salaryFeeIndiPrac       = $rq->salaryFeeIndiPrac;
    	$data->ei_durPosition 		   = $rq->ei_durPosition;
    	$data->contactCity 		       = $rq->contactCity;
    	$data->contactWebSite 		   = $rq->contactWebSite;
		$data->priority                = $rq->priority;
		$data->postAnonymously         = $rq->postAnonymously;
		$data->privateNotes            = $rq->privateNotes;
		$data->companyName             = $rq->companyName;
		$data->name                    = $rq->name;
		$data->locumCharge             = $rq->locumCharge;
		$data->contactName             = $rq->contactName;
		$data->contactEmail            = $rq->contactEmail;
		$data->contactStreetAddressOne = $rq->contactStreetAddressOne;
		$data->contactAddressTwo       = $rq->contactAddressTwo;
		$data->contactState            = $rq->contactState;
		$data->contactZip              = $rq->contactZip;
		$data->ci_country              = $rq->ci_country;
		$data->contactVoicePhone       = $rq->contactVoicePhone;
		$data->contactFax              = $rq->contactFax;
		$data->contactOption           = $rq->contactOption;
		$data->briefDesc         	   = $rq->briefDesc;
		$data->dateAvailable           = $rq->dateAvailable;
		$data->startDateLocum           = $rq->startDateLocum;
		$data->startImmediate          = $rq->startImmediate == false || $rq->startImmediate == 0 ? "no" : "yes";
		$data->startImmediateLocum     = $rq->startImmediateLocum == false || $rq->startImmediateLocum == 0 ? "no" : "yes";
		$data->locumMin                = $rq->locumMin;
		$data->lifeSupportCards_acls   = $rq->lifeSupportCards_acls;
		$data->lifeSupportCards_pals   = $rq->lifeSupportCards_pals;
		$data->anesDirector            = $rq->anesDirector;
		$data->anesPrtnership          = $rq->anesPrtnership;
		$data->type_cases_other        = $rq->type_cases_other;
		$data->ei_ansMedDirecting      = $rq->ei_ansMedDirecting;
		$data->jr_abaCertPain          = $rq->jr_abaCertPain;
		$data->jr_abaCertCrit          = $rq->jr_abaCertCrit;
		$data->obtainingCert           = $rq->obtainingCert;
		$data->obtainingCert           = $rq->obtainingCert;
		$data->ansFirstCall            = $rq->ansFirstCall;
		$data->dayOff                  = $rq->dayOff;
		$data->personalCare            = $rq->personalCare;
		$data->medicalDirecting        = $rq->medicalDirecting;
		$data->percentagePersonalCare  = $rq->percentagePersonalCare;
		$data->anesPreferenceOp        = $rq->anesPreferenceOp;
		$data->anesLaborPref           = $rq->anesLaborPref;
		$data->anesResidencyProgram    = $rq->anesResidencyProgram;
		$data->anesCrnaTraining        = $rq->anesCrnaTraining;
		$data->anesAssistantTraining   = $rq->anesAssistantTraining;
		$data->anesPracticeExp         = $rq->anesPracticeExp;
		$data->anesHospitalSize        = $rq->anesHospitalSize;
		$data->populationDesired       = $rq->populationDesired;
		$data->sponsorVisa             = $rq->sponsorVisa;
		$data->annualDesiredIncome     = $rq->annualDesiredIncome;
		$data->empStatus               = $rq->empStatus;
		$data->salaryFromHospitalOther = $rq->salaryFromHospitalOther;
		$data->salaryFromGroup         = $rq->salaryFromGroup;
		$data->salaryPercFromGroup     = $rq->salaryPercFromGroup;
		$data->salaryFromHospital      = $rq->salaryFromHospital;
		$data->anesDegree              = $rq->anesDegree;
		$data->medical_school          = $rq->medical_school;
		$data->abaCertified            = $rq->abaCertified;
		$data->yearPrimaryCert         = $rq->yearPrimaryCert;
		$data->abaReCertified          = $rq->abaReCertified;
		$data->abaListYear             = $rq->abaListYear;
		$data->anesCommentsCert        = $rq->anesCommentsCert;
		$data->hasFellowship           = $rq->hasFellowship;
		$data->haveSpeciltyNoFellowhip = $rq->haveSpeciltyNoFellowhip;
		$data->newGraduate             = $rq->newGraduate;
		$data->currentEmployer         = $rq->currentEmployer;
		$data->currentEmployerAddress  = $rq->currentEmployerAddress;
		$data->jobReasonSeeking        = $rq->jobReasonSeeking;
		$data->interest                = $rq->interest;
		$data->reference               = $rq->reference;
		$data->referenceEmail          = $rq->referenceEmail;
		$data->referencePhone          = $rq->referencePhone;
		$data->yourCv                  = $rq->yourCv;
		$data->save();


		$states = CvState::where("cv_id", $data->id)->get();

		if($states != "" && count($states) > 0) 
		{
			foreach ($states as $key => $state) 
			{
				$state->forceDelete();
			}
		}

		if(isset($rq->stateToWork) && count($rq->stateToWork) > 0)
		{
			foreach ($rq->stateToWork as $key => $state_src) {
				# code...
				$cv_state = new CvState();
				$cv_state->state = $state_src;
				$cv_state->cv_id = $data->id;
				$cv_state->save();
			}
		}

		$wanted_states = CvWantedStates::where("cv_id", $data->id)->get();
		if($wanted_states != "" && count($wanted_states) > 0) 
		{
			foreach ($wanted_states as $key => $wstate) 
			{
				$wstate->forceDelete();
			}
		}


		if(isset($rq->wanted_states) && count($rq->wanted_states) > 0) 
			{
				foreach ($rq->wanted_states as $key => $state_src) 
				{
					# code...
					$wstate = new CvWantedStates();
					$wstate->state = $state_src;
					$wstate->cv_id = $data->id;
					$wstate->save();
				}
			}


		$country_data = CvCountry::where("cv_id", $data->id)->get();

		if($country_data != "" && count($country_data) > 0) 
		{
			foreach ($country_data as $key => $ctry) 
			{
				$ctry->forceDelete();
			}
		}
		if(isset($rq->anesCountryLicenced) && count($rq->anesCountryLicenced) > 0) 
		{
			foreach ($rq->anesCountryLicenced as $key => $country) 
			{
				$cv_country = new CvCountry();
				$cv_country->cv_id =   $data->id;
				$cv_country->country = $country;
				$cv_country->save();
			}
		}

		$desiredDuration = CvDesiredDuration::where("cv_id", $data->id)->first();
		if($desiredDuration != "" && $desiredDuration != null) 
		{
			$desiredDuration = CvDesiredDuration::where("cv_id", $data->id)->first();
		}else
		{
			$desiredDuration = new CvDesiredDuration();
		}
		
		$desiredDuration->cv_id   			   = $data->id;;
		$desiredDuration->desired_full_time    = $rq->desired_full_time;
		$desiredDuration->desired_locum_tenens = $rq->desired_locum_tenens;
		$desiredDuration->desired_part_time    = $rq->desired_part_time;
		$desiredDuration->desired_prn          = $rq->desired_prn;
		$desiredDuration->desired_fellowship   = $rq->desired_fellowship;
		$desiredDuration->save();

		


		$fellowship = CvFellowship::where("cv_id", $data->id)->first();

		if($fellowship != "") 
		{
			$fellowship = CvFellowship::find($fellowship->id);
		}
		else
		{
			$fellowship = new CvFellowship();
		}
		
		$fellowship->cv_id = $data->id;
		$fellowship->fellowship_pain_management = $rq->fellowship_pain_management;
		$fellowship->fellowhip_cardiothoracic   = $rq->fellowhip_cardiothoracic;
		$fellowship->fellowship_obstetric       = $rq->fellowship_obstetric;
		$fellowship->fellowship_pediatric       = $rq->fellowship_pediatric;
		$fellowship->fellowship_neuro           = $rq->fellowship_neuro;
		$fellowship->fellowship_trauma          = $rq->fellowship_trauma;
		$fellowship->fellowship_critical_care   = $rq->fellowship_critical_care;
		$fellowship->fellowship_outpatient      = $rq->fellowship_outpatient;
		$fellowship->fellowship_office_based    = $rq->fellowship_office_based;
		$fellowship->fellowship_plastic_surgery = $rq->fellowship_plastic_surgery;
		$fellowship->fellowship_dental_surgery  = $rq->fellowship_dental_surgery;
		$fellowship->fellowship_eye_surgery     = $rq->fellowship_eye_surgery;
		$fellowship->fellowship_foot_surgery    = $rq->fellowship_foot_surgery;
		$fellowship->fellowship_transportation  = $rq->fellowship_transportation;
		$fellowship->fellowship_other           = $rq->fellowship_other;
		$fellowship->save();


		if($rq->id != "") 
    	{
    		$cvCases = CvCases::where("cv_id" , $data->id)->first();
    		if($cvCases == "" || $cvCases == null) {
    			$cvCases = new CvCases();
    		}
    	}else
    	{
    		$cvCases = new CvCases();
    	}

    	
    	$cvCases->cv_id						    = $data->id;
		$cvCases->accute_pain_service           = $rq->accute_pain_service;
		$cvCases->anesthesia_out_surgery        = $rq->anesthesia_out_surgery;
		$cvCases->cardiac_anesthesia            = $rq->cardiac_anesthesia; 
		$cvCases->chronic_pain_man              = $rq->chronic_pain_man; 
		$cvCases->critical_care_med             = $rq->critical_care_med; 
		$cvCases->major_vascular_anesthesia     = $rq->major_vascular_anesthesia; 
		$cvCases->orthopedic_anesthesia         = $rq->orthopedic_anesthesia; 
		$cvCases->pediatric_anesthesia          = $rq->pediatric_anesthesia; 
		$cvCases->regional_anesthesia           = $rq->regional_anesthesia; 
		$cvCases->thoracic_anesthesia           = $rq->thoracic_anesthesia; 
		$cvCases->trauma_anesthesia             = $rq->trauma_anesthesia; 
		$cvCases->neuroanesthesia               = $rq->neuroanesthesia; 
		$cvCases->obstetric_anesthesia          = $rq->obstetric_anesthesia; 
		$cvCases->save();


		$response = array();
		$response["status"] = true;
		$response["message"] = "Cv has been saved!.";
		return $response;
		
    }

    public function getCvs() 
    {
    	$cvs = new Cv();
    	$cvs = $cvs->where("userId", Auth::User()->id)->get();

    	foreach ($cvs as $key => $cv) {
			$positions =  CvDesiredDuration::where('cv_id',$cv->id)->first();			
			$cv["desired_full_time"]    = $positions["desired_full_time"];
			$cv["desired_locum_tenens"] = $positions["desired_locum_tenens"];
			$cv["desired_part_time"]    = $positions["desired_part_time"];
			$cv["desired_prn"]          = $positions["desired_prn"];
			$cv["desired_fellowship"]   = $positions["desired_fellowship"];

			$states = CvState::where("cv_id", $cv->id)->get();

			if($states != "") {
				foreach ($states as $key => $state) {
					$cv["licensed_states"]   .=  $state["state"] ."<br>";
				}
				
			}

			$stateToWork = CvState::where("cv_id", $cv->id)->get();
			$stateToWork_array = array();

			if($stateToWork != "" && count($stateToWork) > 0)
			{

				foreach ($stateToWork as $key => $state) {
					if($key == (count($stateToWork) - 1)) 
					{
						$cv["stateToWork"] .= $state["state"];
					}
					else
					{
						$cv["stateToWork"] .= $state["state"].", ";
					}

					
				}
			}



			$priority_data =  CvPriority::find($cv["priority"]);
			$cv["priority_level"] = $priority_data->levelType;

			if($cv["priority_level"] == "1") 
			{
				$cv["priority_level"] = "High Priority";
			}
			else if($cv["priority_level"] == "2")
			{
				$cv["priority_level"] = "Priority";
			}
			else
			{
				$cv["priority_level"] = "General";
			}


			$wstates = CvWantedStates::where("cv_id", $cv->id)->get();

			if($wstates != "") {
				foreach ($wstates as $key => $state) {
					$cv["wanted_states"]   .=  $state["state"] ."<br>";
				}
				
			}


			$cv["updated"] =  date('M-j-Y g:i A', strtotime($cv["updated_at"]));
			$cv["posted"] =  date('M-j-Y g:i A', strtotime($cv["created_at"]));
    	}

    	return $cvs;
    }


    public function getCvById($id) 
    {
    	$cv = Cv::find($id);

		$positions =  CvDesiredDuration::where('cv_id',$cv->id)->first();			
		$cv["desired_full_time"]    = $positions["desired_full_time"];
		$cv["desired_locum_tenens"] = $positions["desired_locum_tenens"];
		$cv["desired_part_time"]    = $positions["desired_part_time"];
		$cv["desired_prn"]          = $positions["desired_prn"];
		$cv["desired_fellowship"]   = $positions["desired_fellowship"];

		$states = CvState::where("cv_id", $cv->id)->get();

		if($states != "") {
			foreach ($states as $key => $state) {
				$cv["licensed_states"]   .=  $state["state"] ."<br>";
			}
			
		}



		$wstates = CvWantedStates::where("cv_id", $cv->id)->get();
		$cv["wanted_states"] = array();
		if($wstates != "") {
			$data_array = array();
			foreach ($wstates as $key => $state) {
				$cv["wanted_states_str"]   .=  $state["state"] ."<br>";
				array_push($data_array, $state["state"]);
			}
		}

		$cv["wanted_states"] = $data_array;

		$cv["updated"] =  date('M-j-Y g:i A', strtotime($cv["updated_at"]));
		$cv["posted"] =  date('M-j-Y g:i A', strtotime($cv["created_at"]));
		
		$stateToWork = CvState::where("cv_id", $cv->id)->get();
		$stateToWork_array = array();

		if($stateToWork != "" && count($stateToWork) > 0)
		{

			foreach ($stateToWork as $key => $state) {
				array_push($stateToWork_array,$state["state"]);
			}
		}

		$cv["stateToWork"] = $stateToWork_array;


		$stateToWork = CvState::where("cv_id", $cv->id)->get();
		$stateToWork_array = array();

		if($stateToWork != "" && count($stateToWork) > 0)
		{
			foreach ($stateToWork as $key => $wanted_state) {
				array_push($stateToWork_array,$wanted_state["state"]);
			}
		}

		$cv["stateToWork"] = $stateToWork_array;

		$anesCountryLicenced = CvCountry::where("cv_id", $cv->id)->get();
		$anesCountryLicenced_array = array();

		if($anesCountryLicenced != "" && count($anesCountryLicenced) > 0)
		{
			foreach ($anesCountryLicenced as $key => $country) {
				array_push($anesCountryLicenced_array,$country["country"]);
			}
		}

		$cv["anesCountryLicenced"] = $anesCountryLicenced_array;
		

		$desired = CvDesiredDuration::where("cv_id" , $cv->id)->first();

		$cv["desired_full_time"] = $desired["desired_full_time"];
		$cv["desired_locum_tenens"] = $desired["desired_locum_tenens"];
		$cv["desired_part_time"] = $desired["desired_part_time"];
		$cv["desired_prn"] = $desired["desired_prn"];
		$cv["desired_fellowship"] = $desired["desired_fellowship"];

		$fellowship = CvFellowship::where("cv_id" , $cv->id)->first();

		$cv["fellowship_pain_management"] = $fellowship["fellowship_pain_management"];
		$cv["fellowhip_cardiothoracic"] = $fellowship["fellowhip_cardiothoracic"];
		$cv["fellowship_obstetric"] = $fellowship["fellowship_obstetric"];
		$cv["fellowship_pediatric"] = $fellowship["fellowship_pediatric"];
		$cv["fellowship_neuro"] = $fellowship["fellowship_neuro"];
		$cv["fellowship_trauma"] = $fellowship["fellowship_trauma"];
		$cv["fellowship_critical_care"] = $fellowship["fellowship_critical_care"];
		$cv["fellowship_outpatient"] = $fellowship["fellowship_outpatient"];
		$cv["fellowship_office_based"] = $fellowship["fellowship_office_based"];
		$cv["fellowship_plastic_surgery"] = $fellowship["fellowship_plastic_surgery"];
		$cv["fellowship_dental_surgery"] = $fellowship["fellowship_dental_surgery"];
		$cv["fellowship_eye_surgery"] = $fellowship["fellowship_eye_surgery"];
		$cv["fellowship_foot_surgery"] = $fellowship["fellowship_foot_surgery"];
		$cv["fellowship_transportation"] = $fellowship["fellowship_transportation"];
		$cv["fellowship_other"] = $fellowship["fellowship_other"];


		$cvCases = CvCases::where("cv_id" , $id)->first();
		$cv["accute_pain_service"]           = $cvCases["accute_pain_service"];
		$cv["anesthesia_out_surgery"]        = $cvCases["anesthesia_out_surgery"];
		$cv["cardiac_anesthesia"]            = $cvCases["cardiac_anesthesia"]; 
		$cv["chronic_pain_man"]              = $cvCases["chronic_pain_man"]; 
		$cv["critical_care_med"]             = $cvCases["critical_care_med"]; 
		$cv["major_vascular_anesthesia"]     = $cvCases["major_vascular_anesthesia"]; 
		$cv["orthopedic_anesthesia"]         = $cvCases["orthopedic_anesthesia"]; 
		$cv["pediatric_anesthesia"]          = $cvCases["pediatric_anesthesia"]; 
		$cv["regional_anesthesia"]           = $cvCases["regional_anesthesia"]; 
		$cv["thoracic_anesthesia"]           = $cvCases["thoracic_anesthesia"]; 
		$cv["trauma_anesthesia"]             = $cvCases["trauma_anesthesia"]; 
		$cv["neuroanesthesia"]               = $cvCases["neuroanesthesia"]; 
		$cv["obstetric_anesthesia"]          = $cvCases["obstetric_anesthesia"]; 


		if($cv["ei_durPosition"] == "Full Time") 
		{
			$cv["full_time_min"] = $cv["min_salary"];
			$cv["full_time_max"] = $cv["max_salary"];
		}
		else if($cv["ei_durPosition"] == "Locum Tenens")
		{
			$cv["locum_min"] = $cv["min_salary"];
			$cv["locum_max"] = $cv["max_salary"];
		}
		else if($cv["ei_durPosition"] == "Part Time")
		{
			$cv["part_time_min"] = $cv["min_salary"];
			$cv["part_time_max"] = $cv["max_salary"];
		}
		else if($cv["ei_durPosition"] == "PRN")
		{
			$cv["prn_min"] = $cv["min_salary"];
			$cv["prn_max"] = $cv["max_salary"];
		}
		else if($cv["ei_durPosition"] == "Fellowship")
		{
			$cv["fellowship_min"] = $cv["min_salary"];
			$cv["fellowship_max"] = $cv["max_salary"];
		}
		


		
    	return $cv;
    }
}
