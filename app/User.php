<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];    
    public function userProfile()
    {
        return $this->hasOne('App\UsersProfile', 'userID');
    }
    public function userAccountTitle(){
        return $this->hasOne('App\UserTitles', 'id','userTitle');
    }
    public function userJobProviderBookmark(){
        return $this->hasOne('App\JobProvider', 'jobProviderId','id');
    }
    public function UserContactList(){
        return $this->hasOne('App\UserContactList', 'user_contact_id', 'id','user_id','id');
    }
    public function bookmark()
    {
        return $this->hasMany('App\JobBookmark', 'userId');
    }

    public function contacts()
    {
        return $this->hasOne('App\UserContactList','user_contact_id');
    }

    public function jobs(){
        return $this->hasMany('App\Jobs', 'userID');
    }

    public function plans(){
        return $this->hasMany('App\PlanUser', 'user_id');
    }
}
