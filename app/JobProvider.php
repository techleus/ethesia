<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobProvider extends Model
{
    protected $table = 'jobs_provider_bookmark';
}
