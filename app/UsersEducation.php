<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsersEducation extends Model
{
    //
    use SoftDeletes;

    protected $table = "users_education";
}
