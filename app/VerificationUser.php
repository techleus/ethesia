<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerificationUser extends Model
{
    protected $table = "user_verification";
}
