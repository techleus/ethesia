<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class postingPlans extends Model
{
    protected $table = "posting_plans";

    public function labels(){
    	return $this->hasMany('App\planLabels', 'label_id');
    }
    public function activeplan(){
    	return $this->hasMany('App\PlanUser', 'plan_id');
    }
}
