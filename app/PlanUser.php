<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanUser extends Model
{
    protected $table = "plan_users";

    public function plan()
    {
    	return $this->belongsTo('App\postingPlans','plan_id');
    }
}
