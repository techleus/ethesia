<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUsMessages extends Model
{
    protected $table = "contact_us_messages";
}
