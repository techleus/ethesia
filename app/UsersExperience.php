<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsersExperience extends Model
{
    //
    use SoftDeletes;
    protected $table = "users_experience";
    
}
