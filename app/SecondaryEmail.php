<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SecondaryEmail extends Model
{
    protected $table = 'secondary_email';
    protected $fillable = ['user_id','secondary_email'];
}
