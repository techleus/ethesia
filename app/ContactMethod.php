<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactMethod extends Model
{
    protected $table = "contact_method";
}
