<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\User;

class MessageSent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $data;
    public $convo_id;
    public $conversation;
    public $message;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data ,$convo_id)
    {
        //
        $this->data = $data;
        $this->message = $data["messages"];
        $this->conversation = $data["conversation"];
        $this->convo_id = $convo_id;
    }


    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('chatroom.'.$this->convo_id);

    }
}
