<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobPostSettings extends Model
{
    protected $table = 'job_post_settings';
    protected $fillable = ['free_posting'];
}
