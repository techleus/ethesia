
app.controller('ParentController', function($q, $scope,$api, $timeout,$rootScope,localStorageService, AunthenticationService,$state,TokenService, $location, $window) {
    
	$scope.header_title = "";
	$scope.header_button_func = "";
	$scope.header_button_label = "";
	$scope.authenticated = false;
	$scope.token;
	$scope.remove = [];
	$scope.loadingPage = true;
    $scope.newMessage = 0;
    $scope.recentConversations = [];
    $scope.notifications = [];
    $scope.current_index = 0;
    $scope.notif_counter = 0;
    $scope.basicInfo = {};
    $scope.socialInfo = {};
    $scope.extraInfo = {};
    console.log(localStorageService.get("profile"))
    $scope.profiletitle = function(){
        if(localStorageService.get("credentials").userType == 2){
            return localStorageService.get("profile").company_name;
        }else{
            return localStorageService.get("profile").firstName + ' ' + localStorageService.get("profile").lastName;
        }
    }

    $scope.testcall = function(){                
        var toogler = $('#nav-toggler').hasClass('ti-menu')        
        if(toogler == true)
        {
            $('#nav-toggler').addClass('ti-menu')
        }else{
            $('#nav-toggler').removeClass('ti-menu')
        }
    }

    $scope.pushNewData = function(data) {
        angular.forEach(data, function(notif) {
            $scope.notifications.push(notif)
        });
    }
    $scope.$watch('online', function(newStatus) { 
        if(newStatus == false && 1 == 2) {
            swal({   
                title: "Connecting...",   
                text: "Please check your internet connection.",   
                showConfirmButton: false 
            });
        }else{
            swal.close();
        }
    });

	$scope.reload_custom = function() {
        
        // $('script[src="js/sidebarmenu.js').remove();
        $('<script>').attr('src', "js/sidebarmenu.js").appendTo('head');

        // $('script[src="js/custom.min.js').remove();
        $('<script>').attr('src', "js/custom.min.js").appendTo('head');

        // $('script[src="assets/plugins/sticky-kit-master/dist/sticky-kit.min.js').remove();
        $('<script>').attr('src', "assets/plugins/sticky-kit-master/dist/sticky-kit.min.js").appendTo('head');

        // console.log("reloaded scripts")
    }

	$rootScope.$on('$stateChangeStart', 
	function(event, toState, toParams, fromState, fromParams){ 
	    $scope.loadingPage = true;
        if(AunthenticationService.isAuthenticated()) {
            //$scope.noActivity();
        }
	});

    $scope.parentTriggger = function() {
        
        $timeout(function() {
            $(window).trigger('resize');
            // console.log("resizing triggered")
        },1200);
        
    }

    $scope.listenNewMessages = function() {
        $scope.Echo.private('newMessage.'+$scope.getUser('id'))
            .listen('NotifyNewMessageUser',function(e){
                // console.log(e)
                counter = 0;
                $scope.audioNewMessage();
                angular.forEach($scope.recentConversations, function(convo, key){
                    // console.log(convo)
                    if(convo.convo_id == e.data.conversation.id) {
                        counter++;
                        $scope.recentConversations[key] = e.recentMessages;
                    }
                });

                if(counter == 0) {
                    $scope.recentConversations.push(e.recentMessages);
                }

                
                $scope.newMessage = $scope.newMessage + 1;
                $scope.$apply();
            });
    }

    $scope.getNotifications = function() {
        var url = '/mailbox/list'
        $api.get(url).then(function(response){
            $scope.notifications = response.data        
            console.log($scope.notifications);
        });
    }

    $scope.showMail = function(mail){
        $state.go('mailbox',{notif_mail : mail});
    }

    $scope.listenNotifications = function() {
        $scope.Echo.private('notifications.'+$scope.getUser('id'))
            .listen('Notifications',function(e){
                // console.log(e)
                $scope.notif_counter = 1;
                $scope.audioNewMessage();
                
             /*   angular.forEach($scope.notifications, function(convo, key){
                    console.log(convo)
                     $scope.notifications[key] = e.data;
                });*/
                if($scope.notif_counter == 0) {
                    $scope.notifications = [];
                    $scope.notifications[0] = e.data;
                    $scope.notif_counter++;
                }else{
                    $scope.notifications.unshift(e.data);
                }
                
                $scope.$apply();
            });
    }

	TokenService.get().then(function(response) {
		$scope.token = response;

        $('meta[name=csrf-token]').attr('content', response.data);

        // $('script[src="assets/plugins/pusher-js/dist/web/pusher.js').remove();
        $('<script>').attr('src', "assets/plugins/pusher-js/dist/web/pusher.js").appendTo('head');

        // $('script[src="assets/plugins/laravel-echo/dist/echo.js').remove();
        $('<script>').attr('src', "assets/plugins/laravel-echo/dist/echo.js").appendTo('head');


        $scope.Echo = new Echo({
            broadcaster: 'pusher',
            key: 'b2c62cb3e6bca24143fa',
            cluster: 'ap1',
            encrypted: true
        });


        $timeout(function(){
            $scope.listenNewMessages();
            $scope.listenNotifications();
        },2000);
        
    
	});

	$scope.goto = function(location) {
		// console.log(location)
		$state.go(location);
	}
	$scope.callFunction = function (fname) {
        if(angular.isFunction($scope[fname]))
           $scope[fname]();
    }

    $scope.isAuthenticated = function() {
    	return $scope.authenticated;
    }
    $scope.logout = function() {

    	notif_warning("Logout", "Logging out.")
    	AunthenticationService.logout().then(function(response) {
    		if(response.data.status) {
    			AunthenticationService.setAuth(false);
				AunthenticationService.setData({});
				localStorageService.remove("credentials");
				$scope.authenticated = false;//parent controller
                //$state.go('login');
                //location.reload();
                $window.location.href = '/';
    		}

            TokenService.get().then(function(response) {
                $scope.token = response;
            });
                
    	});
    }
    $scope.getUserProfile = function(key) {
        var data = localStorageService.get("profile");        
        if(data != null) 
            return data[key];
    }

    $scope.parseUrl = function(path) {
        return path;
    }

    $scope.updateSelectedIndex = function(id) {
        $scope.selected_index = id;
        $scope.$apply();
    }
    
    $scope.getUser = function(key) {
    	var data = localStorageService.get("credentials");        
    	if(data != null) 
    		return data[key];
    }
    
	$scope.getUserProfileDb = function() {
        var url = "/user/getProfile"
        $api.get(url).then(function(response) {
            $scope.basicInfo = response.data;
            $scope.socialInfo = response.data;
            $scope.extraInfo = response.data;
            // console.log($scope.basicInfo)
            $scope.initialization = true;
        });
    }

	$scope.isAdminUser = function() {
    	var data = localStorageService.get("credentials");
		var userAdmin = data.userAdmin;
		 return false;
    }


    $scope.noActivity = function() {
        var timeout = null;

        $(document).on('mousemove', function() {
            clearTimeout(timeout);

            timeout = setTimeout(function() {
                if($scope.authenticated) {
                    $scope.logout();
                    //location.reload();
                    swal("Sorry!", "You have been logout for being idle,please login again.", "warning");
                }
            }, 360000);//3000 for 3 secs if test / or 180000 for 3 mins
        });
    }

    $scope.getConversations = function() {
        var url = "/chat/getConversations";

        $api.get(url).then(function(response) {
            $scope.recentConversations = response.data;

            // console.log($scope.recentConversations)
        });
    }

    $scope.audioNewMessage = function() {
        var audio = new Audio('/application/assets/music/new_message.mp3');
        audio.play();
    };



    $scope.getConversations();
    $scope.reload_custom();
    $scope.parentTriggger();
    $scope.getNotifications();
    $scope.getUserProfileDb();
});

