app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    
    $urlRouterProvider.otherwise('dashboard');
   // $locationProvider.html5Mode(true);

    $stateProvider
        .state('dashboard', {
            url: '/dashboard',
            templateUrl: 'app/pages/dashboard/dashboard.html'+APP_VERSION_URL,
            controller: 'DashboardController'
        })
        
        .state('verification', {
            url: '/verification/:code',
            templateUrl: 'app/pages/dashboard/dashboard.html'+APP_VERSION_URL,
            controller: 'DashboardController',
            params : {
                code : '',
            }
        })

        .state('invites', {
            url: '/invites',
            templateUrl: 'app/pages/invites/invites.html'+APP_VERSION_URL,
            controller: 'InvitesController'
        })

        .state('login', {
            url: '/login',
            templateUrl: 'app/pages/login/login.html'+APP_VERSION_URL,
            controller: 'LoginController'
        })
        .state('register', {
            url: '/register',
            templateUrl: 'app/pages/register/register.html'+APP_VERSION_URL,
            controller: 'RegisterController'
        })
        .state('register_type', {
            url: '/register/user',
            templateUrl: 'app/pages/register/select_type.html'+APP_VERSION_URL,
            controller: 'RegisterController'
        })
        .state('profile', {
            url: '/profile',
            templateUrl: 'app/pages/profile/profile.html'+APP_VERSION_URL,
            controller: 'ProfileController'
        })
        .state('profileEdit', {
            url: '/profileEdit',
            templateUrl: 'app/pages/profile/profileEdit.html'+APP_VERSION_URL,
            controller: 'ProfileEditController'
        })

        .state('subuser', {
            url: '/sub-users',
            templateUrl: 'app/pages/subuser/sub_user.html'+APP_VERSION_URL,
            controller: 'SubUserController'
        })

        .state('notifications', {
            url: '/notifications',
            templateUrl: 'app/pages/notifications/notifications.html'+APP_VERSION_URL,
            controller: 'NotificationsController'
        })

        .state('postsetting', {
            url: '/postsetting',
            templateUrl: 'app/pages/jobpostsetting/jobpostsetting.html'+APP_VERSION_URL,
            controller: 'PostSettingController'
        })

        .state('inqueries', {
            url: '/jobs_list/inqueries/:jobid',
            templateUrl: 'app/pages/inqueries/inqueries.html'+APP_VERSION_URL,
            controller: 'inqueriesController',
            params: {
                jobid: "",  //default value
            }
        })

        .state('respond', {
            url: '/jobs_list/inqueries/:jobid/response/:userid/:fullname',
            templateUrl: 'app/pages/respond/respond.html'+APP_VERSION_URL,
            controller: 'responseController',
            params: {
                jobid: "",  //default value
                userid : "",
                username : "",
                fullname : "",
            }
        })

        .state('preview_notif', {
            url: '/notifications/:notif_id',
            templateUrl: 'app/pages/notifications/notifications_preview.html'+APP_VERSION_URL,
            controller: 'NotificationsController',
            params: {
                notif_id: "",  //default value
            }
        })

        .state('events', {
            url: '/events',
            templateUrl: 'app/pages/events/events.html'+APP_VERSION_URL,
            controller: 'EventsController'
        })


        .state('test2', {
            url: '/test2',
            templateUrl: 'app/pages/test/test.html'+APP_VERSION_URL,            
        })


        .state('credit_cards', {
            url: '/credit_cards',
            templateUrl: 'app/pages/credit_cards/credit_cards.html'+APP_VERSION_URL,
            controller: 'CreditCardController'
        })

        .state('add_credit_card', {
            url: '/credit_cards/new',
            templateUrl: 'app/pages/credit_cards/add_credit_cards.html'+APP_VERSION_URL,
            controller: 'CreditCardController',
            params: {
                card_id: "",  //default value
            }
        })

        .state('edit_card', {
            url: '/credit_cards/:card_id/edit',
            templateUrl: 'app/pages/credit_cards/step2_add_credit_card.html'+APP_VERSION_URL,
            controller: 'CreditCardController',
            params: {
                card_id: "",  //default value
            }
        })

        
        .state('step2_add_credit_card', {
            url: '/credit_cards/setep_2/new',
            templateUrl: 'app/pages/credit_cards/step2_add_credit_card.html'+APP_VERSION_URL,
            controller: 'CreditCardController',
            params: {
                card_id: "",  //default value
            }
        })

        .state('mailbox', {
            url: '/mailbox/:userid/:fullname',
            templateUrl: 'app/pages/mailbox/mailbox.html'+APP_VERSION_URL,
            controller: 'mailboxController',
            params : {
                mailboxid : 1,
                mode : 1,
                compose : 1,
                userid : "user",
                fullname : "new",
                notif_mail : "",
            }            
        })

        .state('mail', {
            url: '/mailbox/mail/:mailid/:recipient_id/:recipient_email/:recipient_username/:message/:subject/:attachments/:fullname',
            templateUrl: 'app/pages/mail/mail.html'+APP_VERSION_URL,
            controller: 'mailController',
            params: {
                mailid : "",
                recipient_id : "",
                recipient_email : "",
                recipient_username : "",
                message : "",
                subject : "",
                attachments : "",
                fullname : "",
            }            
        })

        .state('sentmail', {
            url: '/sentmail/mail/:mailid/:recipient_id/:recipient_email/:recipient_username/:message/:subject/:attachments',
            templateUrl: 'app/pages/sentmail/sentmail.html'+APP_VERSION_URL,
            controller: 'sentmailController',
            params: {
                mailid : "",
                recipient_id : "",
                recipient_email : "",
                recipient_username : "",
                message : "",
                subject : "",
                attachments : "",
            }            
        })


        .state('e_checks', {
            url: '/electronic-checks',
            templateUrl: 'app/pages/credit_cards/e_checks.html'+APP_VERSION_URL,
            controller: 'CreditCardController'
        })

        .state('edit_check', {
            url: '/e_checks/:check_id/edit',
            templateUrl: 'app/pages/credit_cards/step2_add_electronic_checks.html'+APP_VERSION_URL,
            controller: 'CreditCardController',
            params: {
                check_id: "",  //default value
            }
        })
        .state('step2_add_echecks', {
            url: '/echecks/new',
            templateUrl: 'app/pages/credit_cards/step2_add_electronic_checks.html'+APP_VERSION_URL,
            controller: 'CreditCardController',
            params: {
                check_id: "",  //default value
            }
        })

        /*Jobs*/
        .state('job_post_type', {
            url: '/jobs/post/select',
            templateUrl: 'app/pages/jobs/job_post_select.html'+APP_VERSION_URL,
            controller: 'JobsController'
        })
        .state('add_job', {
            url: '/jobs/new',
            templateUrl: 'app/pages/jobs/jobs_form.html'+APP_VERSION_URL,
            controller: 'JobsController',
            params: {
                jobPostType: "",  //default value
                jobTitle: "",
            }
        })
		.state('jobs', {
            url: '/jobs',
            templateUrl: 'app/pages/jobs/jobs.html',
            controller: 'JobsController'
        })
		.state('jobs_search', {
            url: '/jobs_search',
            templateUrl: 'app/pages/jobs/jobs_search.html'+APP_VERSION_URL,
            controller: 'JobsController'
        })
		.state('jobs_list', {
            url: '/jobs_list',
            templateUrl: 'app/pages/jobs/jobs_list.html'+APP_VERSION_URL,
            controller: 'JobsController',
            params: {
                post_job: false,  //default value
            }

        })
		.state('jobs_details', {
            url: '/jobs_details',
            templateUrl: 'app/pages/jobs/jobs_details.html'+APP_VERSION_URL,
            controller: 'JobsController'
        })

        .state('preview_job', {
            url: '/job/:job_id/preview',
            templateUrl: 'app/pages/jobs/jobs_preview.html'+APP_VERSION_URL,
            controller: 'JobsController',
            params: {
                job_id: "",  //default value
            }
        })

        .state('edit_job', {
            url: '/job/:job_id/newjob',
            templateUrl: 'app/pages/jobs/jobs_form.html'+APP_VERSION_URL,
            controller: 'JobsController',
            params: {
                job_id: "",  //default value
            }
        })

        .state('new_job', {
            url: '/newjob',
            templateUrl: 'app/pages/jobs/jobs_new.html'+APP_VERSION_URL,
            controller: 'JobsController',
            params: {
                job_id: "",  //default value
            }
        })
        .state('test', {
            url: '/test',
            templateUrl: 'app/pages/test/test.html'+APP_VERSION_URL,
            controller: 'TestController'
        })

        /*plans*/

        .state('plan_list', {
            url: '/plans',
            templateUrl: 'app/pages/plans/plan_list.html'+APP_VERSION_URL,
            controller: 'PlansController',
            params: {
                job_id: "",  //default value
            }
        })

        .state('update_plan', {
            url: '/plans/update',
            templateUrl: 'app/pages/plans/plans.html'+APP_VERSION_URL,
            controller: 'PlansController',
            params: {
                job_id: "",  //default value
            }
        })



        /*bookmarks*/
        .state('bookmarks', {
            url: '/bookmarks',
            templateUrl: 'app/pages/bookmarks/bookmarks.html'+APP_VERSION_URL,
            controller: 'BookmarksController',
        })


        //resume builder


        .state('resume_cv', {
            url: '/resume/',
            templateUrl: 'app/pages/resume_builder/resume_builder.html'+APP_VERSION_URL,
            controller: 'ResumeController',
            params: {
                resume_id: "",  //default value
            }
        })







        /*cv*/
        .state('add_cv', {
            url: '/cv/add',
            templateUrl: 'app/pages/candidate/add_cv.html'+APP_VERSION_URL,
            controller: 'CandidateController',
            params: {
                cv_id: "",  //default value
            }
        })
		.state('list_cv', {
            url: '/cv/list',
            templateUrl: 'app/pages/candidate/list_cv.html'+APP_VERSION_URL,
            controller: 'CandidateController',
            params: {
                cv_id: "",  //default value
            }
        })

        .state('edit_cv', {
            url: '/cv/:cv_id/edit',
            templateUrl: 'app/pages/candidate/add_cv.html'+APP_VERSION_URL,
            controller: 'CandidateController',
            params: {
                cv_id: "",  //default value
            }
        })



		/*Admin Manage*/
		.state('manage_jobs', {
            url: '/manage/jobs',
            templateUrl: 'app/pages/manage_jobs/jobs.html'+APP_VERSION_URL,
            controller: 'ManageJobsController'
        })
		 .state('manage_preview_job', {
            url: '/manage/:job_id/preview_job',
            templateUrl: 'app/pages/manage_jobs/jobs_preview.html'+APP_VERSION_URL,
            controller: 'ManageJobsController',
            params: {
                job_id: "",  //default value
            }
        })
        .state('manage_edit_job', {
            url: '/manage/:job_id/edit_job',
            templateUrl: 'app/pages/manage_jobs/jobs_form.html'+APP_VERSION_URL,
            controller: 'ManageJobsController',
            params: {
                job_id: "",  //default value
            }
        })
		 .state('manage_delete_job', {
            url: '/manage/:job_id/delete_job',
            templateUrl: 'app/pages/manage_jobs/jobs_delete.html'+APP_VERSION_URL,
            controller: 'ManageJobsController',
            params: {
                job_id: "",  //default value
            }
        })
		
		.state('manage_cv', {
            url: '/manage_cv',
            templateUrl: 'app/pages/manage/manage_cv.html'+APP_VERSION_URL,
            controller: 'ManageController'
        })
		.state('manage_accounts', {
            url: '/manage_accounts',
            templateUrl: 'app/pages/manage/manage_accounts.html'+APP_VERSION_URL,
            controller: 'ManageController'
        })		

		
        .state('404', {
            url: '/404',
            templateUrl: 'app/pages/404/404.html'+APP_VERSION_URL,
            controller: 'PageNotFoundController'
        })







        /*chat*/

        .state('chat', {
            url: '/chat/',
            templateUrl: 'app/pages/chat/chat.html'+APP_VERSION_URL,
            controller: 'ChatController',
            params: {
                convo_id: "",  //default value
            }
        })

  
});



app.filter('capfirst', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});


app.filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});

app.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      var keys = Object.keys(props);

      items.forEach(function(item) {
        var itemMatches = false;

        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});


app.run(['$rootScope', '$state',function($rootScope, $state){
  $rootScope.$on('$stateChangeStart',function(){
      $rootScope.stateIsLoading = true;
 });


}]);

app.run(function($window, $rootScope) {
      $rootScope.online = navigator.onLine;
      $window.addEventListener("offline", function() {
        $rootScope.$apply(function() {
          $rootScope.online = false;
        });
      }, false);

      $window.addEventListener("online", function() {
        $rootScope.$apply(function() {
          $rootScope.online = true;
        });
      }, false);
});



//after
app.run(function ($state , $rootScope, $location, $timeout, $api, AunthenticationService, localStorageService ,$window) {    
    $rootScope.$on("$locationChangeSuccess", function (event, next, current) {
        //ask the service to check if the user is in fact logged in
        if($state.current.name != "" && $state.current.name != "login" && $state.current.name != "register" && $state.current.name != "register_type") {
            var url = "/user/check/login"
            $api.get(url).then(function(response) {
                if(response.data.status == false) {
                    AunthenticationService.setAuth(false);
                    AunthenticationService.setData({});
                    localStorageService.remove("credentials");
                    $window.location.href = '/'; 
                }
            });
        }else{
            if(localStorageService.get("credentials") == null) {
                AunthenticationService.logout();
            }
        }

    });
});

app.filter('stringJobType',
[ '$filter', '$locale', function(filter, locale) {

    var currencyFilter = filter('currency');
    var formats = locale.NUMBER_FORMATS;

    return function(param, currencySymbol) {    

      
        if(param==1){
            return 'Anesthesiologist';
        }else{
            return 'CRNA';  
        }
    }  
 
} ]);

app.filter('to_trusted', ['$sce', function($sce){
    return function(text) {
         var doc = new DOMParser().parseFromString(text, "text/html");
         var   rval= doc.documentElement.textContent;
        return $sce.trustAsHtml(rval)
    };
}]);