var app = angular.module('app', ['ngMask', 'slick','jq-multi-select','datatables','angular-loading-bar' ,'ui.router','LocalStorageModule', 'content-editable']);

var base_url = "/";
var APP_VERSION_URL = "?VERSION=1.0-F";
app.config(function (localStorageServiceProvider) {
  localStorageServiceProvider
    .setPrefix('anesthesia');
});

app.run(['$rootScope', '$state', '$stateParams',
  function ($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
}])
