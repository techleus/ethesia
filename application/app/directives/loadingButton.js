app.directive('selectedSubuser',function($compile, $http){
  return {
    link: function ($scope, element, attrs) {
      console.log($scope.jobsData.sub_user_id)
      console.log(attrs.value)

      if($scope.jobsData.sub_user_id == attrs.value){
        $(element).val(attrs.value)
      }else if($scope.jobsData.sub_user_id == null || $scope.jobsData.sub_user_id == undefined){

      }
    }
  }
})

app.directive('sliderShowmore',function($compile, $http){
  return {
    link: function ($scope, element, attrs) {
      $(element).click(function(){        
        if($(this).text() == 'show more'){
          $('.'+attrs.targetClass).addClass('show')
          $(this).text('show less')
        }else{
          $('.'+attrs.targetClass).removeClass('show')
          $(this).text('show more')
        }        
      })
    }
  }
})

app.directive('switchSetting',function($compile, $http){
  return {
    link: function ($scope, element, attrs) {
      $(element).click(function(){        
        var data = {
          column : attrs.id,
          setting : $(this).is(":checked"),
        }
        console.log(data)        
        $http.post('/emails/change_notif_settings',data).then(function(response){
          console.log(response.data)          
        })
      })
    }
  }
})

app.directive('freePosting',function ($compile, $http) {
  return {
    link: function ($scope, element, attrs) {      
      
      $http.get('/manage/freeposting').then(function(response){
        // console.log(response.data)
        if(response.data == 1){
          $(element).prop('checked',true)
        }else{
          $(element).prop('checked',false)
        }
      })

      $(element).change(function(){
        var enable;
        if($(this).is(':checked') == true){
          enable = 1;
        }else{
          enable = 0;
        }
        var url = '/manage/disablefreeposting';
        var data = { enable : enable }
        
        $http.post(url,data).then(function(response){
          if(response.status == 200){
            swal('success!',response.data.freeposting,'success')
          }
        })
      })
       
    }
  }
})

app.directive('mailTo',function ($compile) {
  return {
    link: function ($scope, element, attrs) {      
      
      $(element).on('input',function(){        
        // console.log($(this).val())        
      })
       
    }
  }
})

app.directive('keyEnter',function ($compile) {
  return {
    link: function ($scope, element, attrs) {      
      // console.log()
      element.bind("keydown keypress", function (event) {
          if (event.which === 13) {
              $scope.$apply(function () {
                  scope.$eval(attrs.ngEnter);
              });
              event.preventDefault();
          }
      });
       
    }
  }
})


app.directive('creditcardMasking',function($compile, $http){
  return {
    link: function ($scope, element, attrs) {
        console.log(element[0])


        var name = document.getElementById('name');
        var cardnumber = document.getElementById('cardnumber');
        var securitycode = document.getElementById('securitycode');
        var output = document.getElementById('output');
        var ccicon = document.getElementById('ccicon');
        var ccsingle = document.getElementById('ccsingle');
        var generatecard = document.getElementById('generatecard');


        var cardnumber_mask = new IMask(element[0], {
            mask: [
                {
                    mask: '0000 000000 00000',
                    regex: '^3[47]\\d{0,13}',
                    cardtype: 'american express'
                },
                {
                    mask: '0000 0000 0000 0000',
                    regex: '^(?:6011|65\\d{0,2}|64[4-9]\\d?)\\d{0,12}',
                    cardtype: 'discover'
                },
                {
                    mask: '0000 000000 0000',
                    regex: '^3(?:0([0-5]|9)|[689]\\d?)\\d{0,11}',
                    cardtype: 'diners'
                },
                {
                    mask: '0000 0000 0000 0000',
                    regex: '^(5[1-5]\\d{0,2}|22[2-9]\\d{0,1}|2[3-7]\\d{0,2})\\d{0,12}',
                    cardtype: 'mastercard'
                },
                // {
                //     mask: '0000-0000-0000-0000',
                //     regex: '^(5019|4175|4571)\\d{0,12}',
                //     cardtype: 'dankort'
                // },
                // {
                //     mask: '0000-0000-0000-0000',
                //     regex: '^63[7-9]\\d{0,13}',
                //     cardtype: 'instapayment'
                // },
                {
                    mask: '0000 000000 00000',
                    regex: '^(?:2131|1800)\\d{0,11}',
                    cardtype: 'jcb15'
                },
                {
                    mask: '0000 0000 0000 0000',
                    regex: '^(?:35\\d{0,2})\\d{0,12}',
                    cardtype: 'jcb'
                },
                {
                    mask: '0000 0000 0000 0000',
                    regex: '^(?:5[0678]\\d{0,2}|6304|67\\d{0,2})\\d{0,12}',
                    cardtype: 'maestro'
                },
                // {
                //     mask: '0000-0000-0000-0000',
                //     regex: '^220[0-4]\\d{0,12}',
                //     cardtype: 'mir'
                // },
                {
                    mask: '0000 0000 0000 0000',
                    regex: '^4\\d{0,15}',
                    cardtype: 'visa'
                },
                {
                    mask: '0000 0000 0000 0000',
                    regex: '^62\\d{0,14}',
                    cardtype: 'unionpay'
                },
                {
                    mask: '0000 0000 0000 0000',
                    cardtype: 'Unknown'
                }
            ],
            dispatch: function (appended, dynamicMasked) {
                var number = (dynamicMasked.value + appended).replace(/\D/g, '');

                for (var i = 0; i < dynamicMasked.compiledMasks.length; i++) {
                    let re = new RegExp(dynamicMasked.compiledMasks[i].regex);
                    if (number.match(re) != null) {
                        return dynamicMasked.compiledMasks[i];
                    }
                }
            }
        });

      swapColor = function (basecolor) {
          document.querySelectorAll('.lightcolor')
              .forEach(function (input) {
                  input.setAttribute('class', '');
                  input.setAttribute('class', 'lightcolor ' + basecolor);
              });
          document.querySelectorAll('.darkcolor')
              .forEach(function (input) {
                  input.setAttribute('class', '');
                  input.setAttribute('class', 'darkcolor ' + basecolor + 'dark');
              });
      };


      cardnumber_mask.on("accept", function () {
      console.log(cardnumber_mask.masked.currentMask.cardtype);
      switch (cardnumber_mask.masked.currentMask.cardtype) {
          case 'american express':
              ccicon.innerHTML = amex;
              ccsingle.innerHTML = amex_single;
              swapColor('green');
              break;
          case 'visa':
              ccicon.innerHTML = visa;
              ccsingle.innerHTML = visa_single;
              swapColor('lime');
              break;
          case 'diners':
              ccicon.innerHTML = diners;
              ccsingle.innerHTML = diners_single;
              swapColor('orange');
              break;
          case 'discover':
              ccicon.innerHTML = discover;
              ccsingle.innerHTML = discover_single;
              swapColor('purple');
              break;
          case ('jcb' || 'jcb15'):
              ccicon.innerHTML = jcb;
              ccsingle.innerHTML = jcb_single;
              swapColor('red');
              break;
          case 'maestro':
              ccicon.innerHTML = maestro;
              ccsingle.innerHTML = maestro_single;
              swapColor('yellow');
              break;
          case 'mastercard':
              ccicon.innerHTML = mastercard;
              ccsingle.innerHTML = mastercard_single;
              swapColor('lightblue');

              break;
          case 'unionpay':
              ccicon.innerHTML = unionpay;
              ccsingle.innerHTML = unionpay_single;
              swapColor('cyan');
              break;
          default:
              ccicon.innerHTML = '';
              ccsingle.innerHTML = '';
              swapColor('grey');
              break;
      }

  });
    }
  }
})

app.directive('select2',function ($compile) {
  return {
    link: function ($scope, element, attrs) {      
      
      $(element).select2()
       
    }
  }
})


app.directive('btnCompose',function ($compile) {
  return {
    link: function ($scope, element, attrs) {      
      var me = $scope
      $(element).click(function(){
        if(me.compose == 2){
          return $(this).text('Back to list')
        }        
        return $(this).text('Compose')
      })   
    }
  }
})


app.directive('selectMail',function ($compile) {
  return {
    link: function ($scope, element, attrs) {      
      $(element).click(function(){
        // $scope.selected
        // console.log(element.mailId)
      })   
    }
  }
})

app.directive('checkStorage', function ($timeout, $compile) {
  return {
    link : function ($scope, element, attrs) {
      // console.log(attrs.storageName)
      $(element).click(function(){
        //alert('test')
      })
    }
  };
});

app.directive('loadingButton', function ($compile) {
    return {
        link: function ($scope, element, attrs) { 
          
        	var form_name = attrs.formName;
        	$(element).click(function() {
            loadingStop = attrs.stopLoading;

            if(loadingStop == "true") {
              return;
            }

       			if(form_name != undefined && form_name != "") {
              if($scope[form_name].$invalid) {
                  // console.log($scope[form_name].$error.required)
                  angular.forEach($scope[form_name].$error.required, function(field) {
                      field.$setPristine();
                      field.$dirty = true;
                  });
                  $scope.$apply();
                  //$.notify("Some fields are invalid or required, pls check.", "warning");
                  return true;
                }
            }
       			
       			$scope.$apply();
        		$scope.activeButton = Ladda.create(element[0]);
				    $scope.activeButton.start();
				    $scope.$eval(attrs.callback);
        	});
           	
        }
    };
});

app.directive('closeRegister', function ($timeout, $compile) {
  return {
    link : function ($scope, element, attrs) {
      $(element).click(function(e) {
        if($(element).hasClass("close-register-form")){                
          $('#close-slider-form').trigger('click')
        }
      });      
    }
  };
});

app.directive('calendar', function ($timeout, $compile) {
  return {
    link : function ($scope, element, attrs) {
        
        !function($) {
            "use strict";

            var CalendarApp = function() {
                this.$body = $("body")
                this.$calendar = $('#calendar'),
                this.$event = ('#calendar-events div.calendar-events'),
                this.$categoryForm = $('#add-new-event form'),
                this.$extEvents = $('#calendar-events'),
                this.$modal = $('#my-event'),
                this.$saveCategoryBtn = $('.save-category'),
                this.$calendarObj = null
            };


            /* on drop */
            CalendarApp.prototype.onDrop = function (eventObj, date) { 
                var $this = this;
                    // console.log(date.format("D/M/Y H:m:s"))
                    // console.log(date.time())
                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = eventObj.data('eventObject');
                    var $categoryClass = eventObj.attr('data-class');
                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject);
                    // assign it the date that was reported
                    copiedEventObject.start = date;
                    if ($categoryClass)
                        copiedEventObject['className'] = [$categoryClass];
                    // render the event on the calendar
                    $this.$calendar.fullCalendar('renderEvent', copiedEventObject, true);
                    // is the "remove after drop" checkbox checked?
                    if ($('#drop-remove').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                        eventObj.remove();
                    }
            },
            /* on click on event */
            CalendarApp.prototype.onEventClick =  function (calEvent, jsEvent, view) {
                var $this = this;
                    var form = $("<form></form>");
                    form.append("<label>Change event name</label>");
                    form.append("<div class='input-group'><input class='form-control' type=text value='" + calEvent.title + "' /><span class='input-group-btn'><button type='submit' class='btn btn-success waves-effect waves-light'><i class='fa fa-check'></i> Save</button></span></div>");
                    $this.$modal.modal({
                        backdrop: 'static'
                    });
                    $this.$modal.find('.delete-event').show().end().find('.save-event').hide().end().find('.modal-body').empty().prepend(form).end().find('.delete-event').unbind('click').click(function () {
                        $this.$calendarObj.fullCalendar('removeEvents', function (ev) {
                            return (ev._id == calEvent._id);
                        });
                        $this.$modal.modal('hide');
                    });
                    $this.$modal.find('form').on('submit', function () {
                        calEvent.title = form.find("input[type=text]").val();
                        $this.$calendarObj.fullCalendar('updateEvent', calEvent);
                        $this.$modal.modal('hide');
                        return false;
                    });
            },
            /* on select */
            CalendarApp.prototype.onSelect = function (start, end, allDay) {
                var $this = this;
                    $this.$modal.modal({
                        backdrop: 'static'
                    });
                    var form = $("<form></form>");
                    form.append("<div class='row'></div>");
                    form.find(".row")
                        .append("<div class='col-md-6'><div class='form-group'><label class='control-label'>Event Name</label><input class='form-control' placeholder='Insert Event Name' type='text' name='title'/></div></div>")
                        .append("<div class='col-md-6'><div class='form-group'><label class='control-label'>Category</label><select class='form-control' name='category'></select></div></div>")
                        .find("select[name='category']")
                        .append("<option value='bg-danger'>Danger</option>")
                        .append("<option value='bg-success'>Success</option>")
                        .append("<option value='bg-purple'>Purple</option>")
                        .append("<option value='bg-primary'>Primary</option>")
                        .append("<option value='bg-pink'>Pink</option>")
                        .append("<option value='bg-info'>Info</option>")
                        .append("<option value='bg-warning'>Warning</option></div></div>");
                    $this.$modal.find('.delete-event').hide().end().find('.save-event').show().end().find('.modal-body').empty().prepend(form).end().find('.save-event').unbind('click').click(function () {
                        form.submit();
                    });
                    $this.$modal.find('form').on('submit', function () {
                        var title = form.find("input[name='title']").val();
                        var beginning = form.find("input[name='beginning']").val();
                        var ending = form.find("input[name='ending']").val();
                        var categoryClass = form.find("select[name='category'] option:checked").val();
                        if (title !== null && title.length != 0) {
                            $this.$calendarObj.fullCalendar('renderEvent', {
                                title: title,
                                start:start,
                                end: end,
                                allDay: false,
                                className: categoryClass
                            }, true);  
                            $this.$modal.modal('hide');
                        }
                        else{
                            alert('You have to give a title to your event');
                        }
                        return false;
                        
                    });
                    $this.$calendarObj.fullCalendar('unselect');
            },
            CalendarApp.prototype.enableDrag = function() {
                //init events
                $(this.$event).each(function () {
                    // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                    // it doesn't need to have a start or end
                    var eventObject = {
                        title: $.trim($(this).text()) // use the element's text as the event title
                    };
                    // store the Event Object in the DOM element so we can get to it later
                    $(this).data('eventObject', eventObject);
                    // make the event draggable using jQuery UI
                    $(this).draggable({
                        zIndex: 999,
                        revert: true,      // will cause the event to go back to its
                        revertDuration: 0  //  original position after the drag
                    });
                });
            }
            /* Initializing */
            CalendarApp.prototype.init = function() {
                this.enableDrag();
                /*  Initialize the calendar  */
                var date = new Date();
                var d = date.getDate();
                var m = date.getMonth();
                var y = date.getFullYear();
                var form = '';
                var today = new Date($.now());

                var defaultEvents =  [{
                        title: 'Released Ample Admin!',
                        start: new Date($.now()),
                        className: 'bg-info'
                    }];

                var $this = this;
                $this.$calendarObj = $this.$calendar.fullCalendar({
                    slotDuration: '00:15:00', /* If we want to split day time each 15minutes */
                    minTime: '08:00:00',
                    maxTime: '19:00:00',  
                    defaultView: 'month',  
                    handleWindowResize: true,   
                     
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'
                    },
                    events: defaultEvents,
                    editable: true,
                    droppable: true, // this allows things to be dropped onto the calendar !!!
                    eventLimit: true, // allow "more" link when too many events
                    selectable: true,
                    drop: function(date) { $this.onDrop($(this), date); },
                    select: function (start, end, allDay) { $this.onSelect(start, end, allDay); },
                    eventClick: function(calEvent, jsEvent, view) { $this.onEventClick(calEvent, jsEvent, view); }

                });

                //on new event
                this.$saveCategoryBtn.on('click', function(){
                    var categoryName = $this.$categoryForm.find("input[name='category-name']").val();
                    var categoryColor = $this.$categoryForm.find("select[name='category-color']").val();
                    if (categoryName !== null && categoryName.length != 0) {
                        $this.$extEvents.append('<div class="calendar-events bg-' + categoryColor + '" data-class="bg-' + categoryColor + '" style="position: relative;"><i class="fa fa-move"></i>' + categoryName + '</div>')
                        $this.enableDrag();
                    }

                });
            },

           //init CalendarApp
            $.CalendarApp = new CalendarApp, $.CalendarApp.Constructor = CalendarApp
            
        }(window.jQuery),

        //initializing CalendarApp
        function($) {
            "use strict";
            $.CalendarApp.init()
        }(window.jQuery);



    }
  };
});




app.directive('searchContacts', function ($compile, $timeout) {
    return {
        link: function ($scope, element, attrs) { 

          var timeoutObj = {};
          var typing = false;

          $(document).click(function(e){
            if(!$(e.target).hasClass(".search-contacts")) {
              $scope.search_contacts = {};
              $scope.$apply();
            }
          });

          $(element).keyup(function() {

            if(typing == true) {
              return;
            }

            typing = true;

            timeoutObj = $timeout(function() {
              // console.log("sending")
              $scope.searchContacts($scope.searchKey);
              typing = false;
            },1300);
              
            
          });
        }
    };
});


app.directive('enterChat', function ($compile, TokenService ) {
    return {
        link: function ($scope, element, attrs) { 
            
         /*   $(element).keydown(function(e) {

              if (e.ctrlKey) {
                return;
              }

              if(e.which == 13) {
                $scope.sendMessage();
              }
            });*/

            $(element).keydown(function (e) {
                if (e.keyCode === 13 && e.ctrlKey) {
                    $(this).val(function(i,val){
                        return val + "\n";
                    });
                }
            }).keypress(function(e){
                if (e.keyCode === 13 && !e.ctrlKey) {
                    $scope.sendMessage();
                    return false;  
                } 
            });



        }
    };
});



/*app.directive('selectPicker', function ($compile, TokenService ) {
    return {
        link: function ($scope, element, attrs) { 
            
            $(element).selectpicker();

        }
    };
});*/


app.directive('tagsInput', function ($compile) {
    return {
        link: function ($scope, element, attrs) {
          $(element).textext({
              plugins : 'autocomplete filter  ajax',
              ajax : {
                  url : '/searchCity',
                  dataType : 'json',
                  cacheResults : false,
                  'dataCallback' : function(query)
                  {
                      return { 'search' : query , "states" : $scope.getSelectedStates()};
                  } 
              }
          });

          $(document).on("click", ".text-suggestion" , function() {
            
            $scope.jobsData.ei_facilityCity = $("#facility-city").val();
          
          });

          $(element).css("width" , "100%");
          $(element).parents(".text-wrap").css("width" , "100%");

        }
    }
});





app.directive('googleApi', function ($compile) {
    return {
      restrict: 'A',
        link: function ($scope, element, attrs) {

          var form_field = attrs.googleApi;
          var form = $(element).parents("form");
          var form_name = $(form[0]).attr("name");
          var override = attrs.changeOther;
          var populate_fields = attrs.populate;
          populate_fields = populate_fields.split(',');

        //city = locality
        //administrative_area_level_1 = state
        //country
        //xip = postal_code
        var field_ids = {};
        var placeSearch, autocomplete;
        var componentForm = {
        //street_number: 'short_name',
        //route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      field_ids["locality"] = populate_fields[0];
      field_ids["administrative_area_level_1"] = populate_fields[1];
      field_ids["postal_code"] = populate_fields[2];
      

      function initAutocomplete(element) {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        element = element[0];

        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(element),
            {types: ['geocode']});
        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function resetFormModels(form) {
        $(form).find("input").each(function() {

          var model_src = $(this).attr("ng-model");
          
          if(model_src != "" && model_src != undefined) {
            var model = model_src.split('.');
            if(model[1] != "id") {
              // console.log(model)  
              $scope[model[0]][model[1]] = $(this).val(); 
              // console.log($scope[model[0]])
              $scope.$apply();
            }
            
          }
        })
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.

          var form_array = form_field.split('.');
          $scope.$apply(function() {
            $scope[form_array[0]][form_array[1]] = $(element).val();
          });
        $scope.infoData.address =  $(element).val();
        var place = autocomplete.getPlace();

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        $scope.$apply( function(){
          // console.log(place)
          // console.log(place)
          for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {

                var val = place.address_components[i][componentForm[addressType]];
                // console.log(addressType)
                // console.log(val)
                if(addressType == "locality"){//city
                    $scope.basicInfo["city"] = val;
                } 
                if(addressType == "administrative_area_level_1"){//city
                    $scope.basicInfo["state"] = val;
                } 

                if(addressType == "country"){//city
                    $scope.basicInfo["country"] = val;
                } 
                if(addressType == "country"){//city
                    $scope.basicInfo["country"] = val;
                } 
            }
          }
        
        });
        
          
        
        
        

      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {

        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }

      $(element).focusin(function(){
        initAutocomplete(element);
        geolocate();
      });
        }
    };
});


app.directive('planAddress', function ($compile) {
    return {
      restrict: 'A',
        link: function ($scope, element, attrs) {

          var form_field = attrs.planAddress;
          var form = $(element).parents("form");
          var form_name = $(form[0]).attr("name");
          var override = attrs.changeOther;
          var populate_fields = attrs.populate;
          populate_fields = populate_fields.split(',');

        //city = locality
        //administrative_area_level_1 = state
        //country
        //xip = postal_code
        var field_ids = {};
        var placeSearch, autocomplete;
        var componentForm = {
        //street_number: 'short_name',
        //route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      field_ids["locality"] = populate_fields[0];
      field_ids["administrative_area_level_1"] = populate_fields[1];
      field_ids["postal_code"] = populate_fields[2];
      

      function initAutocomplete(element) {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        element = element[0];

        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(element),
            {types: ['geocode']});
        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function resetFormModels(form) {
        $(form).find("input").each(function() {

          var model_src = $(this).attr("ng-model");
          
          if(model_src != "" && model_src != undefined) {
            var model = model_src.split('.');
            if(model[1] != "id") {
              // console.log(model)  
              $scope[model[0]][model[1]] = $(this).val(); 
              // console.log($scope[model[0]])
              $scope.$apply();
            }
            
          }
        })
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.

        $scope.plansData.billData.address =  $(element).val();
        var place = autocomplete.getPlace();

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        $scope.$apply( function(){
          // console.log(place)
          // console.log(place)
          for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {

                var val = place.address_components[i][componentForm[addressType]];
                // console.log(addressType)
                // console.log(val)
               /* if(addressType == "locality"){//city
                    $scope.plansData.billData.city = val;
                } 
                if(addressType == "administrative_area_level_1"){//city
                    $scope.plansData.billData.state = val;
                } 

                if(addressType == "country"){//city
                    $scope.plansData.billData.country = val;
                } 
                if(addressType == "country"){//city
                    $scope.plansData.billData.country = val;
                } 

                $scope.$apply();*/
            }
          }
        
        });
        
          
        
        
        

      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {

        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }

      $(element).focusin(function(){
        initAutocomplete(element);
        geolocate();
      });
        }
    };
});



app.directive('paymentAddress', function ($compile) {
    return {
      restrict: 'A',
        link: function ($scope, element, attrs) {

          var form_field = attrs.paymentAddress;
          var form = $(element).parents("form");
          var form_name = $(form[0]).attr("name");
          var override = attrs.changeOther;
          var populate_fields = attrs.populate;
          populate_fields = populate_fields.split(',');

        //city = locality
        //administrative_area_level_1 = state
        //country
        //xip = postal_code
        var field_ids = {};
        var placeSearch, autocomplete;
        var componentForm = {
        //street_number: 'short_name',
        //route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      field_ids["locality"] = populate_fields[0];
      field_ids["administrative_area_level_1"] = populate_fields[1];
      field_ids["postal_code"] = populate_fields[2];
      

      function initAutocomplete(element) {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        element = element[0];

        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(element),
            {types: ['geocode']});
        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function resetFormModels(form) {
        $(form).find("input").each(function() {

          var model_src = $(this).attr("ng-model");
          
          if(model_src != "" && model_src != undefined) {
            var model = model_src.split('.');
            if(model[1] != "id") {
              // console.log(model)  
              $scope[model[0]][model[1]] = $(this).val(); 
              // console.log($scope[model[0]])
              $scope.$apply();
            }
            
          }
        })
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.

          var form_array = form_field.split('.');
          $scope.$apply(function() {
            $scope[form_array[0]][form_array[1]] = $(element).val();
          });

        $scope.billData.address =  $(element).val();
        var place = autocomplete.getPlace();

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        $scope.$apply( function(){
          // console.log(place)
          // console.log(place)
          for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {

                var val = place.address_components[i][componentForm[addressType]];
                // console.log(addressType)
                // console.log(val)
                if(addressType == "locality"){//city
                    $scope.billData["city"] = val;
                } 
                if(addressType == "administrative_area_level_1"){//city
                    $scope.billData["state"] = val;
                } 

                if(addressType == "country"){//city
                    $scope.billData["country"] = val;
                } 
                if(addressType == "country"){//city
                    $scope.billData["country"] = val;
                } 
            }
          }
        
        });
        
          
        
        
        

      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {

        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }

      $(element).focusin(function(){
        initAutocomplete(element);
        geolocate();
      });
        }
    };
});





(function($){
    $.fn.setCursorToTextEnd = function() {
        var $initialVal = this.html();
        this.html($initialVal);
    };
})(jQuery);


app.directive('phoneMask', function ($compile) {
    return {
      restrict: 'A',
        link: function (scope, element, attrs) {
          $(element).mask('(000) 000-0000');
        }
    };
});



app.directive('jobpostButton', function ($compile) {
    return {
        link: function ($scope, element, attrs) { 
          
          var form_name = attrs.formName;
          $(element).click(function() {

            if(form_name != undefined && form_name != "") {
              if($scope[form_name].$invalid) {
                  // console.log($scope[form_name].$error.required)
                  angular.forEach($scope[form_name].$error.required, function(field) {
                      field.$setPristine();
                      field.$dirty = true;
                  });


                  if($("input.ng-invalid").length > 0) {
                    var invalid_input = $("input.ng-invalid");
                  }

                  if($("select.ng-invalid").length > 0) {
                    var invalid_input = $("select.ng-invalid");
                  }

                  if($("textarea.ng-invalid").length > 0) {
                    var invalid_input = $("textarea.ng-invalid");
                  }


                  
                 /* console.log(invalid_input)
                  */


                  $('#registration-form').animate({
                      scrollTop: $(invalid_input[0]).parents(".section").position().top + 50
                  }, 300);

                  $(invalid_input[0]).focus();


                  $scope.$apply();
                  //$.notify("Some fields are invalid or required, pls check.", "warning");
                  return true;
                }
            }
            
            $scope.$apply();
            $scope.activeButton = Ladda.create(element[0]);
            $scope.activeButton.start();
            $scope.$eval(attrs.callback);
          });
            
        }
    };
});


app.directive('closeSlider', function ($compile, $timeout) {
    return {
        link: function ($scope, element, attrs) {
          $(element).click(function() {
            $(".slider-form-close").trigger("click");
          })
        }
    }
});

app.directive('rdJobsteps', function ($compile, $timeout) {
    return {
        link: function ($scope, element, attrs) {
            
            var stepkey = attrs.stepKey;  
            var w = $("#new-job-slider");
            var first_load = 0;

            $("#new-job-slider").find(".section").each(function(i, obj) {
                var counter = i + 1;
                $(obj).attr("id" , "jobsform_"+counter);
                $(obj).attr("step" , counter);
            });


            function scrollEvent() {

                var timeout;
                //test
                $("#new-job-slider").scroll(function(e) {
                  
                  $('.section').each(function(i, obj) {
                      var offset = $(obj).offset();
                      var y_position = offset.top;
                      if(y_position > 20 && y_position < 350) {
                        
                        // $(".section").addClass("slider-faded");
                        // $(obj).removeClass("slider-faded");
                      }
                  });
                });

           
            }
         
            $scope.$watch("rdSteps['"+stepkey+"']['current_step']", function (newValue, oldValue, $scope) {
                // console.log("#"+stepkey+"_"+$scope.rdSteps['jobsform']['current_step'])
                $("#new-job-slider").animate({
                    // scrollTop: $("#"+stepkey+"_"+$scope.rdSteps['jobsform']['current_step']).position().top() + 50
                }, 300);

                $timeout(function() {
                  
                  // console.log("#"+stepkey+"_"+$scope.rdSteps['jobsform']['current_step'])
                  if($("#"+stepkey+"_"+$scope.rdSteps['jobsform']['current_step']).find("input").length > 0) {
                    $("#"+stepkey+"_"+$scope.rdSteps['jobsform']['current_step']).find("input").focus();
                  }
                  if($("#"+stepkey+"_"+$scope.rdSteps['jobsform']['current_step']).find("textarea").length > 0) {
                    $("#"+stepkey+"_"+$scope.rdSteps['jobsform']['current_step']).find("textarea").focus();
                  }
                },400);

                $timeout(function() {
                  scrollEvent()
                },1000);
            });


            $scope.$watch("jobsData.jobPostType", function (newValue, oldValue, $scope) {
                
                first_load++;
                if(first_load == 1) {
                  return;
                }
                // console.log("#"+stepkey+"_"+$scope.rdSteps['jobsform']['current_step'])
                $("#new-job-slider").animate({
                    scrollTop: $("#"+stepkey+"_"+2).position().top + 50
                }, 300);

                $timeout(function() {
                  
                  $("#"+stepkey+"_"+2).find("input").focus();
                  
          

                },400);

                $timeout(function() {
                  scrollEvent()
                },1000);
            });

        
            scrollEvent();
        } 
    };
});


app.directive('dropifyCustom', function ($compile) {
  return {
        link: function (scope, element, attrs) {
          $(element).dropify();
        }
  }
});
app.directive('coverLetter', function ($compile) {
    return {
        link: function (scope, element, attrs) {
            var id = $(element).attr("id");

            console.log(id)
              
            function populateTiny(inst) {
              scope.$watch('extraInfo.coverLetter', function ($value, $old) {
                if($value != null){
                  tinyMCE.get($(element).attr("id")).setContent($value);
                  tinyMCE.execCommand('mceRepaint');
                }                
              });
            }



            tinymce.init({
                  selector: "textarea#"+id,
                  theme: "modern",
                  height: 300,
                  mode : "specific_textareas",
                  setup : function(ed) {
                        ed.on('keydown', function(e) {
                           cambios = true;
                           scope.extraInfo.coverLetter = tinymce.get(id).getContent();
                        });

                        ed.on('init', function() {
                          populateTiny()
                        });
                  },
                  plugins: [
                      "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                      "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                      "save table contextmenu directionality emoticons template paste textcolor"
                  ],
                  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",

          });

        }
    };
});

app.directive('mce', function ($compile) {
    return {
        link: function (scope, element, attrs) {
            var id = $(element).attr("id");

              
            function populateTiny(inst) {
              scope.$watch('basicInfo.description', function ($value, $old) {
                tinyMCE.get(id).setContent($value);
                tinyMCE.execCommand('mceRepaint');
              });
            }



            tinymce.init({
                  selector: "textarea#"+id,
                  theme: "modern",
                  height: 300,
                  mode : "specific_textareas",
                  setup : function(ed) {
                        ed.on('keydown', function(e) {
                           cambios = true;
                           scope.basicInfo.description = tinymce.get(id).getContent();
                        });

                        ed.on('init', function() {
                          populateTiny()
                        });
                  },
                  plugins: [
                      "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                      "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                      "save table contextmenu directionality emoticons template paste textcolor"
                  ],
                  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",

          });

            
        }
    };
});

app.directive('showSlider', function ($timeout, $compile) {
    return {
        link: function ($scope, element, attrs) {
          if(attrs.sliderWidth != undefined && attrs.sliderWidth != "") {
              $("#"+attrs.idTarget).css("width", attrs.sliderWidth);
              // console.log($("#"+attrs.idTarget).find(".slider-form"))
          }
          

          $(element).click(function(e) { 

              var class_target = attrs.class_target;
              var id_target = attrs.idTarget;
              // console.log($(e.target).parents(".no-slide").length)
              if($(e.target).hasClass("no-slide") || $(e.target).parents(".no-slide").length > 0) {
                return;
              }
              
              $timeout(function() {
                  if($(element).hasClass("close-prev-slider")){
                    $('#close-slider-form').trigger('click')
                  }

                  if($(element).hasClass("set-appform-jobid")){                
                    $('#btn-apply').data('value',$(element).data('value'))                
                    $('#btn-apply').data('id',$(element).data('id')) 
                  }

                  if($(element).hasClass("close-prev-slider")){                
                    $scope.selected_job_id = $(element).data("value")
                    $scope.save_job_id = $(element).data("value")
                    //$scope.inquireMessage = 'Hi {company_name},\n\tI noticed your job #'+$(element).data("value")+' and would like to know more about the position. We can discuss any details over chat.'               
                  }              

                  var right = $("#"+id_target).width()

                  $("#"+id_target).css("right","0%");
                  $(".slider-form-overlay").removeClass("hidden");
                  $(".slider-form-close").removeClass("hidden")
                  document.body.style.overflow = "hidden";
                  var id = attrs.showSlider;
                  $scope.updateSelectedIndex(id);
                  console.log($scope.jobs[id])
              }, 400)
                  
              
          });
          $(".slider-form-close").click(function() {

              var class_target = attrs.class_target;
              var id_target = attrs.idTarget;


              $("#"+id_target).css("right","-100%");
              $(".slider-form-overlay").addClass("hidden");
              $(".slider-form-close").addClass("hidden");
              document.body.style.overflow = "";
          });
          $(".slider-form-overlay").click(function() {

              var class_target = attrs.class_target;
              var id_target = attrs.idTarget;

              $("#"+id_target).css("right","-100%");
              $(".slider-form-overlay").addClass("hidden");
              $(".slider-form-close").addClass("hidden");
              document.body.style.overflow = "";
          });

          $(document).keyup(function(e) {
            var class_target = attrs.class_target;
            var id_target = attrs.idTarget;
            if (e.keyCode == 27) { // escape key maps to keycode `27`
               $("#"+id_target).css("right","-100%");
               $(".slider-form-overlay").addClass("hidden")
               document.body.style.overflow = "";
            }
          });
        }
    };
});




app.directive('datePicker', function ($compile) {
    return {
        link: function (scope, element, attrs) {
          $(element).datepicker();
          $(element).on('changeDate', function(ev){
              $(this).datepicker('hide');
          });
        }
    };
});


app.directive('customMask', function ($compile) {
    return {
      restrict: 'A',
        link: function (scope, element, attrs) {
          $(element).mask(attrs.customMask);
        }
    };
});

app.directive('noEnter', function ($compile) {
    return {
        link: function (scope, element, attrs) {
          $(element).keypress(function(event){
              if (event.which === 13) {
                  event.preventDefault();
                  return;
              }
          });
        }
    };
});

app.directive('customScrollbar', function ($compile) {
    return {
        link: function (scope, element, attrs) {
          //$(element).customScrollbar();
        }
    };
});


app.directive('inlineSave', function ($compile) {
    return {
        link: function ($scope, element, attrs) {
            var $key = attrs.key;
            var $parent = attrs.parent;
            var $model = attrs.ngModel;
            var $callback = attrs.callback;
            var timeout;

            
            if($scope[$parent] == "{}" || $scope[$parent] == "" || $scope[$parent] == undefined) {
              return;
            }

            $scope.$watch('initialization', function ($value, $old) {
                if($scope.initialization == true) {//starts watching changes
                    $scope.$watch($model, function ($value, $old) {
                      // console.log($value != $old)
                      if($value != $old) {
                        window.clearTimeout(timeout);
                          timeout = setTimeout(function(){ 
                            $scope[$callback]($key)
                          }, 1000);
                      }
                    });
                }
            });
          
            
        }
    };
});

app.directive('droppyDown', function ($compile, $timeout) {
    return {
        link: function ($scope, element, attrs) {
            var target = $("#"+attrs.droppyDown);
            $(element).focus( function () {
              // console.log($(target))
              target.removeClass("hidden");
            })

            $(element).blur( function () {
              $timeout(function() {
                target.addClass("hidden");
              },300);
              
            })
        }
    };
});


app.directive('triggerClick', function ($compile) {
    return {
        link: function ($scope, element, attrs) {
            $(document).ready(function() {
                $(element).trigger("click")
            })
            
        }
    };
});

app.directive('sweetDelete', function ($compile) {
    return {
        link: function ($scope, element, attrs) {
            
            $(element).click(function(){
              var $callback = attrs.callback;
              var $url = attrs.url;
              swal({   
                  title: "Are you sure?",   
                  text: "this will remove the data from the database.",   
                  type: "warning",   
                  showCancelButton: true,   
                  confirmButtonColor: "#DD6B55",   
                  confirmButtonText: "Yes",   
                  cancelButtonText: "Cancel!",   
                  closeOnConfirm: true,   
                  closeOnCancel: true 
              }, function(isConfirm){   
                  if (isConfirm) {     
                      $scope[$callback]($url);
                  } else {     
                      
                  } 
              });
            });
        }
    };
});



app.directive('sweetAlertConfirm', function ($compile) {
    return {
        link: function ($scope, element, attrs) {
            
            $(element).click(function(){
              var $callback = attrs.callback;
              var $url = attrs.url;
              swal({   
                  title: "Are you sure?",   
                  text: attrs.sweetAlertMessage,   
                  type: attrs.sweetAlertType,   
                  showCancelButton: true,   
                  confirmButtonColor: "#DD6B55",   
                  confirmButtonText: "Yes",   
                  cancelButtonText: "Cancel!",   
                  closeOnConfirm: true,   
                  closeOnCancel: true 
              }, function(isConfirm){   
                  if (isConfirm) {     
                      $scope[$callback]($url);
                  } else {     
                      
                  } 
              });
            });
        }
    };
});


app.directive('reactivateConfirm', function ($compile) {
    return {
        link: function ($scope, element, attrs) {
            
            $(element).click(function(){
              var $callback = attrs.callback;
              var $url = attrs.url;
              swal({   
                  title: "Are you sure?",   
                  text: "This will reactivate the current Job, Do you wish to proceed?.",   
                  type: "warning",   
                  showCancelButton: true,   
                  confirmButtonColor: "#DD6B55",   
                  confirmButtonText: "Yes",   
                  cancelButtonText: "Cancel!",   
                  closeOnConfirm: false,   
                  closeOnCancel: true 
              }, function(isConfirm){   
                  if (isConfirm) {     
                      $scope[$callback]($url);
                  } else {     
                      
                  } 
              });
            });
        }
    };
});



app.directive('toggleSidebar', function ($compile, $timeout) {
    return {
        link: function ($scope, element, attrs) {
            /*$timeout(function() {
              $(".sidebartoggler").trigger("click")
            },1300)*/
            
            // console.log($(".sidebartoggler"))
        }
    };
});

app.directive('triggerProfile', function ($compile, $timeout) {
    return {
        link: function ($scope, element, attrs) {
          $(element).click(function() {
            $("#profilePhoto").trigger("click");
          })
        }
    };
});



app.directive('croppie', function ($compile, $timeout) {
    return {
        link: function ($scope, element, attrs) {

          function upload() {
              var $uploadCrop;

              function readFile(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    
                    reader.onload = function (e) {
                      $('.upload-demo').addClass('ready');
                      $uploadCrop.croppie('bind', {
                        url: e.target.result
                      }).then(function(resp){
                        // console.log(resp);
                        $('#imagebase64').val(resp);
                      });
                      
                    }
                    reader.readAsDataURL(input.files[0]);
                  }
                  else {
                    swal("Sorry - you're browser doesn't support the FileReader API");
                }
              }

              $uploadCrop = $('#upload-demo').croppie({
                viewport: {
                  width: 250,
                  height: 250,
                  type: 'circle'
                },
                enableExif: true
              });

              $('#profilePhoto').on('change', function () { readFile(this); });
              
              $('.upload-result').on('click', function (ev) {
                $uploadCrop.croppie('result', {
                  type: 'blob',
                  size: 'viewport'
                }).then(function (resp) {
                  // console.log(resp)
                  $scope.saveProfilePhoto(resp);
                  /*popupResult({
                    src: resp
                  });*/
                });
              });
          }

          upload();
            
        }
    };
});



app.directive('deactivateConfirm', function ($compile) {
    return {
        link: function ($scope, element, attrs) {
           
            $(element).click(function(){

            var $callback = attrs.callback;
            var $url = attrs.url;
              swal({   
                  title: "Are you sure?",   
                  text: "This will deactivate the current Job, Do you wish to proceed?.",   
                  type: "warning",   
                  showCancelButton: true,   
                  confirmButtonColor: "#DD6B55",   
                  confirmButtonText: "Yes",   
                  cancelButtonText: "Cancel!",   
                  closeOnConfirm: false,   
                  closeOnCancel: true 
              }, function(isConfirm){   
                  if (isConfirm) {     
                      $scope[$callback]($url);
                  } else {     
                      
                  } 
              });
            });
        }
    };
});

app.directive('deleteConfirm', function ($compile) {
    return {
        link: function ($scope, element, attrs) {
            var $callback = attrs.callback;
            var $url = attrs.url;
            $(element).click(function(){
              swal({   
                  title: "Are you sure?",   
                  text: "This will delete the current Job, Do you wish to proceed?.",   
                  type: "warning",   
                  showCancelButton: true,   
                  confirmButtonColor: "#DD6B55",   
                  confirmButtonText: "Yes",   
                  cancelButtonText: "Cancel!",   
                  closeOnConfirm: false,   
                  closeOnCancel: true 
              }, function(isConfirm){   
                  if (isConfirm) {     
                      $scope[$callback]($url);
                  } else {     
                      
                  } 
              });
            });
        }
    };
});


app.directive('wizard', function ($compile) {
    return {
        scope: {
          stepChanging: '=',
           submitJobPost: '&'
        },
        compile: function(element, attr) {
          var form = $(element).show();
          element.steps({
            headerTag: "h6"
                  , bodyTag: "section"
                  , transitionEffect: "fade"
                  , titleTemplate: '<span class="step">#index#</span> #title#'
                  , labels: {
                      finish: "Submit"
                  }
                  , onStepChanging: function (event, currentIndex, newIndex) {
                      if((("true" == $("#isThereSelectedPlan").val()) == false) && currentIndex == 0) {
                          swal("Sorry!", "Please select a plan first.", "warning");
                          return ("true" == $("#isThereSelectedPlan").val());
                      }

                      return true;
                      
                      
                      //return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
                  }
                  , onFinishing: function (event, currentIndex) {

                      $("#postCreditCardBtn").trigger("click")
                      return false;
                      return form.validate().settings.ignore = ":disabled", form.valid()
                  }
                  , onFinished: function (event, currentIndex) {
                      $("#postJobButton").trigger("click");

                  }
        });
          $(".validation-wizard").validate({
                      ignore: "input[type=hidden]"
                      , errorClass: "text-danger"
                      , successClass: "text-success"
                      , highlight: function (element, errorClass) {
                          $(element).removeClass(errorClass)
                      }
                      , unhighlight: function (element, errorClass) {
                          $(element).removeClass(errorClass)
                      }
                      , errorPlacement: function (error, element) {
                          error.insertAfter(element)
                      }
                      , onFinishing: function (error, element) {
                      }


                      , rules: {
                          email: {
                              email: !0
                          }
                      }
              });
        return {
          link: function ($scope, element, attrs) {
                
                $(element).on('submit', function(e) { //use on if jQuery 1.7+
                    alert("submitting")
                });
               
              }
          }
        }
      }
});



app.directive('dropify', function ($compile) {
    return {
        link: function ($scope, element, attrs) {
            
            $(element).dropify();
            // Translated
            $('.dropify-fr').dropify({
              messages: {
                  default: 'Glissez-déposez un fichier ici ou cliquez',
                  replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                  remove: 'Supprimer',
                  error: 'Désolé, le fichier trop volumineux'
              }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element) {
              return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element) {
              alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element) {
              // console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e) {
              e.preventDefault();
              if (drDestroy.isDropified()) {
                  drDestroy.destroy();
              } else {
                  drDestroy.init();
              }
            })
    

        }
      }
});


app.directive('socialText',function ($compile) {
    return {
        link: function ($scope, element, attrs) {                    
          var placeHolder = '';                    
          $(element).on('input',function(){             
            if(placeHolder != attrs.placeholder && $(this).val() != ""){                                        
              placeHolder = attrs.placeholder;              
              var numOfCharSet = attrs.placeholder.length              
              if($(this).val().length == 1){                
                $(element).val(attrs.placeholder + $(this).val());              
              }else{                
                $(element).val(attrs.placeholder + $(this).val().substring(numOfCharSet));                              
              }                                        
            }else{                            
              var numOfCharSet = attrs.placeholder.length              
              $(element).val(attrs.placeholder + $(this).val().substring(numOfCharSet));                                        
            }                                    
          });
        }
    };
});

app.directive('tabSwitch',function ($compile) {
    return {
        link: function ($scope, element, attrs) {                    
          $(element).click(function(){            
            if(attrs.tabNum == 1){                  
              // console.log($scope.billData)
              if($scope.billData.cardNumber != "" && $scope.billData.firstName != "" && $scope.billData.lastName != "" && $scope.billData.expMonth != "" && $scope.billData.expYear != "" && $scope.billData.cardIDNumber != ""){
                // console.log(attrs.tabNum)          
                $('#billing-method').removeClass('active')
                $('#billing-address').addClass('active')  
              }else{
                swal('Sorry!','Please complete all the details needed before you continue','info')
              }              
            }else{
              $('#billing-method').addClass('active')
              $('#billing-address').removeClass('active')
            }
          })
        }
    };
});

app.directive('countContactLabel',function ($compile) {
    return {
        link: function ($scope, element, attrs) {                                      

          $scope.$watch("contactObj", function(newVal, oldVal) {
            var url = "/contact/countcountacts";
            var label_id = attrs.contactId          
            var data = [];
                data['label_id'] = label_id;
            $.get(url,{label_id : label_id}).then(function(response) {
              // console.log(response)                                   
              $(element).text(response)
            });
          });

        }
    };
});

app.directive('newpostNotification',function ($compile) {
    return {
        link: function ($scope, element, attrs) {                                      

          if(attrs.allow == "Yes"){
            $(element).prop('checked', true);
          }


          $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });

          $(element).change(function(){
            
            var elementBackStatus;
            var elementUpdateStatus;

            if( $(element).is(':checked') == true ){
              elementBackStatus = false;
              elementUpdateStatus = "Yes"                                
            }else{
              elementBackStatus = true;
              elementUpdateStatus = "No"                                
            }

            var url = '/contact/up_notif_settings';
              
            var data = {
              contactId : attrs.contactId,
              allowNotif : elementUpdateStatus,
            }

            $.post(url, data).then(function(response){
              notif_success("Contact Notification settings changed to " + elementUpdateStatus, "saved.")
            }).catch(function(fallback) {
              $(element).prop('checked', elementBackStatus);
            });
          
          })

        }
    };
});


app.directive('showModalUpgrade',function ($compile) {
  return {
    link: function ($scope, element, attrs) {
      $(element).click(function(){
        var selected = $scope.checkSelectedJobs();
        if(selected) {
            // console.log($scope.getSelectedJobsId())
            $('#modal-prior').modal('show')
        }else{
          swal("Sorry!","Please select jobs to upgrade","warning")
        }

      })
        }
    }
  });


app.directive('upgradeSwitch',function ($compile) {
  return {
    link: function ($scope, element, attrs) {
      $(element).click(function(){
        if(attrs.tabindex == 1){
          if( $('input[name="priority"]').is(':checked') ){            
            $('#priority-posting').removeClass('active')
            $('#billing-method').removeClass('active')
            $('#select-cards').addClass('active')
          }else{
            swal("Sorry!","Please select Priority Posting","info")
          }           
        }else if(attrs.tabindex == 2){          
          if($('#select-job-card').val() == 1){
            $('#priority-posting').removeClass('active')          
            $('#select-cards').removeClass('active')
            $('#billing-address').removeClass('active')
            $('#billing-method').addClass('active')

            $('#modal-prior .modal-dialog').removeClass('modal-lg')
          }else if($('#select-job-card').val() == 2){

          }else{
            swal("Sorry!","Please Select Card First","info")
          }          
        }else if(attrs.tabindex == 3){          
          if($('#select-job-card').val() == 1){            
            if($scope.billData.cardNumber != "" && 
              $scope.billData.firstName != "" && 
              $scope.billData.lastName != "" && 
              $scope.billData.expMonth != "" && 
              $scope.billData.expYear != "" && 
              $scope.billData.cardIDNumber != "")
            {
              $('#priority-posting').removeClass('active')          
              $('#select-cards').removeClass('active')          
              $('#billing-method').removeClass('active')
              $('#billing-address').addClass('active')
              $('#modal-prior .modal-dialog').removeClass('modal-lg')
              
            }else{
              swal('Sorry!','Please complete all details first','info')
            }    
            
          }else if($('#select-job-card').val() == 2){

          }else{
            swal("Sorry!","Please Select Card First","info")
          }          
        }        
      })
    }
  }
})

app.directive('upgradeSwitchBack',function ($compile) {
  return {
    link: function ($scope, element, attrs) {
      $(element).click(function(){
        if(attrs.tabindex == 2){          
          $('#select-cards').removeClass('active')
          $('#billing-method').removeClass('active')
          $('#billing-address').removeClass('active')
          $('#priority-posting').addClass('active')
          $('#modal-prior .modal-dialog').addClass('modal-lg')
        }else if(attrs.tabindex == 3){          
          $('#priority-posting').removeClass('active')          
          $('#billing-method').removeClass('active')
          $('#billing-address').removeClass('active')
          $('#select-cards').addClass('active')          
          $('#modal-prior .modal-dialog').removeClass('modal-lg')
        }else if(attrs.tabindex == 4){          
          $('#select-cards').removeClass('active')          
          $('#priority-posting').removeClass('active')                    
          $('#billing-address').removeClass('active')
          $('#billing-method').addClass('active')
          $('#modal-prior .modal-dialog').removeClass('modal-lg')
        }          
      })
    }
  }
})

app.directive('selectJobCard',function ($compile) {
  return {
    link: function ($scope, element, attrs) {
      $(element).change(function(){
          if($(this).val() == 2){          
            $('#job-card-old').addClass('active');
          }else{            
            $('#job-card-old').removeClass('active');   
          }
      })
    }
  }
})

app.directive('planId',function ($compile) {
  return {
    link: function ($scope, element, attrs) {      
      if(attrs.idNum == 1){
        $(element).attr('id','tall');
      }else if(attrs.idNum == 2){
        $(element).attr('id','grande');
      }else if(attrs.idNum == 1){
        $(element).attr('id','verti');
      }      
    }
  }
})
