app.controller('JobsController', function($q, $scope, $timeout,$stateParams,$window, $api,JobsService ,localStorageService, $state, AunthenticationService,$http) {

	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	}


    /*
    var deferred = $q.defer();
    deferred.resolve();*/
    $scope.jobSearch = {};
	$scope.primary_status = "active";
	$scope.job_id = $stateParams.job_id;
    $scope.post_job = $stateParams.post_job;
    $scope.jobPostType = $stateParams.jobPostType;
	$scope.type_cases = [];
	$scope.type_cases_options = [];
	$scope.active_job_accordion = "headingOne1";
	$scope.jobs = {};
    $scope.jobsData = {};
    $scope.searchData = {};
    $scope.billData = {};
    $scope.plansData = {};
    $scope.viewSwitch = "cards";
    $scope.upgradeData = {};
    $scope.rdSteps = [];
    $scope.rdSteps['jobsform'] = [];
    $scope.rdSteps['jobsform']['current_step'] = 0;
    $scope.billData.cardNumber = "";
    $scope.billData.firstName = "";
    $scope.billData.lastName = "";
    $scope.billData.expMonth = "";
    $scope.billData.expYear = "";
    $scope.billData.cardIDNumber = "";

    if($scope.jobPostType == "") {
        //$state.go('job_post_type');
    }    

    $scope.getSubUsers = function(){
        var url = "/contact/showUserContactList";
        $api.get(url).then(function(response) {                         
            console.log(response.data)           
            $scope.subusers = response.data;                                
        });
    }

    $scope.fileChanged = function(element){
        $scope.file = element.files
        $scope.$apply();        
        // console.log($scope.file)
    }

    $scope.jobinqueries = function(jobid){
        $state.go('inqueries',{ jobid : jobid });
    }

    $scope.importjobs = function(){
        $http({
            method : "POST",
            url : '/jobupload',            
            headers: { 'Content-Type': undefined},
            transformRequest: function(data) {
                var formData = new FormData();
                formData.append("file", $scope.file[0]);
                return formData;
            },
        }).then(function mySuccess(response) {

            if(response.data.import == "failed"){
                $scope.importerrors = response.data.errors
                angular.element("input[name='file']").val(null);    
            }else{
                // console.log(response.data)            
                $scope.getJobs();                        
                swal('Well done!',"Importing mass job done","success")            
            }            
        }, function myError(response) {
            // console.log(response)
        });
    }

    $scope.showBillingDetail = function() {

        var total_selected_jobs = 0;
        var job_arrays = [];
        var total_price = 0;

        if($scope.upgradeData.priority == undefined) {
            return;
        }
        var choosen_price = JSON.parse($scope.upgradeData.priority).amount;
        
        angular.forEach($scope.jobs, function(job) {
            if(job.selected == true) {
                job_arrays.push(job);
            }
        });

        total_selected_jobs = job_arrays.length;
        total_price = parseInt(choosen_price) * parseInt(total_selected_jobs);
        $scope.temp_price = total_price;
        return total_selected_jobs+" Jobs x $" +choosen_price+ "= $"+total_price.toFixed(2);

    }    

    $scope.checkSelectedJobs = function() {
        var count = 0;
        angular.forEach($scope.jobs, function(job) {
            if(job.selected == true) {
                count++;
            }
        });

        if(count > 0) {
            return true;
        }else{
            return false;
        }
    }

    $scope.selectJob = function(index) {
        $scope.jobs[index].selected = !$scope.jobs[index].selected;
    }

    $scope.repostSelected = function(){

        // console.log($scope.jobs)
        var data = [];
            data.selected = [];

        angular.forEach($scope.jobs, function(value, key) {
            if(value.selected != undefined && value.selected != false){
                // console.log(value.id + ': ' + value.selected);
                data.selected.push({ jobid : value.id })
            }            
        });

        if(data.selected.length == 0){                
            // console.log(data.selected.length)
            swal("Error","Please Make sure to select a job first","info")
            return;
        }
        
        swal({
            title: "Confirm Reposting",
            text: "Selected jobs will be reposted as new",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Repost it as new!",
            closeOnConfirm: true
        },
        function(isConfirm){            
            var url = "/jobs/repost"
            $api.post(data, url).then(function(response){
                if(response.data.repost == true){
                    $scope.getJobs();
                    swal("success","selected jobs are now reposted as new","success")
                }
            })
        });         
    }

    $scope.getSelectedJobsId = function() {
        // console.log($scope.jobs)
        var selected_jobs = [];
        angular.forEach($scope.jobs, function(job) {
            if(job.selected == true) {
                selected_jobs.push(job.id)
            }
        });

        return selected_jobs;
    }
    $scope.resetselected = function(jobid){
        angular.forEach($scope.jobs, function(job) {
            job.selected = false;
        });
        var index = $scope.jobs.findIndex( job => job.id == jobid );        
        $scope.jobs[index].selected = true
    }
    $scope.upgradeselectedjob = function(jobid){
        
        $scope.resetselected(jobid)
        $('#modal-prior').modal('show')
        $('.slider-form-close').trigger('click')
    }
    
    $scope.repostselectedjob = function(jobid){
        $scope.resetselected(jobid)
        $scope.repostSelected()
        $('#modal-prior').modal('show')
        $('.slider-form-close').trigger('click')
    }
    $scope.deleteselectedjob = function(jobid){
        $scope.resetselected(jobid)
        $scope.proceedFunc('delete_job')
        $('#modal-prior').modal('show')
        $('.slider-form-close').trigger('click')
    }
    $scope.callUpgradeFunc = function() {
        if($scope.plansData.selected_payment_method == 1) {
            return;
        }
        if($scope.plansData.existing_card_id == undefined) {
            $scope.activeButton.stop();
            swal("Sorry!", "Select existing cards first.", "warning");
            return;
        }
        $scope.saveUpgradeBill();
    }

    $scope.switchView = function() {
        if($scope.viewSwitch == "cards") {
            $scope.viewSwitch = "list";
        }else{
            $scope.viewSwitch = "cards";
        }
    }

    $scope.saveUpgradeBill = function() {

        var data = [];

        

        $scope.billData.price = $scope.temp_price;
        data["billData"] = $scope.billData;
        data["plansData"] = $scope.plansData;
        data["jobs"] = $scope.getSelectedJobsId();
        data["priority"] = JSON.parse($scope.upgradeData.priority).id;

        var url = "/jobs/upgrade";


        if($scope.plansData.selected_payment_method == 1) {
            if($scope.billData.firstName == "" || $scope.billData.lastName == undefined) {
                swal("Sorry!", "Card first name or last name is required.", "warning");
                $scope.activeButton.stop();
                return;
            }
            if($scope.billData.cardNumber == "" || $scope.billData.cardNumber == undefined) {
                if($scope.billData.id == "") {
                    swal("Sorry!", "Card Number is required.", "warning");
                    $scope.activeButton.stop();
                    return;
                }
            }
            if($scope.billData.expMonth == "" || $scope.billData.expYear == undefined) {
                swal("Sorry!", "Expiration month and year is required.", "warning");
                $scope.activeButton.stop();
                return;
            }
            if($scope.billData.cardIDNumber == "" || $scope.billData.cardIDNumber == undefined) {
                if($scope.billData.id == "") {
                    swal("Sorry!", "Card Id Number is required.", "warning");
                    $scope.activeButton.stop();
                    return;
                }
                
            }
        }
        

        $api.post(data, url).then(function(response) {
            // console.log(response)
            if(response.status != 500) {
                swal("Good job!", response.data.message, "success");
                $scope.activeButton.stop();
                $scope.getJobs($scope.selected_status);
            }else{
                swal("Sorry!", response.data.message, "warning");
                $scope.activeButton.stop();
                $scope.getJobs($scope.selected_status);
            }            
        })

    }

    $scope.getExpYears = function() { 
        var year = 2018;
        var array_year = [];
        for(year; year <= 2035; year = year + 1) {
            array_year.push(year);
        }

        return array_year;
    }

    $scope.getOldCards = function() {
        var url = "/plans/getOldCards"
        $api.get(url).then(function(response) {
            $scope.old_cards = response.data;
        });
    }

    $scope.changeSelectedStatus = function() {
        if($scope.selection.select_all) {
            angular.forEach($scope.jobs, function(job) {
                job.selected = true;
            });
        }else{
            angular.forEach($scope.jobs, function(job) {
                job.selected = false;
            });
        }
    }
    $scope.limitTitle = function(title,description){
        
        var titleCharSet = title.length;
        var maxTitleCharSet = 150;      
        
        if(Number(titleCharSet) <= Number(maxTitleCharSet)){
            var charsetToAdd = maxTitleCharSet - titleCharSet;          
            return title + ", " + description.substring(0,charsetToAdd) + " . . .";
        }else{
            return title;
        }
    }
    $scope.checkProceed = function(process_stat) {
        var count = 0;
        angular.forEach($scope.jobs, function(job) {
            if(job.selected) {
                count++;
            }
        });
        // console.log(count)

        if (count > 0 && process_stat != "") {
            return true;
        }

        return false;
    }

    $scope.sweetConfirm = function(callback) {
        var $callback = callback;
        swal({   
          title: "Are you sure?",   
          text: "Proceed by clicking yes.",   
          type: "warning",   
          showCancelButton: true,   
          confirmButtonColor: "#DD6B55",   
          confirmButtonText: "Yes",   
          cancelButtonText: "Cancel!",   
          closeOnConfirm: true,   
          closeOnCancel: true 
        }, function(isConfirm){   
          if (isConfirm) {     
              $scope[$callback]();
          } else {     
              
          } 
        });
     
    }

    $scope.proceedFunc = function(process_stat) {
        if(!$scope.checkProceed(process_stat)) {
            swal("Sorry!", "Please select the action or select atleast one job to proceed.", "warning");
            return;
        }

        



        if(process_stat == "deactivate_job") {
            $scope.sweetConfirm('multiDeactivateJob');
        }else if(process_stat == "activate_job") {
            $scope.sweetConfirm('multiRestoreJob');
        }else if(process_stat == "delete_job") {
            $scope.sweetConfirm('multiDeleteJob');
        }

    }
    $scope.multiDeleteJob = function() {
        swal({   
            title: "Wait for a while.",   
            text: "Deleting job.",   
            showConfirmButton: false 
        });

        var todeactivate_jobs = [];

        angular.forEach($scope.jobs, function(job) {
            if(job.selected) {
                todeactivate_jobs.push(job.id);
            }
        });

        var url = "/jobs/multiple-delete";
        var data = todeactivate_jobs;


        $api.post(data, url).then(function(response) {
            swal("Good job!", response.data.message, "success");
            $scope.getJobs($scope.selected_status);
        });
    }

    $scope.multiRestoreJob = function() {
        swal({   
            title: "Wait for a while.",   
            text: "Restoring job.",   
            showConfirmButton: false 
        });

        var todeactivate_jobs = [];

        angular.forEach($scope.jobs, function(job) {
            if(job.selected) {
                todeactivate_jobs.push(job.id);
            }
        });

        var url = "/jobs/multiple-restore";
        var data = todeactivate_jobs;


        $api.post(data, url).then(function(response) {
            swal("Good job!", response.data.message, "success");
            $scope.getJobs($scope.selected_status);
        });
    }

    $scope.multiDeactivateJob = function() {
        swal({   
            title: "Wait for a while.",   
            text: "Deactivating job.",   
            showConfirmButton: false 
        });

        var todeactivate_jobs = [];

        angular.forEach($scope.jobs, function(job) {
            if(job.selected) {
                todeactivate_jobs.push(job.id);
            }
        });

        var url = "/jobs/multiple-deactivate";
        var data = todeactivate_jobs;


        $api.post(data, url).then(function(response) {
            swal("Good job!", response.data.message, "success");
            $scope.getJobs($scope.selected_status);
        });
    }

    $scope.proceedToStep = function($event) {
        var step = $($event.currentTarget).parents(".section").attr("step");
        step = parseInt(step) + 1;
        $("#new-job-slider").animate({
            scrollTop: $("#jobsform_"+step).position().top + 50
        }, 300);


        $timeout(function() {
            // console.log("jobsform_"+step)
          if($("#jobsform_"+step).find("input").length > 0) {
            $("#jobsform_"+step).find("input").focus();
          }
          if($("#jobsform_"+step).find("textarea").length > 0) {
            $("#jobsform_"+step).find("textarea").focus();
          }
        },400);
    }

    $scope.bookmark = function(){
        swal("Please Log in as Candidate");
    }

    $scope.testFunc = function() {
        alert("sdsd")
    }
    $scope.searchJobs = function() {
        var url = "/jobs/search";
        $scope.searchData.selected_status = $scope.selected_status;
        var data = $scope.searchData;
        $api.post(data, url).then(function(response) {
            $scope.jobs = response.data;
            $scope.activeButton.stop();
        });
    }

    $scope.startImmediateFunc =  function() {
        $timeout(function() {
            if($scope.jobsData.ei_startImmediate == true) {
                $scope.jobsData.ei_startDate = "";
            }
        }, 500);
    }
    $scope.goEditJob = function(id) {
        swal({   
            title: "Wait for a while.",   
            text: "Processing your data.",   
            showConfirmButton: false 
        });

        $state.go('edit_job', {job_id: id});
    }
    $scope.showType = function(type) {
        if(type == "awaiting_activation") {
            return "Awaiting Activation";
        }else if(type == "inactive") {
            return "Inactive";
        }else if(type == "deleted") {
            return "Deleted";
        }else if(type == "expired") {
            return "expired";
        }else if(type == "active"){
            return "Active";
        }
    }
	$scope.selectAccordion = function(key) {
    	if($scope.active_job_accordion == key) {
    		$scope.active_job_accordion = "";
    		return;
    	}
    	$scope.active_job_accordion = key;
    }

    $scope.confirmJobPost = function() {
    	var url = "/jobs/post/confirm/"+$scope.jobs[$scope.selected_index].id;
    	$api.get(url).then(function(response) {
    		if(response.data.status) {
    			swal("Good job!", response.data.message, "success");
                $(".slider-form-close").click();
                $scope.activeButton.stop();
    			$scope.getJobs();
    		}
		});
    }
    $scope.reactivateJobPost = function(url) {
        $api.get(url).then(function(response) {
            if(response.data.status) {
                swal("Good job!", response.data.message, "success");
                $(".slider-form-close").click();
                $scope.getJobs($scope.selected_status);
            }
        });
    }

    $scope.deleteJobPost = function(url) {
        $api.get(url).then(function(response) {
            if(response.data.status) {
                swal("Good job!", response.data.message, "success");
                $(".slider-form-close").click();
                $scope.getJobs($scope.selected_status);
            }
        });
    }
    
    $scope.getPriority = function() {
    	var url = "/jobs/priorities"
    	$api.get(url).then(function(response) {
			$scope.priorities = response.data;
			// console.log($scope.priorities)
		});
    }

    $scope.savePrio = function(data) {
    	// console.log(data)
    	JobsService.setData("priority" , data.id);
    }

    $scope.getMinSalary = function() {
    	if($scope.jobsData.ei_durPosition == "Full Time") {
    		$scope.jobsData.full_time_min = $scope.jobsData.ei_durMin;
    	}else if($scope.jobsData.ei_durPosition == "Locum Tenens") {
    		$scope.jobsData.locum_min = $scope.jobsData.ei_durMin;
    	}else if($scope.jobsData.ei_durPosition == "Part Time") {
    		$scope.jobsData.part_time_min = $scope.jobsData.ei_durMin;
    	}else if($scope.jobsData.ei_durPosition == "PRN") {
    		$scope.jobsData.prn_min = $scope.jobsData.ei_durMin;
    	}else if($scope.jobsData.ei_durPosition == "Fellowship") {
    		$scope.jobsData.fellowship_min = $scope.jobsData.ei_durMin;
    	}
    }

    $scope.getMaxSalary = function() {
    	if($scope.jobsData.ei_durPosition == "Full Time") {
    		$scope.jobsData.full_time_max = $scope.jobsData.ei_durMax;
    	}else if($scope.jobsData.ei_durPosition == "Locum Tenens") {
    		$scope.jobsData.locum_max = $scope.jobsData.ei_durMax;
    	}else if($scope.jobsData.ei_durPosition == "Part Time") {
    		$scope.jobsData.part_time_max = $scope.jobsData.ei_durMax;
    	}else if($scope.jobsData.ei_durPosition == "PRN") {
    		$scope.jobsData.prn_max = $scope.jobsData.ei_durMax;
    	}else if($scope.jobsData.ei_durPosition == "Fellowship") {
    		$scope.jobsData.fellowship_max = $scope.jobsData.ei_durMax;
    	}
    }

    
	

    $scope.getPrioLabel = function(id) {
    	// console.log(id)
    	var toReturn;
    	angular.forEach($scope.priorities, function(prio) {
    		angular.forEach(prio, function(inside) {
				if(inside.id == id) {
					if(inside.levelType == 1) {//high prio
						toReturn = "High Priority: "+inside.levelName+" - "+inside.amount;
					}
					else if(inside.levelType == 2) {//priority posting
						toReturn = "Priority Posting: "+inside.levelName+" - "+inside.amount;
					}
					else if(inside.levelType == 3) {//General
						toReturn = "General Posting: "+inside.levelName+" - Free";
					}
				}
			});
		});

		return toReturn;
    }	

    $scope.showCases = function(model) {
        // console.log(model)
        if(model == undefined) {
            return false;
        }
        if(model.cases.major_vascular_anesthesia == "Always") {
            return true;
        }
        if(model.cases.chronic_pain_man == "Always") {
            return true;
        }
        if(model.cases.cardiac_anesthesia == "Always") {
            return true;
        }
        if(model.cases.critical_care_med == "Always") {
            return true;
        }
        if(model.cases.neuroanesthesia == "Always") {
            return true;
        }
    }

    $scope.showIfValidService = function(model) {
        if(model != "" && model != undefined && model != "Never" && model != "Sometimes") {
            return true;
        }

        return false;
    }
    $scope.getTypeCases = function() {
    	$scope.type_cases = JobsService.getTypeCases();
    	// console.log($scope.type_cases)
    }
    $scope.getTypeCasesOptions = function() {
    	$scope.type_cases_options = JobsService.getTypeCasesOptions();
    }
    $scope.saveLocal = function(key, data) {
    	JobsService.setData(key , data);
    }

    $scope.initializeSavedJobs = function() {
    	$scope.jobsData = JobsService.getAllJobsData();
    }

    $scope.getStates = function() {
    	$scope.states = JobsService.getStates();
    }

    $scope.getSelectedStates = function() {
        return $scope.jobsData.ei_facilityState;
    }

    $scope.getCountries = function() {
    	$scope.countries = JobsService.getCountries();
    }

    $scope.selectJobPostType = function(type) {
        $scope.jobsData.jobPostType = type;
    }
    $scope.formatPostStatus = function(status) {
        if(status == "awaiting_activation") {
            return "Awaiting Activation";
        }else{
            return status;
        }
    }

    $scope.testpost = function(){
        var url = "/jobs/post/new";
        var data = {test : 'asdasd'}
        $api.post(data, url).then(function(response) {
            if(response.data.upgrade == "error"){
                return swal("Sorry!",response.data.message,"info")
            }
        })
    }
    $scope.submitJobPost = function() {
        
        swal({   
            title: "Please wait for a while.",   
            text: "Requesting new job post",   
            showConfirmButton: false 
        });

    	var url = "/jobs/post/new";
    	var data = $scope.jobsData;

    	$api.post(data, url).then(function(response) {
            console.log(response)
            if(response.data.process == "error"){
                swal("Sorry!","field(s) "+response.data.error.toString()+" should not be empty!","info")                            
            }

    		if(response.data.status) {
    			swal("Good job!", response.data.message, "success");
    			localStorageService.remove("jobsData");
    			$scope.jobsData = {};
                $state.go('jobs_list');
                if($state.current.name == "add_job" || $state.current.name == "edit_job" || $state.current.name == "new_job") {
                    if($scope.jobForm != undefined) {
                        // $scope.jobForm.$setPristine();
                        
                    }
                }                                        
    		}
		});
    }
    $scope.openPdf = function(id) {
        window.open("/pdf/generate/job/"+id, '_blank', 'fullscreen=yes');
    }
    $scope.gotToJobPost = function() {
        var jobPostType = $scope.postSelectionData.lookingFor;
        var jobTitle = $scope.postSelectionData.jobTitle;
        $state.go('add_job',{jobPostType: jobPostType, jobTitle : jobTitle});

    }
    $scope.deactivateJob = function(url) {
        swal({   
            title: "Wait for a while.",   
            text: "Deactivating job.",   
            showConfirmButton: false 
        });

        $api.get(url).then(function(response) {
            swal("Good job!", response.data.message, "success");
            $scope.getJobs($scope.selected_status);
        });

    }
    $scope.updateSelectedIndex = function(id) {
        $scope.selected_index = id;
        $scope.$apply();
    }

    $scope.getPerDaySalary = function() {
    	$scope.per_day_salary = JobsService.getPerDaySalary();
    }

    $scope.getPerAnnumSalary = function() {
    	$scope.per_annum_salary = JobsService.getPerAnnumSalary();
    }



	$scope.getJobs = function(key) {
		if(key != undefined) {
            var url = "/jobs/list"
        }else{
            var url = "/jobs/list"
        }
        
        $scope.jobSearch.status = $scope.selected_status;
        
        var data = $scope.jobSearch;
        // console.log(data)
		$api.post(data , url).then(function(response) {
            console.log(response.data)
    		$scope.jobs = response.data;
            if($scope.activeButton != undefined) {
                $scope.activeButton.stop();
            }
            
		});
	}

    $scope.searchJobs = function() {
  

        var data = $scope.jobSearch;
        var url = "/public/jobs";

        $api.post(data, url).then(function(response) {
            $scope.jobs = response.data;                                                                                          
            $scope.isLoading = false;
            // console.log($scope.jobs)
        });

    }

    $scope.pureText = function(text) {
        var oginaltext = text;
        return oginaltext.replace(/<br[^>]*>/g,"").replace(/(&nbsp;)*/g,"").replace(/(&amp;)*/g,"");
    }

    $scope.showJobType = function(job) {
        if(job.jobPostType == 1) {
            return "Anesthesiologist";
        }else{
            return "CRNA";
        }
    }

	$scope.selectedStatus = function(key) {

        if($scope.selection != undefined) {
            $scope.selection.select_all = false;
        }
        
		$scope.selected_status = key;
        $scope.getJobs(key);
	}
    $scope.restoreJob = function(url) {
        swal({   
            title: "Wait for a while.",   
            text: "restoring job.",   
            showConfirmButton: false 
        });

        $api.get(url).then(function(response) {
            swal("Good job!", response.data.message, "success");
            $scope.getJobs($scope.selected_status);
        });

    }
    $scope.deleteJob = function(url) {
        swal({   
            title: "Wait for a while.",   
            text: "Deleting job.",   
            showConfirmButton: false 
        });

        $api.get(url).then(function(response) {
            swal("Good job!", response.data.message, "success");
            $scope.getJobs($scope.selected_status);
        });

    }



    $scope.showStatus = function(status) {
        if(status == "active") {
            return "Active";
        }else if(status == "awaiting_activation") {
            return "Awaiting Activation";
        }else if(status == "inactive") {
            return "Inactive";
        }else if(status == "deleted") {
            return "Deleted";
        }
    }

	$scope.getJobById = function(id) {
		$scope.getPerAnnumSalary();
		$scope.getPerDaySalary();
		$scope.getStates();
		var url = "/jobs/getJobById/"+id;
		$api.get(url).then(function(response) {
		    $scope.getCountries();
		    $scope.getPriority();
		    $scope.getTypeCases();
		    $scope.getTypeCasesOptions();
			if(response.data.status == true) {
				$timeout(swal.close, 1500);   
	    		$scope.jobsData = response.data.jobs;
                console.log($scope.jobsData)
                angular.element('#subuser').val($scope.jobsData.sub_user_id)
	    		$scope.getMinSalary();
	    		$scope.getMaxSalary();
			}else{
				if(response.data.status == "forbidden") {
					notif_warning("Job data", response.data.message);	
					$state.go('jobs_list');
				}
			}
		}).catch(function(e) {
	        // console.log('Error: ', e);
	        throw e;
	    });
	}

	
   	    
	if($scope.job_id != "" && $scope.job_id != undefined) {//calls only on job edit
		$scope.getJobById($scope.job_id);
	}else{
       /* $timeout(function () {
            $scope.getJobs($scope.primary_status);
        },100);*/
		
        $scope.jobsData.jobPostType = $stateParams.jobPostType;
        $scope.jobsData.jobTitle = $stateParams.jobTitle;
		//$scope.initializeSavedJobs();
	}
	

    //if($state.current.name == "add_job") {
        $timeout(function () {
            $scope.getPerAnnumSalary();
            $scope.getPerDaySalary();
            $scope.getStates();
            $scope.getCountries();
            $scope.getPriority();
            $scope.getTypeCases();
            $scope.getTypeCasesOptions();
            $scope.getOldCards();
        }, 1000)
    //}

    document.body.style.overflow = "";
	// console.log("hee")
    if($scope.post_job == true) {
        // console.log("sdsd")
        $timeout(function() {
            $("#add_job_form_btn").trigger("click");
        }, 0);

    }

    $scope.expYears = $scope.getExpYears()


    $scope.getSubUsers();    


});