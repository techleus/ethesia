app.controller('mailboxController', function($q, $scope, $timeout,$stateParams,$window, $api,JobsService ,localStorageService, $state, AunthenticationService,$http) {
	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	}
	

	$scope.selected = {'test' : 'test'};
	$scope.mailboxid = $stateParams.mailboxid;
	$scope.mode = $stateParams.mode;
	$scope.compose = $stateParams.compose;
	$scope.recipient = '';
	$scope.message = {}
	$scope.message.subject = '';
	$scope.message.message = '';
	$scope.showlist = 'show';	



	$scope.fileChanged = function(element){		
        $scope.file = element.files
        $scope.$apply();        
        // console.log($scope.file)
    }

	$scope.sendMail = function(){

		if(angular.element('#recipient').val() == ""){
			return swal('Recipient is empty!')
		}


		if($scope.message.subject == ""){
    		return swal('Subject is empty!')
    	}

    	if($scope.message.body == ""){
    		return swal('Message is empty!')
    	} 	

    	
        $http({
            method : "POST",
            url : '/jobs/appresponse',            
            headers: { 'Content-Type': undefined},
            transformRequest: function(data) {
                var formData = new FormData();
                if($scope.file != undefined){
                	var ins = $scope.file.length;
					for (var x = 0; x < ins; x++) {
					    formData.append("file["+x+"]", $scope.file[x]);
					}
                }                            	
		        formData.append("userid", $scope.mailtoid);
		        formData.append("subject", $scope.message.subject);
		        formData.append("message", $scope.message.body);
                console.log(formData)               				

                return formData;
            },
        }).then(function mySuccess(response) {
        	// console.log(response)
        	if(response.statusText == 'Created'){
        		angular.element('#recipient').val('')
        		$scope.recipient = ''
        		$scope.message.subject = "";	
				$scope.message.body = "";
				$scope.mailList();
				$scope.sentmailList();
        		swal('Message sent!')                
        	}else if(response.data == "not found"){
        		swal('Sorry!','Recipient not exisit','info')
        	}
            
        }, function myError(response) {
            // console.log(response)
        });
	}

	$scope.mailList = function(){
		var url = '/mailbox/list'
		$api.get(url).then(function(response){
			$scope.mails = response.data
			console.log($scope.mails)
			$scope.inboxCount = 0;
			$scope.starredCount = 0;			
			$scope.trashCount = 0;
			angular.forEach($scope.mails,function(key){
				if(key.status_id == 1){
					$scope.inboxCount++;
				}

				if(key.status_id == 2){
					$scope.starredCount++;
				}
				
				if(key.status_id == 4){
					$scope.trashCount++;
				}


			})			
		})
	}

	$scope.isInList = function(recipient){
		
	}

	$scope.reply = function(mail){		
		
		$scope.compose = 2;
		$scope.setRecipient(mail.sender_id,mail.users.user_profile.firstName + ' ' + mail.users.user_profile.lastName)
	
	}

	$scope.showMail = function(mail){
		$scope.compose = 3;
		console.log(mail);
		$scope.showmail = {};
		$scope.showmail.message = mail.message;
		$scope.showmail.subject = mail.subject;
		$scope.showmail.firstname = mail.users.user_profile.firstName;
		$scope.showmail.lastname = mail.users.user_profile.lastName;
		$scope.showmail.imagepath = mail.users.user_profile.image_path;
		$scope.showmail.detail = mail;		
	}

	$scope.setRecipient = function(userid,fullname){		
		$('#recipient').val(fullname)
		console.log(fullname)
		$scope.recipient = fullname
		$scope.mailtoid = userid;
        $scope.showlist = ''

        console.log($scope.recipient)
		angular.forEach($scope.mailreciepent,function(key,value){
			console.log(key.profile.firstName + ' ' + key.profile.lastName,$scope.recipient)
			var fullname = key.profile.firstName + ' ' + key.profile.lastName;
			if(fullname == $scope.recipient){
				$scope.mailtoid = key.users.id;
				$scope.showlist = ''
			}
		})
		
	}

	$scope.getUniqueUsersFromInbox = function(){

		var url = '/mailbox/getUniqueUsersFromInbox'
		$http.get(url).then(function(response){
			console.log(response.data)
			$scope.mailreciepent = response.data;			
		})
			
	}
	

	$scope.composemail = function(){
		if($scope.compose == 1){
			$scope.compose = 2;
			$scope.getUniqueUsersFromInbox();
			$('.textarea_editor').wysihtml5();
		}else{
			$scope.compose = 1;
		}		
	}

	$scope.setmailboxid = function(id){
		$scope.mailboxid = id;
		$scope.mode = 1;
		$scope.compose = 1;
	}
	$scope.sentitem = function(){
		$scope.mode = 2;	
		$scope.mailboxid = 0;
		$scope.compose = 1;
		// console.log($scope.mode)
	}
	$scope.sentmailList = function(){
		var url = '/mailbox/sentitems'
		$api.get(url).then(function(response){
			$scope.sentmails = response.data
			console.log(response.data)
			// $scope.inboxCount 
		})
	}

	$scope.starmailselected = function(){
		var data = []
			data.id = []
			data.status_id = 2;
		var url = '/mailbox/updatemail'
		angular.forEach($scope.mails,function(key,value){
			if(key.selected == true){
				data.id.push(key.id)
			}			
		})
		$api.post(data,url).then(function(response){
			if(response.status == 200){
				swal('Message mark as starred')
				$scope.mailList()
			}			
		})
	}

	$scope.deletemailselected = function(){
		var data = []
			data.id = []
			data.status_id = 4;
		var url = '/mailbox/updatemail'
		angular.forEach($scope.mails,function(key,value){
			if(key.selected == true){
				data.id.push(key.id)
			}			
		})
		$api.post(data,url).then(function(response){
			if(response.status == 200){
				swal('Message mark as Trash')
				$scope.mailList()
			}			
		})
	}

	$scope.openmail = function(mailid,recipient_id,recipient_email,recipient_username,message,subject,attachments,fullname){		
		$state.go('mail',{ 
			mailid : mailid, 
			recipient_id : recipient_id, 
			recipient_email : recipient_email, 
			recipient_username : recipient_username, 
			message : message,
			subject : subject,
			attachments : attachments,
			fullname : fullname,
		});
	}

	$scope.opensentmail = function(mailid,recipient_id,recipient_email,recipient_username,message,subject,attachments){
		$state.go('sentmail',{ 
			mailid : mailid, 
			recipient_id : recipient_id, 
			recipient_email : recipient_email, 
			recipient_username : recipient_username, 
			message : message,
			subject : subject,
			attachments : attachments,
		});
	}

	$scope.select = function(mailid){
		angular.forEach($scope.mails,function(key,value){
			if(key.id == mailid){
				if(key.selected == undefined){
					key.selected = true
					// console.log(key)
				}else if(key.selected == true){
					key.selected = false
					// console.log(key)
				}else if(key.selected == false){
					key.selected = true
					// console.log(key)
				}
			}			
		})
	}

	$scope.mailList();
	$scope.sentmailList();

	if($stateParams.userid != "user"){
		// $scope.mailtoid = $stateParams.userid;
		// console.log($stateParams.userid)
	}
	
	if($stateParams.fullname != "new"){
		$scope.setRecipient($stateParams.userid,$stateParams.fullname)
		console.log($stateParams.fullname)
	}

	if($stateParams.notif_mail != ""){
		$scope.showMail($stateParams.notif_mail)
	}
	
});