
app.controller('JobSearchController', function($scope, $timeout, $api,localStorageService, $state, AunthenticationService) {

	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$state.go('login');
		}
	}else{
		$state.go('jobs_search');
	}
	
	$scope.active_tab = "jobs";

	$scope.setTab = function(tab_name) {
    	$scope.active_tab = tab_name;
    }

});