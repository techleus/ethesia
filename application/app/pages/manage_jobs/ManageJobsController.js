
app.controller('ManageJobsController', function($scope, $timeout,$stateParams, $api,JobsService ,localStorageService, $state, AunthenticationService) {

	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
			
			var data = localStorageService.get("credentials");
			var userAdmin = data.userAdmin;						
			if(userAdmin!=1){				
				$scope.$parent.authenticated = false;
				AunthenticationService.setAuth(false);
				$window.location.href = '/';
			}
			
		}
	}
	
	$scope.job_id = $stateParams.job_id;
	$scope.type_cases = [];
	$scope.type_cases_options = [];
	$scope.active_job_accordion = "headingOne1";
	$scope.jobs = {};
	
	 $scope.approvedJobPost = function() {
    	var url = "/manage/jobs/approved/"+$scope.jobsData.id;
    	$api.get(url).then(function(response) {
    		if(response.data.status) {
    			swal("Good job!", response.data.message, "success");
    			$state.go('manage_jobs');
    			$scope.getJobs();
    		}
		});
    }
	
    $scope.updateSelectedIndex = function(id) {
        $scope.selected_index = id;
        $scope.$apply();
    }
    
	$scope.getPriority = function() {
    	var url = "/jobs/priorities"
    	$api.get(url).then(function(response) {
			$scope.priorities = response.data;
			console.log($scope.priorities)
		});
    }

    $scope.savePrio = function(data) {
    	console.log(data)
    	JobsService.setData("priority" , data.id);
    }

    $scope.getMinSalary = function() {
    	if($scope.jobsData.ei_durPosition == "Full Time") {
    		$scope.jobsData.full_time_min = $scope.jobsData.ei_durMin;
    	}else if($scope.jobsData.ei_durPosition == "Locum Tenens") {
    		$scope.jobsData.locum_min = $scope.jobsData.ei_durMin;
    	}else if($scope.jobsData.ei_durPosition == "Part Time") {
    		$scope.jobsData.part_time_min = $scope.jobsData.ei_durMin;
    	}else if($scope.jobsData.ei_durPosition == "PRN") {
    		$scope.jobsData.prn_min = $scope.jobsData.ei_durMin;
    	}else if($scope.jobsData.ei_durPosition == "Fellowship") {
    		$scope.jobsData.fellowship_min = $scope.jobsData.ei_durMin;
    	}
    }

    $scope.getMaxSalary = function() {
    	if($scope.jobsData.ei_durPosition == "Full Time") {
    		$scope.jobsData.full_time_max = $scope.jobsData.ei_durMax;
    	}else if($scope.jobsData.ei_durPosition == "Locum Tenens") {
    		$scope.jobsData.locum_max = $scope.jobsData.ei_durMax;
    	}else if($scope.jobsData.ei_durPosition == "Part Time") {
    		$scope.jobsData.part_time_max = $scope.jobsData.ei_durMax;
    	}else if($scope.jobsData.ei_durPosition == "PRN") {
    		$scope.jobsData.prn_max = $scope.jobsData.ei_durMax;
    	}else if($scope.jobsData.ei_durPosition == "Fellowship") {
    		$scope.jobsData.fellowship_max = $scope.jobsData.ei_durMax;
    	}
    }

    
	

    $scope.getPrioLabel = function(id) {
    	console.log(id)
    	var toReturn;
    	angular.forEach($scope.priorities, function(prio) {
    		angular.forEach(prio, function(inside) {
				if(inside.id == id) {
					if(inside.levelType == 1) {//high prio
						toReturn = "High Priority: "+inside.levelName+" - "+inside.amount;
					}
					else if(inside.levelType == 2) {//priority posting
						toReturn = "Priority Posting: "+inside.levelName+" - "+inside.amount;
					}
					else if(inside.levelType == 3) {//General
						toReturn = "General Posting: "+inside.levelName+" - Free";
					}
				}
			});
		});

		return toReturn;
    }	


    $scope.getTypeCases = function() {
    	$scope.type_cases = JobsService.getTypeCases();
    	console.log($scope.type_cases)
    }
    $scope.getTypeCasesOptions = function() {
    	$scope.type_cases_options = JobsService.getTypeCasesOptions();
    }
    $scope.saveLocal = function(key, data) {
    	JobsService.setData(key , data);
    }

    $scope.initializeSavedJobs = function() {
    	$scope.jobsData = JobsService.getAllJobsData();
    }

    $scope.getStates = function() {
    	$scope.states = JobsService.getStates();
    }

    $scope.getCountries = function() {
    	$scope.countries = JobsService.getCountries();
    }
    $scope.submitJobPost = function() {
    	var url = "/jobs/post/new";
    	var data = $scope.jobsData;
    	$api.post(data, url).then(function(response) {
    		if(response.data.status) {
    			swal("Good job!", response.data.message, "success");
    			localStorageService.remove("jobsData");
    			$(window).scrollTop(0);
    			$state.go('manage_preview_job', {job_id:response.data.id});
    		}
		});
    }

    $scope.getPerDaySalary = function() {
    	$scope.per_day_salary = JobsService.getPerDaySalary();
    }

    $scope.getPerAnnumSalary = function() {
    	$scope.per_annum_salary = JobsService.getPerAnnumSalary();
    }
	
	$scope.getJobs = function() {
		var url = "/manage/jobs"
		$api.get(url).then(function(response) {
    		$scope.jobs = response.data;
		});
	}
	
	$scope.getJobById = function(id) {
		var url = "/manage/getJobById/"+id;
		$api.get(url).then(function(response) {
			if(response.data.status == true) {
				$timeout(swal.close, 1500);   
	    		$scope.jobsData = response.data.jobs;
	    		$scope.getMinSalary();
	    		$scope.getMaxSalary();
			}else{
				if(response.data.status == "forbidden") {
					notif_warning("Job data", response.data.message);	
					$state.go('manage_jobs');
				}
			}
		}).catch(function(e) {
	        console.log('Error: ', e);
	        throw e;
	    });
	}
	
	$scope.getJobs();
	$scope.getStates();
    $scope.getCountries();
    $scope.getPriority();
    $scope.getTypeCases();
    $scope.getTypeCasesOptions();
    $scope.getPerAnnumSalary();
    $scope.getPerDaySalary();
   	
	if($scope.job_id != "" && $scope.job_id != undefined) {//calls only on job edit
		$scope.getJobById($scope.job_id);

	}else{
		$scope.initializeSavedJobs();
	}
	
});