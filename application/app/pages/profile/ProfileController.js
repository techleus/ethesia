
app.controller('ProfileController', function($q ,$scope, $timeout,$sce, $api,localStorageService, $state, AunthenticationService,TokenService, $location, $window) {

	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	}

	var deferred = $q.defer();
    deferred.resolve();

	$scope.active_tab = "profile";
	$scope.infoData = {};
	$scope.billData = {};
	$scope.loginInfo = {};
	$scope.loginInfo.password = "";
	$scope.loginInfo.new_password = "";
	$scope.loginInfo.confirm_password = "";
	$scope.initial_load_count = 0;
	$scope.initialization = false;
	$scope.educations = [];
	$scope.activeButton = {};
	$scope.experiences = [];
	$scope.educationForm = false;	
	$scope.secondary = {}
	$scope.secondary.email = ''; 
	$scope.secondaryemail = '';
	$scope.modalEmail = '';
	$scope.updatemailid = '';
	$scope.resendPassword = function() {    
    	var url = "/emails/sendPasswordForgot";
    	var data = {
    		email : localStorageService.get("credentials").email
    	};
    	    	
    	$api.post(data, url).then(function(response) {
    		$scope.activeButton.stop();
    		if(response.data.status == true) {
    			$scope.successMessage = response.data.message;
    			$timeout(function() {
    				$scope.successMessage = "";
    			}, 3000);
    		}else{

    			$scope.errorMessage = response.data.message;

    			$timeout(function() {
    				$scope.errorMessage = "";
    			}, 3000);
    			
    		}
		});
	}

	$scope.updateSecondaryEmail = function(mailid){
		$scope.updatemailid = mailid
		$('#modalSecEmail').modal('show')
	}

	$scope.updateSecEmail = function(email){		
		var url = '/profile/proceedupdatesecemail'
		var data = {
			mailid : $scope.updatemailid,
			email : email,
		}
		$api.post(data,url).then(function(response){
			if(response.status == 422){
				swal('Sorry!',"The given email was invalid.",'info')
			}else if(response.status == 200){
				$scope.modalEmail = '';
				swal('Good job!',"You have successfully change a secondary email.",'info')
				$scope.getSecondarymail()
				$('#modalSecEmail').modal('hide')
			}
		})
	}

	$scope.deleteSecondaryEmail = function(mailid){		
		swal({   
            title: "Confirmation!",   
            text: "Please confirm to proceed.",   
            type: "warning",               
        }, function(isConfirm){   
            if(isConfirm){
            	var data = {
            		mailid : mailid
            	}
            	var url = '/profile/deleteSecEmail'
            	$api.post(data,url).then(function(response){
            		$scope.getSecondarymail()
            	})
            }
        });
	}

	$scope.getSecondarymail = function(){
		var url = '/profile/getSecondarymail'
		$api.get(url).then(function(response){
			$scope.secondaryMailList = response.data
		})
	}

	$scope.addSecondaryEmail = function(secondaryemail){
		console.log(secondaryemail)
		if(secondaryemail == ''){
			return swal('Sorry!','secondary email is empty','info')
		}
		var data = {
			email : secondaryemail
		}
		var url = '/profile/secondarymail';
		$api.post(data,url).then(function(response){
			console.log(response)
			if(response.status == 422){
				swal('Sorry!',"The given email was invalid.",'info')
			}else if(response.status == 200){
				swal('Good job!',"You have successfully added a secondary email.",'info')
				$scope.secondaryemail = ''
				$scope.getSecondarymail()
			}
		});
	}

	$scope.notificationSettings = function(){				

		var url = '/emails/notif_settings';
		$api.get(url).then(function(response){			
			if(response.status == 200){
				if(response.data.sms_application == 1){
					angular.element('#sms_application').prop('checked',true)
				}
				if(response.data.sms_messages == 1){
					angular.element('#sms_messages').prop('checked',true)
				}
				if(response.data.sms_job_expiration == 1){
					angular.element('#sms_job_expiration').prop('checked',true)
				}
				if(response.data.email_applicaton == 1){
					angular.element('#email_applicaton').prop('checked',true)
				}
				if(response.data.email_messages == 1){
					angular.element('#email_messages').prop('checked',true)
				}
				if(response.data.email_job_expiration == 1){
					angular.element('#email_job_expiration').prop('checked',true)
				}
			}
		});
	}

	$scope.setTab = function(tab_name) {
    	$scope.active_tab = tab_name;
    }

    $scope.showEducationForm = function() {
    	$scope.educationForm = !$scope.educationForm;
    }

    $scope.getUser = function(key) {
    	var data = localStorageService.get("credentials");
    	if(data != null) 
    		return data[key];
    }

    $scope.sendVerificationEmail = function() {
    	var url = "/emails/sendConfirmationEmail";
    	var data = {};
    	data.email = $scope.getUser("email");
    	$api.post(data, url).then(function(response) {
    		if(response.data.status == true) {
    			$scope.activeButton.stop();
    			notif_success("Email confirmation was sent. Check ur email.", "Success!.")	
    		}else{
    			$scope.activeButton.stop();
    		}
		});
    }

    $scope.showExperienceForm = function() {
    	$scope.experienceForm = !$scope.experienceForm;
    }
    $scope.populateEdit = function(data) {
    	$scope.educationInfo = data;
    	$scope.educationForm = true;
    }
    $scope.populateExperienceEdit = function(data) {
    	$scope.experienceInfo = data;
    	$scope.experienceForm = true;
    }
    $scope.cancelEducation = function() {
    	$scope.educationForm = false;
    	$scope.educationInfo = {};
    }
    $scope.testFunc = function() {
    	$scope.activeButton.stop();
    }
    $scope.saveNewExperience = function() {
    	var data = $scope.experienceInfo;
    	var url = "/user/experience/new";

    	$api.post(data, url).then(function(response) {
    		$scope.activeButton.stop();
			notif_success(response.data.message, "saved.")	
			$scope.experienceInfo = {};
			$scope.experienceForm = false;
			$scope.getUserExperience();
		});
    }
	$scope.cancelExperience = function() {
		$scope.experienceForm = false;
    	$scope.experienceInfo = {};
	}
    $scope.saveNewEducation = function() {
    	var data = $scope.educationInfo;
    	var url = "/user/education/new";

    	$api.post(data, url).then(function(response) {
    		$scope.activeButton.stop();
			notif_success("New Education has been saved.", "saved.")	
			$scope.educationInfo = {};
			$scope.educationForm = false;
			$scope.getUserEducations();
		});
    }
    $scope.removeExperience = function(url) {
		swal({   
            title: "Wait for a while.",   
            text: "Deleting data.",   
            showConfirmButton: false 
        });

		var url = url;
    	$api.get(url).then(function(response) {
			$scope.experienceForm = false;
			$scope.getUserExperience();
			swal("Good job!", response.data.message, "success");
		});
	}
    $scope.removeEducation = function(url) {
		swal({   
            title: "Wait for a while.",   
            text: "Deleting data.",   
            showConfirmButton: false 
        });

		var url = url;
    	$api.get(url).then(function(response) {
			$scope.educationForm = false;
			$scope.getUserEducations();
			swal("Good job!", response.data.message, "success");
		});
	}


    $scope.selectAccordion = function(key) {
    	if($scope.active_payment_accordion == key) {
    		$scope.active_payment_accordion = "";
    		return;
    	}
    	$scope.active_payment_accordion = key;
    }
    	
    $scope.saveProfilePhoto = function(blob) {
    	var data = {};
    	var url = "/users/upload/photo";
    	var elem = $("#profilePhoto")
    	data.blob = blob;
    	// console.log(data)
    	/*data.file = elem[0].files;
    	if(data.file.length == 0) {
    		swal("Sorry!", "You need to select an image to proceed.", "warning");
    		return;
    	}*/
    	$api.post_file(data, url).then(function(response) {
    		$scope.basicInfo.image_path = response.data.path;
    		$('#uploadPhoto').modal('toggle');
    		swal("Successfull!", "Profile has been uploaded.", "success");
    	})
    }

    $scope.saveLoginInfo = function() {
    	if($scope.loginInfo.password == "") {
    		swal("Sorry!", "Password is required.", "warning");
    		$scope.activeButton.stop();
    		return;
    	}
    	if($scope.loginInfo.new_password == "") {
    		swal("Sorry!", "Please input your new password.", "warning");
    		$scope.activeButton.stop();
    		return;
    	}
   		console.log($scope.loginInfo)
    	var url = "/user/newPassword";
    	$api.post($scope.loginInfo, url).then(function(response) {
    		if(response.data.status) {
    			swal("Success!", "Password has been updated.", "success");
    			$scope.activeButton.stop();
    			swal({   
		            title: "Password changing needs logout.",   
		            text: "After clicking yes, the app will be logout for security purposes.",   
		            type: "warning",   
		            showCancelButton: false,   
		            confirmButtonColor: "#DD6B55",   
		            confirmButtonText: "Logout",   
		            closeOnConfirm: true,   
		        }, function(isConfirm){   
		            notif_warning("Logout", "Logging out.")
			    	AunthenticationService.logout().then(function(response) {
			    		if(response.data.status) {
			    			AunthenticationService.setAuth(false);
							AunthenticationService.setData({});
							localStorageService.remove("credentials");
							$scope.authenticated = false;//parent controller
			                //$state.go('login');
			                //location.reload();
			                $window.location.href = '/';
			    		}

			            TokenService.get().then(function(response) {
			                $scope.token = response;
			            });
			                
			    	}); 
		        });
    		}else{
    			notif_danger("Password Status", response.data.message)
    			$scope.activeButton.stop();
    		}
    	})
    }
    $scope.saveSocialInfo = function() {
    	var data = $scope.socialInfo;
    	var url = "/user/socialInfo/update";
    	console.log(data)
    	$api.post(data, url).then(function(response) {
    		$scope.activeButton.stop();
			notif_success("Social Info Updated", "saved.")	
			$scope.socialInfo.id = response.data.id;
		});
    }
    $scope.saveBasicInfo = function() {
    	var data = $scope.basicInfo;
    	var url = "/user/basicInfo/update";

    	$api.post(data, url).then(function(response) {
    		$scope.activeButton.stop();
			notif_success("Profile Updated", "saved.")	
			$scope.basicInfo.id = response.data.id;
		});
    }

    $scope.saveExtraInfo = function() {
    	var data = [];
    	var url = "/user/extraInfo/update";

    	var elem = $('#input-file-max-fs')
    	
    	
    	data.file = elem[0].files;    
    	data.src_data = $scope.extraInfo;

    	// console.log(data.src_data)    	    	

    	var coverletter = $scope.extraInfo.coverLetter
    	var currentSalary = $scope.extraInfo.currentSalary
    	var expectedSalary = $scope.extraInfo.expectedSalary
    	var positionDuration = $scope.extraInfo.positionDuration
    	
    	if(coverletter != "" && currentSalary != "" && expectedSalary != "" && positionDuration != ""){
    		$api.single_upload(data, url).then(function(response) {
	    		//add if tdiri boss
	    		if(response.status == 200){
	    			$scope.activeButton.stop();
					notif_success("Profile Updated", "saved.")	
					$scope.extraInfo.id = response.data.id;
	    		}	    		
			});
    	}else{
    		swal("Sorry!", "Please check all fields that is needed", "error");
    		$scope.activeButton.stop();
    	}    	
    }

    $scope.saveProfileInfo = function(key) {
    	var data = [];
    	var url = "/user/update"
    	data[key] = $scope.infoData[key];
    	data["id"] = $scope.infoData.id;


    	if(key == "address") {
    		var google_places = ["city","state","zipcode","country"];
    		angular.forEach(google_places, function(field) {
    			data[field] = $scope.infoData[field];
    			data["id"] = $scope.infoData.id;
			    $api.post(data, url).then(function(response) {
					$scope.infoData.id = response.data.id;
				});
			});

			var data = [];
	    	data[key] = $scope.infoData[key];
	    	data["id"] = $scope.infoData.id;

    		$api.post(data, url).then(function(response) {
				//notif_success("Profile Update", response.data.message)	
				$scope.infoData.id = response.data.id;
			});
    		return;
    	}
    	$api.post(data, url).then(function(response) {
			notif_success("Profile Update", "saved.")	
			$scope.infoData.id = response.data.id;
		});
    }

    $scope.changeBillingDefault = function() {
    	// console.log($scope.billData.isDefault)
    }


    $scope.saveBillingCardInfo = function(key) {
    	var data = [];
    	var url = "/user/card/update"
    	data[key] = $scope.billData[key];
    	data["id"] = $scope.billData.id;

    	if(key == "address") {
    		var google_places = ["city","state","zipcode","country"];
    		angular.forEach(google_places, function(field) {
    			data[field] = $scope.billData[field];
    			data["id"] = $scope.billData.id;
			    $api.post(data, url).then(function(response) {
					$scope.billData.id = response.data.id;
				});
			});

			var data = [];
	    	data[key] = $scope.billData[key];
	    	data["id"] = $scope.billData.id;

    		$api.post(data, url).then(function(response) {
				//notif_success("Profile Update", response.data.message)	
				$scope.billData.id = response.data.id;
			});
    		return;
    	}
    	$api.post(data, url).then(function(response) {
			//notif_success("Profile Update", response.data.message)	
			$scope.billData.id = response.data.id;
		});
    }

    $scope.urlReady = function(param) {
    	if(param == undefined) {
    		return  "usa";
    	}
    	var str = param.replace(/\s+/g, '+');
		return str;
    }

    $scope.getUserProfile = function() {
    	var url = "/user/getProfile"
    	$api.get(url).then(function(response) {
			$scope.basicInfo = response.data;
			$scope.socialInfo = response.data;
			$scope.extraInfo = response.data;
			console.log($scope.basicInfo)
			$scope.initialization = true;
			$scope.showInitMapUrl();
		});
    }

    $scope.getUserEducations = function() {
    	var url = "/user/getUserEducations"
    	$api.get(url).then(function(response) {
			$scope.educations = response.data;
		});
    }
    $scope.getUserExperience = function() {
    	var url = "/user/getUserExperience"
    	$api.get(url).then(function(response) {
			$scope.experiences = response.data;
		});
    }

    $scope.getUserBillingDetails = function() {
    	var url = "/user/getBillingDetails"
    	$api.get(url).then(function(response) {
			$scope.billData = response.data;
		});
    }
    $scope.showInitMapUrl = function() {
    	var url = "https://www.google.com/maps/embed/v1/place?key=AIzaSyCE9HEf15FYzLSCC53fhU_XCwP13ky8CLg&q="+$scope.urlReady($scope.basicInfo.address);
    	$("#profile_map").attr("src", $sce.trustAsResourceUrl(url))
    }
    $scope.showMapUrl = function() {
    	var url = "https://www.google.com/maps/embed/v1/place?key=AIzaSyCE9HEf15FYzLSCC53fhU_XCwP13ky8CLg&q="+$scope.urlReady($scope.infoData.address);
    	return $sce.trustAsResourceUrl(url); 
    }	

    $scope.getUserProfile();
    //$scope.getUserBillingDetails();


   /* $scope.profileUpdateWatcher = function() {
    	var company_timeout;
	    $scope.$watch('infoData.company_name', function ($value, $old) {
	    	if($value != $old) {
	    		window.clearTimeout(company_timeout);
		    	company_timeout = setTimeout(function(){ 
		    		$scope.saveProfileInfo("company_name")
		    	}, 1000);
	    	}
	    }); 

	    var firstName_timeout;
	    $scope.$watch('infoData.firstName', function ($value, $old) {
	    	
	    	if($value != $old) {
				window.clearTimeout(firstName_timeout);
		    	firstName_timeout = setTimeout(function(){ 
		    		$scope.saveProfileInfo("firstName")
		    	}, 1000);
			}
	    }); 

	    var lastName_timeout;
	    $scope.$watch('infoData.lastName', function ($value, $old) {
	    	
	    	if($value != $old) {
				window.clearTimeout(lastName_timeout);
		    	lastName_timeout = setTimeout(function(){ 
		    		$scope.saveProfileInfo("lastName")
		    	}, 1000);
			}
	    });

	    var address_timeout;
	    $scope.$watch('infoData.address', function ($value, $old) {
	    	
	    	if($value != $old) {
				window.clearTimeout(address_timeout);
		    	address_timeout = setTimeout(function(){ 
		    		$scope.saveProfileInfo("address")
		    	}, 1000);
			}
	    });

	    var state_timeout;
	    $scope.$watch('infoData.state', function ($value, $old) {
	    	
	    	if($value != $old) {
				window.clearTimeout(state_timeout);
		    	state_timeout = setTimeout(function(){ 
		    		$scope.saveProfileInfo("state")
		    	}, 1000);
			}
	    });
	    var country_timeout;
	    $scope.$watch('infoData.country', function ($value, $old) {
	    	
	    	if($value != $old) {
				window.clearTimeout(country_timeout);
		    	country_timeout = setTimeout(function(){ 
		    		$scope.saveProfileInfo("country")
		    	}, 1000);
			}
	    });
	    var zipcode_timeout;
	    $scope.$watch('infoData.zipcode', function ($value, $old) {
	    	
	    	if($value != $old) {
				window.clearTimeout(zipcode_timeout);
		    	zipcode_timeout = setTimeout(function(){ 
		    		$scope.saveProfileInfo("zipcode")
		    	}, 1000);
			}
	    });
	    var phone_timeout;
	    $scope.$watch('infoData.phone', function ($value, $old) {
	    	
	    	if($value != $old) {
				window.clearTimeout(phone_timeout);
		    	phone_timeout = setTimeout(function(){ 
		    		$scope.saveProfileInfo("phone")
		    	}, 1000);
			}
	    })
	    var website_timeout;
	    $scope.$watch('infoData.website', function ($value, $old) {
	    	
	    	if($value != $old) {
				window.clearTimeout(website_timeout);
		    	phone_timeout = setTimeout(function(){ 
		    		$scope.saveProfileInfo("website")
		    	}, 1000);
			}
	    })
	    var fax_timeout;
	    $scope.$watch('infoData.fax', function ($value, $old) {
	    	
	    	if($value != $old) {
				window.clearTimeout(fax_timeout);
		    	fax_timeout = setTimeout(function(){ 
		    		$scope.saveProfileInfo("fax")
		    	}, 1000);
			}
	    })	
    }*/

    
    $scope.getUserEducations();
    $scope.getUserExperience();
    $scope.notificationSettings();
    $scope.getSecondarymail();
});