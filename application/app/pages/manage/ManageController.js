
app.controller('ManageController', function($scope, $timeout, $api,localStorageService, $state, AunthenticationService) {

	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	}
	
	$scope.getCandidates = function() {
		var url = "/manage/candidates";
		$api.get(url).then(function(response) {
            $scope.candidates = response.data;
        });
     }

});