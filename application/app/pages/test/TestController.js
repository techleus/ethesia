
app.controller('TestController', function($scope, $timeout,$sce, $api,localStorageService, $state, AunthenticationService) {

	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$state.go('login');
		}
	}

	$scope.testData = {};

	$scope.submitProfile = function() {
		var url = "/test/new";
    	var data = $scope.testData;
    	$api.post(data, url).then(function(response) {
    		if(response.data.status) {
    			swal("Good job!", response.data.message, "success");
    			localStorageService.remove("jobsData");
    		}
		});
	}
});