app.controller('ContactListController', function($scope, $timeout, $state, $stateParams, ChatService, $api,localStorageService, $state, AunthenticationService) {

	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	}
	

	$scope.contactObj = 0;
	$scope.conversations = [];
	$scope.sendMessage = function(user_id,user_name,user_email){		
		$scope.message_user_id = user_id;
		$scope.message_user_name = user_name;
		$scope.message_user_email = user_email;
	}
	$scope.submitMessage = function(user_message){
		if(user_message == undefined || user_message == ""){
			swal("Sorry!","Please leave a message before sending","info")
		}else{
			swal("Message Sent!","Your message has been successfully sent to "+ $scope.message_user_name,"success")
		}		
	}
	$scope.countallcontacts = function(){
    	var url = "/contact/countcountacts";
    	var data = [];
    	$api.get(url).then(function(response) {           	                    	
        	$scope.countall = response.data;        	
        });
    }

	$scope.contactTypes = function() {
        var url = "/contact/types";
        var data = [];        
        $api.get(url).then(function(response) {           
            console.log(response.data);
            $scope.UserContactLabels = response.data;
        });
    }    
    
    $scope.searchContact = function(){    	
    	var searchUser = $('#search-contact').val()    	
    	if(searchUser == "" || searchUser == null){
    		$scope.showUserList();	
    	}
    	$scope.showUserList(searchUser);    	
    }

    $scope.searchContactList = function(){    	
    	var username = $('.search-contact-list').val()
    	$scope.contactlist(username)
    }
    
    $scope.showUserList = function(username){
    	if(username == null){
    		var url = "/contact/userlist";
	    	var data = [];        
	        $api.get(url).then(function(response) {           
	            console.log(response.data);
	            $scope.userlist = response.data;
	        });
    	}else{
    		var url = "/contact/searchUserlist";
	    	var data = [];
	    		data["username"] = username;
	        $api.post(data, url).then(function(response) {           
	            console.log(response.data);
	            $scope.userlist = response.data;
	        });
    	}    	
    }
    $scope.contactlist = function(username){
    	if(username == undefined){
    		var url = "/contact/showUserContactList";
	    	$api.get(url).then(function(response) {           	            
	        	console.log(response.data)           
	        	$scope.contacts = response.data;      	        	
	        	
	        });
    	}else{
    		var url = "/contact/showUserContactList";
    		var data = [];
    			data['username'] = username
	    	$api.post(data,url).then(function(response) {           	            
	        	console.log(response.data)           
	        	$scope.contacts = response.data; 	        	
	        });
    	} 	
    }
    $scope.AddToContactList = function(contactId,contactUsername,contactgroupid) {    	
    	if($scope.contactgroup != undefined){
    		swal({
			  title: "Add "+contactUsername+ " to Contacts?",
			  text: "",
			  type: "info",
			  showCancelButton: true,
			  confirmButtonClass: "btn-info",
			  confirmButtonText: "Yes",
			  closeOnConfirm: true
			},
			function(){		  			  				
		    	var url = "/contact/addtocontacts";
		    	var data = [];
		    		data["contactId"] = contactId;
		    		data["contactUsername"] = contactUsername;
		    		data["contactgroupid"] = contactgroupid;
		        $api.post(data, url).then(function(response) {           	            
		        	console.log(response.data)
		            swal("Added!", response.data, "success");
		            $scope.contactlist();
		            $scope.showUserList();
		            $scope.countallcontacts();		
		            $scope.contactObj++;           
		            $('#search-contact').val("");		            
		            $scope.contactgroup = undefined;
		        });
			});
    	}else{
    		swal("Sorry!", "Please Select Contact Group before adding "+contactUsername+" to contacts", "error");
    	}    	
    }
    $scope.removeToContacts = function(contactId){      
    	var url = "/contact/removecontact";
    	var data = [];
    		data["id"] = contactId;
    	$api.post(data, url).then(function(response) {           	            
	    	console.log(response.data)
	        swal("Removed!", response.data, "success");
	        //var searchUser = $('#search-contact').val()    	
	        $scope.contactlist();
	        $scope.showUserList();
	        $scope.countallcontacts();	
	        $scope.contactObj++;        
	    });
    }
    $scope.showContactListByLabel = function(id) {
    	var url = "/contact/showcontactbylabel";
		var data = [];
			data['label_id'] = id
    	$api.post(data,url).then(function(response) {           	            
        	$scope.contacts = response.data;
        });
    }
    $scope.selectUserFromSearch = function(user_id)
    {
        var url = "/chat/message/openConvoFromUser";
        var data = [];
        var pageNum = 1;
        data["user_id"] = user_id;        
        data["page"] = pageNum;
        $scope.scroll = 2;
        $api.post(data, url).then(function(response) {
            $scope.processSelectedConvo(response);
            console.log(response)
            $('.chat-rbox').scroll(function(){                
                if($scope.scroll == 2){
                    if($('.chat-rbox').scrollTop() <= 10){
                        data["page"]++;                    
                        $api.post(data, url).then(function(response) {                        
                            console.log(response)                        
                            if($scope.selected_convo["messages"] != null){                     
                                for(var i in response.data.messages){                                
                                    $scope.selected_convo["messages"].splice(0, 0, response.data.messages[i]);
                                    $('.chat-rbox').scrollTop(5)                                
                                }                                   
                                console.log($scope.selected_convo["messages"])
                                ChatService.setConvo($scope.selected_convo);
                            }                        
                        });                        
                    }                
                }                
            });
        });
    }
    $scope.processSelectedConvo = function(response) {
        $scope.selected_convo = [];
        $scope.selected_convo = response.data;
        console.log($scope.selected_convo["messages"])
        ChatService.setConvo($scope.selected_convo);
        $(".chat-rbox").animate({ scrollTop: 20000000 }, "slow");

        var counter = 0;
        angular.forEach($scope.conversations, function(convo, key){
            if(convo.id == $scope.selected_convo.conversation.id) {
                counter++;
            }
        });

        if(counter == 0) {
            $scope.conversations.push(response.data);
        }
        console.log($scope.selected_convo["conversation"]["id"])
        $scope.$parent.Echo.private('chatroom.'+$scope.selected_convo["conversation"]["id"])
            .listen('MessageSent',function(e){
            $scope.pushNewMessage(e);  
            console.log("listining to new event")  
        });
    }
    $scope.contactlist();
    $scope.contactTypes()
    $scope.showUserList();    
    $scope.countallcontacts()
});