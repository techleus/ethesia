app.controller('BookmarksController', function($scope, $timeout, $api,localStorageService, $state, AunthenticationService) {

	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	}

	$scope.jobs = {};



	$scope.getJobs = function() {
		var url = "/public/bookmarkedJobs"
		$api.get(url).then(function(response) {
    		$scope.jobs = response.data;    		
    		console.log($scope.jobs)
		});
	}

	$scope.updateSelectedIndex = function(id) {
        $scope.selected_index = id;
        $scope.$apply();
    }
	
	$scope.showJobType = function(job) {
        if(job.jobPostType == 1) {
            return "Anesthesiologist";
        }else{
            return "CRNA";
        }
    }

    $scope.pureText = function(text) {
        var oginaltext = text;
        return oginaltext.replace(/<br[^>]*>/g,"").replace(/(&nbsp;)*/g,"").replace(/(&amp;)*/g,"");
    }
   
	$scope.updateSelectedIndex = function(id) {
        $scope.selected_index = id;
        $scope.$apply();
    }
	$scope.removeBookmark = function(userType,jobId){
		swal({
		  title: "Are you sure?",
		  text: "Job will be remove in Bookmarks",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  closeOnConfirm: false
		},
		function(){
			var url = "/browse-jobs/bookmark";
			var data = {userType : userType, jobId : jobId}
			$api.post(data,url).then(function(response){    			
				$scope.getJobs();
				swal("Deleted!", "Job successfully removed from bookmark.", "success");    			
			});		  
		});
	}	
	$scope.getJobs();
});