
app.controller('PlansController', function($scope, $timeout, $api,localStorageService, $state, AunthenticationService) {

	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	}

	$scope.plan_selected = [];
	$scope.billingDate = [];
	$scope.plansData = [];
	$scope.plan = [];
	$scope.cardtype = "";
	$scope.billData = [];
	$scope.billData.numberOfmonths = "";
	$scope.billData.recurring = "";
	$scope.billData.existing_card_id = "";
	$scope.package = function(id,name,fee){		
		$scope.billData.planid = id;
		$scope.plan.name = name;
		$scope.plan.fee = fee;
		//$('#modalPackage').modal('show')		
		$('#packages').fadeOut();
		$('#selectmethod').fadeIn()
	}
	$scope.returnPackages = function(){		
		$scope.plan_selected = [];
		$scope.billingDate = [];
		$scope.plansData = [];
		$scope.plan = [];
		$scope.cardtype = "";
		$scope.billData = [];
		$scope.billData.numberOfmonths = "";
		$scope.billData.recurring = "";
		$scope.plansData.existing_card_id = "";
		$('#packages').fadeIn();
		$('#selectmethod').fadeOut()
	}
	$scope.billingMethod = function(cardtype,months,recurring,cardid){
		if(cardtype == ""){
			swal('Sorry!','Please select Type of card','info')
			return;
		}else if(months == ""){
			swal('Sorry!','Please select number of months','info')
			return;
		}else if(recurring == ""){
			swal('Sorry!','Please select if the package is recurring or not, thanks!','info')
			return;
		}


		if(cardtype == 1){
			$('#addcardModal').modal('show')
		}else if(cardtype == 2){
			if(cardid == ""){
				swal('Sorry!','Please select Card first!','info')
				return;
			}
			$scope.packageexistingcard()
		}
	}
	$scope.planid = function(id){
		if(id == 1){
			return 'tall'
		}else if(id == 2){
			return 'grande'
		}else if(id == 3){
			return 'venti'
		}
	}
	$scope.getPostingPlans = function(){
		var url = '/plans/list'
		$api.get(url).then(function(response) {
    		$scope.postingPlans = response.data;
    		// console.log($scope.postingPlans)
		});
	}
	$scope.getPlans = function() {
		var url = "/plans/list"
		$api.get(url).then(function(response) {
    		$scope.plans = response.data;
		});
	}

	$scope.getUserPlans = function() {
		var url = "/plans/getUserPlans"
		$api.get(url).then(function(response) {
    		$scope.user_plans = response.data;
		});
	}

	$scope.isThereSelectedPlan = function() {
		var isThere = false;
		angular.forEach($scope.plans, function(plan_src) {
			if(plan_src.selected == true) {
				isThere = true;
			}
		});

		return isThere;
	}

	$scope.getNextBillingDate = function() {
		var url = "/plans/getNextBillingDate"
		var data = $scope.plansData;
		$api.post(data, url).then(function(response) {
    		// console.log(response)
    		$scope.plansData.plan_selected.next_billing = response.data.next_billing;
    		$scope.plansData.plan_selected.total_price = response.data.total_price;
			// console.log($scope.plansData.plan_selected)

		});
	}

	$scope.selectPlan = function(plan) {

		angular.forEach($scope.plans, function(plan_src) {
			plan_src.selected = false;
		});

		plan.selected = true;
		$scope.plansData.plan_selected = plan;
		$scope.defaultBillingInterval();
		// console.log($scope.plansData.plan_selected)
	}
	$scope.packagenewcard = function(){
		if( $scope.billData.cardNumber == undefined || $scope.billData.cardNumber == "" ){
			swal("Sorry!","Card number is empty","info")
			return;
		}

		if( $scope.billData.firstName == undefined || $scope.billData.firstName == "" ){
			swal("Sorry!","First name is empty","info")
			return;
		}

		if( $scope.billData.lastName == undefined || $scope.billData.lastName == "" ){
			swal("Sorry!","Last name is empty","info")
			return;
		}

		if( $scope.billData.expMonth == undefined || $scope.billData.expMonth == "" ){
			swal("Sorry!","Expiration month is empty","info")
			return;
		}

		if( $scope.billData.expYear == undefined || $scope.billData.expYear == "" ){
			swal("Sorry!","Expiration year is empty","info")
			return;
		}
		
		if( $scope.billData.cardIDNumber == undefined || $scope.billData.cardIDNumber == "" ){
			swal("Sorry!","Card ID number is empty","info")
			return;
		}

		if( $scope.billData.recurring == undefined || $scope.billData.recurring == "" ){
			swal("Sorry!","Please confirm if the package will be recurring or not!","info")
			return;
		}
		
		$('#addcardModal').modal('hide')

		swal({   
            title: "Wait for a while.",   
            text: "Creating your plan.",   
            showConfirmButton: false 
        });

		var data = $scope.billData
		var url = '/plans/packagenewcard'

		$api.post(data, url).then(function(response){						
    		if(response.data.status == true) {
    			
    			swal("Good job, redirecting...!", response.data.message, "success");
    			$state.go('jobs_list');
    			
    		}else{    			
    			swal({
				    title: 'Sorry!',
				    text: response.data.message,
				    type: 'warning',
				    showCancelButton: true,
				    confirmButtonColor: '#3085d6',
				    cancelButtonColor: '#d33',
				    confirmButtonText: 'Confirm!'
				},function(callback){
				  $scope.returnPackages()
				});
    			
    		}
		})

	}


	$scope.packageexistingcard = function(){
		var data = $scope.billData
		var url = '/plans/packageexistingcard'

		$api.post(data, url).then(function(response){						
    		if(response.data.status == true) {
    			
    			swal("Good job, redirecting...!", response.data.message, "success");
    			$state.go('jobs_list');
    			
    		}else{    			
    			swal({
				    title: 'Sorry!',
				    text: response.data.message,
				    type: 'warning',
				    showCancelButton: true,
				    confirmButtonColor: '#3085d6',
				    cancelButtonColor: '#d33',
				    confirmButtonText: 'Confirm!'
				},function(callback){
				  $scope.returnPackages()
				});
    			
    		}
		})
	}
	$scope.getExpYears = function() { 
        var year = 2018;
        var array_year = [];
        for(year; year <= 2035; year = year + 1) {
            array_year.push(year);
        }

        return array_year;
    }
	
	$scope.postCreditCard = function() {
		swal({   
            title: "Wait for a while.",   
            text: "Creating your plan.",   
            showConfirmButton: false 
        });
		var url = "/plans/update"
		var data = $scope.plansData;
		$api.post(data, url).then(function(response) {
    		if(response.data.status == true) {
    			swal("Good job, redirecting...!", response.data.message, "success");
    			$timeout(function() {
    				$state.go('plan_list');
    			}, 1500)
    			
    		}else{
    			swal("Sorry!", response.data.message, "warning");
    		}
		});
	}
	$scope.selectBillingInterval = function(interval) {
		$scope.plansData.plan_selected.interval = interval;

		$scope.getNextBillingDate();
	}
	$scope.defaultBillingInterval = function() {
		$scope.plansData.plan_selected.interval = 1;
		$scope.getNextBillingDate();
	}
	$scope.getOldCards = function() {
		var url = "/plans/getOldCards"
		$api.get(url).then(function(response) {
    		$scope.old_cards = response.data;
		});
	}

	$scope.selectedMonthToPay = function(month_to_pay) {
		$scope.plansData.plan_selected.month_to_pay = month_to_pay;
	}

	$scope.processSubTotal = function() {
		if($scope.plansData.plan_selected != undefined) {
			var interval = 0;
			if($scope.plansData.plan_selected.recurring == 'no') {
				interval = $scope.plansData.plan_selected.month_to_pay;
			}else{
				interval = $scope.plansData.plan_selected.interval;
			}
			// console.log(interval)
			return interval * $scope.plansData.plan_selected.plan_price;
		}else{
			return "0.00";
		}
	}

	$scope.getProratedDetails = function() {
		var url = "/prorated/details"
		var data = $scope.plansData;
		$api.post(data, url).then(function(response) {
    		$scope.prorated_details = response.data;
		});
	}

	$scope.getBillingDates = function() {
		$scope.billingDate = [];
		for(var i = 1;i <=31; i++ ) {
			$scope.billingDate.push($scope.getGetOrdinal(i))
		}
	}

	$scope.getGetOrdinal = function(n) {
	    var s=["th","st","nd","rd"],
	    v=n%100;
	    return n+(s[(v-20)%10]||s[v]||s[0]);
	 }

	$scope.displayFee = function() {
		return $scope.plan_selected.plan_price + " / " + "month";
	}
	$scope.expYears = $scope.getExpYears();
	$scope.getPlans();
	$scope.getBillingDates();
	$scope.getOldCards();
	$scope.getUserPlans();
	$scope.getPostingPlans();
});