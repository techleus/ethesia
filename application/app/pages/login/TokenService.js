
app.service('TokenService', function($http, $q) {

	var pipe = {};

	pipe.get = function() {

		var deferred = $q.defer();

		$http({
	        method : "GET",
	        url : "/token",
	    }).then(function mySuccess(response) {
	        deferred.resolve(response);
	    }, function myError(response) {
	    	console.log(response)
	    });

	    return deferred.promise;
	}
	
	return pipe;
}); 