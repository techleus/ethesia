
app.controller('LoginController', function($scope, $timeout, $api,localStorageService, TokenService, $state,AunthenticationService) {


	$window.location.href = '/';
	return;

	
	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
			$state.go('dashboard');
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$state.go('login');
		}
	}else{
		$state.go('dashboard');
	}

	TokenService.get().then(function(response) {
		//$scope.token = response;
	});

	$scope.token;
	$scope.loginData = {};

	$scope.loginData.email = "provider@gmail.com";
	$scope.loginData.password = "looping123";

/*	TokenService.get().then(function(response) {
		$scope.token = response;
	});
*/
	$scope.isAuthenticated = function() {
    	return false;
    }


	$scope.submitLogin = function() {
		
		angular.forEach($scope.loginForm.$error.required, function(field) {
		    field.$dirty = true;
		    field.$setPristine();
		});

		if($scope.loginForm.$valid) {
			AunthenticationService.login($scope.loginData).then(function(response) {
				
				if(response.data.status == true) {
					$scope.activeButton.stop();
					$scope.loginData = {};
					$scope.loginForm.$setPristine();
					AunthenticationService.setAuth(true);
					$scope.$parent.authenticated = true;
					AunthenticationService.setData(response.data.credentials);
					localStorageService.set("credentials", response.data.credentials);
					localStorageService.set("profile", response.data.profile);
					//$.notify(response.data.message+"!", "success");
					console.log(localStorageService.get("credentials"))
					notif_success("Login", "Welcome to your dashboard.")
					$state.go('dashboard');
					
				}else{
					//$.notify("Wrong Credentials, please try again.", "warning");
					$scope.activeButton.stop();
					$scope.loginData = {};
					notif_danger("Login", "Sorry Credentials didnt match. Please try again.")
				}
			});
		}
	}

});