app.controller('CreditCardController', function($scope, $timeout, $state, $stateParams, $api,localStorageService, $state, AunthenticationService) {

	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	}

    $('#myModal2').modal('show')

    $scope.steps = {}
    $scope.cardinfo = {}

	$scope.card_id = $stateParams.card_id;
	$scope.check_id = $stateParams.check_id;
	$scope.initialization = false;
	$scope.billData = {};
	$scope.checkInfo = {};
	$scope.cards = {};
	$scope.checks = {};
    $scope.billData.cardNumber = "";
    $scope.billData.firstName = "";
    $scope.billData.lastName = "";
    $scope.billData.expMonth = "";
    $scope.billData.expYear = "";
    $scope.billData.cardIDNumber = "";
    $scope.billData.address = "";

    $scope.is_blank = function(src) {
        if(src == undefined || src == '') {
            return true
        }

        return false
    }

    $scope.address_checker = function() {
        if($scope.is_blank($scope.cardinfo.address)) {
            return false
        }

        if($scope.is_blank($scope.cardinfo.state)) {
            return false
        }

        if($scope.is_blank($scope.cardinfo.city)) {
            return false
        }

        if($scope.is_blank($scope.cardinfo.zipcode)) {
            return false
        }

        return true
    }

    $scope.basic_info_checker = function() {
       
        if($scope.is_blank($scope.cardinfo.name)) {
            return false
        }

        if($scope.is_blank($scope.cardinfo.card_number)) {
            return false
        }

        if($scope.is_blank($scope.cardinfo.exp_month)) {
            return false
        }

        if($scope.is_blank($scope.cardinfo.exp_year)) {
            return false
        }

        if($scope.is_blank($scope.cardinfo.cvc)) {
            return false
        }

        return true
    }


    $scope.steps.current = "basic_info"
    $scope.steps.rules =  {
        "basic_info" : $scope.basic_info_checker()
    }

    $scope.stepsGoto = function(step) {
        $scope.steps.current = step
    }

	$scope.removeCards = function(url) {
		swal({   
            title: "Wait for a while.",   
            text: "Deleting data.",   
            showConfirmButton: false 
        });

		var url = url;
    	$api.get(url).then(function(response) {
			$scope.getCreditCards();
			swal("Good job!", response.data.message, "success");
		});
	}

	$scope.removeChecks = function(url) {
		var url = url;
    	$api.get(url).then(function(response) {
			$scope.getChecks();
			swal("Good job!", response.data.message, "success");
		});
	}
	
    

	$scope.getUserBillingDetails = function() {
		swal({   
            title: "Wait for a while.",   
            text: "Getting your credit card data from a secured source.",   
            showConfirmButton: false 
        });
    	var url = "/user/getBillingDetails/"+$scope.card_id;
    	$api.get(url).then(function(response) {
			$scope.billData = response.data;
			$scope.initialization = true;
			swal.close();
		});
    }

    $scope.getUserEChecksDetails = function() {
    	var url = "/user/getEChecksDetails"
    	$api.get(url).then(function(response) {
			$scope.checkInfo = response.data;
			$scope.initialization = true;
		});
    }

    $scope.selectForm = function() {
    	if($scope.selected_payment == "credit_card") {
    		$state.go('step2_add_credit_card');
    	}else{
    		$state.go('step2_add_echecks');
    	}

    }

    $scope.saveECheckInfo = function(key) {
    	var data = [];
    	var url = "/user/echeck/update"
    	data[key] = $scope.checkInfo[key];
    	data["id"] = $scope.checkInfo.id;
    	if($scope.checkInfo[key] == undefined || $scope.checkInfo[key] == "") {
    		return;
    	}
    	$api.post(data, url).then(function(response) {
			swal("Good job!", response.data.message, "success");
			$scope.checkInfo.id = response.data.id;
		});
    }
    $scope.updatemodal = function(card){
        $('#updatemodal').modal('show')                
        $scope.cardinfo = card        
        console.log($scope.cardinfo)
        $scope.defaultcard = false;
    }
    $scope.updatecard = function(){        
        
        var data = [];
        var url = '/user/cardupdate'
        data['id'] = $scope.cardinfo.id
        data['name'] = $scope.cardinfo.name
        data['default'] = $scope.defaultcard
        
        $api.post(data, url).then(function(response) {
            console.log(response)
            if(response.status == 200){
                $('#updatemodal').modal('hide')        
                $scope.getCreditCards();
                swal("Good job!", "Card Info Changed", "success");
            }else{
                swal("Sorry!", response.data.message, "info");                
            }            
        });
    }

  
    $scope.manageBackStep = function(){
        if($scope.steps.current == "address") {
            $scope.stepsGoto('basic_info')
        }
    }


    $scope.saveAllBillingInfo = function() {
    	var data = $scope.cardinfo;
    	var url = "/user/card/update"
    	if($scope.cardinfo.name == "" || $scope.cardinfo.name == undefined) {
    		swal("Sorry!", "Card holders name is required.", "warning");
    		$scope.activeButton.stop();
    		return;
    	}
    	if($scope.cardinfo.card_number == "" || $scope.cardinfo.card_number == undefined) {
    		if($scope.billData.id == "") {
    			swal("Sorry!", "Card Number is required.", "warning");
    			$scope.activeButton.stop();
    			return;
    		}
    	}
    	if($scope.cardinfo.exp_month == "" || $scope.cardinfo.exp_month == undefined) {
    		swal("Sorry!", "Expiration month and year is required.", "warning");
    		$scope.activeButton.stop();
    		return;
    	}
    	if($scope.cardinfo.cvc == "" || $scope.cardinfo.cvc == undefined) {
    		if($scope.billData.id == "") {
    			swal("Sorry!", "Card Id Number is required.", "warning");
    			$scope.activeButton.stop();
    			return;
    		}
    		
    	}

        if($scope.cardinfo.address == "") {
            swal("Sorry!", "billing address is required.", "warning");
            $scope.activeButton.stop();
            return;        
        }

      /*  if($scope.cardinfo.state == "" || $scope.cardinfo.state == undefined) {
            swal("Sorry!", "billing state required.", "warning");
            $scope.activeButton.stop();
            return;
            
        }

        if($scope.cardinfo.city == "" || $scope.cardinfo.city == undefined) {
            swal("Sorry!", "billing city required.", "warning");
            $scope.activeButton.stop();
            return;
            
        }*/
                    

    	$api.post(data, url).then(function(response) {
    		if(response.data.status) {
    			swal("Good job!", response.data.message, "success");
				$scope.activeButton.stop();
                $scope.getCreditCards();
                $scope.cardinfo = {}
                $('#myModal2').modal('hide')        
    		}else{
    			swal("Sorry!", response.data.message, "warning");
				$scope.activeButton.stop();
    		}
			
		});
    }

    $scope.saveAllElectronicCheck = function() {
    	var url = "/user/echeck/update"
    	if($scope.checkInfo.name == "" || $scope.checkInfo.name == undefined) {
    		swal("Sorry!", "Name is required.", "warning");
    		return;
    	}
    	if($scope.checkInfo.routingNumber == "" || $scope.checkInfo.routingNumber == undefined) {
    		swal("Sorry!", "Routing Number is required.", "warning");
    		return;
    	}
    	if($scope.checkInfo.bankAccountNumber == "" || $scope.checkInfo.bankAccountNumber == undefined) {
    		swal("Sorry!", "Bank Account Number is required.", "warning");
    		return;
    	}



    	$api.post($scope.checkInfo, url).then(function(response) {
			swal("Good job!", response.data.message, "success");
			$scope.checkInfo.id = response.data.id;
		});
    }
	$scope.saveBillingCardInfo = function(key) {
    	var data = [];
    	var url = "/user/card/update"
    	data[key] = $scope.billData[key];
    	data["id"] = $scope.billData.id;

    	if($scope.billData[key] == undefined || $scope.billData[key] == "") {
    		return;
    	}

    	if(key == "address") {
    		var google_places = ["city","state","zipcode","country"];
    		angular.forEach(google_places, function(field) {
    			data[field] = $scope.billData[field];
    			data["id"] = $scope.billData.id;
			    $api.post(data, url).then(function(response) {
					$scope.billData.id = response.data.id;
				});
			});

			var data = [];
	    	data[key] = $scope.billData[key];
	    	data["id"] = $scope.billData.id;

    		$api.post(data, url).then(function(response) {
				notif_success("Card Updated", response.data.message)	
				$scope.billData.id = response.data.id;
			});
    		return;
    	}
    	$api.post(data, url).then(function(response) {
			swal("Good job!", response.data.message, "success");
			$scope.billData.id = response.data.id;
		});
    }

	$scope.getCreditCards = function() {
		var url = "/user/credit_cards/list";
		$api.get(url).then(function(response) {
			$scope.cards = response.data;
			$scope.initialization = true;
		});
	}

	$scope.getChecks = function() {
		var url = "/user/e_checks/list";
		$api.get(url).then(function(response) {
			$scope.checks = response.data;
		});
	}

	$scope.getEChecks = function() {
		var url = "/e_checks/list";
		$api.get(url).then(function(response) {
			$scope.e_checks = response.data;
		});
	}	
	if($state.current.name == "edit_card" || $state.current.name == "step2_add_credit_card") {
		if($scope.card_id != "") {
			$scope.getUserBillingDetails();
		}else{
			$scope.initialization = true;
		}
	}else if($state.current.name == "step2_add_echecks" || $state.current.name == "edit_check") {
		if($scope.check_id != "") {
			$scope.getUserEChecksDetails();
		}else{
			$scope.initialization = true;
		}
	}
    $scope.getExpYears = function() { 
        var year = 2018;
        var array_year = [];
        for(year; year <= 2035; year = year + 1) {
            array_year.push(year);
        }

        return array_year;
    }
	
	$scope.getCreditCards();
	//$scope.getChecks();
    $scope.expYears = $scope.getExpYears()
});