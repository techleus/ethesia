app.service('LoginService', function($http, $q) {

	var pipe = {};
	var selected_account = "";

	pipe.setSelectedAccount = function(selected) {
		selected_account = selected;
	}

	pipe.getSelectedAccount = function() {
		return selected_account;
	}

	return pipe;
});