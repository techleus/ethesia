
app.controller('RegisterTypeController', function($scope, $timeout, $api,localStorageService, $state, AunthenticationService, TokenService) {

	$scope.selectedTitles = [];
	$scope.regData = [];

	TokenService.get().then(function(response) {
		$scope.token = response;
	});

	alert("adsd")

	$scope.getUserTypes = function() {
		var url = "/user/account_types";
		$api.get(url).then(function(response) {
			$scope.userTypes = response.data;
			console.log($scope.userTypes[0])
			$timeout(function () {
				var radio1 = $(".custom-radio")[0];
			    console.log($(".custom-radio")[0])
			    $(radio1).find(".custom-control-input").trigger("click")
			 }, 300);
			
		});
	}

	$scope.getAccountTitles = function() {
		var url = "/user/account_titles";
		$api.get(url).then(function(response) {
			$scope.userAccountTitles = response.data;
		});
	}

	$scope.changeSelectedTitle = function(id) {
		$scope.selectedTitles = [];
		$scope.regData.account_type = id;

		angular.forEach($scope.userAccountTitles, function(obj) {
		   if(id == obj.user_type_id) {
		   		$scope.selectedTitles.push(obj);
		   }
		});
	}

	$scope.submitRegistrationForm = function() {

		var url = "/user/new";
		var data = $scope.regData;
		
		$api.post(data, url).then(function(response) {
			if(response.data.status == true) {
				notif_success("Registration Success", response.message);
				$scope.activeButton.stop();
				$scope.regData = {};
				$scope.regForm.$setPristine();
				$window.location.href = '/';
			}
		});
	}

	$scope.accountTitlePlaceHolder = function(id) {
		console.log(id)
		if(id == 1) {
			return "Please Select Your Profession.";
		}else{
			return "Please Select Your Account Type.";
		}
	}

	$scope.getUserTypes();
	$scope.getAccountTitles();

	
});