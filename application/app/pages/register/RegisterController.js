
app.controller('RegisterController', function($scope, $timeout,$window, $api, LoginService,localStorageService, $state, AunthenticationService, TokenService) {

	$scope.selectedTitles = [];
	$scope.regData = [];
	TokenService.get().then(function(response) {
		$scope.token = response;
	});

	$scope.selected_account	= "";

	$scope.setSelectedAccount = function() {
		LoginService.setSelectedAccount($scope.selected_account);
		$state.go('register');
	}

	$scope.getSelectedAccount = function() {
		$scope.selected_account = LoginService.getSelectedAccount();

		if($scope.selected_account == undefined || $scope.selected_account == "") {
			$state.go("register_type");
			return;
		}
		if($scope.selected_account == "candidate") {
			$scope.changeSelectedTitle(1);//candidate id
			$scope.regData.account_type = 1;
		}else{
			$scope.changeSelectedTitle(2);//job provider id
			$scope.regData.account_type = 2;
		}
		
	}

	$scope.getUserTypes = function() {
		var url = "/user/account_types";
		$api.get(url).then(function(response) {
			$scope.userTypes = response.data;
		});
	}

	$scope.getAccountTitles = function() {
		var url = "/user/account_titles";
		$api.get(url).then(function(response) {
			$scope.userAccountTitles = response.data;
			$scope.getSelectedAccount();
		});
	}

	$scope.changeSelectedTitle = function(id) {
		$scope.selectedTitles = [];
		$scope.regData.account_type = id;

		angular.forEach($scope.userAccountTitles, function(obj) {
		   if(id == obj.user_type_id) {
		   		$scope.selectedTitles.push(obj);
		   }
		});
	}

	$scope.submitRegistrationForm = function() {

		var url = "/user/new";
		var data = $scope.regData;
		data["selected"] = $scope.selected_account;
		$api.post(data, url).then(function(response) {
			if(response.data.status == true) {
				notif_success("Registration Success", response.message);
				$scope.activeButton.stop();
				$scope.regData = {};
				$scope.regForm.$setPristine();
				$window.location.href = '/';
			}else if(response.data.status == false){
				notif_warning("Registration Failed", response.message);
				$scope.activeButton.stop();
				$scope.regData.email = "";
			}
		});
	}

	$scope.accountTitlePlaceHolder = function(id) {
		console.log(id)
		if(id == 1) {
			return "Please Select Your Profession.";
		}else{
			return "Please Select Your Account Type.";
		}
	}

	$scope.getUserTypes();
	$scope.getAccountTitles();

	
});