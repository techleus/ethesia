app.controller('PostSettingController', function($q, $scope, $timeout,$stateParams,$window, $api,JobsService ,localStorageService, $state, AunthenticationService) {
	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	}

	$scope.GetMessages = function(){
		var url = '/admin/messages';
		var data = [];

		$api.post(data, url).then(function(response){
			$scope.messages = response.data
		});
	}

	$scope.GetMessages();
});