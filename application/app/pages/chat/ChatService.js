app.service('ChatService', function($http, $q) {

	var pipe = {};
	var selected_convo = "";

	pipe.setConvo = function(obj) {
		selected_convo = obj;
	}
	pipe.getConvo = function() {
		return selected_convo;
	}


	return pipe;
});