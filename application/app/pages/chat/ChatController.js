



app.controller('ChatController', function($scope,$sce, $timeout ,ChatService, CvService, $state, $stateParams, $api,localStorageService, $state, AunthenticationService) {

	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	};
    $scope.convo_id = $stateParams.convo_id;
    $scope.messages = [];
    $scope.conversations = [];
    $scope.selected_convo = [];
    $scope.sending = [];    
    $scope.scroll = 0;

    $scope.sendMessage = function() {
        var url = "/chat/message";
        var data = [];
        data["message"] = $scope.chat.current_message;
        data["convo_id"] = $scope.selected_convo.conversation.id;

        $scope.sending.status = true;
        $scope.sending.message = data["message"];
        $scope.sending.image_path = $scope.getUser('image_path');
        $(".chat-rbox").animate({ scrollTop: 20000000 }, "slow");
        $scope.chat.current_message = "";
        $api.post(data, url).then(function(response) {
            
            console.log(response.data);
        });
    }

    $scope.getUser = function(key) {
        var data = localStorageService.get("credentials");
        if(data != null) 
            return data[key];
    }

    $scope.pushNewMessage = function(e) {
        //$scope.selected_convo = ChatService.getConvo();
 
        if(angular.equals($scope.selected_convo.messages, {})) {
            $scope.selected_convo.messages = [];
        } 
        $scope.sending = [];
        console.log(e)
        $scope.selected_convo["messages"].push(e.message);
        $scope.$apply();

        $(".chat-rbox").animate({ scrollTop: 20000000 }, "slow");

    }


    $scope.openConvo = function(convoID) {
        var data = {};
        var convo_id = "";
        var pageNum = 1;
        $scope.scroll = 1;
        data["convo_id"] = convoID;
        data["page"] = pageNum;

        var url = "/chat/openConversation";

        $api.post(data, url).then(function(response) {
            $scope.processSelectedConvo(response);            
            console.log(response)

            $('.chat-rbox').scroll(function(){                
                if($('.chat-rbox').scrollTop() <= 10){
                    if($scope.scroll == 1){
                        data["page"]++;                    
                        $api.post(data, url).then(function(response) {                        
                            console.log(response)                        
                            if($scope.selected_convo["messages"] != null){                     
                                for(var i in response.data.messages){                                
                                    $scope.selected_convo["messages"].splice(0, 0, response.data.messages[i]);
                                    $('.chat-rbox').scrollTop(20)                                
                                }                                   
                                console.log($scope.selected_convo["messages"])
                                ChatService.setConvo($scope.selected_convo);
                            }                        
                        });                        
                    }                    
                }                
            });
        });
    }



    $scope.processSelectedConvo = function(response) {
        $scope.selected_convo = [];
        $scope.selected_convo = response.data;
        console.log($scope.selected_convo["messages"])
        ChatService.setConvo($scope.selected_convo);
        $(".chat-rbox").animate({ scrollTop: 20000000 }, "slow");

        var counter = 0;
        angular.forEach($scope.conversations, function(convo, key){
            if(convo.id == $scope.selected_convo.conversation.id) {
                counter++;
            }
        });

        if(counter == 0) {
            $scope.conversations.push(response.data);
        }
        console.log($scope.selected_convo["conversation"]["id"])
        $scope.$parent.Echo.private('chatroom.'+$scope.selected_convo["conversation"]["id"])
            .listen('MessageSent',function(e){
            $scope.pushNewMessage(e);  
            console.log("listining to new event")  
        });
    }

    $scope.getUserProfile = function(key) {
        var data = localStorageService.get("profile");
        if(data != null) 
            return data[key];
    }

    $scope.searchContacts = function($key) {
        var url = "/chat/message/searchContacts";
        var data = [];
        data["key"] = $key;

        $api.post(data, url).then(function(response) {
            $scope.search_contacts = response.data;
        });
    }

    $scope.selectUserFromSearch = function(user_id)
    {
        var url = "/chat/message/openConvoFromUser";
        var data = [];
        var pageNum = 1;
        data["user_id"] = user_id;        
        data["page"] = pageNum;
        $scope.scroll = 2;
        $api.post(data, url).then(function(response) {
            $scope.processSelectedConvo(response);
            console.log(response)
            $('.chat-rbox').scroll(function(){                
                if($scope.scroll == 2){
                    if($('.chat-rbox').scrollTop() <= 10){
                        data["page"]++;                    
                        $api.post(data, url).then(function(response) {                        
                            console.log(response)                        
                            if($scope.selected_convo["messages"] != null){                     
                                for(var i in response.data.messages){                                
                                    $scope.selected_convo["messages"].splice(0, 0, response.data.messages[i]);
                                    $('.chat-rbox').scrollTop(5)                                
                                }                                   
                                console.log($scope.selected_convo["messages"])
                                ChatService.setConvo($scope.selected_convo);
                            }                        
                        });                        
                    }                
                }                
            });
        });
    }


    $scope.getConversations = function() {
        var url = "/chat/getConversations";

        $api.get(url).then(function(response) {
            $scope.conversations = response.data;
            console.log($scope.conversations)
        });
    }   


    if($scope.convo_id != "") {
        $scope.openConvo($scope.convo_id);
    }
    $scope.getConversations();
    
});