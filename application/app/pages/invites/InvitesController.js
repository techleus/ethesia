app.controller('InvitesController', function($q, $scope, $timeout,$stateParams,$window, $api,JobsService ,localStorageService, $state, AunthenticationService,$http) {
	
	"use strict";

	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	}
	
	$scope.invitation = {}
	$scope.invitation.email = '';
	$scope.invitation.message = ''
	$scope.sendInvitation = function(){
		
		if($scope.inviteForm.$error.required == true || $scope.invitation.email == ''){
			return console.log('email is required');
		}
		
		if($scope.inviteForm.$error.email){
        	return console.log('email not valid');
		}

		var url = '/invites/sendinvite';
		var data = {
			email : $scope.invitation.email,
			message : $scope.invitation.message,
		};

		$api.post(data,url).then(function(response){
			console.log(response)
		})

	}


});