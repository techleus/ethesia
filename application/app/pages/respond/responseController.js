app.controller('responseController', function($q, $scope, $timeout,$stateParams,$window, $api,JobsService ,localStorageService, $state, AunthenticationService,$http) {

	var me = this;

	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	}
	$scope.jobid = $stateParams.jobid;	
	$scope.userid = $stateParams.userid;
	$scope.username = $stateParams.username;
    $scope.fullname = $stateParams.fullname;
	$scope.message ="";	
	$scope.subject ="";
	$scope.file = {}
	console.log($scope.userid)
	$scope.fileChanged = function(element){		
        $scope.file = element.files
        $scope.$apply();        
        console.log($scope.file)
    }
    
    $scope.appResponse = function(){       	
    	
    	if($scope.subject == ""){
    		return swal('Subject is empty!')
    	}

    	if($scope.message == ""){
    		return swal('Message is empty!')
    	} 	

    	
        $http({
            method : "POST",
            url : '/jobs/appresponse',            
            headers: { 'Content-Type': undefined},
            transformRequest: function(data) {
                var formData = new FormData();
                console.log($scope.file)
            	var ins = $scope.file.length;
				for (var x = 0; x < ins; x++) {
				    formData.append("file["+x+"]", $scope.file[x]);
				}
		        formData.append("userid", $scope.userid);
		        formData.append("subject", $scope.subject);
		        formData.append("message", $scope.message);
                console.log(formData)               				

                return formData;
            },
        }).then(function mySuccess(response) {
        	console.log(response)
        	if(response.statusText == 'Created'){
        		$scope.message = "";	
				$scope.subject = "";
        		swal('Message sent!')
                if($scope.jobid != '#'){
                    $state.go('jobs_list')
                }else{
                    $state.go('mailbox')
                }
        	}
            
        }, function myError(response) {
            console.log(response)
        });
    }


});