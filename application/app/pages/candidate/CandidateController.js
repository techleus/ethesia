app.controller('CandidateController', function($scope,$sce, $timeout ,CvService, $state, $stateParams, $api,localStorageService, $state, AunthenticationService) {

	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	};

    $scope.cv_id = $stateParams.cv_id;
    $scope.selected_index = "";

    $api.get("/preferences/contact_method").then(function(response){
        $scope.contact_method = response.data;
    });

    $api.get("/preferences/position_duration").then(function(response){
        $scope.position_duration = response.data;
    });

    $scope.getPriority = function() {
        var url = "/cv/priorities"
        $api.get(url).then(function(response) {
            $scope.priorities = response.data;
            console.log($scope.priorities)
        });
    }

    $scope.updateSelectedIndex = function(id) {
        $scope.selected_index = id;
        $scope.$apply();
    }

    $scope.saveLocal = function(key, data) {
        console.clear();
        //CvService.setData(key , data);
        //console.log(CvService.getAllcvData());
    }

    $scope.getTypeCases = function() {
        $scope.type_cases = CvService.getTypeCases();
        console.log($scope.type_cases)
    }
    $scope.getTypeCasesOptions = function() {
        $scope.type_cases_options = CvService.getTypeCasesOptions();
    }
    $scope.saveLocal = function(key, data) {
       // CvService.setData(key , data);
    }
    $scope.getCountries = function() {
            $scope.countries = CvService.getCountries();
    }
    $scope.getPerDaySalary = function() {
            $scope.per_day_salary = CvService.getPerDaySalary();
    }
    $scope.getPerAnnumSalary = function() {
        $scope.per_annum_salary = CvService.getPerAnnumSalary();
    }

    $scope.savePrio = function(data) {
        console.log(data)
        CvService.setData("priority" , data.id);
    }
    $scope.initializeSavedCvs = function() {
        $scope.cvData = CvService.getAllCvsData();
        console.log($scope.cvData)
    }

    $scope.isCountrySelected = function(country) {
        var status = false;
        if($scope.cvData == null || $scope.cvData == undefined) {
            return status;
        }
        angular.forEach($scope.cvData.anesCountryLicenced, function(country_saved) {
            if(country_saved == country) {
                status =  true;
            }
        });
        return status;
    }

    $scope.isStateSelected = function(state) {
        var status = false;
        if($scope.cvData == null || $scope.cvData == undefined) {
            return status;
        } 
        angular.forEach($scope.cvData.wanted_states, function(state_saved) {
            if(state_saved == state) {
                status =  true;
            }
        });
        return status;
    }

    $scope.isLicensedStateSelected = function(state) {
        var status = false;
        if($scope.cvData == null || $scope.cvData == undefined) {
            return status;
        } 
        angular.forEach($scope.cvData.stateToWork, function(state_saved) {
            if(state_saved == state) {
                status =  true;
            }
        });
        return status;
    }

    

    $scope.processDateAvailable = function() {
        if($scope.cvData.startImmediate == true || $scope.cvData.startImmediate == "yes") {
            $scope.cvData.dateAvailable = ""
            $scope.$apply();
        }
    }


    $scope.submitCv = function() {
        $scope.cvData.locumTenensEndDate = $("#locumTenensEndDate").val();
        var url = "/cv/post/new";
        var data = $scope.cvData;
        $api.post(data, url).then(function(response) {
            if(response.data.status) {
                swal("Good job!", response.data.message, "success");
                //localStorageService.remove("cvData");
                $(window).scrollTop(0);
                $state.go('list_cv');
            }
        });
    }

    $scope.getCvsDb = function() {
        var url = "/cv/list";
        $api.get(url).then(function(response) {
                //localStorageService.remove("cvData");
                //$state.go('preview_job', {job_id:response.data.id});
                console.log(response.data)
                $scope.cv_list = response.data;
        });
    }
    
    $scope.trustHtml = function(html) {
        return $sce.trustAsHtml(html);
    }

    $scope.getCvById = function() {
        swal({   
            title: "Wait for a while.",   
            text: "Processing your data.",   
            showConfirmButton: false 
        });
        var url = "/cv/getCvById/"+$scope.cv_id;
        $api.get(url).then(function(response) {
            $scope.states = CvService.getStates();
            $scope.cvData = {}
            $scope.getPriority();
            $scope.getCountries();
            $scope.getPerAnnumSalary();
            $scope.getPerDaySalary();
            $scope.cvData = response.data;
            swal.close();
        });
    }



    if($scope.cv_id != "" && $scope.cv_id != undefined) {
        $scope.getTypeCases();
        $scope.getTypeCasesOptions();
        $scope.getCvById();
    }else{
        $scope.initializeSavedCvs();
        $scope.getCvsDb();
    }


    if($state.current.name == "add_cv") {
        $scope.states = CvService.getStates();
        $scope.cvData = {}
        $scope.getTypeCases();
        $scope.getTypeCasesOptions();
        $scope.getPriority();
        $scope.getCountries();
        $scope.getPerAnnumSalary();
        $scope.getPerDaySalary();
    }
    
    
});