
app.controller('DashboardController', function($scope, $window, $timeout, $api,localStorageService, $state, AunthenticationService) {

	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	}else{
		$state.go('dashboard');
	}
				
	var is_confirmed = localStorageService.get("credentials").is_confirmed;
	
	$scope.verified = true;

	if(is_confirmed == null || is_confirmed == '' || is_confirmed == 'no'){		
		$scope.verified = false;	
	}

	$scope.getApplicants = function(){
		$api.get('/user/get/applicants').then(function(response){
			console.log(response.data);
			$scope.jobs_count = response.data.jobs_count;
			$scope.applicants = response.data.applicants;			
			$scope.application_count = response.data.application_count;
			$scope.post_limit = response.data.limit;
		});
	}

	$scope.checkUserConfirmation = function(){
		
		$api.get('/user/credentials').then(function(response){	
			
			if(response.data == 'yes'){
				
				if($scope.verified == false){
					
					var cred = localStorageService.get("credentials")
						cred.is_confirmed = 'yes'
						
					localStorageService.set('credentials',cred)

					swal('Congratulation!','Your account successfully verified','success')

				}
				
				$scope.verified = true;
			
				localStorageService.get("credentials").is_confirmed = true;			
			
			}
		})
	
	}

	$scope.resendVerifiction = function(){
				
 
		var url = "/emails/resendAccoutVerification";
    	var data = {
    		email : localStorageService.get("credentials").email	
    	};
    	 
    	swal('System','Hello '+ localStorageService.get("credentials").username + ', an account verification will be sent to you now.' , 'info')
    	$api.post(data, url).then(function(response) {
    		
    		swal('', 'Hello '+ localStorageService.get("credentials").username + ' account verification successfully sent, please check it now', 'success')
    		
		});
	}

	$scope.checkUserConfirmation();	
	$scope.getApplicants();
});