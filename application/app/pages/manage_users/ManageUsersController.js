
app.controller('ManageUsersController', function($scope, $timeout,$stateParams, $api,JobsService ,localStorageService, $state, AunthenticationService) {

	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
			
			var data = localStorageService.get("credentials");
			var userAdmin = data.userAdmin;
			if(userAdmin!=1){
				$scope.$parent.authenticated = false;
				AunthenticationService.setAuth(false);
				$state.go('login');
			}
			
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$state.go('login');
		}
	}
	
	
	
});