app.controller('NotificationsController', function($q, $scope, $timeout,$stateParams,$window, $api,JobsService ,localStorageService, $state, AunthenticationService) {
	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	}

	$scope.current_index = 0;
	$scope.notifications = [];
	$scope.notif_id = $stateParams.notif_id;	

    $scope.respond = function(userid, fullname){
        //$state.go('respond',{ jobid : job_id, userid : userid, fullname : fullname });
        $state.go('mailbox',{ compose : 2, userid : userid, fullname : fullname });
    }

	$scope.getNotifications = function() {
        var url = "/notifications/list?current_index="+$scope.current_index;

        $api.get(url).then(function(response) {
        	if($scope.current_index == 0) {
        		$scope.notifications = response.data;
                console.log($scope.notifications)
        	}else{
        		$scope.pushNewData(response.data)
        	}
            
            $scope.current_index++;
        });
    }

    $scope.pushNewData = function(data) {
    	angular.forEach(data, function(notif) {
            $scope.notifications.push(notif)
        });
    }
    
    $scope.getNotifById = function() {
    	var url = "/notifications/preview/"+$scope.notif_id;

        $api.get(url).then(function(response) {
        	$scope.notif_data = response.data;
        });
    }

    if($state.current.name == "notifications") {
    	$scope.getNotifications();
    }
    else if($state.current.name == "preview_notif") {
    	$scope.getNotifById();
    }

    
});