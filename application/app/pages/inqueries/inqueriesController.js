app.controller('inqueriesController', function($q, $scope, $timeout,$stateParams,$window, $api,JobsService ,localStorageService, $state, AunthenticationService,$http) {

	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	}
	console.log(localStorageService.get("credentials"))
	$scope.jobid = $stateParams.jobid;	

	$scope.getInqueries = function(){
		var data = [];
			data.jobid = $scope.jobid;
		var url = '/jobs/inqueries'
		$api.post(data,url).then(function(response){
			console.log(response.data)
			$scope.inqueries = response.data
		})
	}
	$scope.checkstorage = function(id,cvfilename){
		var url = '/storagecheck'
		$api.get(url).then(function(response){
			console.log(response.data)
			return true;
		})
	}

	$scope.respond = function(userid, fullname){
		
		//$state.go('respond',{ jobid : $scope.jobid, userid : userid, username : username, fullname : fullname });
		$state.go('mailbox',{ compose : 2, userid : userid, fullname : fullname });
	}
	
	$scope.getInqueries();
});