
app.controller('JobListController', function($scope, $timeout, $api,localStorageService, $state, AunthenticationService) {

	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$state.go('login');
		}
	}
	
	$scope.jobs = {};

	$scope.getJobs = function() {
		var url = "/jobs/list"
		$api.get(url).then(function(response) {
    		$scope.jobs = response.data;
		});
	}
	
	$scope.getJobs();

});