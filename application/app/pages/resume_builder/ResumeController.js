app.controller('ResumeController', function($scope, $timeout,$sce, $api,localStorageService, $state, AunthenticationService) {

	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$state.go('login');
		}
	}

	$scope.active_tab = "cover_letter";


	$scope.setTab = function(tab_name) {
    	$scope.active_tab = tab_name;
    }
});