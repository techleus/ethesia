app.controller('mailController', function($q, $scope, $timeout,$stateParams,$window, $api,JobsService ,localStorageService, $state, AunthenticationService,$http) {
	if(!AunthenticationService.isAuthenticated()) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	}	
	
	$scope.mail = {};
	$scope.mail.mailid = $stateParams.mailid;
	$scope.mail.recipient_id = $stateParams.recipient_id;
	$scope.mail.recipient_email = $stateParams.recipient_email;
	$scope.mail.username = $stateParams.recipient_username;
	$scope.mail.message = $stateParams.message;
	$scope.mail.subject = $stateParams.subject;
	$scope.mail.fullname = $stateParams.fullname;
	
	
	$scope.respond = function(){
		$state.go('respond',{ jobid : '#', userid : $scope.mail.recipient_id, username : $scope.mail.username });
	}

	$scope.getAttachments = function(){
		var url = '/jobs/attachments'
		var data = [];		
		data.id = $scope.mail.recipient_id;
		$api.post(data,url).then(function(response){
			console.log(response.data)
			$scope.mail.attachments = response.data
		});
	}
	$scope.getAttachments();
});