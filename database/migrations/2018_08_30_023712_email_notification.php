<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmailNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_message_notification',function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id');
            $table->boolean('sms_application');
            $table->boolean('sms_messages');
            $table->boolean('sms_job_expiration');            
            $table->boolean('email_applicaton');
            $table->boolean('email_messages');
            $table->boolean('email_job_expiration');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
