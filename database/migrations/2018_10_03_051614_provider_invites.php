<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProviderInvites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_invitation_key',function(Blueprint $table){
            $table->increments('id');
            $table->integer('provider_id');
            $table->string('invited_email');
            $table->string('invitation_key');
        });

        Schema::create('user_invited_by',function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('provider_id');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
