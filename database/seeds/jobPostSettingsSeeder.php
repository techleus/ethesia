<?php

use Illuminate\Database\Seeder;
use App\JobPostSettings;

class jobPostSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('job_post_settings')->truncate();

        $instance = new JobPostSettings();
        $instance->truncate();
        $instance->free_posting = 1;
        $instance->save();
        
    }
}
