<?php

use Illuminate\Database\Seeder;
use App\applicationMessagesStatus;

class appMsgStatus extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
    	DB::table('application_messages_status')->truncate();

    	$instance = new applicationMessagesStatus();
    	$instance->status = 'inbox';
        $instance->save();

        $instance = new applicationMessagesStatus();
    	$instance->status = 'starred';
        $instance->save();

        $instance = new applicationMessagesStatus();
    	$instance->status = 'draft';
        $instance->save();

        $instance = new applicationMessagesStatus();
    	$instance->status = 'trash';
        $instance->save();       
    }
}
